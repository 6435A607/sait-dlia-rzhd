import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import Page from './components/page.jsx';

ReactDOM.render(
    <Router>
        <Route path="/" component={Page} />
    </Router>
  , 
  document.getElementById('app')); 
  