import React from 'react';

export default function MIChapter4(props) {
    return (
      <React.Fragment>
        <h4 className="text-center">Определения терминов, сокращения, обозначения</h4>
        <br/>
        <p>В тексте настоящего РД используются термины, определения и сокращения, приведенные в МС ИСО 9000:2005 и РД 2.1100.0512-001 «Глоссарий СМК». Наиболее важные и часто встречаемые в настоящем РД термины, определения и сокращения, приведены ниже.</p>
        <table class="table range table-responsive table-striped table-secondary">
          <thead>
            <tr>
              <th>№</th>
              <th>Сокращение</th>
              <th>Определение</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td scope="row">ОАО «РЖД»</td>
              <td>Открытое акционерное общество "Российские железные дороги"</td>
            </tr>
            <tr>
              <td scope="row">ОЖД</td>
              <td>Октябрьская железная дорога</td>
            </tr>
            <tr>
              <td scope="row">ЦТ</td>
              <td>Дирекция тяги</td>
            </tr>
            <tr>
              <td scope="row">РД</td>
              <td>Руководящий документ</td>
            </tr>
            <tr>
              <td scope="row">ДСП</td>
              <td>Дежурный по станции</td>
            </tr>
            <tr>
              <td scope="row">ДНЦ</td>
              <td>Поездной диспетчер</td>
            </tr>
            <tr>
              <td scope="row">ТЧЭ</td>
              <td>Начальник эксплуатационного локомотивного депо</td>
            </tr>
            <tr>
              <td scope="row">ТЧГЭ</td>
              <td>Главный инженер эксплуатационного локомотивного депо</td>
            </tr>
            <tr>
              <td scope="row">ТЧЗЭЭ</td>
              <td>Заместитель начальника эксплуатационного локомотивного депо по эксплуатации</td>
            </tr>
            <tr>
              <td scope="row">ТЧМИ</td>
              <td>Машинист инструктор</td>
            </tr>
            <tr>
              <td scope="row">ТЧД</td>
              <td>Дежурный по эксплуатационному локомотивному депо</td>
            </tr>
            <tr>
              <td scope="row">ТЧМ</td>
              <td>Машинист локомотива</td>
            </tr>
            <tr>
              <td scope="row">ТЧМП</td>
              <td>Помощник машиниста локомотива</td>
            </tr>
            <tr>
              <td scope="row">ТМ</td>
              <td>Тормозная магистраль</td>
            </tr>
            <tr>
              <td scope="row">ЭПК</td>
              <td>Электропневматический клапан автостопа</td>
            </tr>
            <tr>
              <td scope="row">ТО-2</td>
              <td>Техническое обслуживание второго объема</td>
            </tr>
          </tbody>
        </table>
      </React.Fragment>
    );
  }