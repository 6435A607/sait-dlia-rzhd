import React from 'react';

export default function MIChapter9_8(props) {
  return (
    <React.Fragment>
      <p><span className="text-danger">9.8. </span>Выполнение полного опробования тормозов в пассажирских  поездах:</p>
      <p><span className="text-danger">9.8.1. </span>Перед проведением полного опробования тормозов убедиться в выключенном источнике питания ЭПТ на локомотиве.</p>
      <p><span className="text-danger">9.8.2. </span>Проверка производится после полной зарядки ТМ  поезда, и выполняется с продувки ТМ в хвосте состава путем открытия концевого крана и срабатывания ускорителей экстренного торможения, машинист видит снижение давления в ТМ на 1,5 – 2,5 кгс/см<sup>2</sup> , отпуск произвести установкой управляющего органа крана машиниста в положение, обеспечивающее повышение давления выше зарядного до зарядного давления и продолжить зарядку поездным положением крана машиниста.</p>
      <ul className="ul-article">
        <li>провести замер плотности ТМ состава путем перекрытия комбинированного крана и провести замер по истечении  20-ти  секунд снижение давления допускается не более чем на 0,2 кгс/см<sup>2</sup> течении 60 секунд (1 минута ).</li>
        <li>провести проверку действия ЭПТ путем служебного торможения  до получения давления в ТЦ локомотива 1,0 – 1,5 кгс/см<sup>2</sup> при этом напряжение  источника питания не ниже  45 вольт.</li>
        <li>проверка исправности ЭПТ  путем  отключения источника питания  на локомотиве на 15-25 секунд тормоза должны отпустить и после включения линии не должны прийти в действие.</li>
        <li>проверка автоматических тормозов провести путем снижения давления в УР в один прием на  0,5 – 0,6 кгс/см<sup>2</sup> от зарядного давления, по истечении 2х минут вагонники проверяют действие тормозов по  всему поезду.</li>
      </ul>
      <p>Отпуск тормозов провести  постановкой ручки крана в поездное положение.</p>
      <p>В  «Справке об обеспечении поезда тормозами и исправном их действии» отмечается напряжение в цепях ЭПТ на хвостовом вагоне в режиме торможения ( более 30 вольт ).</p>
      <img className="img-fluid img-thumbnail mx-auto d-block" alt="image" src="../img/9-8.png" />
      <ol className="ol-article">
        <li>Продувка ТМ  поезда.</li>
        <li>Проверка  плотности ТМ (через 20 секунд после перекрытия комбинированного крана 367 блокировки) не должно быть  более 0,2 кгс/см<sup>2</sup> за 1 мин или 0,5кгс/см<sup>2</sup>  в течение 2,5 мин.</li>
        <li>Проверку действия ЭПТ (наполнение ТЦ локомотива 1,0-1,5 кгс/см<sup>2</sup> ) напряжение не ниже 45 вольт.</li>
        <li>Проверка работы диода (отключение линии ЭПТ на 15-25 секунд).</li>
        <li>Отпуск поездным положением.</li>
        <li>Проверка автоматических тормозов выпуск 0,5-0,6 кгс/см<sup>2</sup> через 2 минуты вагонники проверяют тормоза.</li>
        <li>Отпуск поездным положением крана машиниста № 395.</li>
      </ol>


    </React.Fragment>
  );
}