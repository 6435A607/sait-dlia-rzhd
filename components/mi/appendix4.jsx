import React from 'react';

export default function Appendix4(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">Время снижения скорости движения на установленную величину при проверке действия автотормозов в неустановленных местах.</h4>
      <br />
      <table className="table table-striped table-bordered table-responsive">
        <caption>Груженые поезда.</caption>
        <thead>
          <tr>
            <th rowSpan="2">Уклон ‰</th>
            <th className="tcw20" colSpan="2">80 км/час</th>
            <th className="tcw20" colSpan="2">70 км/час</th>
            <th className="tcw20" colSpan="2">60 км/час</th>
            <th className="tcw20" colSpan="2">50 км/час</th>
            <th className="tcw12">40 км/час</th>
          </tr>
          <tr>
            <th>s</th>
            <th>t</th>
            <th>s</th>
            <th>t</th>
            <th>s</th>
            <th>t</th>
            <th>s</th>
            <th>t</th>
            <th>s</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th>0</th>
            <td>502</td>
            <td>23,4</td>
            <td>429</td>
            <td>23,0</td>
            <td>356</td>
            <td>22,4</td>
            <td>285</td>
            <td>21,7</td>
            <td>216</td>
          </tr>
          <tr>
            <th>1</th>
            <td>532</td>
            <td>24,8</td>
            <td>454</td>
            <td>24,3</td>
            <td>376</td>
            <td>23,6</td>
            <td>300</td>
            <td>22,8</td>
            <td>226</td>
          </tr>
          <tr>
            <th>2</th>
            <td>564</td>
            <td>23,6</td>
            <td>480</td>
            <td>25,7</td>
            <td>396</td>
            <td>24,9</td>
            <td>315</td>
            <td>23,9</td>
            <td>237</td>
          </tr>
          <tr>
            <th>4</th>
            <td>631</td>
            <td>29,4</td>
            <td>534</td>
            <td>28,6</td>
            <td>439</td>
            <td>25,6</td>
            <td>347</td>
            <td>26,3</td>
            <td>259</td>
          </tr>
          <tr>
            <th>5</th>
            <td>668</td>
            <td>31,1</td>
            <td>564</td>
            <td>30,2</td>
            <td>462</td>
            <td>29,0</td>
            <td>364</td>
            <td>27,6</td>
            <td>270</td>
          </tr>
          <tr>
            <th>6</th>
            <td>707</td>
            <td>32,9</td>
            <td>596</td>
            <td>31,9</td>
            <td>487</td>
            <td>30,6</td>
            <td>382</td>
            <td>29,0</td>
            <td>282</td>
          </tr>
          <tr>
            <th>7</th>
            <td>749</td>
            <td>34,9</td>
            <td>629</td>
            <td>33,7</td>
            <td>513</td>
            <td>32,2</td>
            <td>401</td>
            <td>30,4</td>
            <td>295</td>
          </tr>
          <tr>
            <th>8</th>
            <td>794</td>
            <td>37,0</td>
            <td>666</td>
            <td>35,6</td>
            <td>541</td>
            <td>34,0</td>
            <td>421</td>
            <td>32,0</td>
            <td>309</td>
          </tr>
          <tr>
            <th>10</th>
            <td>898</td>
            <td>41,9</td>
            <td>749</td>
            <td>40,1</td>
            <td>603</td>
            <td>37,9</td>
            <td>465</td>
            <td>35,4</td>
            <td>338</td>
          </tr>
          <tr>
            <th>12</th>
            <td>1030</td>
            <td>48,2</td>
            <td>851</td>
            <td>45,7</td>
            <td>679</td>
            <td>42,8</td>
            <td>517</td>
            <td>39,4</td>
            <td>371</td>
          </tr>
          <tr>
            <th>14</th>
            <td>1209</td>
            <td>56,6</td>
            <td>986</td>
            <td>53,1</td>
            <td>775</td>
            <td>48,9</td>
            <td>581</td>
            <td>44,4</td>
            <td>410</td>
          </tr>
        </tbody>
      </table>
      <p>Тормозной путь (S) и время  (t) при снижении скорости на 10 км/час. </p>
      <br />
      <table className="table table-striped table-bordered table-responsive">
        <caption>Порожние составы.</caption>
        <thead>
          <tr>
            <th colSpan="1" rowSpan="2">Уклон ‰</th>
            <th className="tcw20" colSpan="2">80 км/час</th>
            <th className="tcw20" colSpan="2">70 км/час</th>
            <th className="tcw20" colSpan="2">60 км/час</th>
            <th className="tcw20" colSpan="2">50 км/час</th>
            <th className="tcw12">40 км/час</th>
          </tr>
          <tr>
            <th>s</th>
            <th>t</th>
            <th>s</th>
            <th>t</th>
            <th>s</th>
            <th>t</th>
            <th>s</th>
            <th>t</th>
            <th>s</th>
          </tr>
        </thead>
        <tbody>
          <tr><th>0</th>	<td>303</td>	<td>13,8</td>	<td>263</td>	<td>13,6</td>	<td>223</td>	<td>13,5</td>	<td>183</td>	<td>13,4</td>	<td>144</td></tr>
          <tr><th>1</th>	<td>315</td>	<td>14,3</td>	<td>273</td>	<td>14,2</td>	<td>231</td>	<td>14,0</td>	<td>189</td>	<td>13,8</td>	<td>148</td></tr>
          <tr><th>2</th>	<td>327</td>	<td>14,8</td>	<td>283</td>	<td>14,8</td>	<td>239</td>	<td>14,5</td>	<td>196</td>	<td>14,2</td>	<td>153</td></tr>
          <tr><th>3</th>	<td>339</td>	<td>15,4</td>	<td>293</td>	<td>15,2</td>	<td>247</td>	<td>15,0</td>	<td>202</td>	<td>14,7</td>	<td>157</td></tr>
          <tr><th>4</th>	<td>351</td>	<td>15,9</td>	<td>303</td>	<td>15,7</td>	<td>255</td>	<td>15,5</td>	<td>208</td>	<td>15,1</td>	<td>162</td></tr>
          <tr><th>5</th>	<td>364</td>	<td>16,5</td>	<td>313</td>	<td>16,3</td>	<td>264</td>	<td>16,0</td>	<td>214</td>	<td>15,6</td>	<td>166</td></tr>
          <tr><th>6</th>	<td>376</td>	<td>17,0</td>	<td>324</td>	<td>16,8</td>	<td>272</td>	<td>16,5</td>	<td>221</td>	<td>16,0</td>	<td>171</td></tr>
          <tr><th>7</th>	<td>389</td>	<td>17,6</td>	<td>334</td>	<td>17,3</td>	<td>280</td>	<td>17,0</td>	<td>227</td>	<td>16,5</td>	<td>175</td></tr>
          <tr><th>8</th>	<td>401</td>	<td>18,2</td>	<td>345</td>	<td>17,9</td>	<td>289</td>	<td>17,5</td>	<td>234</td>	<td>17,0</td>	<td>180</td></tr>
          <tr><th>10</th>	<td>428</td>	<td>19,4</td>	<td>367</td>	<td>19,0</td>	<td>306</td>	<td>18,5</td>	<td>247</td>	<td>18,0</td>	<td>189</td></tr>
          <tr><th>12</th>	<td>455</td>	<td>20,6</td>	<td>389</td>	<td>10,2</td>	<td>324</td>	<td>19,6</td>	<td>260</td>	<td>19,0</td>	<td>199</td></tr>
          <tr><th>14</th>	<td>483</td>	<td>21,9</td>	<td>413</td>	<td>21,4</td>	<td>353</td>	<td>20,6</td>	<td>275</td>	<td>20,0</td>	<td>209</td></tr>
        </tbody>
      </table>
      <p>Тормозной путь (S) и время  (t) при снижении скорости на 4-6 км/час</p>
    </React.Fragment>
  );
}