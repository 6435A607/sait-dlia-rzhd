import React from 'react';

export default function MIChapter7_4(props) {
    return (
      <React.Fragment>
        <ul className="ul-article"><span className="text-danger">7.4 </span>При смене локомотивных бригад (из рук в руки) принимающая локомотивная бригада дополнительно обязана:
          <li>проверить работу вспомогательного, а затем автоматического тормоза (чувствительность ВР к торможению и отпуск) по манометру тормозных цилиндров.</li>
          <li>после приведения локомотива в движение выполнить проверку вспомогательного тормоза при скорости 3-5 км/час до полной остановки.</li>
        </ul>
        <ul className="ul-article">Проверку крана вспомогательного тормоза так же проверять при достижении скорости 3-5км/ч:
          <li>после смены кабин управления, смены локомотивных бригад;</li>
          <li>после приемки из ремонта и технического обслуживания;</li>
          <li>после подготовки локомотива в рабочее состояние;</li>
          <li>после отцепки локомотива от состава (для пассажирских, грузовых, хозяйственных поездов, после прибытия поездным порядком);</li>
          <li>локомотив (занятый в маневровом виде движения) с вагонами, с включенными и опробованными автотормозами, в начале маневровых передвижений, произвести проверку автотормозов  с составом при скорости 3-10 км/час,до полной остановки.</li>
        </ul>
      </React.Fragment>
    );
  }