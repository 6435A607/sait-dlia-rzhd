import React from 'react';
import Acceptance from './acceptance.jsx';

export default function MIChapter7_5(props) {
  return (
    <React.Fragment>
      <p><span className="text-danger">7.5 </span>Данные проверки выполнять при приёмке после ТО, или отстоя без локомотивной бригады</p>
      <Acceptance />
    </React.Fragment>
  );
}