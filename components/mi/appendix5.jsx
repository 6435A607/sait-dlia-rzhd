import React from 'react';

export default function Appendix5(props) {
  return (
    <React.Fragment>
      <table className="table table-striped table-bordered table-responsive">
        <caption>ДОПУСКАЕМЫЕ СКОРОСТИ проследования светофора с одним желтым (немигающим) огнем, находящегося на расстоянии менее требуемого тормозного пути от следующего светофора, неконтролируемые устройствами АЛСН.</caption>
        <thead>
          <tr>
            <th className="tcw46">Наименование станций</th>
            <th>Литер светофора</th>
            <th className="tcw12">Длина ограждаемого участка</th>
            <th className="tcw12">Допускаемые скорости проследования светофора с желтым огнем</th>
          </tr>
        </thead>
        <tbody>
          <tr><th colSpan="4">Тосно - Гатчина</th></tr>
          <tr>
            <td>Новолисино             27,539</td>
            <td>НМ1А</td>
            <td>249</td>
            <td>20</td>
          </tr>
          <tr>
            <td>26,732</td>
            <td>ЧМ5</td>
            <td>166</td>
            <td>20</td>
          </tr>
          <tr>
            <td>25,985</td>
            <td>1ЧВ</td>
            <td>1191</td>
            <td>20</td>
          </tr>
          <tr>
            <td>26,890</td>
            <td>НМ5А</td>
            <td>166</td>
            <td>20</td>
          </tr>
          <tr><th colSpan="4">Волховстрой-Обухово (четное направление)</th></tr>
          <tr>
            <td>Мга                          48,658</td>
            <td>НМ1</td>
            <td>528</td>
            <td>25</td>
          </tr>
          <tr>
            <td>45,090</td>
            <td>3</td>
            <td>675</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Горы                        41,619</td>
            <td>III</td>
            <td>499</td>
            <td>20</td>
          </tr>
          <tr>
            <td>Рыбацкое			12,970</td>
            <td>4</td>
            <td>700</td>
            <td>50</td>
          </tr>
          <tr>
            <td>12,270</td>
            <td>2</td>
            <td>560</td>
            <td>35</td>
          </tr>
          <tr><th colSpan="4">СПб Варшавский - Луга (нечетное направление)</th></tr>
          <tr>
            <td>Броневая</td>
            <td>НВ</td>
            <td>530</td>
            <td>25</td>
          </tr>
          <tr>
            <td>Шоссейная              10,409</td>
            <td>НП</td>
            <td>704</td>
            <td>40</td>
          </tr>
          <tr><th colSpan="4">СПб Варшавский-Луга (четное направление)</th></tr>
          <tr>
            <td>Луга                        137,181</td>
            <td>ЧМ1</td>
            <td>639</td>
            <td>40</td>
          </tr>
          <tr>
            <td>Шоссейная              10,144</td>
            <td>2</td>
            <td>775</td>
            <td>55</td>
          </tr>
          <tr>
            <td>Предпортовая           7,537</td>
            <td>Ч2Б</td>
            <td>671</td>
            <td>45</td>
          </tr>
          <tr>
            <td>Броневая                   3,700</td>
            <td>ЧШ</td>
            <td>700</td>
            <td>50</td>
          </tr>
          <tr>
            <td>3,000</td>
            <td>ЧШМ</td>
            <td>629</td>
            <td>40</td>
          </tr>
          <tr><th colSpan="4">СПб Балтийский-Лигово-Ораниенбаум II (нечетное напрвление)</th></tr>
          <tr>
            <td>СПб Балтийский      0,514</td>
            <td>Н1</td>
            <td>636</td>
            <td>40</td>
          </tr>
          <tr>
            <td>1,150</td>
            <td>5</td>
            <td>475</td>
            <td>20</td>
          </tr>
          <tr>
            <td>2,200</td>
            <td>3</td>
            <td>575</td>
            <td>35</td>
          </tr>
          <tr><th colSpan="4">СПб Витебский – Езерище (нечетное направление)</th></tr>
          <tr>
            <td>СПб Вит.пасс.          2,323</td>
            <td>Н1</td>
            <td>550</td>
            <td>35</td>
          </tr>
          <tr>
            <td>Павловск                 26,342</td>
            <td>НМ1</td>
            <td>604</td>
            <td>35</td>
          </tr>
          <tr><th colSpan="4">СПб Витебский – Езерище (четное направление)</th></tr>
          <tr>
            <td>Павловск                 26,439</td>
            <td>ЧМ2</td>
            <td>472</td>
            <td>20</td>
          </tr>
          <tr>
            <td>СПб Вит.пасс.          2,450</td>
            <td>Ч</td>
            <td>617</td>
            <td>40</td>
          </tr>
          <tr>
            <td>по 3 пути                   1,833</td>
            <td>ЧМIIIА</td>
            <td>546</td>
            <td>35</td>
          </tr>
          <tr>
            <td>Цветочная                 6,067</td>
            <td>ЧМ2</td>
            <td>450</td>
            <td>20</td>
          </tr>
          <tr><th colSpan="4">СПб-Выборг (нечетное направление)</th></tr>

          <tr>
            <td>СПб пасс.Финл.        0,858</td>
            <td>Н1М</td>
            <td>527</td>
            <td>20</td>
          </tr>
          <tr>
            <td>                         1,383</td>
            <td>НМ1Б</td>
            <td>514</td>
            <td>20</td>
          </tr>
          <tr>
            <td>                         1,897</td>
            <td>Н1А</td>
            <td>675</td>

            <td>45</td>
          </tr>
          <tr>
            <td>                         3,450</td>
            <td>3</td>
            <td>600</td>
            <td>35</td>
          </tr>
          <tr>
            <td>                         4,050</td>
            <td>1</td>
            <td>650</td>
            <td>40</td>
          </tr>
          <tr>
            <td>Шувалово                10,550</td>
            <td>Н</td>
            <td>700</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Парголово			17,900 </td>
            <td>1</td>
            <td>619</td>
            <td>40</td>
          </tr>
          <tr>
            <td>Левашово                23,260</td>
            <td>13</td>
            <td>700</td>
            <td>50</td>
          </tr>
          <tr>
            <td>Белоостров              48,450</td>
            <td>1</td>
            <td>720</td>
            <td>50</td>
          </tr>

          <tr>
            <th colSpan="4">СПб-Выборг (четное направление)</th></tr>
          <tr>
            <td>Зеленогорск            50,861</td>
            <td>ЧМII</td>
            <td>538</td>
            <td>30</td>
          </tr>
          <tr>
            <td>Парголово               15,699</td>
            <td>ЧМ2</td>
            <td>296</td>
            <td>20</td>
          </tr>
          <tr>
            <td>Удельная                  6,722</td>
            <td>Ч2</td>
            <td>807</td>
            <td>50</td>
          </tr>
          <tr>
            <td>                                   4,810</td>
            <td>6</td>
            <td>610</td>
            <td>40</td>
          </tr>
          <tr>
            <td>                                   4,200</td>
            <td>4</td>
            <td>500</td>
            <td>20</td>
          </tr>
          <tr>
            <td>СПб пасс.Финл.        2,895</td>	<td>ЧЛ</td>
            <td>575</td>
            <td>25</td>
          </tr>
          <tr>
            <td>                                   2,320</td>
            <td>ЧМ2</td>
            <td>510</td>
            <td>20</td>
          </tr>
          <tr><th colSpan="4">          СПб – Сосново – Кузнечное (нечетное направление)</th></tr>
          <tr>
            <td>СПб пасс.Финл.        0,260</td>
            <td>НМ9</td>
            <td>598</td>
            <td>35</td>
          </tr>
          <tr>
            <td>0,327</td>
            <td>НМ3</td>
            <td>531</td>
            <td>25</td>
          </tr>
          <tr>
            <td>0,886</td>
            <td>Н3М</td>
            <td>527</td>
            <td>25</td>
          </tr>
          <tr>
            <td>1,383</td>
            <td>НМ3Б</td>
            <td>514</td>
            <td>20</td>
          </tr>
          <tr>
            <td>1,383</td>
            <td>НМ4Б</td>
            <td>514</td>
            <td>20</td>
          </tr>
          <tr>
            <td>Кушелевка                4,435</td>
            <td>НФ3</td>
            <td>438</td>
            <td>20</td>
          </tr>
          <tr>
            <td>Пискаревка               4,249</td>
            <td>НМ2</td>
            <td>526</td>
            <td>25</td>
          </tr>
          <tr>
            <td>Пери                        36,020</td>
            <td>НМ1А</td>
            <td>284</td>
            <td>20</td>
          </tr>
          <tr>
            <td>36,020</td>
            <td>НМ2А</td>
            <td>441</td>
            <td>20</td>
          </tr>
          <tr><th colSpan="4">          СПб – Сосново – Кузнечное (четное направление)</th></tr>
          <tr>
            <td>Пискаревка               3,423</td>
            <td>Ч2</td>
            <td>556</td>
            <td>30</td>
          </tr>
          <tr>
            <td>Кушелевка                4,943</td>
            <td>Ч2</td>
            <td>604</td>
            <td>40</td>
          </tr>
        </tbody>
      </table>
    </React.Fragment>
  );
}