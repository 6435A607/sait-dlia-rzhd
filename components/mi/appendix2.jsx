import React from 'react';

export default function Appendix2(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">Величина тормозного пути одиночно следующих локомотивов, в том числе в «сплотках», а также поездов с составом до 5 вагонов включительно, в зависимости от скорости движения V и уклона J</h4>
      <br />
      <table className="table table-responsive table-striped">
        <thead>
          <tr>
            <th>J</th>
            <th className="tcw20" rowSpan="2">0,00</th>
            <th className="tcw20" rowSpan="2">-0,02</th>
            <th className="tcw20" rowSpan="2">-0,04</th>
            <th className="tcw20" rowSpan="2">-0,06</th>
            <th className="tcw20" rowSpan="2">-0,08</th>
          </tr>
          <tr>
            <th>V КМ/Ч</th>
          </tr>
        </thead>
        <tbody>
          <tr> <td>40</td> <td>207</td> <td>247</td> <td>268</td> <td>307</td> <td>349</td> </tr>
          <tr> <td>50</td> <td>261</td> <td>301</td> <td>352</td> <td>384</td> <td>437</td> </tr>
          <tr> <td>60</td> <td>326</td> <td>347</td> <td>387</td> <td>407</td> <td>458</td> </tr>
          <tr> <td>70</td> <td>391</td> <td>432</td> <td>492</td> <td>512</td> <td>446</td> </tr>
          <tr> <td>80</td> <td>434</td> <td>455</td> <td>525</td> <td>568</td> <td>601</td> </tr>
        </tbody>
      </table>
      <br />
      <h4 className="text-center">Расчетные тормозные пути при проверке действия тормозов одиночно следующего локомотива при снижении скорости на 10 км/ч на заданной скорости</h4>
      <table className="table table-responsive  table-striped table-bordered">
        <caption>Электротяга</caption>
        <thead>
          <tr>
            <th rowSpan="2">Скорость и расстояния в метрах на заданных уклонах по сериям тяги</th>
            <th colSpan="2">ВЛ10</th>
            <th colSpan="2">ВЛ10У</th>
            <th colSpan="2">ВЛ15</th>
            <th colSpan="2">2ЭС4К, 3ЭС4К</th>
          </tr>
          <tr>
            <th className="tcw12">до 4 ‰</th>
            <th className="tcw12">свыше 4 ‰</th>
            <th className="tcw12">до 4 ‰</th>
            <th className="tcw12">свыше 4 ‰</th>
            <th className="tcw12">до 4 ‰</th>
            <th className="tcw12">свыше 4 ‰</th>
            <th className="tcw12">до 4 ‰</th>
            <th className="tcw12">свыше 4 ‰</th>
          </tr>
        </thead>
        <tbody>
          <tr> <td>80-70 км/час</td> <td>329</td> <td>377</td> <td>349</td> <td>283</td> <td>339</td> <td>391</td> <td>304</td> <td>344</td> </tr>
          <tr> <td>70-60 км/час</td> <td>272</td> <td>301</td> <td>288</td> <td>239</td> <td>280</td> <td>312</td> <td>251</td> <td>275</td> </tr>
          <tr> <td>60-50 км/час</td> <td>226</td> <td>251</td> <td>239</td> <td>193</td> <td>232</td> <td>260</td> <td>208</td> <td>229</td> </tr>
          <tr> <td>50-40 км/час</td> <td>179</td> <td>198</td> <td>190</td> <td>149</td> <td>184</td> <td>205</td> <td>165</td> <td>180</td> </tr>
          <tr> <td>40-30 км/час</td> <td>134</td> <td>147</td> <td>142</td> <td>107</td> <td>138</td> <td>152</td> <td>124</td> <td>134</td> </tr>
        </tbody>
      </table>

      <table className="table table-responsive  table-striped table-bordered">
        <caption>Теплотяга</caption>
        <thead>
          <tr>
            <th rowSpan="2">Скорость и расстояния в метрах на заданных уклонах по сериям тяги</th>
            <th colSpan="2">М62, 2М62</th>
            <th colSpan="2">2М62у, ЗМ62У</th>
            <th colSpan="2">2ТЭ116</th>
            <th colSpan="2">2ТЭ116у</th>
            <th colSpan="2">ТЭМ18</th>
          </tr>
          <tr>
            <th className="tcw12">до 4 ‰</th>
            <th className="tcw12">свыше 4 ‰</th>
            <th className="tcw12">до 4 ‰</th>
            <th className="tcw12">свыше 4 ‰</th>
            <th className="tcw12">до 4 ‰</th>
            <th className="tcw12">свыше 4 ‰</th>
            <th className="tcw12">до 4 ‰</th>
            <th className="tcw12">свыше 4 ‰</th>
            <th className="tcw12">до 4 ‰</th>
            <th className="tcw12">свыше 4 ‰</th>
          </tr>
        </thead>
        <tbody>
          <tr> <td>80-70км/час</td>	<td>322</td> <td>370</td> <td>333</td> <td>386</td> <td>354</td> <td>414</td> <td>357</td> <td>419</td> <td>354</td> <td>415</td> </tr>
          <tr> <td>70-60км/час</td>	<td>266</td> <td>295</td> <td>275</td> <td>307</td> <td>292</td> <td>330</td> <td>295</td> <td>334</td> <td>293</td> <td>331</td> </tr>
          <tr> <td>60-50км/час</td>	<td>221</td> <td>246</td> <td>228</td> <td>256</td> <td>242</td> <td>275</td> <td>245</td> <td>278</td> <td>243</td> <td>276</td> </tr>
          <tr> <td>50-40км/час</td>	<td>175</td> <td>194</td> <td>181</td> <td>202</td> <td>192</td> <td>216</td> <td>194</td> <td>219</td> <td>193</td> <td>217</td> </tr>
          <tr> <td>40-30км/час</td>	<td>131</td> <td>144</td> <td>136</td> <td>150</td> <td>144</td> <td>160</td> <td>145</td> <td>162</td> <td>144</td> <td>161</td> </tr>
        </tbody>
      </table>
      <table className="table table-responsive  table-striped table-bordered">
        <thead>
          <tr>
            <th rowSpan="2">Скорость и расстояния в метрах на заданных уклонах по сериям тяги</th>
            <th colSpan="2">ТЭМ2</th>
            <th colSpan="2">ТЭМ7</th>
            <th colSpan="2">ТЭМ7А</th>
            <th colSpan="2">ТЭП70</th>
            <th colSpan="2">ЧМЭЗ</th>
          </tr>
          <tr>
            <th className="tcw12">до 4 ‰</th>
            <th className="tcw12">свыше 4 ‰</th>
            <th className="tcw12">до 4 ‰</th>
            <th className="tcw12">свыше 4 ‰</th>
            <th className="tcw12">до 4 ‰</th>
            <th className="tcw12">свыше 4 ‰</th>
            <th className="tcw12">до 4 ‰</th>
            <th className="tcw12">свыше 4 ‰</th>
            <th className="tcw12">до 4 ‰</th>
            <th className="tcw12">свыше 4 ‰</th>
          </tr>
        </thead>
        <tr> <td>80-70 км/час</td> <td>348</td> <td>407</td> <td>331</td> <td>382</td> <td>331</td> <td>382</td> <td>316</td> <td>362</td> <td>348</td> <td>407</td> </tr>
        <tr> <td>70-60 км/час</td> <td>288</td> <td>324</td> <td>273</td> <td>304</td> <td>273</td> <td>304</td> <td>261</td> <td>288</td> <td>288</td> <td>324</td> </tr>
        <tr> <td>60-50 км/час</td> <td>239</td> <td>270</td> <td>227</td> <td>254</td> <td>227</td> <td>254</td> <td>216</td> <td>240</td> <td>239</td> <td>270</td> </tr>
        <tr> <td>50-40 км/час</td> <td>189</td> <td>213</td> <td>180</td> <td>200</td> <td>180</td> <td>200</td> <td>172</td> <td>189</td> <td>189</td> <td>213</td> </tr>
        <tr> <td>40-30 км/час</td> <td>142</td> <td>158</td> <td>135</td> <td>148</td> <td>135</td> <td>148</td> <td>129</td> <td>141</td> <td>142</td> <td>158</td> </tr>
      </table>

    </React.Fragment>
  );
}