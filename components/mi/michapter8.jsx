import React from 'react';
import TapePulls from './tapepulls.jsx';

export default function MIChapter8(props) {
    return (
        <React.Fragment>
            <h4 className="text-center">Обязательные протяжки ленты</h4>
            <br />
            <TapePulls />
        </React.Fragment>
    );
}