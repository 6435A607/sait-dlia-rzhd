import React from 'react';
import BrakesShusharyVolhov from '../brakes/brakesshusharyvolhov.jsx';
import BrakesVolhovShushary from '../brakes/brakesvolhovshushary.jsx';
import BrakesVolhovSvyr from '../brakes/brakesvolhovsvyr.jsx';
import BrakesSvyrVolhov from '../brakes/brakessvyrvolhov.jsx';
import BrakesVolhovBabaevo from '../brakes/brakesvolhovbabaevo.jsx';
import BrakesBabaevoVolhov from '../brakes/brakesbabaevovolhov.jsx';
import BrakesSpbVitebDno from '../brakes/brakesspbvitebdno.jsx';
import BrakesDnoSpbViteb from '../brakes/brakesdnospbviteb.jsx';
import BrakesSpbVitebVladLugaPskov from '../brakes/brakesspbvitebvladlugapskov.jsx';
import BrakesPskovLugaVladSpbViteb from '../brakes/brakespskovlugavladspbviteb.jsx';
import BrakesShusharyVladNarva from '../brakes/brakesshusharyvladnarva.jsx';
import BrakesNarvaVladShushary from '../brakes/brakesnarvavladshushary.jsx';
import BrakesShusharyShosNarva from '../brakes/brakesshusharyshosnarva.jsx';
import BrakesNarvaShosShushary from '../brakes/brakesnarvashosshushary.jsx';
import BrakesShusharyShosVeimarnLuzh from '../brakes/brakesshusharyshosveimarnluzh.jsx';
import BrakesLuzhVladShusharySpsm from '../brakes/brakesluzhvladshusharyspsm.jsx';
import BrakesShusharyLigovoKalLuzh from '../brakes/brakesshusharyligovokalluzh.jsx';
import BrakesLuzhKalLigovoSpsm from '../brakes/brakesluzhkalligovospsm.jsx';
import BrakesGatchinaMgaBudo from '../brakes/brakesgatchinamgabudo.jsx';
import BrakesBudoMgaGatchina from '../brakes/brakesbudomgagatchina.jsx';
import BrakesVeimarnSlan from '../brakes/brakesveimarnslan.jsx';
import BrakesSlanVeimarn from '../brakes/brakesslanveimarn.jsx';
import BrakesSpbBaltGatchina from '../brakes/brakesspbbaltgatchina.jsx';
import BrakesGatchinaSpbBalt from '../brakes/brakesgatchinaspbbalt.jsx';
import BrakesDnoPskov from '../brakes/brakesdnopskov.jsx';
import BrakesPskovDno from '../brakes/brakespskovdno.jsx';
import BrakesUzel from '../brakes/brakesuzel.jsx';

export default function Appendix3(props) {
  return (
    <React.Fragment>
     <h4 className="text-center">Места, скорости движения при проверке действия тормозов в пути следования и расчетные тормозные пути при снижении скорости на 10 км/ч </h4>
      <BrakesShusharyVolhov />
      <BrakesVolhovShushary />
      <BrakesVolhovSvyr />
      <BrakesSvyrVolhov />
      <BrakesVolhovBabaevo />
      <BrakesBabaevoVolhov />
      <BrakesSpbVitebDno />
      <BrakesDnoSpbViteb />
      <BrakesSpbVitebVladLugaPskov />
      <BrakesPskovLugaVladSpbViteb />
      <BrakesShusharyVladNarva />
      <BrakesNarvaVladShushary />
      <BrakesShusharyShosNarva />
      <BrakesNarvaShosShushary />
      <BrakesShusharyShosVeimarnLuzh />
      <BrakesLuzhVladShusharySpsm />
      <BrakesShusharyLigovoKalLuzh />
      <BrakesLuzhKalLigovoSpsm />
      <BrakesGatchinaMgaBudo />
      <BrakesBudoMgaGatchina />
      <BrakesVeimarnSlan />
      <BrakesSlanVeimarn />
      <BrakesSpbBaltGatchina />
      <BrakesGatchinaSpbBalt />
      <BrakesDnoPskov />
      <BrakesPskovDno />
      <BrakesUzel />
    </React.Fragment>
  );
}