import React from 'react';

export default function DownloadButton(props) {
    return(
      <React.Fragment>
        <button className="btn btn-success" onClick={props.onClick}>Скачать документ</button>
      </React.Fragment>
    );
  }