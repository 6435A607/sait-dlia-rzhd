import React from 'react';

export default function ButtonCollapseSpeed(props) {
  return (
    <React.Fragment>
      <button className="col" data-toggle="collapse" data-target={"#" + props.id}>
        {props.text}
      </button>
    </React.Fragment>
  );

}