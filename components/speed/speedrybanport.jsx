import React from 'react';
import TableSpeed from './tablespeed.jsx';
import ButtonCollapseSpeed from './buttoncollapsespeed.jsx';

export default function SpeedRybaNPort(props) {
  let id = "rybanport";
  return (
    <React.Fragment>
      <ButtonCollapseSpeed id={id} text="Рыбацкое - Новый Порт" />
      <div className="collapse" id={id}>
        <TableSpeed tbody={data} />
      </div>
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    РЫБАЦКОЕ </td> </tr>
  <tr> <td>I </td> <td>1 км 6 пк - 2 км 2 пк от стр.пер. № 56 до сигн. ЧК</td> <td></td> <td>70</td> <td>70</td> <td></td> </tr>
  <tr> <td></td> <td>1 км 6 пк - 7 пк</td> <td></td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 5, 9, 10, 11, 12</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
  <tr> <td colSpan="6">    РЫБАЦКОЕ - КУПЧИНСКАЯ </td> </tr>
  <tr> <td>I</td> <td>2 км 3 пк - 4 км 5 пк</td> <td></td> <td>70</td> <td>70</td> <td></td> </tr>
  <tr> <td colSpan="6">    КУПЧИНСКАЯ </td> </tr>
  <tr> <td>II</td> <td>4 км 5 пк - 7 км 1 пк</td> <td></td> <td>70</td> <td>70</td> <td></td> </tr>
  <tr> <td></td> <td>5 км 4 пк - 6 км 8 пк</td> <td></td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td colSpan="6">    КУПЧИНСКАЯ - СРЕДНЕРОГАТСКАЯ </td> </tr>
  <tr> <td>I</td> <td>7 км 1 пк - 8 км 8 пк</td> <td></td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td colSpan="6">    СРЕДНЕРОГАТСКАЯ </td> </tr>
  <tr> <td>I</td> <td>8 км 8 пк - 11 км 4 пк</td> <td></td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>съезды 3/5, 21/19, 6/4</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>1 гл. путь (на ст.Шоссейная)</td> <td></td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td colSpan="6">    СРЕДНЕРОГАТСКАЯ - ПРЕДПОРТОВАЯ </td> </tr>
  <tr> <td>I</td> <td>11 км 4 пк - 12 км 6 пк</td> <td></td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>11 км 4 пк - 12 км 4 пк</td> <td></td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПРЕДПОРТОВАЯ </td> </tr>
  <tr> <td>III</td> <td>12 км 6 пк - 16 км 1 пк</td> <td></td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 13</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>от стр.пер. № 77 до стр.пер. № 63а (АВ)                       (на ст. Автово)</td> <td></td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПРЕДПОРТОВАЯ - НАРВСКАЯ </td> </tr>
  <tr> <td>I</td> <td>16 км 1 пк - 18 км 1 пк</td> <td></td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td colSpan="6">    НАРВСКАЯ </td> </tr>
  <tr> <td>III</td> <td>18 км 1 пк - 19 км 7 пк</td> <td></td> <td>50</td> <td>50</td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 2</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">    НАРВСКАЯ - НОВЫЙ ПОРТ </td> </tr>
  <tr> <td>I</td> <td>19 км 7 пк - 21 км 5 пк</td> <td></td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td colSpan="6">    НОВЫЙ ПОРТ </td> </tr>
  <tr> <td>I</td> <td>21 км 5 пк - 24 км 5 пк</td> <td></td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк сортировочно-отправочный Б </td> </tr>
  <tr> <td></td> <td>путь 2, 3, 4, 5, 6, 8, 9</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
</React.Fragment>;