import React from 'react';
import TableSpeed from './tablespeed.jsx';

export default function SpeedMgaBudoOvinishe(props) {
    return (
        <React.Fragment>
            <h4 className="text-center">Мга - Будогощь - Овинище I</h4>
            <br />
            <TableSpeed tbody={data} />
        </React.Fragment>
    );
}

const data = <React.Fragment>
    <tr> <td colSpan="6">    МГА</td> </tr>
    <tr> <td>IV</td> <td>0 км 2 пк - 3 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>0 км 2 пк - 1 км 7 пк стрелочные переводы</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 8</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td></td> <td>съезд 9/11 для пасс.поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
    <tr> <td colSpan="6">    МГА - СОЛОГУБОВКА</td> </tr>
    <tr> <td>I</td> <td>3 км 8 пк - 7 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>3 км 10 пк - 5 км 2 пк кривые</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    СОЛОГУБОВКА</td> </tr>
    <tr> <td>I</td> <td>7 км 7 пк - 9 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    СОЛОГУБОВКА - МАЛУКСА</td> </tr>
    <tr> <td>I</td> <td>9 км 7 пк - 25 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>13 км 1 пк - 14 км 2 пк кривые</td> <td>100</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>14 км 3 пк - 10 пк кривые</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    МАЛУКСА</td> </tr>
    <tr> <td>I</td> <td>25 км 3 пк - 27 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 4, 5</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">    МАЛУКСА - ПОГОСТЬЕ</td> </tr>
    <tr> <td>I</td> <td>27 км 4 пк - 34 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ПОГОСТЬЕ</td> </tr>
    <tr> <td>I</td> <td>34 км 1 пк - 35 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ПОГОСТЬЕ - ЖАРОК</td> </tr>
    <tr> <td>I</td> <td>35 км 7 пк - 41 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ЖАРОК</td> </tr>
    <tr> <td>I</td> <td>41 км 7 пк - 43 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 2</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">    ЖАРОК - ПОСАДНИКОВО</td> </tr>
    <tr> <td>I</td> <td>43 км 5 пк - 58 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>56 км 4 пк больное земляное полотно</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    ПОСАДНИКОВО</td> </tr>
    <tr> <td>I</td> <td>58 км 5 пк - 60 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>60 км 1 пк - 6 пк кривые</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 2</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">    ПОСАДНИКОВО - ОСТ. ПУНКТ 63 КМ</td> </tr>
    <tr> <td>I</td> <td>60 км 6 пк - 63 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>60 км 6 пк - 10 пк кривые</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>62 км 7 пк - 10 пк кривые</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ОСТ. ПУНКТ 63 КМ</td> </tr>
    <tr> <td>I</td> <td>63 км 7 пк - 64 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td colSpan="6">    ОСТ. ПУНКТ 63 КМ - КИРИШИ</td> </tr>
    <tr> <td>I</td> <td>64 км 2 пк - 66 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>66 км 1 пк - 5 пк кривые</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    КИРИШИ</td> </tr>
    <tr> <td>I</td> <td>66 км 5 пк - 70 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>66 км 5 пк - 10 пк кривые</td> <td>80</td> <td>60</td> <td>60</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>съезды 1/3, 26/28, 33/31, 19/17, 57/55</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">      Парк А</td> </tr>
    <tr> <td></td> <td>путь 6, 7, 8, 9, 10, 11</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">      Парк З</td> </tr>
    <tr> <td></td> <td>путь 1, 2, 3, 4, 5, 6</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">    КИРИШИ - ПЧЕВЖА</td> </tr>
    <tr> <td>I</td> <td>70 км 2 пк - 87 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ПЧЕВЖА</td> </tr>
    <tr> <td>I</td> <td>87 км 3 пк - 89 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>87 км 8 пк - 88 км 9 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>88 км 9 пк - 89 км 4 пк порожние грузовые вагоны</td> <td>90</td> <td>90</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 3</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td></td> <td>путь 8</td> <td> </td> <td> </td> <td> </td> <td>15</td> </tr>
    <tr> <td colSpan="6">    ПЧЕВЖА - БУДОГОЩЬ</td> </tr>
    <tr> <td>I</td> <td>89 км 5 пк - 98 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    БУДОГОЩЬ</td> </tr>
    <tr> <td>II</td> <td>98 км 1 пк - 100 км 3 пк</td> <td>100</td> <td>90</td> <td>90</td> <td> </td> </tr>
    <tr> <td></td> <td>98 км 1 пк - 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>98 км 7 пк - 99 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>98 км 7 пк - 100 км 3 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td></td> <td>путь 3</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>глухое пересечение № 19/21-23/25</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    БУДОГОЩЬ - ТАЛЬЦЫ-МОЛОГСКИЕ</td> </tr>
    <tr> <td>I</td> <td>100 км 3 пк - 127 км 3 пк</td> <td>100</td> <td>90</td> <td>90</td> </tr>
    <tr> <td></td> <td>100 км 3 пк - 101 км 10 пк</td> <td>60</td> <td>60</td> <td>60</td> </tr>
    <tr> <td></td> <td>108 км 8 пк - 109 км 1 пк</td> <td>85</td> <td>85</td> <td>85</td> </tr>
    <tr> <td></td> <td>110 км 4 пк - 127 км 2 пк</td> <td>80</td> <td>80</td> <td>80</td> </tr>
    <tr> <td colSpan="6">    ТАЛЬЦЫ-МОЛОГСКИЕ</td> </tr>
    <tr> <td>I</td> <td>127 км 3 пк - 128 км 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ТАЛЬЦЫ-МОЛОГСКИЕ - ВОДОГОН</td> </tr>
    <tr> <td>I</td> <td>128 км 6 пк - 133 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ВОДОГОН</td> </tr>
    <tr> <td>I</td> <td>133 км 1 пк - 135 км 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ВОДОГОН - НЕБОЛЧИ</td> </tr>
    <tr> <td>I</td> <td>135 км 6 пк - 154 км 8 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>150 км 1 пк - 154 км 7 пк дефектность шпал</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    НЕБОЛЧИ</td> </tr>
    <tr> <td>III</td> <td>154 км 8 пк - 156 км 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>155 км 2 пк - 156 км 6 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    НЕБОЛЧИ - КИПРИЯ</td> </tr>
    <tr> <td>I</td> <td>156 км 6 пк - 187 км 8 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>156 км 6 пк - 161 км 1 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>166 км 3 пк - 166 км 5 пк</td> <td>80</td> <td>80</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>166 км 6 пк - 169 км 5 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>170 км 1 пк - 173 км 5 пк</td> <td>80</td> <td>80</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>174 км 3 пк - 174 км 7 пк</td> <td>80</td> <td>80</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>175 км 3 пк - 176 км 1 пк</td> <td>80</td> <td>80</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>176 км 6 пк - 178 км 1 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>178 км 2 пк - 178 км 7 пк</td> <td>80</td> <td>80</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>179 км 3 пк - 179 км 6 пк</td> <td>80</td> <td>80</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>179 км 7 пк - 185 км 1 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>по 2 пути разъезда Окулово 166 км 8 пк – 168 км 1 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td></td> <td>по 2 пути разъезда Теребутенец 176 км 6 пк – 177 км 9 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td colSpan="6">    КИПРИЯ</td> </tr>
    <tr> <td>I</td> <td>187 км 8 пк - 189 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>188 км 1 пк - 188 км 3 пк</td> <td>80</td> <td>80</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 3, 4</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">    КИПРИЯ - АНЦИФЕРОВО-МОЛОГСКОЕ</td> </tr>
    <tr> <td>I</td> <td>189 км 5 пк - 199 км 2 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>191 км 5 пк - 192 км 5 пк</td> <td>80</td> <td>80</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>193 км 8 пк – 194 км 1 пк </td> <td>80</td> <td>80</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>194 км 8 пк – 194 км 10 пк </td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>197 км 1 пк – 197 км 10 пк</td> <td>80</td> <td>80</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>198 км 1 пк – 198 км 4 пк</td> <td>80</td> <td>80</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>198 км 10 пк - 199 км 2 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    АНЦИФЕРОВО-МОЛОГСКОЕ</td> </tr>
    <tr> <td>I</td> <td>199 км 2 пк - 200 км 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>199 км 2 пк - 3 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    АНЦИФЕРОВО-МОЛОГСКОЕ - СОМИНКА</td> </tr>
    <tr> <td>I</td> <td>200 км 6 пк - 203 км 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    СОМИНКА</td> </tr>
    <tr> <td>I</td> <td>203 км 10 пк - 205 км 8 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    СОМИНКА - ПЕСЬ</td> </tr>
    <tr> <td>I</td> <td>205 км 8 пк - 219 км 7 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>207 км 5 пк - 6 пк дефектность ИССО</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td colSpan="6">    ПЕСЬ</td> </tr>
    <tr> <td>I</td> <td>219 км 7 пк - 221 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 3</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">    ПЕСЬ - ХВОЙНАЯ</td> </tr>
    <tr> <td>I</td> <td>221 км 3 пк - 229 км 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ХВОЙНАЯ</td> </tr>
    <tr> <td>I</td> <td>229 км 6 пк - 231 км 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>229 км 10 пк - 231 км 3 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ХВОЙНАЯ - КУШАВЕРА</td> </tr>
    <tr> <td>I</td> <td>231 км 6 пк - 239 км 9 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    КУШАВЕРА</td> </tr>
    <tr> <td>I</td> <td>239 км 9 пк - 241 км 2 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>239 км 10 пк - 241 км 2 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    КУШАВЕРА - КАБОЖА</td> </tr>
    <tr> <td>I</td> <td>241 км 2 пк - 263 км 10 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    КАБОЖА</td> </tr>
    <tr> <td>I</td> <td>263 км 10 пк - 266 км 9 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    КАБОЖА - БУГРЫ</td> </tr>
    <tr> <td>I</td> <td>266 км 9 пк - 278 км 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>265 км 5 пк - 275 км 10 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    БУГРЫ</td> </tr>
    <tr> <td>I</td> <td>278 км 6 пк - 280 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    БУГРЫ - АБРОСОВО</td> </tr>
    <tr> <td>I</td> <td>280 км 3 пк - 291 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>290 км 8 пк - 291 км 3 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    АБРОСОВО</td> </tr>
    <tr> <td>I</td> <td>291 км 3 пк - 292 км 9 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>291 км 3 пк - 5 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    АБРОСОВО - ПЕСТОВО</td> </tr>
    <tr> <td>I</td> <td>292 км 9 пк - 317 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>301 км 9 пк - 303 км 6 пк кривые</td> <td>70</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>307 км 2 пк - 309 км 1 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    ПЕСТОВО</td> </tr>
    <tr> <td>I</td> <td>317 км 5 пк - 319 км 4 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>317 км 8 пк - 318 км 8 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 5, 6</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">    ПЕСТОВО - САНДОВО</td> </tr>
    <tr> <td>I</td> <td>319 км 4 пк - 356 км 6 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>331 км 1 пк - 332 км 6 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>336 км 8 пк - 382 км 2 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    САНДОВО</td> </tr>
    <tr> <td>II</td> <td>356 км 6 пк - 358 км 4 пк</td> <td>80</td> <td>60</td> <td>60</td> <td> </td> </tr>
    <tr> <td></td> <td>по переезду 357 км 10 пк головой поезда</td> <td>25</td> <td>25</td> <td>25</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    САНДОВО - ДЫНИНО</td> </tr>
    <tr> <td>I</td> <td>358 км 4 пк - 372 км 4 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>358 км 4 пк - 368 км 2 пк </td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    ДЫНИНО</td> </tr>
    <tr> <td>I</td> <td>372 км 4 пк - 374 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ДЫНИНО - ОВИНИЩЕ II</td> </tr>
    <tr> <td>I</td> <td>374 км 1 пк - 393 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td colSpan="6">    ОВИНИЩЕ II</td> </tr>
    <tr> <td>II</td> <td>393 км 1 пк - 7 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ОВИНИЩЕ II - ОВИНИЩЕ I</td> </tr>
    <tr> <td>I</td> <td>393 км 7 пк - 397 км 4 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ОВИНИЩЕ I</td> </tr>
    <tr> <td>III</td> <td>397 км 4 пк - 398 км 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 1</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
</React.Fragment>;