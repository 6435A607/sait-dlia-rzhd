import React from 'react';
import TableSpeed from './tablespeed.jsx';
import ButtonCollapseSpeed from './buttoncollapsespeed.jsx';

export default function SpeedSrednerogShushary(props) {
  let id = "srednerogshushary";
  return (
    <React.Fragment>
      <ButtonCollapseSpeed id={id} text="Среднерогатская - Шушары" />
      <div className="collapse" id={id} >
        <TableSpeed tbody={data} />
      </div>
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    СРЕДНЕРОГАТСКАЯ </td> </tr>
  <tr> <td>II</td> <td>1 км 1 пк - 2 км 8 пк</td> <td></td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>2 км 3 пк - 8 пк</td> <td></td> <td>25</td> <td>25</td> <td></td> </tr>
  <tr> <td>II Ш</td> <td>1 км 1 пк - 10 пк</td> <td></td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>съезды 3/5, 21/19, 6/4</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">    СРЕДНЕРОГАТСКАЯ - ШУШАРЫ  </td> </tr>
  <tr> <td>I</td> <td>2 км 1 пк - 6 пк</td> <td></td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td colSpan="6">    ШУШАРЫ </td> </tr>
  <tr> <td>I</td> <td>2 км 6 пк - 3 км 7 пк</td> <td></td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>3 км 2 пк - 3 км 7 пк</td> <td></td> <td>25</td> <td>25</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>съезд 125/127</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
</React.Fragment>;