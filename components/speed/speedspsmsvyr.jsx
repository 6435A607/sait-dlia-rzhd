import React from 'react';
import TableSpeed from './tablespeed.jsx';

export default function SpeedSPSMSvyr(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">СПСМ - Свирь</h4>
      <br />
      <TableSpeed tbody={data} />
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-СОРТ-МОСКОВСКИЙ  </td> </tr>
  <tr> <td>IКиров.</td> <td>10 км 3 пк - 13 км 4 пк  (парк «Обухово»                       1 Кировский)</td> <td>60</td> <td>60</td> <td>60</td> <td>40</td> </tr>
  <tr> <td></td> <td>12 км 1 пк стр.пер. № 4 </td> <td>40</td> <td>40</td> <td>40</td> <td></td>  </tr>
  <tr> <td>IIкиров</td> <td>12 км 3 пк - 7 пк (парк «Обухово»                                 2 Кировский)</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td>III</td> <td>Санкт-Петербург-Сортировочный-Московский (парк «Обухово»), 3 гл. путь                                    (3 Кировский)</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>IV</td> <td>Санкт-Петербург-Сортировочный-Московский (парк «Обухово»), 4 гл. путь                                 (4 Кировский)</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-СОРТ-МОСКОВСКИЙ   - РЫБАЦКОЕ </td> </tr>
  <tr> <td>I</td> <td>13 км 4 пк - 14 км 2 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>12 км 7 пк - 14 км 2 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>III</td> <td>Санкт-Петербург-Сортировочный-Московский (парк «Обухово») – Рыбацкое  3 гл. путь</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>IV</td> <td>Санкт-Петербург-Сортировочный-Московский (парк «Обухово») – Рыбацкое 4 гл. путь</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    РЫБАЦКОЕ </td> </tr>
  <tr> <td>I</td> <td>14 км 3 пк - 16 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>14 км 3 пк - 7 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>14 км 8 пк - 15 км 3 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>15 км 4 пк - 16 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>14 км 3 пк - 16 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>14 км 5 пк - 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>III К</td> <td>14 км 3 пк - 5 пк</td> <td>50</td> <td>50</td> <td>50</td> <td> </td> </tr>
  <tr> <td>III</td> <td>14 км 3 пк - 16 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>14 км 3 пк - 16 км 5 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>IV</td> <td>14 км 8 пк - 16 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>14 км 8 пк - 16 км 5 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>от стр.пер. № 40 (на ст.Славянку)</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 5, 9, 10, 11, 12</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
  <tr> <td colSpan="6">    РЫБАЦКОЕ - ИЖОРЫ </td> </tr>
  <tr> <td>I</td> <td>16 км 6 пк - 19 км 5 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td>II</td> <td>16 км 6 пк - 19 км 5 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>19 км 1 пк - 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>III</td> <td>16 км 10 пк - 19 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>IV</td> <td>16 км 7 пк - 19 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ИЖОРЫ </td> </tr>
  <tr> <td>I</td> <td>19 км 5 пк - 21 км 6 пк</td> <td>120</td> <td>90</td> <td>90</td> <td> </td> </tr>
  <tr> <td></td> <td>21 км 4 пк - 6 пк</td> <td>90</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>19 км 5 пк - 21 км 6 пк</td> <td>120</td> <td>90</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>19 км 5 пк - 8 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>20 км 10 пк - 21 км 3 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>21 км 4 пк - 6 пк кривые</td> <td>80</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td>III</td> <td>19 км 5 пк - 21 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>19 км 5 пк - 21 км 2 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td>IV</td> <td>19 км 2 пк - 20 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>19 км 6 пк - 20 км 8 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>20 км 9 пк - 10 пк</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>стр.пер. № 1, 5, 7, 9 бок. напр.</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ИЖОРЫ - САПЕРНАЯ </td> </tr>
  <tr> <td>I</td> <td>21 км 6 пк - 25 км 2 пк</td> <td>90</td> <td>90</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>21 км 6 пк - 22 км 2 пк </td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>22 км 3 пк - 9 пк кривые</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>23 км 4 пк – 25 км 2 пк </td> <td>90</td> <td>90</td> <td>70</td> <td></td> </tr>
  <tr> <td>II</td> <td>21 км 6 пк - 25 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>21 км 6 пк - 23 км 3 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    САПЕРНАЯ </td> </tr>
  <tr> <td>I</td> <td>25 км 2 пк - 28 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>25 км 2 пк - 26 км 5 пк</td> <td>90</td> <td>80</td> <td>70</td> <td> </td> </tr>
  <tr> <td>II</td> <td>25 км 2 пк - 27 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>25 км 7 пк - 26 км 3 пк кривые</td> <td>80</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 6, 7</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    САПЕРНАЯ - ПЕЛЛА </td> </tr>
  <tr> <td>I</td> <td>28 км 1 пк - 35 км 7 пк</td> <td>120</td> <td>90</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>31 км 1 пк - 10 пк</td> <td>90</td> <td>80</td> <td>70</td> <td></td> </tr>
  <tr> <td></td> <td>32 км 1 пк - 35 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>27 км 10 пк - 35 км 7 пк</td> <td>120</td> <td>90</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>27 км 10 пк - 29 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>31 км 1 пк - 32 км 7 пк кривые</td> <td>100</td> <td>80</td> <td>70</td> <td></td> </tr>
  <tr> <td></td> <td>33 км 9 пк - 34 км 6 пк кривые</td> <td>100</td> <td>80</td> <td>70</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПЕЛЛА </td> </tr>
  <tr> <td>I</td> <td>35 км 7 пк - 38 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>35 км 7 пк – 10 пк</td> <td>120</td> <td>80</td> <td>70</td> <td> </td> </tr>
  <tr> <td>II</td> <td>35 км 7 пк - 38 км 5 пк</td> <td>120</td> <td>90</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>36 км 1 пк – 38 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ПЕЛЛА - ГОРЫ </td> </tr>
  <tr> <td>I</td> <td>38 км 5 пк - 41 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>38 км 5 пк - 41 км 7 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>38 км 5 пк - 39 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ГОРЫ </td> </tr>
  <tr> <td>I</td> <td>41 км 9 пк - 44 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>41 км 7 пк - 44 км 2 пк</td> <td>120</td> <td>90</td> <td>90</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезд 17/19 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ГОРЫ - МГА </td> </tr>
  <tr> <td>I</td> <td>44 км 2 пк - 48 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>44 км 2 пк - 48 км 4 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>44 км 1 пк - 45 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    МГА </td> </tr>
  <tr> <td>I</td> <td>48 км 4 пк - 52 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>48 км 8 пк - 52 км 2 пк кривые</td> <td>80</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td>II</td> <td>48 км 4 пк - 52 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>13 путь (от стр.пер. № 130 до вх.сигн. ЧН через стр.пер. № 128, 76, 70, 68, 56, 16 и 10) на ст.Невдубстрой</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>III, IIIБГ, IIIГ гл. пути (от стр.пер. № 89/89с до стр.пер. № 8 (ст.Блокпост 4 км) через стр.пер. № 130, 106, 78, 54, 44, 30, 14 и 12) на ст.Войтоловка</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>IIIА гл.путь (от стр.пер. №27/27с через стр. пер №29/29с, 35, 79, 91/91с по стр.пер .№89/89с), съезд 27/27с-29/29с, стр.пер. № 89/89С</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>IVБГ, IVГ гл. пути (от стр.пер. № 48 до стр.пер. № 6 (ст.Блокпост 4 км) через стр.пер. № 36) на ст.Войтоловка</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>IVA гл. путь (от стр.пер. № 45 до вх.сигн. IVНБ через стр.пер. № 39, 23/23с, 13/13с и 1) на ст.Сологубовка</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 8</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>съезд 9/11 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td colSpan="6">    МГА - НАЗИЯ </td> </tr>
  <tr> <td>I</td> <td>52 км 7 пк - 66 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>52 км 7 пк - 66 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>53 км 7 пк - 54 км 4 пк кривые</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    НАЗИЯ </td> </tr>
  <tr> <td>I</td> <td>66 км 10 пк - 69 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>66 км 10 пк - 69 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    НАЗИЯ - ЖИХАРЕВО </td> </tr>
  <tr> <td>I</td> <td>69 км 5 пк - 79 км 7 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>69 км 5 пк - 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>69 км 5 пк - 79 км 7 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td> км 5 пк - 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ЖИХАРЕВО </td> </tr>
  <tr> <td>I</td> <td>79 км 7 пк - 81 км 7 пк</td> <td>120</td> <td>90</td> <td>90</td> <td> </td> </tr>
  <tr> <td></td> <td>80 км 1 пк - 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>79 км 7 пк - 81 км 7 пк</td> <td>120</td> <td>90</td> <td>90</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ЖИХАРЕВО - ВОЙБОКАЛО </td> </tr>
  <tr> <td>I</td> <td>81 км 7 пк - 90 км 7 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td>II</td> <td>81 км 7 пк - 90 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ВОЙБОКАЛО </td> </tr>
  <tr> <td>I</td> <td>90 км 7 пк - 92 км 10 пк</td> <td>120</td> <td>90</td> <td>90</td> <td> </td> </tr>
  <tr> <td></td> <td>90 км 8 пк - 91 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>91 км 1 пк - 92 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>90 км 7 пк - 92 км 7 пк</td> <td>120</td> <td>90</td> <td>90</td> <td> </td> </tr>
  <tr> <td>II</td> <td>92 км 3 пк - 92 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 5</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>съезды 2/4, 6/8, 1/3, 5/7 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ВОЙБОКАЛО - НОВЫЙ БЫТ </td> </tr>
  <tr> <td>I</td> <td>92 км 10 пк - 99 км 10 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td>II</td> <td>92 км 7 пк - 99 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>93 км 1 пк - 93 км 6 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td colSpan="6">    НОВЫЙ БЫТ </td> </tr>
  <tr> <td>I</td> <td>99 км 10 пк - 102 км 1 пк</td> <td>120</td> <td>90</td> <td>90</td> <td> </td> </tr>
  <tr> <td></td> <td>100 км 5 пк - 101 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>99 км 10 пк - 102 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 1/3, 6/8 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    НОВЫЙ БЫТ - ПУПЫШЕВО </td> </tr>
  <tr> <td>I</td> <td>102 км 1 пк - 109 км 3 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>108 км 7 пк - 109 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>102 км 1 пк - 109 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПУПЫШЕВО </td> </tr>
  <tr> <td>I</td> <td>109 км 3 пк - 112 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>109 км 3 пк - 112 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 2/4, 6/8, 1/3, 7/9 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ПУПЫШЕВО - БЛОКПОСТ 116 КМ </td> </tr>
  <tr> <td>I</td> <td>112 км 1 пк - 116 км 5 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td>II</td> <td>112 км 1 пк - 116 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    БЛОКПОСТ 116 КМ </td> </tr>
  <tr> <td>I</td> <td>116 км 5 пк - 8 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td>  </tr>
  <tr> <td>II</td> <td>116 км 6 пк - 117 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>съезды 2/4, 8/10, стр.пер. № 6, 14 бок. напр. для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    БЛОКПОСТ 116 КМ - ВОЛХОВСТРОЙ I </td> </tr>
  <tr> <td>I</td> <td>116 км 8 пк - 119 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>117 км 9 пк - 119 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ВОЛХОВСТРОЙ I </td> </tr>
  <tr> <td>II</td> <td>119 км 2 пк - 123 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>119 км 5 пк - 123 км 1 пк</td> <td>70</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>123 км 1 пк - 2 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>IА</td> <td>119 км 5 пк - 123 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>119 км 5 пк - 123 км 1 пк</td> <td>70</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>123 км 1 пк - 2 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>съезд 2/4 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td></td> <td>путь 3, 4, 5, 6, 7, 9, 10, 41, 22, 23</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ВОЛХОВСТРОЙ I - ВОЛХОВСТРОЙ II </td> </tr>
  <tr> <td>I</td> <td>123 км 2 пк - 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>123 км 2 пк - 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ВОЛХОВСТРОЙ II </td> </tr>
  <tr> <td>Iмп</td> <td>123 км 10 пк - 126 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>124 км 4 пк - 125 км 4 пк</td> <td>50</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>125 км 4 пк - 126 км 2 пк</td> <td>80</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>126 км 3 пк - 5 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td> </td> </tr>
  <tr> <td>IIмп</td> <td>123 км 10 пк - 126 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>124 км 2 пк - 125 км 3 пк</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>125 км 3 пк - 126 км 2 пк</td> <td>80</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>126 км 3 пк - 5 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>ст.Волховстрой II (Мурманский парк)</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезд 5/7 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td></td> <td>соединительный путь № 1</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ВОЛХОВСТРОЙ II - МУРМАНСКИЕ ВОРОТА </td> </tr>
  <tr> <td>I</td> <td>126 км 5 пк</td> <td>80</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>126 км 5 пк</td> <td>80</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    МУРМАНСКИЕ ВОРОТА </td> </tr>
  <tr> <td>I</td> <td>126 км 5 пк - 129 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>126 км 5 пк - 8 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>126 км 9 пк - 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>127 км 1 пк - 128 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>128 км 9 пк - 129 км 3 пк порожние грузовые вагоны</td> <td>100</td> <td>80</td> <td>60</td> <td> </td> </tr>
  <tr> <td>II</td> <td>126 км 5 пк - 129 км 3 пк</td> <td>120</td> <td>90</td> <td>90</td> <td> </td> </tr>
  <tr> <td></td> <td>126 км 5 пк - 8 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>126 км 9 пк - 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>127 км 1 пк - 128 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>128 км 9 пк - 129 км 3 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 6, 7</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    МУРМАНСКИЕ ВОРОТА - КОЛЧАНОВО </td> </tr>
  <tr> <td>I</td> <td>129 км 3 пк - 145 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>129 км 3 пк - 5 пк порожние грузовые вагоны</td> <td>100</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>130 км 7 пк - 131 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>135 км 7 пк - 137 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>142 км 1 пк - 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>143 км 1 пк - 144 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>144 км 4 пк - 145 км 1 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>129 км 3 пк - 145 км 2 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>129 км 3 пк - 5 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>136 км 1 пк - 137 км 1 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>142 км 1 пк - 10 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>143 км 1 пк - 145 км 2 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    КОЛЧАНОВО </td> </tr>
  <tr> <td>I</td> <td>145 км 1 пк - 146 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>145 км 1 пк - 2 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>145 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>145 км 2 пк - 146 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>145 км 2 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>145 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 2/4, 1/3 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    КОЛЧАНОВО - ЛУНГАЧИ</td> </tr>
  <tr> <td>I</td> <td>146 км 10 пк - 153 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>146 км 10 пк - 153 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ЛУНГАЧИ </td> </tr>
  <tr> <td>I</td> <td>153 км 10 пк - 155 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>153 км 10 пк - 155 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 2/4, 6/8, 1/3, 13/15 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ЛУНГАЧИ - ЮГИ </td> </tr>
  <tr> <td>I</td> <td>155 км 10 пк - 170 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>155 км 10 пк - 170 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ЮГИ</td> </tr>
  <tr> <td>I</td> <td>170 км 3 пк - 171 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>170 км 3 пк - 171 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 3</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>съезды 2/4, 1/3 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ЮГИ - ПАША </td> </tr>
  <tr> <td>I</td> <td>171 км 10 пк - 193 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>187 км 6 пк - 188 км 8 пк порожние грузовые вагоны</td> <td>100</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>171 км 10 пк - 193 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>184 км 9 пк - 185 км 6 пк порожние грузовые вагоны</td> <td>100</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>186 км 5 пк - 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>187 км 6 пк - 188 км 8 пк порожние грузовые вагоны</td> <td>100</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПАША </td> </tr>
  <tr> <td>I</td> <td>193 км 10 пк - 195 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>195 км 7 пк - 9 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>193 км 10 пк - 195 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезд 2/4 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ПАША - ОЯТЬ-ВОЛХОВСТРОЕВСКИЙ </td> </tr>
  <tr> <td>I</td> <td>195 км 9 пк - 202 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>195 км 9 пк - 197 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>195 км 9 пк - 202 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>196 км 2 пк - 5 пк порожние грузовые вагоны</td> <td>100</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    ОЯТЬ-ВОЛХОВСТРОЕВСКИЙ </td> </tr>
  <tr> <td>I</td> <td>202 км 7 пк - 204 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>203 км 1 пк - 204 км 4 пк кривые</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>204 км 4 пк - 7 пк кривые</td> <td>90</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>202 км 7 пк - 204 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>203 км 2 пк - 204 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>стр.пер. № 9 бок. напр. для пасс. поездов</td> <td></td> <td> </td> <td></td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ОЯТЬ-ВОЛХОВСТРОЕВСКИЙ - ЗАОСТРОВЬЕ </td> </tr>
  <tr> <td>I</td> <td>204 км 7 пк - 223 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>206 км 3 пк - 206 км 7 пк кривые</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>206 км 9 пк кривые</td> <td>110</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>219 км 10 пк - 220 км 7 пк кривые</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>204 км 7 пк - 223 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>204 км 7 пк - 206 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>216 км 6 пк - 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>219 км 10 пк - 221 км 8 пк кривые</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ЗАОСТРОВЬЕ </td> </tr>
  <tr> <td>I</td> <td>223 км 10 пк - 225 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>224 км 4 пк - 225 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>223 км 10 пк - 225 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>224 км 1 пк - 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>224 км 3 пк - 225 км 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>225 км 6 пк - 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 1/3, 2/4, стр.пер. № 7 бок. напр. для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ЗАОСТРОВЬЕ - ЛОДЕЙНОЕ ПОЛЕ </td> </tr>
  <tr> <td>I</td> <td>225 км 10 пк - 242 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>225 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>230 км 7 пк - 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>239 км 6 пк - 240 км 9 пк кривые</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>240 км 10 пк - 242 км 2 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>242 км 3 пк - 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>225 км 10 пк - 242 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>225 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>229 км 5 пк - 230 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>239 км 6 пк - 240 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>240 км 10 пк - 241 км 5 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>241 км 6 пк - 242 км 2 пк</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>242 км 2 пк - 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ЛОДЕЙНОЕ ПОЛЕ </td> </tr>
  <tr> <td>I</td> <td>242 км 4 пк - 244 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>242 км 6 пк - 244 км 4 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td>II</td> <td>242 км 4 пк - 244 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>242 км 7 пк - 244 км 5 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>3 гл. путь</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ЛОДЕЙНОЕ ПОЛЕ - ЯНЕГА </td> </tr>
  <tr> <td>I</td> <td>244 км 8 пк - 251 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>244 км 6 пк - 251 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ЯНЕГА </td> </tr>
  <tr> <td>I</td> <td>251 км 9 пк - 253 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>252 км 2 пк - 253 км 7 пк кривые</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>251 км 9 пк - 253 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>252 км 3 пк - 253 км 6 пк кривые</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезд 2/4, стр.пер. № 13 бок. напр. для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ЯНЕГА - ЯНДЕБА </td> </tr>
  <tr> <td>I</td> <td>253 км 10 пк - 270 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>253 км 9 пк - 270 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ЯНДЕБА</td> </tr>
  <tr> <td>I</td> <td>270 км 7 пк - 272 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>271 км 1 пк - 272 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>270 км 7 пк - 272 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>270 км 10 пк - 272 км 5 пк </td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезд 1/3, стр.пер. № 5 бок. напр. для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ЯНДЕБА - ПОДПОРОЖЬЕ </td> </tr>
  <tr> <td>I</td> <td>272 км 5 пк - 279 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>272 км 5 пк - 10 пк кривые</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>279 км 1 пк - 8 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>272 км 5 пк - 279 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>272 км 5 пк - 10 пк </td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>279 км 1 пк - 8 пк </td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПОДПОРОЖЬЕ</td> </tr>
  <tr> <td>I</td> <td>279 км 8 пк - 281 км 8 пк</td> <td>100</td> <td>80</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>279 км 8 пк - 280 км 1 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>280 км 1 пк - 281 км 6 пк</td> <td>80</td> <td>70</td> <td>70</td> <td> </td> </tr>
  <tr> <td></td> <td>281 км 6 пк - 8 пк порожние грузовые вагоны</td> <td>80</td> <td>70</td> <td>60</td> <td> </td> </tr>
  <tr> <td>II</td> <td>279 км 8 пк - 281 км 8 пк</td> <td>90</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>279 км 8 пк - 281 км 5 пк </td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>281 км 6 пк - 8 пк порожние грузовые вагоны</td> <td>80</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезд 5/7, стр.пер. № 12 бок. напр. для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ПОДПОРОЖЬЕ - БЛОКПОСТ 284 КМ </td> </tr>
  <tr> <td>I</td> <td>281 км 8 пк - 284 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>281 км 8 пк - 9 пк порожние грузовые вагоны</td> <td>80</td> <td>70</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>282 км 7 пк - 284 км 2 пк </td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>284 км 2 пк - 3 пк стрелочный перевод</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>281 км 8 пк - 284 км 3 пк</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>281 км 8 пк - 9 пк порожние грузовые вагоны</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>281 км 10 пк - 283 км 6 пк порожние грузовые вагоны</td> <td>90</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>283 км 7 пк - 284 км 3 пк порожние грузовые вагоны</td> <td>90</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    БЛОКПОСТ 284 КМ </td> </tr>
  <tr> <td>I</td> <td>284 км 3 пк - 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>284 км 3 пк - 4 пк стрелочный перевод</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td>II</td> <td>284 км 3 пк - 4 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td colSpan="6">    БЛОКПОСТ 284 КМ - СВИРЬ </td> </tr>
  <tr> <td>I</td> <td>284 км 5 пк - 285 км 2 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    СВИРЬ </td> </tr>
  <tr> <td>I</td> <td>285 км 2 пк - 290 км 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>287 км 2 пк - 290 км 4 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td>II</td> <td>285 км 8 пк - 290 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>285 км 8 пк - 287 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>287 км 2 пк - 290 км 4 пк</td> <td>80</td> <td>60</td> <td>60</td> <td> </td> </tr>

  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
</React.Fragment>;