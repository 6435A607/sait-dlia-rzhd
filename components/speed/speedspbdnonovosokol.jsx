import React from 'react';
import TableSpeed from './tablespeed.jsx';

export default function SpeedSPbDnoNovosokol(props) {
    return (
        <React.Fragment>
          <h4 className="text-center">СПб - Дно - Новосокольники</h4>
          <br />
          <TableSpeed tbody = {data} />
        </React.Fragment>
      );
}

const data = <React.Fragment>
    <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-ВИТЕБСКИЙ</td> </tr>
    <tr> <td>I</td> <td>1 км 2 пк - 3 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>1 км 2 пк - 7 пк</td> <td>25</td> <td>25</td> <td>25</td> <td> </td> </tr>
    <tr> <td></td> <td>1 км 7 пк - 3 км 1 пк</td> <td>50</td> <td>50</td> <td>50</td> <td> </td> </tr>
    <tr> <td>II</td> <td>1 км 1 пк - 3 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>1 км 1 пк - 7 пк</td> <td>25</td> <td>25</td> <td>25</td> <td></td> </tr>
    <tr> <td></td> <td>1 км 7 пк - 3 км 2 пк</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
    <tr> <td>III</td> <td>1 км 2 пк - 2 км 10 пк от моста                    «Обводный канал» до выходного светофора «Ч» </td> <td>70</td> <td>70</td> <td>70</td> <td> </td> </tr>
    <tr> <td></td> <td>1 км 2 пк - 7 пк</td> <td>25</td> <td>25</td> <td>25</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td></td> <td>приём и отправление со всех перронных путей до моста «Обводный канал»</td> <td>25</td> <td>25</td> <td>25</td> <td>25</td> </tr>
    <tr> <td></td> <td>стр.пер. № 53</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td></td> <td>съезды 43/44, 101/103</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
    <tr> <td></td> <td>съезды 12/13, 54/55, 3/4</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td>I</td> <td>3 км 4 пк - 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>3 км 5 пк - 8 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-ТОВ-ВИТЕБСКИЙ</td> </tr>
    <tr> <td>III</td> <td>6 км 5 пк - 7 км 2 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>IIг</td> <td>3 км 8 пк - 7 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>4 км 1 пк - 2 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
    <tr> <td></td> <td>4 км 3пк - 7 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по переезду 4 км пк 8</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>4 км 9 пк - 6 км 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td>Iгр</td> <td>1 км 1 пк - 7 пк</td> <td>25</td> <td>25</td> <td>25</td> <td> </td> </tr>
    <tr> <td>Iгл</td> <td>3 км 8 пк - 7 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>4 км 1 пк - 3 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
    <tr> <td></td> <td>4 км 3 пк - 8 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по переезду 4 км пк 8</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>4 км 10 пк - 6 км 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td></td> <td>от стр.пер. № 13 до стр.пер. № 8 через стр.пер. № 19, 11, 27, 33, 24, 20, 18 и 10</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td></td> <td>от стр.пер. № 17 до стр.пер. № 27 через стр.пер. № 21, 23 и 25</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td></td> <td>съезды 1/3, 2/4, 6/8</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-ТОВ-ВИТЕБСКИЙ - ШУШАРЫ</td> </tr>
    <tr> <td>I</td> <td>7 км 2 пк - 11 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>7 км 2 пк - 11 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>III</td> <td>7 км 2 пк - 13 км 9 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ШУШАРЫ</td> </tr>
    <tr> <td>I</td> <td>11 км 9 пк - 19 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>11 км 9 пк - 16 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>11 км 9 пк - 19 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>14 км 3 пк - 19 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>III</td> <td>13 км 10 пк - 14 км 7 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>25</td> <td>1 км 1 пк - 4 км 10 пк</td> <td> </td> <td>25</td> <td>25</td> <td> </td> </tr>
    <tr> <td>Iгл</td> <td>1 км 1 пк - 5 пк</td> <td> </td> <td>25</td> <td>25</td> <td> </td> </tr>
    <tr> <td></td> <td>а) выход из 1 парка через стр.пер. № 122, 120, 116, 114, 110, 108, 106 и 104 до стр.пер. № 102</td> <td>25</td> <td>25</td> <td>25</td> <td></td> </tr>
    <tr> <td></td> <td>б) выход из 4 парка по пути № 112 на ст.Среднерогатская через стр.пер. № 123, 117, 115, 109 и 107</td> <td>25</td> <td>25</td> <td>25</td> <td>15</td> </tr>
    <tr> <td></td> <td>в) выход из 4 парка по пути № 112 на III гл. путь через стр.пер. № 123, 117, 115, 105 и 103</td> <td>25</td> <td>25</td> <td>25</td> <td>15</td> </tr>
    <tr> <td></td> <td>г) выход из 4 парка по пути № 113 на ст.Среднерогатская через стр.пер. № 121, 113, 111 и 109</td> <td>25</td> <td>25</td> <td>25</td> <td>15</td> </tr>
    <tr> <td></td> <td>д) выход из 4 парка по пути № 113 на ст.Купчинская через стр.пер. № 121, 113 и 111</td> <td>25</td> <td>25</td> <td>25</td> <td>15</td> </tr>
    <tr> <td></td> <td>е) выход в 1 парк через стр.пер. № 111, 113, 115, 123, 127, 131, 139, 143 и далее по пути № 15</td> <td>15</td> <td>15</td> <td>15</td> <td>15</td> </tr>
    <tr> <td></td> <td>ж) вход во 2 парк от стр.пер. № 2 через стр.пер. № 4 до стр.пер. № 6</td> <td>40</td> <td>40</td> <td>40</td> <td>40</td> </tr>
    <tr> <td></td> <td>з) путь № 114</td> <td>25</td> <td>25</td> <td>25</td> <td>15</td> </tr>
    <tr> <td></td> <td>и) соединительный путь № 25</td> <td>25</td> <td>25</td> <td>25</td> <td></td> </tr>
    <tr> <td></td> <td>в направлении поста 19 км</td> <td>25</td> <td>25</td> <td>25</td> <td></td> </tr>
    <tr> <td></td> <td>в направлении поста 16 км</td> <td>25</td> <td>25</td> <td>25</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td></td> <td>съезд 125/127</td> <td> </td> <td> </td> <td> </td> <td>15</td> </tr>
    <tr> <td colSpan="6">    ШУШАРЫ - ЦАРСКОЕ СЕЛО</td> </tr>
    <tr> <td>I</td> <td>19 км 7 пк - 21 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>19 км 10 пк - 20 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ЦАРСКОЕ СЕЛО</td> </tr>
    <tr> <td>I</td> <td>21 км 9 пк - 24 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>22 км 3 пк - 23 км 9 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>23 км 10 пк - 24 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td>II</td> <td>20 км 9 пк - 23 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>22 км 2 пк - 23 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">    ЦАРСКОЕ СЕЛО - ПАВЛОВСК</td> </tr>
    <tr> <td>I</td> <td>24 км 5 пк - 26 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>23 км 5 пк - 26 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>23 км 5 пк - 24 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ПАВЛОВСК</td> </tr>
    <tr> <td>I</td> <td>26 км 3 пк - 28 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>26 км 4 пк - 9 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>26 км 10 пк - 28 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>26 км 2 пк - 28 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>26 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>26 км 4 пк - 9 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
    <tr> <td></td> <td>26 км 10 пк - 28 км 2 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>28 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td></td> <td>съезды 1/3, 6/8</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
    <tr> <td colSpan="6">    ПАВЛОВСК - АНТРОПШИНО</td> </tr>
    <tr> <td>I</td> <td>28 км 1 пк - 32 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>28 км 2 пк - 32 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>28 км 2 пк - 30 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    АНТРОПШИНО</td> </tr>
    <tr> <td>I</td> <td>32 км 7 пк - 34 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>32 км 10 пк - 34 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td>II</td> <td>32 км 7 пк - 34 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>32 км 9 пк - 34 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>34 км 3 пк - 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    АНТРОПШИНО - КОБРАЛОВО</td> </tr>
    <tr> <td>I</td> <td>34 км 4 пк - 38 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>34 км 7 пк - 38 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>34 км 7 пк - 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>34 км 9 пк - 10 пк дефектность ИССО</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    КОБРАЛОВО</td> </tr>
    <tr> <td>I</td> <td>38 км 6 пк - 41 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>38 км 10 пк - 40 км 8 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td>  </tr>
    <tr> <td>II</td> <td>38 км 6 пк - 41 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>38 км 10 пк - 40 км 8 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>съезд 6/8 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
    <tr> <td colSpan="6">    КОБРАЛОВО - СЕМРИНО</td> </tr>
    <tr> <td>I</td> <td>41 км 3 пк - 44 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>41 км 3 пк - 44 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    СЕМРИНО</td> </tr>
    <tr> <td>I</td> <td>44 км 3 пк - 47 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>44 км 6 пк - 45 км 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>44 км 3 пк - 47 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>44 км 6 пк - 46 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>съезды 2/4, 5/7 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
    <tr> <td colSpan="6">    СЕМРИНО - ВЫРИЦА</td> </tr>
    <tr> <td>I</td> <td>47 км 9 пк - 58 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>55 км 3 пк - 8 пк кривые</td> <td>110</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>47 км 9 пк - 58 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ВЫРИЦА</td> </tr>
    <tr> <td>II</td> <td>58 км 10 пк - 61 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>59 км 3 пк - 61 км 3 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
    <tr> <td>V</td> <td>58 км 10 пк - 61 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по переезду 59 км пк 1 </td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>59 км 3 пк - 61 км 2 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ВЫРИЦА - НОВИНКА</td> </tr>
    <tr> <td>I</td> <td>61 км 7 пк - 84 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>61 км 7 пк - 84 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    НОВИНКА</td> </tr>
    <tr> <td>I</td> <td>84 км 3 пк - 86 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>84 км 6 пк - 85 км 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td>II</td> <td>84 км 3 пк - 85 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>84 км 5 пк - 85 км 10 пк</td> <td>70</td> <td>70</td> <td>70</td> <td> </td> </tr>
    <tr> <td></td> <td>стр.пер. № 6 бок. напр. (85 км пк 9 – 86 км               пк 1)</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    НОВИНКА - ЧАЩА</td> </tr>
    <tr> <td>I</td> <td>86 км 4 пк - 96 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ЧАЩА</td> </tr>
    <tr> <td>I</td> <td>96 км 1 пк - 98 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>96 км 7 пк - 97 км 9 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ЧАЩА - ЧОЛОВО</td> </tr>
    <tr> <td>I</td> <td>98 км 2 пк - 109 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ЧОЛОВО</td> </tr>
    <tr> <td>I</td> <td>109 км 10 пк - 111 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>110 км 4 пк - 111 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ЧОЛОВО - ТАРКОВИЧИ</td> </tr>
    <tr> <td>I</td> <td>111 км 10 пк - 121 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>114 км 10 пк - 117 км 7 пк кривые</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ТАРКОВИЧИ</td> </tr>
    <tr> <td>I</td> <td>121 км 7 пк - 123 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>121 км 10 пк - 123 км 2 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ТАРКОВИЧИ - ОРЕДЕЖ</td> </tr>
    <tr> <td>I</td> <td>123 км 4 пк - 127 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>124 км 6 пк - 125 км 7 пк кривые</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>126 км 2 пк - 127 км 7 пк кривые</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ОРЕДЕЖ</td> </tr>
    <tr> <td>II</td> <td>127 км 7 пк - 132 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>127 км 7 пк - 9 пк кривые</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>129 км 1 пк - 130 км 7 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">    ОРЕДЕЖ - ЗАКЛИНЬЕ</td> </tr>
    <tr> <td>I</td> <td>132 км 4 пк - 136 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ЗАКЛИНЬЕ</td> </tr>
    <tr> <td>I</td> <td>136 км 7 пк - 138 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>137 км 10 пк - 138 км 9 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ЗАКЛИНЬЕ - БАТЕЦКАЯ</td> </tr>
    <tr> <td>I</td> <td>138 км 9 пк - 147 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    БАТЕЦКАЯ</td> </tr>
    <tr> <td>I</td> <td>147 км 1 пк - 149 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>147 км 5 пк - 149 км 1 пк</td> <td>70</td> <td>70</td> <td>70</td> <td> </td> </tr>
    <tr> <td></td> <td>по переезду 149 км пк 2</td> <td>70</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>149 км 3 пк - 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 6</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">    БАТЕЦКАЯ - БАХАРЕВО</td> </tr>
    <tr> <td>I</td> <td>149 км 5 пк - 156 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    БАХАРЕВО</td> </tr>
    <tr> <td>I</td> <td>156 км 5 пк - 158 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    БАХАРЕВО - ПЕРЕДОЛЬСКАЯ</td> </tr>
    <tr> <td>I</td> <td>158 км 5 пк - 166 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ПЕРЕДОЛЬСКАЯ</td> </tr>
    <tr> <td>I</td> <td>166 км 7 пк - 168 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>167 км 1 пк - 5 пк стрелочные переводы</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ПЕРЕДОЛЬСКАЯ - КЧЕРЫ</td> </tr>
    <tr> <td>I</td> <td>168 км 4 пк - 177 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    КЧЕРЫ</td> </tr>
    <tr> <td>I</td> <td>177 км 8 пк - 179 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    КЧЕРЫ - УТОРГОШ</td> </tr>
    <tr> <td>I</td> <td>179 км 8 пк - 188 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    УТОРГОШ</td> </tr>
    <tr> <td>I</td> <td>188 км 5 пк - 190 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    УТОРГОШ - СОЛЬЦЫ</td> </tr>
    <tr> <td>I</td> <td>190 км 5 пк - 205 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    СОЛЬЦЫ</td> </tr>
    <tr> <td>I</td> <td>205 км 5 пк - 208 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    СОЛЬЦЫ - КУКЛИНО</td> </tr>
    <tr> <td>I</td> <td>208 км 1 пк - 215 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    КУКЛИНО</td> </tr>
    <tr> <td>I</td> <td>215 км 1 пк - 217 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    КУКЛИНО - ЛЕМЕНКА</td> </tr>
    <tr> <td>I</td> <td>217 км 1 пк - 224 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ЛЕМЕНКА</td> </tr>
    <tr> <td>I</td> <td>224 км 10 пк - 227 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>224 км 10 пк - 226 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 3</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">    ЛЕМЕНКА - ГАЧКИ</td> </tr>
    <tr> <td>I</td> <td>227 км 1 пк - 234 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ГАЧКИ</td> </tr>
    <tr> <td>I</td> <td>234 км 10 пк - 236 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>235 км 1 пк - 236 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ГАЧКИ - ОСТ.ПУНКТ 241 КМ</td> </tr>
    <tr> <td>I</td> <td>236 км 7 пк - 240 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ОСТ.ПУНКТ 241 КМ</td> </tr>
    <tr> <td>I</td> <td>240 км 3 пк - 242 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>240 км 7 пк - 242 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td>II</td> <td>241 км 10 пк - 242 км 1 пк</td> <td>50</td> <td>50</td> <td>50</td> <td> </td> </tr>
    <tr> <td></td> <td>241 км 10 пк стр.пер. № 4 бок. напр.</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ОСТ.ПУНКТ 241 КМ - ДНО</td> </tr>
    <tr> <td>I</td> <td>242 км 1 пк - 244 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>243 км 4 пк - 244 км 1 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td>СВ</td> <td>242 км 1 пк - 243 км 10 пк ост.пункт 241 км – Дно, соединительная ветвь</td> <td>50 </td> <td>50 </td> <td>50 </td> <td></td> </tr>
    <tr> <td colSpan="6">    ДНО</td> </tr>
    <tr> <td>I</td> <td>244 км 1 пк - 248 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td>
    </td> </tr>
    <tr> <td></td> <td>244 км 1 пк - 2 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td>
    </td> </tr>
    <tr> <td></td> <td>244 км 2 пк - 245 км 8 пк</td> <td>50</td> <td>50</td> <td>50</td> <td>
    </td> </tr>
    <tr> <td></td> <td>245 км 8 пк</td> <td>40</td> <td>40</td> <td>40</td> <td>
    </td> </tr>
    <tr> <td></td> <td>245 км 8 пк - 246 км 7 пк</td> <td>50</td> <td>50</td> <td>50</td> <td>
    </td> </tr>
    <tr> <td></td> <td>246 км 7 пк стрелочный перевод</td> <td>40</td> <td>40</td> <td>40</td> <td>
    </td> </tr>
    <tr> <td></td> <td>246 км 8 пк</td> <td>50</td> <td>50</td> <td>50</td> <td>
    </td> </tr>
    <tr> <td></td> <td>стр.пер. № 2/4</td> <td>25</td> <td>25</td> <td>25</td> <td>
    </td> </tr>
    <tr> <td></td> <td>от стр.пер. № 146 до стр.пер. № 2/4</td> <td>60</td> <td>60</td> <td>60</td> <td>
    </td> </tr>
    <tr> <td></td> <td>247 км 8 пк - 10 пк кривые</td> <td>80</td> <td>80</td> <td>80</td> <td>
    </td> </tr>
    <tr> <td></td> <td>стр.пер. № 6, 87 бок. напр.</td> <td>40</td> <td>40</td> <td>40</td> <td>
    </td> </tr>
    <tr> <td></td> <td>стр.пер. № 146</td> <td>40</td> <td>40</td> <td>40</td> <td>
    </td> </tr>
    <tr> <td></td> <td>пути № 100, 13 (от стр.пер. № 263 (искл) до стр.пер. № 122 (искл))</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td></td> <td>путь № 34</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
    <tr> <td>II</td> <td>243 км 10 пк - 244 км 1 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">      Парк Дно-1 Д</td> </tr>
    <tr> <td></td> <td>путь 13, 100</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ДНО - ВЯЗЬЕ</td> </tr>
    <tr> <td>I</td> <td>248 км 5 пк - 256 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ВЯЗЬЕ</td> </tr>
    <tr> <td>I</td> <td>256 км 1 пк - 257 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ВЯЗЬЕ - ДЕДОВИЧИ</td> </tr>
    <tr> <td>I</td> <td>257 км 9 пк - 276 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ДЕДОВИЧИ</td> </tr>
    <tr> <td>I</td> <td>276 км 4 пк - 278 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ДЕДОВИЧИ - СУДОМА</td> </tr>
    <tr> <td>I</td> <td>278 км 3 пк - 286 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>278 км 5 пк - 279 км 3 пк порожние грузовые вагоны</td> <td>100</td> <td>80</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>281 км 6 пк - 10 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>282 км 4 пк - 283 км 6 пк порожние грузовые вагоны</td> <td> 100</td> <td>80</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    СУДОМА</td> </tr>
    <tr> <td>I</td> <td>286 км 5 пк - 288 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td></td> <td>путь 3</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    СУДОМА - ПЛОТОВЕЦ</td> </tr>
    <tr> <td>I</td> <td>288 км 5 пк - 296 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ПЛОТОВЕЦ</td> </tr>
    <tr> <td>I</td> <td>296 км 10 пк - 299 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ПЛОТОВЕЦ - ЧИХАЧЕВО</td> </tr>
    <tr> <td>I</td> <td>299 км 1 пк - 308 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ЧИХАЧЕВО</td> </tr>
    <tr> <td>I</td> <td>308 км 2 пк - 309 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>309 км 6 пк - 7 пк стрелочный перевод</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 3</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">    ЧИХАЧЕВО - АШЕВО</td> </tr>
    <tr> <td>I</td> <td>309 км 10 пк - 318 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    АШЕВО</td> </tr>
    <tr> <td>I</td> <td>318 км 6 пк - 320 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    АШЕВО - ЛОЗОВИЦЫ</td> </tr>
    <tr> <td>I</td> <td>320 км 4 пк - 328 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ЛОЗОВИЦЫ</td> </tr>
    <tr> <td>I</td> <td>328 км 6 пк - 330 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ЛОЗОВИЦЫ - СУЩЕВО</td> </tr>
    <tr> <td>I</td> <td>330 км 6 пк - 340 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    СУЩЕВО</td> </tr>
    <tr> <td>II</td> <td>340 км 1 пк - 342 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>341 км 7 пк - 8 пк </td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td></td> <td>путь 3</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    СУЩЕВО - ЗАГОСКИНО</td> </tr>
    <tr> <td>I</td> <td>342 км 2 пк - 349 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>347 км 1 пк - 348 км 1 пк порожние грузовые вагоны</td> <td>90</td> <td>80</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    ЗАГОСКИНО</td> </tr>
    <tr> <td>I</td> <td>349 км 2 пк - 351 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>349 км 6 пк стрелочный перевод</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>стр.пер. № 3 бок. напр.</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ЗАГОСКИНО - ЛОКНЯ</td> </tr>
    <tr> <td>I</td> <td>351 км 1 пк - 364 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ЛОКНЯ</td> </tr>
    <tr> <td>I</td> <td>364 км 3 пк - 367 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>366 км 7 пк - 8 пк стрелочный перевод</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ЛОКНЯ - СТРИМОВИЧИ</td> </tr>
    <tr> <td>I</td> <td>367 км 3 пк - 374 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    СТРИМОВИЧИ</td> </tr>
    <tr> <td>I</td> <td>374 км 9 пк - 376 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>375 км 4 пк - 376 км 5 пк кривые</td> <td>70</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 3</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">    СТРИМОВИЧИ - САМОЛУКОВО</td> </tr>
    <tr> <td>I</td> <td>376 км 10 пк - 385 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    САМОЛУКОВО</td> </tr>
    <tr> <td>I</td> <td>385 км 2 пк - 387 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    САМОЛУКОВО - КИСЕЛЕВИЧИ</td> </tr>
    <tr> <td>I</td> <td>387 км 1 пк - 402 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    КИСЕЛЕВИЧИ</td> </tr>
    <tr> <td>I</td> <td>402 км 8 пк - 404 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 3</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">    КИСЕЛЕВИЧИ - ШУБИНО</td> </tr>
    <tr> <td>I</td> <td>404 км 6 пк - 412 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ШУБИНО</td> </tr>
    <tr> <td>I</td> <td>412 км 5 пк - 414 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>412 км 9 пк - 413 км 1 пк стрелочные переводы</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 3</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">    ШУБИНО - НОВОСОКОЛЬНИКИ</td> </tr>
    <tr> <td>I</td> <td>414 км 6 пк - 421 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>420 км 9 пк - 421 км 2 пк кривые</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    НОВОСОКОЛЬНИКИ</td> </tr>
    <tr> <td>IIВ</td> <td>421 км 2 пк - 424 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>421 км 2 пк - 3 пк кривые</td> <td>90</td> <td>80</td> <td>80</td> <td> </td> </tr>
    <tr> <td></td> <td>421 км 10 пк - 424 км 2 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
    <tr> <td></td> <td>424 км 4 пк - 8 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td> </td> </tr>
    <tr> <td></td> <td>парк «Л»</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>парк «Л» 421 км пк 4-9</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td></td> <td>парк «В»</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>пути № 1С, 2С, 3В, 3Л</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td></td> <td>Ветвь № 1 (от стр.пер. № 43 до стр.пер № 8)</td> <td></td> <td></td> <td></td> <td>30</td> </tr>
    <tr> <td></td> <td>Ветвь № 2 (от стр.пер. № 11 до стр.пер. № 30)</td> <td></td> <td></td> <td></td> <td>30</td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
    <tr> <td></td> <td>пути 1С, 2С, 3В, 3Л</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
</React.Fragment>;