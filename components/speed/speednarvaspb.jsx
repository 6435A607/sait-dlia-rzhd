import React from 'react';
import TableSpeed from './tablespeed.jsx';
import ButtonCollapseSpeed from './buttoncollapsespeed.jsx';

export default function SpeedNarvaSPb(props) {
  let id = "narvaspb";
  return (
    <React.Fragment>
      <ButtonCollapseSpeed id={id} text="Нарвская - СПб-Балтийский" />
      <div className="collapse" id={id}>
        <TableSpeed tbody={data} />
      </div>
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    НАРВСКАЯ </td> </tr>
  <tr> <td>V</td> <td>1 км 1 пк - 2 км 4 пк</td> <td></td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td colSpan="6">    НАРВСКАЯ - САНКТ-ПЕТЕРБУРГ-БАЛТИЙСКИЙ </td> </tr>
  <tr> <td>I</td> <td>2 км 4 пк - 3 км 3 пк</td> <td></td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-БАЛТИЙСКИЙ </td> </tr>
  <tr> <td>IV</td> <td>3 км 3 пк - 7 пк</td> <td></td> <td>50</td> <td>50</td> <td></td> </tr>
</React.Fragment>;