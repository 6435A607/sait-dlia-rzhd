import React from 'react';
import TableSpeed from './tablespeed.jsx';

export default function SpeedVyritsaPoselok(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">Вырица - Поселок</h4>
      <br />
      <TableSpeed tbody={data} />
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    ВЫРИЦА </td> </tr>
  <tr> <td>I</td> <td>2 км 3 пк - 3 км 3 пк</td> <td>80</td> <td>80</td> <td>80 </td>  <td></td></tr>
  <tr> <td></td> <td>2 км 3 пк - 8 пк кривые</td> <td>50</td> <td>50</td> <td>50 </td> <td></td></tr>
  <tr> <td></td> <td>3 км 1 пк - 3 пк  порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60 </td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам   </td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ВЫРИЦА - ПОСЕЛОК </td> </tr>
  <tr> <td>I</td> <td>3 км 3 пк - 7 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>6 км 4 пк - 9 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПОСЕЛОК </td> </tr>
  <tr> <td>II</td> <td>7 км 3 пк - 10 пк</td> <td>80</td> <td>80</td> <td>80 </td> <td></td> </tr>
  <tr> <td></td> <td>7 км 6 пк - 10 пк</td> <td>25</td> <td>25</td> <td>25 </td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям   </td> <td></td> <td></td> <td></td> <td>15</td> </tr>
</React.Fragment>;