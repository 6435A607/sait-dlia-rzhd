import React from 'react';

export default function TableSpeed(props) {
  return (
    <React.Fragment>
      <table className="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <td rowSpan="2">Путь</td>
            <td rowSpan="2">Наименование участков, перегонов, станций и путей</td>
            <td colSpan="4">Допускаемая скорость в км/ч</td>
          </tr>
          <tr>
            <td>пассажирские</td>
            <td>грузовые</td>
            <td>грузовые с порожними вагонами</td>
            <td>по приемо-отправочным путям и стрелочным переводам</td>
          </tr>
        </thead>
        <tbody>
          {props.tbody}
        </tbody>
      </table>
    </React.Fragment>
  );
}