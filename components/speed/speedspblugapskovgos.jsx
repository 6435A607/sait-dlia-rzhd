import React from 'react';
import TableSpeed from './tablespeed.jsx';

export default function SpeedSpbLugaPskovGos(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">СПб - Луга I - Псков - Госграница</h4>
      <br />
      <TableSpeed tbody={data} />
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    БРОНЕВАЯ </td> </tr>
  <tr> <td>IIВ</td> <td>3 км 5 пк - 4 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>3 км 5 пк - 9 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>3 км 9 пк - 4 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>IВ</td> <td>3 км 5 пк - 4 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>3 км 5 пк - 4 км 2 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    БРОНЕВАЯ - ПРЕДПОРТОВАЯ </td> </tr>
  <tr> <td>I</td> <td>4 км 8 пк - 7 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>4 км 8 пк - 7 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>6 км 4 пк - 7 км 9 пк</td> <td></td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПРЕДПОРТОВАЯ </td> </tr>
  <tr> <td>I</td> <td>7 км 9 пк - 10 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>8 км 2 пк - 10 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>7 км 9 пк - 10 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>8 км 2 пк - 10 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 13</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>от стр.пер. № 77 до стр.пер. № 63а (АВ)                  (на ст. Автово)</td> <td> </td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПРЕДПОРТОВАЯ - ШОССЕЙНАЯ </td> </tr>
  <tr> <td>I</td> <td>10 км 5 пк - 11 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>10 км 5 пк - 11 км 7 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>10 км 5 пк - 11 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>10 км 5 пк - 11 км 8 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ШОССЕЙНАЯ </td> </tr>
  <tr> <td>I</td> <td>11 км 8 пк - 14 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>11 км 8 пк - 13 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>11 км 9 пк - 14 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>11 км 9 пк - 13 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 3, съезды 6/4, 15/17, 9/11</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    ШОССЕЙНАЯ - АЛЕКСАНДРОВСКАЯ </td> </tr>
  <tr> <td>I</td> <td>14 км 3 пк - 21 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>14 км 3 пк - 21 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    АЛЕКСАНДРОВСКАЯ </td> </tr>
  <tr> <td>I</td> <td>21 км 6 пк - 23 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>21 км 10 пк - 23 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>21 км 6 пк - 23 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>22 км 1 пк - 23 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезд 1/3 бок. напр. для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    АЛЕКСАНДРОВСКАЯ - ВЕРЕВО </td> </tr>
  <tr> <td>I</td> <td>23 км 9 пк - 33 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>23 км 9 пк - 33 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ВЕРЕВО</td> </tr>
  <tr> <td>I</td> <td>33 км 3 пк - 35 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>33 км 3 пк - 35 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>33 км 7 пк - 34 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>съезды 1/3, 2/4</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ВЕРЕВО - ГАТЧИНА-ВАРШАВСКАЯ </td> </tr>
  <tr> <td>I</td> <td>35 км 2 пк - 43 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>42 км 9 пк - 43 км 10 пк</td> <td>115</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>35 км 2 пк - 43 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>42 км 9 пк - 43 км 10 пк</td> <td>115</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ГАТЧИНА-ВАРШАВСКАЯ </td> </tr>
  <tr> <td>I</td> <td>43 км 10 пк - 48 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>43 км 10 пк - 44 км 3 пк</td> <td>115</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>44 км 3 пк - 48 км 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>43 км 10 пк - 48 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>43 км 10 пк - 44 км 3 пк</td> <td>115</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>44 км 4 пк - 48 км 7 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ГАТЧИНА-ВАРШАВСКАЯ - СУЙДА </td> </tr>
  <tr> <td>I</td> <td>48 км 10 пк - 53 км 8 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>48 км 10 пк - 53 км 8 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    СУЙДА</td> </tr>
  <tr> <td>I</td> <td>53 км 8 пк - 55 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>53 км 8 пк - 55 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td colSpan="6">    СУЙДА-СИВЕРСКАЯ </td> </tr>
  <tr> <td>I</td> <td>53 км 8 пк - 66 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>66 км 2 пк - 5 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>53 км 8 пк - 66 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    СИВЕРСКАЯ </td> </tr>
  <tr> <td>I</td> <td>66 км 5 пк - 69 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>66 км 5 пк - 7 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>66 км  8 пк - 68 км 7 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>66 км 5 пк - 69 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>66 км 8 пк - 68 км 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 12/14, 34/36, 39/43</td> <td></td> <td></td> <td></td> <td></td> </tr>
  <tr> <td colSpan="6">    СИВЕРСКАЯ - СТРОГАНОВО </td> </tr>
  <tr> <td>I</td> <td>69 км 4 пк - 76 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>69 км 4 пк - 76 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    СТРОГАНОВО </td> </tr>
  <tr> <td>I</td> <td>76 км 7 пк - 78 км 8 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>76 км 10 пк - 78 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>76 км 7 пк - 78 км 8 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>77 км 1 пк - 78 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 2/4, 5/7, 8/10 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    СТРОГАНОВО - МШИНСКАЯ </td> </tr>
  <tr> <td>I</td> <td>78 км 8 пк - 105 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>78 км 8 пк - 105 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    МШИНСКАЯ </td> </tr>
  <tr> <td>I</td> <td>105 км 2 пк - 107 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>105 км 2 пк - 107 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>105 км 7 пк - 107 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 3, 6, 7</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    МШИНСКАЯ - ТОЛМАЧЕВО </td> </tr>
  <tr> <td>I</td> <td>107 км 7 пк - 121 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>107 км 7 пк - 121 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ТОЛМАЧЕВО </td> </tr>
  <tr> <td>I</td> <td>121 км 1 пк - 125 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>121 км 1 пк - 125 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>121 км 4 пк - 125 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 1/3, 5/7, 24/26, 32/34 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ТОЛМАЧЕВО - ГЕНЕРАЛА ОМЕЛЬЧЕНКО ( Б.РЗД 131 КМ) </td> </tr>
  <tr> <td>I</td> <td>125 км 6 пк - 131 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>125 км 6 пк - 131 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ГЕНЕРАЛА ОМЕЛЬЧЕНКО ( Б.РЗД 131 КМ) </td> </tr>
  <tr> <td>I</td> <td>131 км 3 пк - 133 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>131 км 6 пк - 132 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>131 км 3 пк - 133 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>131 км 5 пк - 132 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 1/3, 2/4, 12/14, 17/19 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td></td> <td>пути № 4, 6</td> <td> </td> <td> </td> <td> </td> <td>15</td> </tr>
  <tr> <td colSpan="6">    ГЕНЕРАЛА ОМЕЛЬЧЕНКО ( Б.РЗД 131 КМ) - ЛУГА I </td> </tr>
  <tr> <td>I</td> <td>133 км 4 пк - 135 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>133 км 4 пк - 135 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ЛУГА I </td> </tr>
  <tr> <td>I</td> <td>135 км 4 пк - 139 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>135 км 9 пк - 137 км 10 пк</td> <td>70</td> <td>70</td> <td>70</td> <td> </td> </tr>
  <tr> <td></td> <td>137 км 10 пк - 139 км 1 пк</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>139 км 1 пк - 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>135 км 4 пк - 139 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>135 км 8 пк - 137 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>137 км 2 пк - 138 км 2 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>138 км 2 пк - 139 км 1 пк</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>съезды 79/81, 2/4, 87/89, 34/36</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезд 5/5а для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td></td> <td>парк «Южный» </td> <td>40</td> <td>40</td> <td>40</td> <td>40</td> </tr>
  <tr> <td></td> <td>парк «Южный» пути № 6, 8, 10, съезды 8/10, 38/40, стр.пер. № 14, 16, 42 бок. напр.</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>парк «Северный» пути № 4, 7, 9</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>парк «Северный» стр.пер. № 13, 15 бок. напр., пути 3, 5, 6</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>парк «Пассажирский» пути № 20, 22 и стр.пер. на них, пути 3, 5</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td colSpan="6">    ЛУГА I - СЕРЕБРЯНКА </td> </tr>
  <tr> <td>I</td> <td>139 км 6 пк - 159 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> </tr>
  <tr> <td colSpan="6">    СЕРЕБРЯНКА </td> </tr>
  <tr> <td>I</td> <td>159 км 2 пк - 160 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    СЕРЕБРЯНКА - ЛЯМЦЕВО </td> </tr>
  <tr> <td>I</td> <td>160 км 9 пк - 170 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ЛЯМЦЕВО</td> </tr>
  <tr> <td>I</td> <td>170 км 6 пк - 172 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>171 км 1 пк - 172 км 4 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>171 км 1 пк - 172 км 6 пк</td> <td>80</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>171 км 1 пк - 2 пк стр.пер. бок</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>171 км 2 пк - 172 км 4 пк</td> <td>50</td> <td>50</td> <td>50</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>стр.пер. № 1, 3 бок. напр.</td> <td></td><td></td><td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ЛЯМЦЕВО - ПЛЮССА </td> </tr>
  <tr> <td>I</td> <td>172 км 6 пк - 181 км 10 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>172 км 6 пк - 181 км 10 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>172 км 8 пк - 177 км 10 пк дефектность скреплений</td> <td>70</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПЛЮССА </td> </tr>
  <tr> <td>I</td> <td>181 км 10 пк - 184 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>181 км 10 пк - 184 км 7 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td>II</td> <td>181 км 10 пк - 184 км 9 пк</td> <td>80</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>15</td> </tr>
  <tr> <td></td> <td>съезды 1/3, 2/4, 6/8, 11/13</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезд 2/4 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ПЛЮССА - СТРУГИ КРАСНЫЕ </td> </tr>
  <tr> <td>I</td> <td>184 км 9 пк - 204 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>184 км 9 пк - 204 км 10 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    СТРУГИ КРАСНЫЕ </td> </tr>
  <tr> <td>I</td> <td>204 км 10 пк - 206 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>204 км 10 пк - 205 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>204 км 10 пк - 206 км 9 пк</td> <td>80</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 7</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
  <tr> <td colSpan="6">    СТРУГИ КРАСНЫЕ - ВЛАДИМИРСКИЙ ЛАГЕРЬ</td> </tr>
  <tr> <td>I</td> <td>206 км 9 пк - 210 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>206 км 9 пк - 210 км 6 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    ВЛАДИМИРСКИЙ ЛАГЕРЬ </td> </tr>
  <tr> <td>I</td> <td>210 км 6 пк - 211 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>210 км 6 пк - 211 км 10 пк</td> <td>80</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>210 км 7 пк - 211 км 9 пк стрелочные переводы</td> <td>80</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ВЛАДИМИРСКИЙ ЛАГЕРЬ - ЛАПИНО </td> </tr>
  <tr> <td>I</td> <td>211 км 10 пк - 217 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>211 км 10 пк - 217 км 5 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    ЛАПИНО </td> </tr>
  <tr> <td>I</td> <td>217 км 5 пк - 219 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>217 км 5 пк - 219 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>217 км 5 пк - 218 км 10 пк</td> <td>80</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>съезды 2/4, 7/5</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ЛАПИНО - НОВОСЕЛЬЕ </td> </tr>
  <tr> <td>I</td> <td>219 км 2 пк - 227 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>219 км 2 пк - 227 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    НОВОСЕЛЬЕ </td> </tr>
  <tr> <td>I</td> <td>227 км 6 пк - 229 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>227 км 6 пк - 229 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>227 км 8 пк - 229 км 4 пк</td> <td>80</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    НОВОСЕЛЬЕ - МОЛОДИ </td> </tr>
  <tr> <td>I</td> <td>229 км 4 пк - 238 км 8 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>229 км 4 пк - 238 км 8 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    МОЛОДИ </td> </tr>
  <tr> <td>I</td> <td>238 км 8 пк - 241 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>238 км 8 пк - 241 км 2 пк</td> <td>80</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>241 км 1 пк - 2 пк стр.пер. № 4 бок. напр.</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td colSpan="6">    МОЛОДИ - ТОРОШИНО </td> </tr>
  <tr> <td>I</td> <td>241 км 3 пк - 253 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ТОРОШИНО </td> </tr>
  <tr> <td>I</td> <td>253 км 3 пк - 254 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 4, 6</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    ТОРОШИНО - ЛЮБЯТОВО </td> </tr>
  <tr> <td>I</td> <td>254 км 10 пк - 269 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ЛЮБЯТОВО </td> </tr>
  <tr> <td>I</td> <td>269 км 2 пк - 272 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>271 км 8 пк - 272 км 2 пк </td> <td>80</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>по переезду 272 км пк 3 головой поезда</td> <td>50</td> <td>50</td> <td>50</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ЛЮБЯТОВО - ПСКОВ-ПАССАЖИРСКИЙ </td> </tr>
  <tr> <td>I</td> <td>272 км 3 пк - 273 км 5 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПСКОВ-ПАССАЖИРСКИЙ </td> </tr>
  <tr> <td>I</td> <td>273 км 5 пк - 274 км 9 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>273 км 8 пк - 274 км 6 пк</td> <td>50</td> <td>50</td> <td>50</td> <td> </td> </tr>
  <tr> <td></td> <td>274 км 6 пк - 7 пк съезд 71/25</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>274 км 7 пк - 9 пк</td> <td>50</td> <td>50</td> <td>50</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 4, 8, 12</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
  <tr> <td colSpan="6">    ПСКОВ-ПАССАЖИРСКИЙ - ПСКОВ-ТОВАРНЫЙ </td> </tr>
  <tr> <td>I</td> <td>274 км 9 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПСКОВ-ТОВАРНЫЙ </td> </tr>
  <tr> <td>I</td> <td>274 км 9 пк - 276 км 4 пк</td> <td>70</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>274 км 9 пк - 276 км 3 пк</td> <td>50</td> <td>50</td> <td>50</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 2, 3, 6</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>съезды 35/39, 41/47</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">      ПСКОВ-ТОВАРНЫЙ ПАРК ПСКОВ II </td> </tr>
  <tr> <td></td> <td>путь 3, 4, 11</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
  <tr> <td colSpan="6">    ПСКОВ-ТОВАРНЫЙ - ПСКОВ-ТУРИСТСКИЙ </td> </tr>
  <tr> <td>I</td> <td>276 км 4 пк</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПСКОВ-ТУРИСТСКИЙ </td> </tr>
  <tr> <td>I</td> <td>276 км 4 пк - 278 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по переезду 276 км пк 6 головой поезда</td> <td>25</td> <td>25</td> <td>25</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    ПСКОВ-ТУРИСТСКИЙ - ЧЕРЕХА </td> </tr>
  <tr> <td>I</td> <td>278 км 1 пк - 280 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ЧЕРЕХА </td> </tr>
  <tr> <td>I</td> <td>280 км 2 пк - 282 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>280 км 2 пк - 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по переезду 280 км пк 9 </td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>280 км 10 пк - 281 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 6</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
  <tr> <td></td> <td>стр.пер. № 4</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td colSpan="6">    ЧЕРЕХА - ЧЕРСКАЯ </td> </tr>
  <tr> <td>I</td> <td>282 км 4 пк - 298 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ЧЕРСКАЯ </td> </tr>
  <tr> <td>I</td> <td>298 км 10 пк - 301 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 4</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
  <tr> <td colSpan="6">    ЧЕРСКАЯ - ДУЛОВСКАЯ </td> </tr>
  <tr> <td>I</td> <td>301 км 1 пк - 310 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ДУЛОВСКАЯ </td> </tr>
  <tr> <td>I</td> <td>310 км 2 пк - 312 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>311 км 8 пк - 312 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ДУЛОВСКАЯ - ОСТРОВ</td> </tr>
  <tr> <td>I</td> <td>312 км 3 пк - 325 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ОСТРОВ </td> </tr>
  <tr> <td>I</td> <td>325 км 3 пк - 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>325 км 8 пк - 326 км 7 пк </td> <td>70</td> <td>70</td> <td>70</td> <td> </td> </tr>
  <tr> <td></td> <td>326 км 7 пк - 327 км 5 пк</td> <td>50</td> <td>50</td> <td>50</td> <td> </td> </tr>
  <tr> <td></td> <td>327 км 5 пк - 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 4</td>  <td></td> <td></td> <td></td><td>15</td> </tr>
  <tr> <td colSpan="6">    ОСТРОВ - БРЯНЧАНИНОВО </td> </tr>
  <tr> <td>I</td> <td>327 км 6 пк - 337 км 8 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    БРЯНЧАНИНОВО </td> </tr>
  <tr> <td>I</td> <td>337 км 8 пк - 339 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>338 км 2 пк - 339 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    БРЯНЧАНИНОВО - РИТУПЕ </td> </tr>
  <tr> <td>I</td> <td>339 км 7 пк - 353 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>339 км 7 пк - 341 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    РИТУПЕ </td> </tr>
  <tr> <td>I</td> <td>353 км 1 пк - 355 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    РИТУПЕ - ПЫТАЛОВО </td> </tr>
  <tr> <td>I</td> <td>355 км 1 пк - 367 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>365 км 1 пк - 367 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПЫТАЛОВО </td> </tr>
  <tr> <td>II</td> <td>367 км 6 пк - 369 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>367 км 6 пк - 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>367 км 7 пк - 369 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 3, 5, 6, 8</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>съезды 15/17, 10/12</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    ПЫТАЛОВО - БЕЛКИНО </td> </tr>
  <tr> <td>I</td> <td>369 км 4 пк - 374 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    БЕЛКИНО</td> </tr>
  <tr> <td>I</td> <td>374 км 4 пк - 377 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    БЕЛКИНО - СКАНГАЛИ </td> </tr>
  <tr> <td>I</td> <td>377 км 3 пк - 392 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    СКАНГАЛИ </td> </tr>
  <tr> <td>I</td> <td>392 км 5 пк - 395 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    СКАНГАЛИ - КАРСАВА </td> </tr>
  <tr> <td>I</td> <td>395 км 5 пк - 397 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
</React.Fragment>;