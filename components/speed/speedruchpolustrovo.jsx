import React from 'react';
import TableSpeed from './tablespeed.jsx';
import ButtonCollapseSpeed from './buttoncollapsespeed.jsx';

export default function SpeedRuchPolustrovo(props) {
  let id = "ruchpolustrovo";
  return (
    <React.Fragment>
      <ButtonCollapseSpeed id={id} text="Ручьи - Полюстрово" />
      <div className="collapse" id={id}>
        <TableSpeed tbody={data} />
      </div>
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    РУЧЬИ </td> </tr>
  <tr> <td>IП</td> <td>2 км 1 пк - 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>съезды 85/87, 25/27</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    РУЧЬИ - ПОЛЮСТРОВО </td> </tr>
  <tr> <td>I</td> <td>2 км 7 пк - 3 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПОЛЮСТРОВО </td> </tr>
  <tr> <td>I</td> <td>3 км 8 пк - 5 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>4 км 1 пк - 5 км 9 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
</React.Fragment>;