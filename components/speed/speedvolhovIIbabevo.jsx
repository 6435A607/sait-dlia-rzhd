import React from 'react';
import TableSpeed from './tablespeed.jsx';

export default function SpeedVolhovIIBabaevo(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">Волховстрой II - Бабаево</h4>
      <br />
      <TableSpeed tbody={data} />
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    ВОЛХОВСТРОЙ II </td> </tr>
  <tr> <td>I</td> <td>125 км 1 пк - 128 км 3 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>125 км 1 пк - 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>126 км 1 пк - 128 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>124 км 10 пк - 128 км 3 пк</td> <td>140</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>124 км 10 пк - 126 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>126 км 1 пк - 128 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>128 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>съезд 5/7 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ВОЛХОВСТРОЙ II - КУКОЛЬ </td> </tr>
  <tr> <td>I</td> <td>128 км 3 пк - 133 км 3 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td>II</td> <td>128 км 3 пк - 133 км 3 пк</td> <td>140</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>128 км 3 пк - 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>133 км 2 пк - 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    КУКОЛЬ </td> </tr>
  <tr> <td>I</td> <td>133 км 3 пк - 135 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>133 км 7 пк - 135 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>135 км 4 пк - 5 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td>II</td> <td>133 км 3 пк - 135 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>133 км 5 пк - 135 км 2 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 6/8, 10/12, 1/3, 5/7 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td colSpan="6">    КУКОЛЬ - МЫСЛИНО </td> </tr>
  <tr> <td>I</td> <td>135 км 5 пк - 139 км 2 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td>II</td> <td>135 км 5 пк - 139 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    МЫСЛИНО </td> </tr>
  <tr> <td>I</td> <td>139 км 2 пк - 141 км 10 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>139 км 6 пк - 140 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>139 км 2 пк - 141 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 3</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>съезды 1/3, 9/11 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td colSpan="6">    МЫСЛИНО - ВАЛЯ </td> </tr>
  <tr> <td>I</td> <td>141 км 10 пк - 164 км 2 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>157 км 2 пк - 157 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>141 км 10 пк - 164 км 2 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>141 км 10 пк - 149 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>150 км 1 пк - 156 км 1 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>156 км 2 пк - 157 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>157 км 8 пк - 164 км 2 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td colSpan="6">    ВАЛЯ </td> </tr>
  <tr> <td>I</td> <td>164 км 2 пк - 166 км 10 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>166 км 7 пк - 166км 10 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td>II</td> <td>164 км 2 пк - 166 км 10 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>164 км 6 пк - 166 км 7 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 1/3, 5/7 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ВАЛЯ - ЦВЫЛЕВО </td> </tr>
  <tr> <td>I</td> <td>166 км 10 пк - 182 км 8 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>176 км 3 пк - 178 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>178 км 5 пк - 10 пк кривые</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>179 км 1 пк - 180 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>166 км 10 пк - 182 км 8 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>170 км 3 пк - 178 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>178 км 5 пк - 10 пк кривые</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>179 км 1 пк - 182 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ЦВЫЛЕВО </td> </tr>
  <tr> <td>I</td> <td>182 км 8 пк - 184 км 10 пк</td> <td>100</td> <td>90</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>183 км 1 пк - 184 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>184 км 5 пк - 10 пк </td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>182 км 8 пк - 184 км 10 пк</td> <td>100</td> <td>90</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>182 км 8 пк - 182 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>182 км 10 пк - 184 км 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>184 км 6 пк - 10 пк </td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 1/3, 5/7 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ЦВЫЛЕВО - ТИХВИН</td> </tr>
  <tr> <td>I</td> <td>184 км 10 пк - 198 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>184 км 10 пк - 198 км 10 пк</td> <td>120</td> <td>90</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>188 км 1 пк – 190 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ТИХВИН </td> </tr>
  <tr> <td>I</td> <td>198 км 10 пк - 201 км 7 пк</td> <td>120</td> <td>90</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>198 км 10 пк - 199 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>199 км 3 пк - 201 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>201 км 1 пк – 201 км 6 пк</td> <td>110</td> <td>90</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>198 км 10 пк - 201 км 7 пк</td> <td>120</td> <td>90</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>198 км 10 пк - 199 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>199 км 4 пк - 201 км 2 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>201 км 2 пк – 201 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>4 гл. путь (на ст.Будогощь)</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ТИХВИН - БОЛЬШОЙ ДВОР </td> </tr>
  <tr> <td>I</td> <td>201 км 7 пк - 225 км 5 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>201 км 1 пк - 201 км 6 пк </td> <td>110</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>205 км 1 пк - 205 км 10 пк кривые</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>214 км 1 пк - 214 км 5 пк кривые</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>215 км 9 пк - 216 км 3 пк </td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>218 км 7 пк - 219 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>201 км 7 пк - 225 км 5 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>201 км 1 пк - 201 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>205 км 1 пк - 205 км 10 пк кривые</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>214 км 1 пк - 214 км 6 пк кривые</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>215 км 9 пк - 216 км 3 пк </td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>218 км 7 пк - 219 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>219 км 2 пк - 225 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    БОЛЬШОЙ ДВОР </td> </tr>
  <tr> <td>I</td> <td>225 км 5 пк - 227 км 7 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>227 км 1 пк - 227 км 4 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>227 км 5 пк - 7 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>225 км 5 пк - 227 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>225 км 9 пк - 227 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>227 км 2 пк - 7 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">    БОЛЬШОЙ ДВОР - ПИКАЛЕВО I </td> </tr>
  <tr> <td>I</td> <td>227 км 7 пк - 237 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>227 км 7 пк - 227 км 10 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>231 км 1 пк - 237 км 1 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>227 км 7 пк - 237 км 1 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПИКАЛЕВО I </td> </tr>
  <tr> <td>I</td> <td>237 км 1 пк - 239 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>237 км 1 пк - 5 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>237 км 5 пк стрелочный перевод</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>238 км 9 пк - 239 км 3 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>237 км 1 пк - 239 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>237 км 1 пк - 4 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>237 км 4 пк - 6 пк стрелочный перевод</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>238 км 9 пк - 239 км 3 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ПИКАЛЕВО I - ПИКАЛЕВО II </td> </tr>
  <tr> <td>I</td> <td>239 км 3 пк - 244 км 2 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>239 км 3 пк - 244 км 2 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПИКАЛЕВО II </td> </tr>
  <tr> <td>I</td> <td>244 км 2 пк - 246 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>244 км 2 пк - 5 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>244 км 5 пк - 245 км 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>244 км 2 пк - 246 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>244 км 2 пк - 5 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>244 км 6 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>244 км 6 пк - 245 км 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ПИКАЛЕВО II - КОЛИ </td> </tr>
  <tr> <td>I</td> <td>246 км 3 пк - 262 км 9 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>246 км 3 пк - 248 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>248 км 9 пк - 10 пк кривые</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>249 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>254 км 1 пк - 255 км 5 пк кривые</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>255 км 6 пк - 256 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>256 км 10 пк - 257 км 5 пк порожние грузовые вагоны</td> <td>90</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>257 км 6 пк - 260 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>261 км 1 пк - 6 пк порожние грузовые вагоны</td> <td>90</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>261 км 7 пк - 262 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>246 км 3 пк - 262 км 9 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>246 км 3 пк - 248 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>248 км 9 пк - 10 пк кривые</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>249 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>254 км 1 пк - 255 км 5 пк кривые</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>255 км 6 пк - 256 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>256 км 10 пк - 257 км 5 пк порожние грузовые вагоны</td> <td>90</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>257 км 6 пк - 260 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>261 км 1 пк - 6 пк порожние грузовые вагоны</td> <td>90</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>261 км 7 пк - 262 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    КОЛИ </td> </tr>
  <tr> <td>I</td> <td>262 км 9 пк - 264 км 9 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>262 км 9 пк - 263 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>263 км 2 пк - 264 км 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>262 км 9 пк - 264 км 9 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>262 км 9 пк - 263 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>263 км 2 пк - 264 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">    КОЛИ - ЕФИМОВСКАЯ </td> </tr>
  <tr> <td>I</td> <td>264 км 9 пк - 273 км 8 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>269 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>269 км 9 пк - 272 км 1 пк кривые</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>272 км 2 пк - 7 пк порожние грузовые вагоны</td> <td>90</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>272 км 8 пк - 273 км 8 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>264 км 9 пк - 273 км 8 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>269 км 6 пк - 8 пк </td> <td>100</td> <td>85</td> <td>85</td> <td></td> </tr>
  <tr> <td></td> <td>269 км 9 пк - 270 км 2 пк порожние грузовые вагоны</td> <td>90</td> <td>85</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>270 км 3 пк - 272 км 1 пк кривые</td> <td>90</td> <td>85</td> <td>85</td> <td></td> </tr>
  <tr> <td></td> <td>272 км 2 пк - 7 пк порожние грузовые вагоны</td> <td>90</td> <td>85</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>272 км 8 пк - 273 км 2 пк кривые</td> <td>90</td> <td>85</td> <td>85</td> <td></td> </tr>
  <tr> <td></td> <td>273 км 3 пк - 8 пк</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ЕФИМОВСКАЯ </td> </tr>
  <tr> <td>I</td> <td>273 км 8 пк - 276 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>273 км 8 пк - 274 км 4 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>274 км 5 пк - 6 пк стрелочный перевод</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>274 км 7 пк - 275 км 4 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>275 км 5 пк - 276 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>273 км 8 пк - 276 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>273 км 8 пк - 10 пк </td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>274 км 1 пк - 4 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>274 км 5 пк - 6 пк стрелочный перевод</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>274 км 7 пк - 275 км 4 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>275 км 5 пк - 10 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 6</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td colSpan="6">    ЕФИМОВСКАЯ - ПОДБОРОВЬЕ </td> </tr>
  <tr> <td>I</td> <td>276 км 1 пк - 294 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>276 км 1 пк - 2 пк кривые</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>276 км 1 пк - 294 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>276 км 1 пк - 2 пк кривые</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПОДБОРОВЬЕ </td> </tr>
  <tr> <td>I</td> <td>294 км 10 пк - 297 км 8 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>294 км 10 пк - 295 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>294 км 10 пк - 297 км 8 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>294 км 10 пк - 295 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>путь № 4, съезды 6/8, 10/12, 14/16, 1/3, 9/11 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ПОДБОРОВЬЕ - ЗАБОРЬЕ </td> </tr>
  <tr> <td>I</td> <td>297 км 8 пк - 305 км 6 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td>II</td> <td>297 км 8 пк - 305 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ЗАБОРЬЕ </td> </tr>
  <tr> <td>I</td> <td>305 км 6 пк - 307 км 8 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td>II</td> <td>305 км 5 пк - 307 км 8 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>305 км 5 пк - 305 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 2/4, 8/10, 1/3, 9/11 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ЗАБОРЬЕ - ВЕРХНЕВОЛЬСКИЙ </td> </tr>
  <tr> <td>I</td> <td>307 км 8 пк - 317 км 4 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td>II</td> <td>307 км 8 пк - 317 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ВЕРХНЕВОЛЬСКИЙ </td> </tr>
  <tr> <td>I</td> <td>317 км 4 пк - 319 км 9 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>317 км 4 пк - 6 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>319 км 6 пк - 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>317 км 4 пк - 319 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 1/3, 5/7, 6/8 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ВЕРХНЕВОЛЬСКИЙ - ТЕШЕМЛЯ </td> </tr>
  <tr> <td>I</td> <td>319 км 9 пк - 329 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>319 км 9 пк - 329 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>329 км 5 пк - 6 пк кривые</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ТЕШЕМЛЯ </td> </tr>
  <tr> <td>I</td> <td>329 км 6 пк - 332 км 3 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>329 км 6 пк - 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>329 км 10 пк - 331 км 10 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td>II</td> <td>329 км 6 пк - 332 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>329 км 6 пк - 8 пк кривые</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 2/4, 18/20 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ТЕШЕМЛЯ - БАБАЕВО </td> </tr>
  <tr> <td>I</td> <td>332 км 3 пк - 350 км 3 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>333 км 5 пк - 10 пк</td> <td>115</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td>II</td> <td>332 км 3 пк - 350 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    БАБАЕВО </td> </tr>
  <tr> <td>I</td> <td>350 км 3 пк - 357 км 10 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>350 км 8 пк - 357 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>350 км 3 пк - 357 км 10 пк</td> <td>120</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>350 км 3 пк - 8 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>350 км 8 пк - 355 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>357 км 1 пк - 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 2/4, 6/8, 16/18, 240/242 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td></td> <td>по системе весового контроля (357 км пк 8) нечетным поездам - при взвешивании</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
</React.Fragment>;