import React from 'react';
import TableSpeed from './tablespeed.jsx';
import ButtonCollapseSpeed from './buttoncollapsespeed.jsx';

export default function SpeedSPSMNPort(props) {
  let id = "spsmnport";
  return (
    <React.Fragment>
      <ButtonCollapseSpeed id={id} text="СПСМ - Новый Порт" />
      <div className="collapse" id={id} >
        <TableSpeed tbody={data} />
      </div>
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-СОРТ-МОСКОВСКИЙ </td> </tr>
  <tr> <td>I</td> <td>1 км 8 пк - 2 км 2 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>1 км 4 пк - 2 км 9 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>стр.пер. №609</td> <td></td> <td></td> <td></td> <td>40</td>  </tr>
  <tr> <td></td> <td>съезды 149/151, 541/547, 521/527, 515/517,821/835, 801/803, 533/539</td><td> </td><td> </td> <td> </td> <td>15</td> </tr>
  <tr> <td></td> <td>съезды 575/211/213, 131/133, 145/185, 212/210, 561/567, 532/526, 505/507, 522/520, 555/557, 571/573, 536/538, 624/602, 632/857, 640/642, 199/135, 146/136, 668/666, 164/162, 120/122, 664/662/670, 207/209, 214/216, 529/531, 504/506, 225/223, 160/688</td> <td></td> <td> </td><td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>по всем глухим пересечениям на путях 1, 5 и 6 парка</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
  <tr> <td colSpan="6">      Парк приёмо-отправочный № 1 </td> </tr>
  <tr> <td></td> <td>путь 12, 13, 14, 15, 16, 17, 18, 19, 110, 111, 112, 114</td> <td> </td> <td> </td> <td></td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк приемо-отправочный № 2 </td> </tr>
  <tr> <td></td> <td>путь 25, 26</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк нечетный приемо-отправления № 5 </td> </tr>
  <tr> <td></td> <td>путь 51, 52, 53, 54, 55, 56, 57, 58, 59, 510, 511, 512, 513, 514, 515, 516, 517, 518, 519</td> <td></td> <td></td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк четный приемо-отправочный № 6 </td> </tr>
  <tr> <td></td> <td>путь 61, 62, 63, 64, 65, 66, 67, 68, 69, 610, 611, 612, 613</td> <td></td> <td></td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк Обухово </td> </tr>
  <tr> <td></td> <td>путь 7</td> <td></td> <td></td> <td> </td> <td>15</td> </tr>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-СОРТ-МОСКОВСКИЙ - ВОЛКОВСКАЯ </td> </tr>
  <tr> <td>I</td> <td>2 км 2 пк - 9 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    ВОЛКОВСКАЯ </td> </tr>
  <tr> <td>I</td> <td>2 км 9 пк - 4 км 8 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>3 км 3 пк - 4 км 4 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>4 км 5 пк - 8 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td>II</td> <td>2 км 9 пк - 4 км 10 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>3 гл. путь (на ст. Бадаевская)</td> <td>25</td> <td>25</td> <td>25</td> <td></td> </tr>
  <tr> <td colSpan="6">      Парк Бадаевская </td> </tr>
  <tr> <td></td> <td>путь 2, съезд 11/12</td> <td> </td> <td></td> <td></td> <td>15</td> </tr>
  <tr> <td colSpan="6">      Парк приемо-отправочный парк «Б» </td> </tr>
  <tr> <td></td> <td>путь 6, 7, 8, 9, съезд 41/43</td> <td> </td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк сортировочно-отправочный парк «В» </td> </tr>
  <tr> <td></td> <td>путь 10, 11, 12, 13, 14, 15, 16</td> <td></td> <td></td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    ВОЛКОВСКАЯ - ЦВЕТОЧНАЯ </td> </tr>
  <tr> <td>I</td> <td>4 км 9 пк - 5 км 10 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td>II</td> <td>4 км 10 пк - 5 км 10 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td colSpan="6">    ЦВЕТОЧНАЯ </td> </tr>
  <tr> <td>I</td> <td>5 км 10 пк - 8 км 6 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>5 км 10 пк - 6 км 2 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>6 км 2 пк - 8 км 5 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td>II</td> <td>5 км 10 пк - 8 км 9 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>5 км 10 пк - 6 км 6 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>6 км 6 пк - 8 км 9 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>1 гл. путь (на ст. Нарвская)</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>1 гл. путь (на ст. Броневая)</td> <td>25</td> <td>25</td> <td>25</td> <td></td> </tr>
  <tr> <td></td> <td>соединительный путь № 10</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 3</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
  <tr> <td colSpan="6">      Парк Музей ж.д. транспорта </td> </tr>
  <tr> <td></td> <td>путь 5, 5м, 6м</td> <td> </td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td colSpan="6">    ЦВЕТОЧНАЯ - НОВЫЙ ПОРТ </td> </tr>
  <tr> <td>II</td> <td>8 км 9 пк - 11 км 2 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td colSpan="6">    НОВЫЙ ПОРТ </td> </tr>
  <tr> <td>II</td> <td>11 км 2 пк - 13 км 8 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>13 км 1 пк - 13 км 7 пк</td> <td>45</td> <td>45</td> <td>45</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк сортировочно-отправочный Б </td> </tr>
  <tr> <td></td> <td>путь 2, 3, 4, 5, 6, 8, 9</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
</React.Fragment>;