import React from 'react';
import TableSpeed from './tablespeed.jsx';
import ButtonCollapseSpeed from './buttoncollapsespeed.jsx';

export default function SpeedPredportLigovo(props) {
  let id = "predportligovo";
  return (
    <React.Fragment>
      <ButtonCollapseSpeed id={id} text="Предпортовая - Лигово" />
      <div className="collapse" id={id}>
        <TableSpeed tbody={data} />
      </div>
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    ПРЕДПОРТОВАЯ </td> </tr>
  <tr> <td>I</td> <td>1 км 4 пк - 7 пк  </td> <td> </td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам  </td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 13  </td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ПРЕДПОРТОВАЯ - ЛИГОВО </td> </tr>
  <tr> <td>I</td> <td>1 км 8 пк - 5 км 6 пк</td> <td> </td> <td>60</td> <td>60</td> <td>  </td> </tr>
  <tr> <td colSpan="6">    ЛИГОВО </td> </tr>
  <tr> <td>IП</td> <td>5 км 6 пк - 9 пк  </td> <td> </td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>5 км 8 пк - 9 пк  </td> <td> </td> <td>50</td> <td>50</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам  </td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
</React.Fragment>;