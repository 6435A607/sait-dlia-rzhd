import React from 'react';
import TableSpeed from './tablespeed.jsx';
import ButtonCollapseSpeed from './buttoncollapsespeed.jsx';

export default function SpeedVolkovVitebTov(props) {
  let id = "volkovvitebtov";
  return (
    <React.Fragment>
      <ButtonCollapseSpeed id={id} text="Волковская - СПб-Тов.-Витебский" />
      <div className="collapse" id={id} >
        <TableSpeed tbody={data} />
      </div>
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    ВОЛКОВСКАЯ </td> </tr>
  <tr> <td>II</td> <td>1 км 4 пк - 7 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td>I</td> <td>0 км 10 пк - 1 км 6 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">      Парк Бадаевская </td> </tr>
  <tr> <td></td> <td>путь 2, съезд 11/12</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
  <tr> <td colSpan="6">      Парк приемо-отправочный Б </td> </tr>
  <tr> <td></td> <td>путь 6, 7, 8, 9, съезд 41/43</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк сортировочно-отправочный В </td> </tr>
  <tr> <td></td> <td>путь 10, 11, 12, 13, 14, 15, 16</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td colSpan="6">    ВОЛКОВСКАЯ - САНКТ-ПЕТЕРБУРГ-ТОВ-ВИТЕБСКИЙ </td> </tr>
  <tr> <td>I</td> <td>1 км 6 пк - 8 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>1 км 7 пк - 8 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td>II</td> <td>1 км 7 пк - 10 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>1 км 8 пк - 10 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-ТОВ-ВИТЕБСКИЙ </td> </tr>
  <tr> <td>I</td> <td>1 км 8 пк - 4 км 4 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td>II</td> <td>2 км 1 пк - 4 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>от стр.пер. № 13 до стр.пер. № 8 через стр.пер. № 19, 11, 27, 33, 24, 20, 18 и 10 (1 км пк 9 –                           4 км пк 4)</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>от стр.пер. № 17 до стр.пер. № 27 через стр.пер. № 21, 23 и 25 (1 км пк 9 – 2 км пк 5)</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>от стр.пер. № 13 до стр.пер. № 8 через стр.пер. № 19, 11, 27, 33, 24, 20, 18 и 10</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>от стр.пер. № 17 до стр.пер. № 27 через стр.пер. № 21, 23 и 25</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 1/3, 2/4, 6/8</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
</React.Fragment>;