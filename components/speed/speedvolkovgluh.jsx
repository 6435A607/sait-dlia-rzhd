import React from 'react';
import TableSpeed from './tablespeed.jsx';
import ButtonCollapseSpeed from './buttoncollapsespeed.jsx';

const data = <React.Fragment>
  <tr> <td colSpan="6">    ВОЛКОВСКАЯ</td>  </tr>
  <tr> <td>III</td> <td>1 км 1 пк - 9 пк</td> <td>50</td> <td>50</td> <td>50</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">      Парк Бадаевская</td>  </tr>
  <tr> <td></td> <td>путь 2, съезд 11/12</td> <td> </td> <td> </td> <td> </td> <td>15</td> </tr>
  <tr> <td colSpan="6">      Парк приемо-отправочный Б</td> </tr>
  <tr> <td></td> <td>путь 6, 7, 8, 9, съезд 41/43</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк сортировочно-отправочный В</td>  </tr>
  <tr> <td></td> <td>путь 10, 11, 12, 13, 14, 15, 16</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    ВОЛКОВСКАЯ - ГЛУХООЗЕРСКАЯ</td> </tr>
  <tr> <td>I</td> <td>1 км 9 пк - 10 пк</td> <td>0,191</td> <td>50</td> <td>50</td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ГЛУХООЗЕРСКАЯ</td> </tr>
  <tr> <td>Iа</td> <td>2 км 1 пк - 9 пк</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
</React.Fragment>;

export default function SpeedVolkovGluh(props) {
  let id = "volkovgluh";
  return (
    <React.Fragment>
      <ButtonCollapseSpeed id={id} text="Волковская - Глухоозерская" />
      <div className="collapse" id={id} >
        <TableSpeed tbody={data} />
      </div>
    </React.Fragment>
  );
}
