import React from 'react';
import SpeedKupchShushary from './speedkupchshushary.jsx';
import SpeedNarvaAvtovo from './speednarvaavtovo.jsx';
import SpeedNarvaSPb from './speednarvaspb.jsx';
import SpeedPredportAvtovo from './speedpredportavtovo.jsx';
import SpeedPredportLigovo from './speedpredportligovo.jsx';
import SpeedRuchPolustrovo from './speedruchpolustrovo.jsx';
import SpeedRybaNPort from './speedrybanport.jsx';
import SpeedShosSrednerog from './speedshossrednerog.jsx';
import SpeedSPSMGluh from './speedspsmgluh.jsx';
import SpeedSPSMKupch from './speedspsmkupch.jsx';
import SpeedSPSMNPort from './speedspsmnport.jsx';
import SpeedSPSMObuhovo from './speedspsmobuhovo.jsx';
import SpeedSPSMSPb from './speedspsmspb.jsx';
import SpeedSPSMVolkov from './speedspsmvolkov.jsx';
import SpeedSrednerogShushary from './speedsrednerogshushary.jsx';
import SpeedTsvetBron from './speedtsvetbron.jsx';
import SpeedTsvetNarva from './speedtsvetnarva.jsx';
import SpeedVolkovVitebTov from './speedvolkovvitebtov.jsx';
import SpeedVolkovGluh from './speedvolkovgluh.jsx';

export default function SpeedUzel(props) {
    return (
      <React.Fragment>
        <SpeedKupchShushary />
        <SpeedNarvaAvtovo />
        <SpeedNarvaSPb />
        <SpeedPredportAvtovo />
        <SpeedPredportLigovo />
        <SpeedRuchPolustrovo />
        <SpeedRybaNPort />
        <SpeedShosSrednerog />
        <SpeedSPSMGluh />
        <SpeedSPSMKupch />
        <SpeedSPSMNPort />
        <SpeedSPSMObuhovo />
        <SpeedSPSMSPb />
        <SpeedSPSMVolkov />
        <SpeedSrednerogShushary />
        <SpeedTsvetBron />
        <SpeedTsvetNarva />
        <SpeedVolkovVitebTov />
        <SpeedVolkovGluh />
      </React.Fragment>
    );
}