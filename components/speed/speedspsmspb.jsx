import React from 'react';
import TableSpeed from './tablespeed.jsx';
import ButtonCollapseSpeed from './buttoncollapsespeed.jsx';

export default function SpeedSPSMSPb(props) {
  let id = "spsmspb";
  return (
    <React.Fragment>
      <ButtonCollapseSpeed id={id} text="СПСМ - СПб-Финляндский" />
      <div className="collapse" id={id} >
        <TableSpeed tbody={data} />
      </div>
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-СОРТ-МОСКОВСКИЙ </td> </tr>
  <tr> <td>I</td> <td>0 км 7 пк - 2 км 2 пк</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td><td> </td><td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 149/151, 541/547, 521/527, 515/517, 160/688, 821/835, 801/803, 533/539</td><td> </td><td> </td> <td> </td> <td>15</td> </tr>
  <tr> <td></td> <td>съезды 575/211/213, 131/133, 145/185, 212/210, 561/567, 532/526, 505/507, 522/520, 555/557, 571/573, 529/531, 536/538, 624/602, 632/857, 640/642, 504/506, 146/136, 668/666, 164/162, 120/122, 225/223, 207/209, 214/216, 199/135, 664/662/670, 160/688</td> <td> </td><td> </td><td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>по всем глухим пересечениям на путях 1, 5 и 6 парка</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
  <tr> <td colSpan="6">      Парк приёмо-отправочный № 1 </td> </tr>
  <tr> <td></td> <td>путь 11, 12, 13, 14, 15, 16, 17, 18, 19, 110, 111, 112, 114</td> <td> </td><td> </td><td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк приемо-отправочный № 2 </td> </tr>
  <tr> <td></td> <td>путь 25, 26</td> <td> </td><td> </td><td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк нечетный приемо-отправления № 5 </td> </tr>
  <tr> <td></td> <td>путь 51, 52, 53, 54, 55, 56, 57, 58, 59, 510, 511, 512, 513, 514, 515, 516, 517, 518, 519</td><td> </td><td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк четный приемо-отправочный № 6 </td> </tr>
  <tr> <td></td> <td>путь 61, 62, 63, 64, 65, 66, 67, 68, 69, 610, 611, 612, 613</td><td> </td><td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк Обухово </td> </tr>
  <tr> <td></td> <td>путь 7</td> <td> </td><td> </td><td> </td> <td>15</td> </tr>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-СОРТ-МОСКОВСКИЙ - ГЛУХООЗЕРСКАЯ </td> </tr>
  <tr> <td>I</td> <td>2 км 3 пк - 9 пк</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td colSpan="6">    ГЛУХООЗЕРСКАЯ </td> </tr>
  <tr> <td>I</td> <td>2 км 10 пк - 4 км 5 пк</td> <td>70</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>2 км 10 пк - 3 км 5 пк</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td>II</td> <td>3 км 3 пк - 4 км 5 пк</td> <td>70</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>3 км 3 пк - 5 пк стр.пер.бок.напр</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ГЛУХООЗЕРСКАЯ - ДАЧА ДОЛГОРУКОВА (ЛАДОЖСКИЙ ВОКЗАЛ) </td> </tr>
  <tr> <td>I</td> <td>4 км 5 пк - 5 км 7 пк Финляндский мост </td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>4 км 5 пк - 5 км 7 пк</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>4 км 10 пк - 5 км 5 пк дефектность ИССО Финляндский мост </td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    ДАЧА ДОЛГОРУКОВА (ЛАДОЖСКИЙ ВОКЗАЛ) </td> </tr>
  <tr> <td>I</td> <td>5 км 7 пк - 9 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>5 км 7 пк дефектность ИССО</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>5 км 7 пк - 6 км 8 пк </td> <td>70</td> <td>70</td> <td>70</td> <td> </td> </tr>
  <tr> <td></td> <td>6 км 9 пк - 7 км 4 пк</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>7 км 4 пк - 8 км 2 пк</td> <td>70</td> <td>70</td> <td>70</td> <td> </td> </tr>
  <tr> <td></td> <td>8 км 2 пк - 9 пк кривые</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td>II</td> <td>5 км 7 пк - 8 км 1 пк</td> <td>70</td> <td>70</td> <td>70</td> <td> </td> </tr>
  <tr> <td></td> <td>5 км 7 пк - 10 пк</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>7 км 9 пк - 8 км 1 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>съезд 1/3 бок. напр. для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td></td> <td>путь № 3, стр.пер. № 71-69, 65-67, 21 (бок. напр.)</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td colSpan="6">    ДАЧА ДОЛГОРУКОВА (ЛАДОЖСКИЙ ВОКЗАЛ) - ПОЛЮСТРОВО </td> </tr>
  <tr> <td>I</td> <td>9 км 1 пк - 10 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПОЛЮСТРОВО </td> </tr>
  <tr> <td>II</td> <td>10 км 7 пк - 13 км 4 пк</td> <td> 100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>11 км 2 пк - 12 км 4 пк</td> <td> 80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>12 км 4 пк - 13 км 2 пк</td> <td>70</td> <td>70</td> <td>70</td> <td></td> </tr>
  <tr> <td></td> <td>13 км 3 пк - 4 пк кривые</td> <td> 40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td></td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ПОЛЮСТРОВО - ПИСКАРЕВКА </td> </tr>
  <tr> <td>I</td> <td>13 км 4 пк - 8 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПИСКАРЕВКА </td> </tr>
  <tr> <td>V</td> <td>13 км 8 пк - 15 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>13 км 8 пк - 14 км 3 пк кривые</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>14 км 4 пк - 15 км 8 пк стрелочный перевод</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td></td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезд 2/4 бок. напр. для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td></td> <td>II гл. путь (от стр.пер. № 1 до стр.пер. № 48 через стр.пер. № 7 и 17) на ст.Ручьи</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>стр.пер. № 17 бок. напр.</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>3 гл. путь (от стр.пер. № 17 до стр.пер. № 32 через стр.пер. № 15, 46 и 40) на ст.Ручьи</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>I гл, 5Б пути (от стр.пер. № 9 до стр.пер. № 4 через стр.пер. № 13, 54, 44, 42, 36, 26, 22, 10 и 6) на ст.Ржевка</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>съезд 42/44</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>V гл. путь (от стр.пер. № 3 до стр.пер. № 9)</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>V гл. путь (от стр.пер. № 9 до стр.пер. № 20 через стр.пер. № 11, 56, 52 и 34)</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>IА гл. путь (от стр.пер. № 2 до стр.пер. № 44)</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>Пост 2 км на ст.Ручьи (от стр.пер. № 14 до стр.пер. № 16)</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>Пост 2 км на ст.Полюстрово (от стр.пер. № 10 до стр.пер. № 12)</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПИСКАРЕВКА - КУШЕЛЕВКА  </td> </tr>
  <tr> <td>I</td> <td>15 км 10 пк - 17 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>2 км 9 пк - 3 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    КУШЕЛЕВКА </td> </tr>
  <tr> <td>I</td> <td>17 км 2 пк - 18 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>17 км 7 пк - 9 пк</td> <td> 80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>1 км 1 пк - 2 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>1 км 1 пк - 2 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>III</td> <td>19 км 1 пк - 9 пк</td> <td> 100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>19 км 1 пк - 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>19 км 5 пк - 9 пк кривые</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td>IV</td> <td>19 км 1 пк - 9 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>19 км 1 пк - 5 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>19 км 5 пк - 9 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>стр.пер.№ 3 по бок.напр.</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">      Парк сортировочно-отправочный Б </td> </tr>
  <tr> <td></td> <td>путь 30, 31, 32, 33, 34, 35, 36</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
  <tr> <td colSpan="6">      Парк сортировочно-отправочный Д </td> </tr>
  <tr> <td></td> <td>путь 23</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
  <tr> <td colSpan="6">    КУШЕЛЕВКА-САНКТ-ПЕТЕРБУРГ-ФИНЛЯНДСКИЙ </td> </tr>
  <tr> <td>III</td> <td>19 км пк 9 - 21 км 4 пк </td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>19 км 9 пк - 10 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td>IV</td> <td>19 км пк 9 - 21 км 4 пк </td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>19 км 9 пк - 10 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-ФИНЛЯНДСКИЙ </td> </tr>
  <tr> <td>III</td> <td>21 км 4 пк - 23 км 8 пк</td> <td> 80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>IV</td> <td>21 км 4 пк - 23 км 7 пк</td> <td> 100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td></td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>съезды 2/4, 10/12, 14/16, 18/20</td> <td> </td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">      Парк приемо-отправочный А </td> </tr>
  <tr> <td></td> <td>путь 5А, 6А, 7А, 8</td> <td></td> <td></td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк сортировочно-отправочный Д </td> </tr>
  <tr> <td></td> <td>путь 3, 4, 5</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
  <tr> <td colSpan="6">      Парк перонный П </td> </tr>
  <tr> <td></td> <td>путь 2П, 3П, 4П, 6П, 8П, 10П</td> <td> </td> <td></td> <td></td> <td>25</td> </tr>
</React.Fragment>;