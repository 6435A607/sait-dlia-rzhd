import React from 'react';
import TableSpeed from './tablespeed.jsx';
import ButtonCollapseSpeed from './buttoncollapsespeed.jsx';

export default function SpeedShosSrednerog(props) {
  let id = "shossrednerog";
  return (
    <React.Fragment>
      <ButtonCollapseSpeed id={id} text="Шоссейная - Среднерогатская" />
      <div className="collapse" id={id} >
        <TableSpeed tbody={data} />
      </div>
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    ШОССЕЙНАЯ </td> </tr>
  <tr> <td>III</td> <td>0 км 10 пк - 1 км 4 пк</td> <td></td> <td>50</td> <td>50</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 3</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>съезды 6/4, 15/17, 9/11</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td colSpan="6">    ШОССЕЙНАЯ - СРЕДНЕРОГАТСКАЯ </td> </tr>
  <tr> <td>I</td> <td>1 км 4 пк - 2 км 3 пк</td> <td> </td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>1 км 9 пк - 10 пк</td> <td> </td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td colSpan="6">    СРЕДНЕРОГАТСКАЯ </td> </tr>
  <tr> <td>IV</td> <td>2 км 3 пк - 3 км 10 пк</td> <td></td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>съезды 3/5, 21/19, 6/4</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
</React.Fragment>;