import React from 'react';
import TableSpeed from './tablespeed.jsx';
import ButtonCollapseSpeed from './buttoncollapsespeed.jsx';

export default function SpeedPredportAvtovo(props) {
  let id = "predportavtovo";
  return (
    <React.Fragment>
      <ButtonCollapseSpeed id={id} text="Предпортовая - Автово" />
      <div className="collapse" id={id}>
        <TableSpeed tbody={data} />
      </div>
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    ПРЕДПОРТОВАЯ </td> </tr>
  <tr> <td>IV</td> <td>1 км 1 пк - 2 км 1 пк</td>  <td></td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td>III</td> <td>12 км 6 пк - 16 км 1 пк</td> <td></td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 13</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>от стр.пер. № 77 до стр.пер. № 63а (АВ)                (на ст. Автово)</td> <td></td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПРЕДПОРТОВАЯ - АВТОВО </td> </tr>
  <tr> <td>I</td> <td>2 км 1 пк - 3 км 9 пк</td> <td></td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td>    АВТОВО </td> </tr>
  <tr> <td>XIX</td> <td>3 км 9 пк - 5 км 1 пк</td> <td></td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
</React.Fragment>;