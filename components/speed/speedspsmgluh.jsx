import React from 'react';
import TableSpeed from './tablespeed.jsx';
import ButtonCollapseSpeed from './buttoncollapsespeed.jsx';

export default function SpeedSPSMGluh(props) {
  let id = "spsmgluh";
  return (
    <React.Fragment>
      <ButtonCollapseSpeed id={id} text="СПСМ - Глухоозерская" />
      <div className="collapse" id={id} >
        <TableSpeed tbody={data} />
      </div>
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-ТОВ-МОСКОВСКИЙ</td> </tr>
  <tr> <td>I</td> <td>1 км 1 пк - 3 пк</td> <td>15</td> <td>15</td> <td>15</td> <td> </td> </tr>
  <tr> <td>II</td> <td>2 км 7 пк</td> <td>30</td> <td>30</td> <td>30</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 342/341, 350/351, 353/352, 357/356, 333/332, 347-344/346</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк А-Н</td> </tr>
  <tr> <td></td> <td>путь 51, 52, 53</td> <td> </td> <td> </td> <td> </td> <td>15</td> </tr>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-ТОВ-МОСКОВСКИЙ - ГЛУХООЗЕРСКАЯ</td> </tr>
  <tr> <td>I</td> <td>1 км 3 пк</td> <td>15</td> <td>15</td> <td>15</td> <td> </td> </tr>
  <tr> <td>II</td> <td>2 км 7 пк - 9 пк</td> <td>30</td> <td>30</td> <td>30</td> <td> </td> </tr>
  <tr> <td colSpan="6">    ГЛУХООЗЕРСКАЯ</td> </tr>
  <tr> <td>I </td> <td>1 км 3 пк - 5 пк</td> <td>15</td> <td>15</td> <td>15</td> <td></td> </tr>
  <tr> <td>II Г</td> <td>2 км 9 пк - 4 км 2 пк</td> <td>30</td> <td>30</td> <td>30</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
</React.Fragment>;
