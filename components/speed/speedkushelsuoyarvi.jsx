import React from 'react';
import TableSpeed from './tablespeed.jsx';

export default function SpeedKushelSuoyarvi(props) {
    return (
        <React.Fragment>
            <h4 className="text-center">Кушелевка - Суоярви</h4>
            <br />
            <TableSpeed tbody={data} />
        </React.Fragment>
    );
}

const data = <React.Fragment>
    <tr> <td colSpan="6">    КУШЕЛЕВКА </td> </tr>
    <tr> <td>II</td> <td>1 км 1 пк - 2 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>1 км 1 пк - 2 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>I</td> <td>17 км 2 пк - 18 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>17 км 7 пк - 9 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>III</td> <td>19 км 1 пк - 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>19 км 1 пк - 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>19 км 5 пк - 9 пк кривые</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
    <tr> <td>IV</td> <td>19 км 1 пк - 9 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
    <tr> <td></td> <td>19 км 1 пк - 5 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td></td> <td>19 км 5 пк - 9 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
    <tr> <td colSpan="6">      Парк сортировочно-отправочный Б </td> </tr>
    <tr> <td></td> <td>путь 30, 31, 32, 33, 34, 35, 36</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
    <tr> <td colSpan="6">      Парк сортировочно-отправочный Д </td> </tr>
    <tr> <td></td> <td>путь 23</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
    <tr> <td colSpan="6">    КУШЕЛЕВКА - ПИСКАРЕВКА </td> </tr>
    <tr> <td>II</td> <td>2 км 9 пк - 3 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>I</td> <td>15 км 10 пк - 17 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ПИСКАРЕВКА</td> </tr>
    <tr> <td>II</td> <td>3 км 9 пк - 5 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>5 км 5 пк - 8 пк</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td>III</td> <td>4 км 4 пк - 5 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>5 км 5 пк - 8 пк кривые</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td> </td> <td></td> <td>40</td> </tr>
    <tr> <td></td> <td>съезд 2/4 бок. напр. для пасс. поездов</td> <td> </td> <td></td> <td></td> <td>50</td> </tr>
    <tr> <td></td> <td>съезды 1/3, 5/7, 56/54, 40/38, 36/34, 30/32, 28/26, 22/20, 6/8, 13/15, 52/42, 46/48</td> <td></td> <td></td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>II гл. путь 4 км 1 пк - 5 км 3 пк (от стр.пер. № 1 до стр.пер. № 48 через стр.пер. № 7 и 17) на ст.Ручьи</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>стр.пер. № 17 бок. напр.</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td></td> <td>3 гл. путь 4 км 4 пк - 5 км 3 пк (от стр.пер. № 17 до стр.пер. № 32 через стр.пер. № 15, 46 и 40) на ст.Ручьи</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td></td> <td>I гл, 5Б пути (от стр.пер. № 9 до стр.пер. № 4 через стр.пер. № 13, 54, 44, 42, 36, 26, 22, 10 и 6) на ст.Ржевка</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>съезд 42/44</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td></td> <td>V гл. путь (от стр.пер. № 3 до стр.пер. № 9)</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>V гл. путь (от стр.пер. № 9 до стр.пер. № 20 через стр.пер. № 11, 56, 52 и 34) на ст.Полюстрово</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td></td> <td>IА гл. путь (от стр.пер. № 2 до стр.пер. № 44)</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>Пост 2 км на ст.Ручьи (от стр.пер. № 14 до стр.пер. № 16)</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td></td> <td>Пост 2 км на ст.Полюстрово (от стр.пер. № 10 до стр.пер. № 12)</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td colSpan="6">    ПИСКАРЕВКА - РУЧЬИ </td> </tr>
    <tr> <td>I</td> <td>5 км 8 пк - 6 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>5 км 8 пк - 6 км 5 пк</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    РУЧЬИ </td> </tr>
    <tr> <td>I</td> <td>6 км 5 пк - 12 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>6 км 5 пк - 6 пк кривые</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>11 км 6 пк - 12 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>6 км 5 пк - 11 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>6 км 5 пк - 6 пк</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>11 км 7 пк - 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td></td> <td></td> <td>25</td> </tr>
    <tr> <td></td> <td>путь 1 парк ПО</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td></td> <td>съезды 25/27, 85/87 бок. напр. для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
    <tr> <td></td> <td>съезд 1м-2м</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td></td> <td>от стр.пер. № 60 до стр.пер. № 16, стр.пер.              № 3м</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td></td> <td>от стр.пер. № 64 до стр.пер. № 22</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td></td> <td>прием и отправление с парка «А» по пути № 7</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
    <tr> <td colSpan="6">    РУЧЬИ - ДЕВЯТКИНО </td> </tr>
    <tr> <td>I</td> <td>12 км 1 пк - 2 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>11 км 10 пк - 12 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ДЕВЯТКИНО </td> </tr>
    <tr> <td>I</td> <td>12 км 2 пк - 14 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>12 км 2 пк - 13 км 4 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>12 км 1 пк - 14 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>12 км 1 пк - 13 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 9</td> <td></td> <td></td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">    ДЕВЯТКИНО - КАПИТОЛОВО </td> </tr>
    <tr> <td>I</td> <td>14 км 1 пк - 16 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>14 км 1 пк - 16 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    КАПИТОЛОВО </td> </tr>
    <tr> <td>I</td> <td>16 км 10 пк - 18 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>16 км 10 пк - 18 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">    КАПИТОЛОВО - ТОКСОВО </td> </tr>
    <tr> <td>I</td> <td>18 км 10 пк - 24 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>18 км 10 пк - 24 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ТОКСОВО </td> </tr>
    <tr> <td>I</td> <td>24 км 1 пк - 26 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>24 км 4 пк - 25 км 5 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>25 км 5 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>25 км 6 пк - 26 км 1 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td>II</td> <td>24 км 3 пк - 25 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>24 км 4 пк - 25 км 4 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>25 км 6 пк - 10 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 6</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
    <tr> <td colSpan="6">    ТОКСОВО - ПЕРИ </td> </tr>
    <tr> <td>I</td> <td>26 км 1 пк - 35 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>26 км 1 пк - 27 км 7 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>27 км 8 пк - 28 км 4 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>28 км 5 пк - 7 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td>II</td> <td>25 км 10 пк - 35 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>25 км 10 пк - 28 км 7 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    ПЕРИ </td> </tr>
    <tr> <td>I</td> <td>35 км 9 пк - 38 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>35 км 9 пк - 38 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>36 км 2 пк - 37 км 8 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 6, съезды 6/8, 1/3</td> <td> </td> <td></td> <td></td> <td>25</td> </tr>
    <tr> <td colSpan="6">    ПЕРИ - ГРУЗИНО </td> </tr>
    <tr> <td>I</td> <td>38 км 2 пк - 44 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>38 км 1 пк - 44 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ГРУЗИНО </td> </tr>
    <tr> <td>I</td> <td>44 км 3 пк - 46 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>44 км 3 пк - 46 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>45 км 5 пк - 8 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 6, съезды 1/3, 10/8</td> <td></td> <td></td> <td> </td> <td>15</td> </tr>
    <tr> <td colSpan="6">    ГРУЗИНО - ВАСКЕЛОВО </td> </tr>
    <tr> <td>I</td> <td>46 км 6 пк - 51 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>46 км 2 пк - 51 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ВАСКЕЛОВО </td> </tr>
    <tr> <td>I</td> <td>51 км 2 пк - 52 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>51 км 2 пк - 52 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ВАСКЕЛОВО - ОРЕХОВО </td> </tr>
    <tr> <td>I</td> <td>52 км 10 пк - 63 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>58 км 5 пк - 10 пк кривые</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>59 км 1 пк - 61 км 9 пк кривые</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>52 км 10 пк - 63 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>60 км 3 пк - 61 км 9 пк кривые</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ОРЕХОВО </td> </tr>
    <tr> <td>I</td> <td>63 км 6 пк - 65 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>63 км 6 пк - 65 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td></td> <td></td> <td>25</td> </tr>
    <tr> <td colSpan="6">    ОРЕХОВО - СОСНОВО </td> </tr>
    <tr> <td>I</td> <td>65 км 6 пк - 73 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>65 км 6 пк - 73 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    СОСНОВО </td> </tr>
    <tr> <td>I</td> <td>73 км 3 пк - 75 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>74 км 4 пк - 9 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td>II</td> <td>73 км 3 пк - 76 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>73 км 6 пк - 74 км 4 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td> </td> <td>25</td> </tr>
    <tr> <td></td> <td>съезд 2/4 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
    <tr> <td></td> <td>съезд 2/4 для груз. поездов, стр.пер. № 12, 14 бок. напр.</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td colSpan="6">    СОСНОВО - ЛОСЕВО I </td> </tr>
    <tr> <td>I</td> <td>75 км 5 пк - 89 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>78 км 4 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td>II</td> <td>76 км 9 пк - 89 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ЛОСЕВО I </td> </tr>
    <tr> <td>IС</td> <td>89 км 4 пк - 90 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>от стр. пер.№ 3/3С до стр.пер.№ 13/13С</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td>I</td> <td>90 км 1 пк - 91 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td>II</td> <td>89 км 4 пк - 92 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>от стр.пер. №6 до сигн. Ч</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td>III</td> <td>90 км 1 пк - 92 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>стр.пер. № 1/1с, 3/3с, 13/13с бок. напр.</td> <td></td> <td></td> <td></td> <td>60</td> </tr>
    <tr> <td></td> <td>съезды 2/4, 12/14, 29/31 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
    <tr> <td></td> <td>съезды 12/14, 29/31 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
    <tr> <td></td> <td>съезд 1/3</td> <td></td> <td></td> <td></td> <td>60</td> </tr>
    <tr> <td colSpan="6">    ЛОСЕВО I - ГРОМОВО </td> </tr>
    <tr> <td>I</td> <td>92 км 3 пк - 101 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>95 км 1 пк - 4 пк кривые</td> <td>70</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td colSpan="6">    ГРОМОВО </td> </tr>
    <tr> <td>I</td> <td>101 км 9 пк - 103 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ГРОМОВО - ОТРАДНОЕ </td> </tr>
    <tr> <td>I</td> <td>103 км 10 пк - 113 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ОТРАДНОЕ </td> </tr>
    <tr> <td>I</td> <td>113 км 10 пк - 115 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 3</td> <td> </td> <td></td> <td></td> <td>25</td> </tr>
    <tr> <td colSpan="6">    ОТРАДНОЕ - МЮЛЛЮПЕЛЬТО </td> </tr>
    <tr> <td>I</td> <td>115 км 9 пк - 124 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    МЮЛЛЮПЕЛЬТО </td> </tr>
    <tr> <td>I</td> <td>124 км 7 пк - 126 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td colSpan="6">    МЮЛЛЮПЕЛЬТО - ПРИОЗЕРСК </td> </tr>
    <tr> <td>I</td> <td>126 км 10 пк - 139 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>139 км 6 пк - 7 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    ПРИОЗЕРСК </td> </tr>
    <tr> <td>I</td> <td>139 км 7 пк - 141 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>139 км 7 пк - 10 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td></td> <td></td> <td> </td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 5, 7, 8</td> <td> </td> <td></td> <td></td> <td>25</td> </tr>
    <tr> <td colSpan="6">    ПРИОЗЕРСК - КУЗНЕЧНОЕ </td> </tr>
    <tr> <td>I</td> <td>141 км 10 пк - 154 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>145 км 1 пк - 7 пк кривые</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>148 км 1 пк - 10 пк кривые</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>152 км 1 пк - 153 км 10 пк кривые</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    КУЗНЕЧНОЕ </td> </tr>
    <tr> <td>I</td> <td>154 км 4 пк - 157 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>155 км 1 пк - 7 пк до капитального ремонта</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td></td> <td></td> <td>25</td> </tr>
    <tr> <td></td> <td>путь 5</td> <td> </td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 7</td> <td></td> <td></td> <td> </td> <td>15</td> </tr>
    <tr> <td colSpan="6">    КУЗНЕЧНОЕ - ХИЙТОЛА </td> </tr>
    <tr> <td>I</td> <td>157 км 2 пк - 173 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>157 км 6 пк - 158 км 3 пк кривые</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>169 км 1 пк - 173 км 2 пк кривые</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    ХИЙТОЛА </td> </tr>
    <tr> <td>II</td> <td>173 км 2 пк - 174 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>173 км 2 пк - 4 пк кривые</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>173 км 5 пк - 10 пк кривые</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>174 км 1 пк - 7 пк кривые</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td></td> <td></td> <td>25</td> </tr>
    <tr> <td></td> <td>стр.пер. № 2 бок. напр.</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td></td> <td>стр.пер. № 11 бок. напр. в кривой 93 км пк 1-2 (направление Выборг – Хийтола)</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td colSpan="6">    ХИЙТОЛА - АЛХО </td> </tr>
    <tr> <td>I</td> <td>174 км 8 пк - 183 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>179 км 9 пк - 180 км 4 пк кривые</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>182 км 1 пк - 182 км 10 пк кривые</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    АЛХО </td> </tr>
    <tr> <td>I</td> <td>183 км 10 пк - 185 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>184 км 1 пк - 2 пк стрелочный перевод</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
    <tr> <td colSpan="6">    АЛХО - ЭЛИСЕНВААРА </td> </tr>
    <tr> <td>I</td> <td>185 км 3 пк - 193 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ЭЛИСЕНВААРА </td> </tr>
    <tr> <td>I</td> <td>193 км 8 пк - 195 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>194 км 3 пк - 195 км 5 пк до капитального ремонта</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>195 км 6 пк стр.пер. № 2 бок. напр.</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td></td> <td>195 км 6 пк - 7 пк</td> <td>80</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td></td> <td></td> <td>25</td> </tr>
    <tr> <td colSpan="6">    ЭЛИСЕНВААРА - ЯККИМА </td> </tr>
    <tr> <td>I</td> <td>195 км 7 пк - 218 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>205 км 6 пк - 207 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>207 км 9 пк - 209 км 6 пк</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>209 км 7 пк - 211 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>211 км 2 пк - 212 км 1 пк</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>213 км 6 пк - 215 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>215 км 5 пк - 216 км 2 пк</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>216 км 3 пк - 218 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ЯККИМА </td> </tr>
    <tr> <td>I</td> <td>218 км 5 пк - 219 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>218 км 8 пк - 219 км 7 пк</td> <td>70</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td></td> <td></td> <td> </td> <td>25</td> </tr>
    <tr> <td></td> <td>путь 3</td> <td></td> <td></td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ЯККИМА - КУОККАНИЭМИ </td> </tr>
    <tr> <td>I</td> <td>219 км 10 пк - 240 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>219 км 10 пк - 220 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>220 км 5 пк - 221 км 9 пк</td> <td>100</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>221 км 10 пк - 222 км 5 пк</td> <td>80</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>222 км 6 пк - 224 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>224 км 7 пк - 225 км 1 пк</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>225 км 2 пк - 230 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>233 км 4 пк - 233 км 9 пк</td> <td>80</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>238 км 6 пк - 240 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    КУОККАНИЭМИ </td> </tr>
    <tr> <td>I</td> <td>240 км 8 пк - 241 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>240 км 9 пк - 241 км 8 пк</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td></td> <td></td> <td>25</td> </tr>
    <tr> <td colSpan="6">    КУОККАНИЭМИ - СОРТАВАЛА </td> </tr>
    <tr> <td>I</td> <td>241 км 8 пк - 258 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>241 км 8 пк - 242 км 5 пк</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>242 км 6 пк - 244 км 3 пк</td> <td>90</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>244 км 4 пк - 248 км 6 пк</td> <td>80</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>246 км 8 пк - 247 км 4 пк порожние грузовые вагоны</td> <td>80</td> <td>70</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>252 км 1 пк - 252 км 6 пк</td> <td>80</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>255 км 2 пк - 258 км 3 пк</td> <td>80</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>по переезду 257 км пк 8 головой поезда</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td colSpan="6">    СОРТАВАЛА</td> </tr>
    <tr> <td>I</td> <td>258 км 3 пк - 261 км 1 пк</td> <td>80</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>259 км 1 пк - 7 пк стрелочный перевод</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>259 км 7 пк - 260 км 2 пк стрелочный перевод</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>по переезду 260 км пк 3</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>260 км 3 пк - 10 пк стрелочный перевод</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>260 км 10 пк - 261 км 1 пк</td> <td>80</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td></td> <td></td> <td>25</td> </tr>
    <tr> <td colSpan="6">    СОРТАВАЛА - ХЕЛЮЛЯ </td> </tr>
    <tr> <td>I</td> <td>261 км 1 пк - 264 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>261 км 1 пк - 263 км 7 пк</td> <td>80</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>263 км 8 пк - 9 пк </td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    ХЕЛЮЛЯ </td> </tr>
    <tr> <td>I</td> <td>264 км 7 пк - 266 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>265 км 9 пк - 265 км 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>266 км 1 пк - 266 км 4 пк</td> <td>80</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td></td> <td></td> <td>25</td> </tr>
    <tr> <td colSpan="6">    ХЕЛЮЛЯ - КААЛАМО </td> </tr>
    <tr> <td>I</td> <td>266 км 6 пк - 286 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>266 км 6 пк - 270 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>274 км 8 пк - 276 км 4 пк</td> <td>80</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>276 км 5 пк - 278 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>278 км 6 пк - 279 км 3 пк</td> <td>80</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>279 км 4 пк - 283 км 4 пк</td> <td>90</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>283 км 5 пк - 284 км 7 пк</td> <td>80</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>284 км 8 пк - 286 км 2 пк</td> <td>90</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>286 км 3 пк - 386 км 4 пк</td> <td>80</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td colSpan="6">    КААЛАМО </td> </tr>
    <tr> <td>I</td> <td>286 км 4 пк - 288 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>286 км 4 пк - 286 км 8 пк</td> <td>80</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>288 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td>  </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td></td> <td>путь 3, 4</td> <td> </td> <td></td> <td></td> <td>25</td> </tr>
    <tr> <td colSpan="6">    КААЛАМО - МАТКАСЕЛЬКЯ </td> </tr>
    <tr> <td>I</td> <td>288 км 5 пк - 295 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>288 км 5 пк - 288 км 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>291 км 10 пк - 292 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    МАТКАСЕЛЬКЯ </td> </tr>
    <tr> <td>II</td> <td>295 км 5 пк - 297 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>295 км 7 пк - 295 км 9 пк</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>295 км 10 пк - 296 км 9 пк кривые</td> <td>70</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>296 км 10 пк - 297 км 4 пк</td> <td>80</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td> </td> <td>25</td> </tr>
    <tr> <td colSpan="6">    МАТКАСЕЛЬКЯ - ЯНИСЪЯРВИ </td> </tr>
    <tr> <td>I</td> <td>297 км 4 пк - 319 км 5 пк</td> <td>80</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td colSpan="6">    ЯНИСЪЯРВИ </td> </tr>
    <tr> <td>II</td> <td>319 км 5 пк - 322 км 2 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>319 км 5 пк - 320 км 4 пк</td> <td>80</td> <td>70</td> <td>70</td> <td></td> </tr>
    <tr> <td></td> <td>320 км 4 пк - 5 пк стрелочный перевод</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td></td> <td>320 км 6 пк - 321 км 6 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td></td> <td></td> <td>25</td> </tr>
    <tr> <td></td> <td>стр.пер. № 1, 3 бок. напр.</td> <td></td> <td></td> <td></td> <td></td>  </tr>
    <tr> <td colSpan="6">    ЯНИСЪЯРВИ - ЛЕППЯСЮРЬЯ </td> </tr>
    <tr> <td>I</td> <td>322 км 2 пк - 342 км 7 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>323 км 2 пк - 3 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td colSpan="6">    ЛЕППЯСЮРЬЯ </td> </tr>
    <tr> <td>I</td> <td>342 км 7 пк - 344 км 4 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td></td> <td></td> <td> </td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ЛЕППЯСЮРЬЯ - РАЙКОНКОСКИ </td> </tr>
    <tr> <td>I</td> <td>344 км 4 пк - 356 км 8 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    РАЙКОНКОСКИ </td> </tr>
    <tr> <td>I</td> <td>356 км 8 пк - 358 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td></td> <td></td> <td>25</td> </tr>
    <tr> <td colSpan="6">    РАЙКОНКОСКИ - ЛОЙМОЛА </td> </tr>
    <tr> <td>I</td> <td>358 км 5 пк - 368 км 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ЛОЙМОЛА </td> </tr>
    <tr> <td>I</td> <td>368 км 10 пк - 370 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td></td> <td></td> <td>25</td> </tr>
    <tr> <td colSpan="6">    ЛОЙМОЛА - ПИЙТСИЕКИ </td> </tr>
    <tr> <td>I</td> <td>370 км 5 пк - 389 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    ПИЙТСИЕКИ </td> </tr>
    <tr> <td>I</td> <td>389 км 1 пк - 390 км 4 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td colSpan="6">    ПИЙТСИЕКИ - СУОЯРВИ I </td> </tr>
    <tr> <td>I</td> <td>390 км 4 пк - 402 км 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td colSpan="6">    СУОЯРВИ I </td> </tr>
    <tr> <td>I</td> <td>402 км 10 пк - 404 км 9 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
    <tr> <td></td> <td>403 км 1 пк - 2 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>403 км 2 пк - 3 пк стрелочный перевод</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
    <tr> <td></td> <td>403 км 3 пк - 404 км 6 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>404 км 6 пк - 9 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
    <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td></td> <td></td> <td>25</td> </tr>
    <tr> <td></td> <td>стр.пер. № 7</td> <td> </td> <td></td> <td></td> <td>40</td> </tr>
    <tr> <td></td> <td>съезд 22/24, стр.пер. № 14 бок. напр. для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
</React.Fragment>;