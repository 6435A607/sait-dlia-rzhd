import React from 'react';
import TableSpeed from './tablespeed.jsx';

export default function SpeedGatchinaFrezer(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">Гатчина-Варшавская - Фрезерный</h4>
      <br />
      <TableSpeed tbody={data} />
    </React.Fragment>
  );
}

const data = <React.Fragment>
<tr> <td colSpan="6">    ГАТЧИНА-ВАРШАВСКАЯ</td> </tr>
<tr> <td>I</td> <td>1 км 1 пк - 2 пк</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
<tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
<tr> <td colSpan="6">    ГАТЧИНА-ВАРШАВСКАЯ - ФРЕЗЕРНЫЙ</td> </tr>
<tr> <td>I</td> <td>1 км 2 пк - 9 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
<tr> <td colSpan="6">    ФРЕЗЕРНЫЙ</td> </tr>
<tr> <td>I</td> <td>1 км 9 пк - 2 км 5 пк </td> <td>50</td> <td>50</td> <td>50</td> <td> </td> </tr>
<tr> <td></td> <td>2 км 4 пк - 5 пк съезд 2/4</td> <td>50</td> <td>40</td> <td>40</td> <td> </td> </tr>
<tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
<tr> <td></td> <td>путь 3, 5, 7, 9, 11, 13</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
<tr> <td></td> <td>съезды 10/12, 6/8</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
</React.Fragment>;