import React from 'react';
import TableSpeed from './tablespeed.jsx';
import ButtonCollapseSpeed from './buttoncollapsespeed.jsx';

const data = <React.Fragment>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-ГЛАВНЫЙ</td> </tr>
  <tr> <td>III тов</td> <td>1 км 10 пк - 2 км 7 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td>IV тов</td> <td>1 км 7 пк - 2 км 7 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>съезды 8/13, 111/101/116, 9/11, 23/38, 21/22, 18/19, 63/111/101, 88/76-113/103, 83/84, 16/63, 560/558, 31/32, 576/578</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>прием на все перронные пути</td> <td>25</td> <td>25</td> <td>25</td> <td>25</td> </tr>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-ГЛАВНЫЙ - САНКТ-ПЕТЕРБУРГ-ТОВ-МОСКОВСКИЙ</td> </tr>
  <tr> <td>3т</td> <td>2 км 7 пк - 3 км 2 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td>4т</td> <td>2 км 7 пк - 10 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-ТОВ-МОСКОВСКИЙ</td> </tr>
  <tr> <td>III тов</td> <td>3 км 2 пк - 4 км 8 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td>VIт</td> <td>1 км 3 пк - 2 км 7 пк</td> <td> </td> <td>25</td> <td>25</td> <td></td> </tr>
  <tr> <td>50</td> <td>2 км 10 пк - 5 км 3 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 342/341, 350/351, 357/356, 347/344/346, 333/332, 353/352</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк А-Н</td> </tr>
  <tr> <td></td> <td>путь 51, 52, 53</td> <td> </td> <td> </td> <td> </td> <td>15</td> </tr>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-ТОВ-МОСКОВСКИЙ - САНКТ-ПЕТЕРБУРГ-СОРТ-МОСКОВСКИЙ</td> </tr>
  <tr> <td>3т</td> <td>4 км 8 пк - 5 км 3 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>4 км 8 пк - 10 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td>4т</td> <td>5 км 4 пк - 9 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-СОРТ-МОСКОВСКИЙ</td> </tr>
  <tr> <td>III</td> <td>5 км 3 пк - 10 км 2 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>8 км 7 пк - 8 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>8 км 9 пк - 9 км 2 пк кривые</td> <td>15</td> <td>15</td> <td>15</td> <td></td> </tr>
  <tr> <td></td> <td>9 км 3 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>9 км 7 пк - 10 км 2 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td>IV</td> <td>5 км 9 пк - 11 км 7 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>5 км 9 пк - 8 км 1 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>9 км 3 пк - 10 км 5 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>11 км 6 пк - 7 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td>V </td> <td>1 км 1 пк - 3 км 10 пк</td> <td> </td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td>VI</td> <td>3 км 5 пк - 4 км 4 пк</td> <td> </td> <td>25</td> <td>25</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 149/151, 541/547, 521/527, 515/517, 821/835, 801/803, 533/539</td> <td> </td> <td> </td> <td> </td> <td>15</td> </tr>
  <tr> <td></td> <td>съезды 575/211/213, 131/133, 145/185, 212/210, 214/216, 561/567, 532/526, 505/507, 522/520, 555/557, 529/531, 536/538, 624/602, 632/857, 640/642, 504/506, 146/136, 668/666, 164/162, 120/122, 664/662/670, 207/209, 571/573, 199/135, 225/223, 160/688</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>по всем глухим пересечениям на путях 1, 5 и 6 парка</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
  <tr> <td></td> <td>путь 12, 13, 14, 15, 16, 17, 18, 19, 110, 111, 112, 114</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 25, 26</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 51, 52, 53, 54, 55, 56, 57, 58, 59, 510, 511, 512, 513, 514, 515, 516, 517, 518, 519</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк четный приемо-отправочный парк № 6</td> </tr>
  <tr> <td></td> <td>путь 61, 62, 63, 64, 65, 66, 67, 68, 69, 610, 611, 612, 613</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк Обухово</td> </tr>
  <tr> <td></td> <td>путь 7</td> <td> </td> <td> </td> <td> </td> <td>15</td> </tr>
</React.Fragment>;

export default function SpeedSPSMObuhovo(props) {
  let id = "spsmobuhovo";
  return (
    <React.Fragment>
      <ButtonCollapseSpeed id={id} text='СПСМ (парк "Обухово")' />
      <div className="collapse" id={id} >
        <TableSpeed tbody={data} />
      </div>
    </React.Fragment>
  );
}

