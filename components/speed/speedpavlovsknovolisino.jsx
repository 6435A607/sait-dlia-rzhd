import React from 'react';
import TableSpeed from './tablespeed.jsx';

export default function SpeedPavlovskNovolisino(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">Павловск - Новолисино</h4>
      <br />
      <TableSpeed tbody={data} />
    </React.Fragment>
  );
}


const data = <React.Fragment>
<tr> <td colSpan="6">    ПАВЛОВСК </td> </tr>
<tr> <td>I</td> <td>28 км 2 пк - 29 км 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
<tr> <td></td> <td>28 км 2 пк - 3 пк</td> <td>25</td> <td>25</td> <td>25</td> <td></td> </tr>
<tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
<tr> <td></td> <td>съезд 6/8 для груз. поездов</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
<tr> <td></td> <td>съезды 1/3, 6/8 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
<tr> <td colSpan="6">    ПАВЛОВСК - НОВОЛИСИНО </td> </tr>
<tr> <td>I</td> <td>29 км 6 пк - 42 км 7 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
<tr> <td></td> <td>42 км 1 пк - 6 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
<tr> <td colSpan="6">    НОВОЛИСИНО </td> </tr>
<tr> <td>V</td> <td>42 км 7 пк - 44 км 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
<tr> <td></td> <td>42 км 10 пк - 43 км 10 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
<tr> <td></td> <td>43 км 10 пк стр.пер.бок.напр.</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
<tr> <td></td> <td>44 км 2 пк стр.пер.бок.напр.</td> <td>50</td> <td>40</td> <td>40</td> <td></td> </tr>
<tr> <td></td> <td>44 км 2 пк - 6 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
<tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
<tr> <td></td> <td>съезды 1/3, 5/7, 2/4, 12/14 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
<tr> <td></td> <td>пути № 5, 5А (от стр.пер. № 8 до стр.пер. № 31)</td>  <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
<tr> <td></td> <td>стр.пер. № 21 бок. напр.</td>  <td>50</td> <td>40</td> <td>40</td> <td></td> </tr>
</React.Fragment>;