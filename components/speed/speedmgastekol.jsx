import React from 'react';
import TableSpeed from './tablespeed.jsx';

export default function SpeedMgaStekol(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">Мга - Стекольный</h4>
      <br />
      <TableSpeed tbody={data} />
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    МГА </td> </tr>
  <tr> <td>I</td> <td>0 км 2 пк - 4 км 10 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>0 км 2 пк - 4 км 9 пк кривые</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>1 км 8 пк - 4 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>1 км 8 пк - 2 км 4 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 8</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>съезд 9/11 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td></td> <td>13 путь (от стр.пер. № 130 до вх.сигн. ЧН через стр.пер. № 128, 76, 70, 68, 56, 16 и 10) на ст.Невдубстрой</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>III, IIIБГ, IIIГ гл. пути (от стр.пер. № 89/89с до стр.пер. № 8 (ст.Блокпост 4 км) через стр.пер.         № 130, 106, 78, 54, 44, 30, 14 и 12) на ст. Войтоловка</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>IVБГ, IVГ гл. пути (от стр.пер. № 48 до стр.пер. № 6 (ст.Блокпост 4 км) через стр.пер. № 36) на ст.Войтоловка</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>IVA гл. путь (от стр.пер. № 45 до вх.сигн. IVНБ через стр.пер. № 39, 23/23с, 13/13с и 1) на ст.Сологубовка</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    МГА - ВОЙТОЛОВКА </td> </tr>
  <tr> <td>I</td> <td>5 км 1 пк - 7 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>5 км 1 пк - 8 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ВОЙТОЛОВКА </td> </tr>
  <tr> <td>I</td> <td>7 км 10 пк - 9 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>8 км 1 пк - 9 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ВОЙТОЛОВКА - ПУСТЫНЬКА </td> </tr>
  <tr> <td>I</td> <td>9 км 9 пк - 16 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>9 км 8 пк - 16 км 9 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>9 км 8 пк - 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПУСТЫНЬКА </td> </tr>
  <tr> <td>I</td> <td>16 км 10 пк - 19 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>16 км 10 пк - 19 км 3 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>17 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>17 км 1 пк - 18 км 7 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>18 км 7 пк - 19 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>19 км 2 пк - 19 км 3 пк</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ПУСТЫНЬКА - ОСТАНОВОЧНЫЙ ПУНКТ 22 КМ </td> </tr>
  <tr> <td>I</td> <td>19 км 3 пк - 22 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>19 км 4 пк - 19 км 9 пк кривые</td> <td>95</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>20 км 6 пк - 21 км 5 пк кривые</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>19 км 3 пк - 22 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>19 км 3 пк - 19 км 8 пк кривые</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>20 км 6 пк - 21 км 5 пк кривые</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ОСТАНОВОЧНЫЙ ПУНКТ 22 КМ </td> </tr>
  <tr> <td>I</td> <td>22 км 4 пк - 23 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>22 км 5 пк - 9 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>22 км 9 пк - 23 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>22 км 4 пк - 23 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>22 км 5 пк - 9 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>23 км 1 пк - 3 пк кривые</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>стр.пер. № 5, 11 бок. напр. для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ОСТАНОВОЧНЫЙ ПУНКТ 22 КМ - СТЕКОЛЬНЫЙ</td> </tr>
  <tr> <td>I</td> <td>23 км 1 пк - 32 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>I</td> <td>23 км 3 пк - 23 км 9 пк порожние вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>23 км 3 пк - 32 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>23 км 3 пк - 8 пк кривые</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>29 км 7 пк - 30 км 4 пк кривые</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    СТЕКОЛЬНЫЙ </td> </tr>
  <tr> <td>I</td> <td>32 км 1 пк - 33 км 9 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>33 км 8 пк - 9 пк съезд 6/8</td> <td>50</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td>IV</td> <td>32 км 1 пк - 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>съезд 2/4 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
</React.Fragment>;