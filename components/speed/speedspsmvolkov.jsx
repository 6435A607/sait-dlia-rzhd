import React from 'react';
import TableSpeed from './tablespeed.jsx';
import ButtonCollapseSpeed from './buttoncollapsespeed.jsx';

const data = <React.Fragment>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-ТОВ-МОСКОВСКИЙ</td> </tr>
  <tr> <td>IВолк</td> <td>1 км 1 пк - 4 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 342/341, 350/351, 357/356, 347/344/346, 333/332, 353/352</td> <td> </td> <td> </td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 51, 52, 53</td> <td> </td> <td> </td> <td></td> <td>15</td> </tr>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-ТОВ-МОСКОВСКИЙ - ВОЛКОВСКАЯ</td> </tr>
  <tr> <td>I</td> <td>1 км 4 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td colSpan="6">    ВОЛКОВСКАЯ</td> </tr>
  <tr> <td>IV</td> <td>1 км 4 пк - 2 км 7 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 2, съезд 11/12</td> <td> </td> <td> </td> <td></td> <td>15</td> </tr>
  <tr> <td colSpan="6">      Парк приемо-отправочный «Б»</td> </tr>
  <tr> <td></td> <td>путь 6, 7, 8, 9, съезд 41/43</td> <td> </td> <td> </td> <td></td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк сортировочно-отправочный «В»</td> </tr>
  <tr> <td></td> <td>путь 10, 11, 12, 13, 14, 15, 16</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
</React.Fragment>
export default function SpeedSPSMVolkov(props) {
  let id = "spsmvolkov";
  return (
    <React.Fragment>
      <ButtonCollapseSpeed id={id} text="СПСМ - Волковская" />
      <div className="collapse" id={id} >
        <TableSpeed tbody={data} />
      </div>
    </React.Fragment>
  );
}