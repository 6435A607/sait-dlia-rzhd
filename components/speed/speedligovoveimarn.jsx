import React from 'react';
import TableSpeed from './tablespeed.jsx';

export default function SpeedLigovoVeimarn(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">Лигово - Веймарн</h4>
      <br />
      <TableSpeed tbody={data} />
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    ЛИГОВО</td> </tr>
  <tr> <td>I </td> <td>14 км 2 пк - 15 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>14 км 2 пк - 15 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по переезду 15 км пк 2 </td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II </td> <td>13 км 10 пк - 15 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>13 км 10 пк - 14 км 2 пк стрелочный перевод</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>14 км 2 пк - 10 пк от стр.пер. № 8 до стр.пер.            № 55</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>по переезду 15 км пк 2</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    ЛИГОВО - СТРЕЛЬНА</td> </tr>
  <tr> <td>I</td> <td>15 км 10 пк - 22 км 7 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>15 км 5 пк - 23 км 2 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td colSpan="6">    СТРЕЛЬНА</td> </tr>
  <tr> <td>I</td> <td>22 км 7 пк - 24 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>23 км 6 пк - 24 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>23 км 2 пк - 24 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>23 км 5 пк - 24 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    СТРЕЛЬНА - НОВЫЙ ПЕТЕРГОФ</td> </tr>
  <tr> <td>I</td> <td>24 км 10 пк - 28 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>24 км 10 пк - 28 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    НОВЫЙ ПЕТЕРГОФ</td> </tr>
  <tr> <td>I</td> <td>28 км 4 пк - 30 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>28 км 8 пк - 29 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>29 км 5 пк - 10 пк стрелочный перевод</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>29 км 10 пк - 30 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>28 км 9 пк - 30 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>29 км 5 пк - 10 пк стрелочный перевод</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    НОВЫЙ ПЕТЕРГОФ - ОРАНИЕНБАУМ</td> </tr>
  <tr> <td>I</td> <td>30 км 3 пк - 39 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>30 км 3 пк - 39 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ОРАНИЕНБАУМ</td> </tr>
  <tr> <td>I</td> <td>39 км 3 пк - 42 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>39 км 9 пк - 40 км 4 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>40 км 5 пк - 8 пк </td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>40 км 9 пк - 42 км 3 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>42 км 3 пк - 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>39 км 3 пк - 40 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>39 км 8 пк - 40 км 4 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>40 км 5 пк - 7 пк </td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 4, 11, 13, 15</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 17, 19, 21</td> <td> </td> <td> </td> <td> </td> <td>15</td> </tr>
  <tr> <td colSpan="6">    ОРАНИЕНБАУМ - БРОНКА</td> </tr>
  <tr> <td>I</td> <td>42 км 6 пк - 46 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td colSpan="6">    БРОНКА</td> </tr>
  <tr> <td>I</td> <td>46 км 3 пк - 48 км 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 4, 5, 6, съезд 3/5</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    БРОНКА - БОЛЬШАЯ ИЖОРА</td> </tr>
  <tr> <td>I</td> <td>48 км 6 пк - 51 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    БОЛЬШАЯ ИЖОРА</td> </tr>
  <tr> <td>I</td> <td>51 км 5 пк - 53 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>51 км 5 пк - 52 км 9 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>52 км 10 пк - 53 км 2 пк</td> <td>85</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 2</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    БОЛЬШАЯ ИЖОРА - ЛЕБЯЖЬЕ</td> </tr>
  <tr> <td>I</td> <td>53 км 2 пк - 60 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>I</td> <td>53 км 2 пк - 53 км 4 пк</td> <td>85</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ЛЕБЯЖЬЕ</td> </tr>
  <tr> <td>I</td> <td>60 км 7 пк - 62 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>62 км 1 пк - 4 пк кривые</td> <td>90</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 3, 4</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 5, 12</td> <td> </td> <td> </td> <td> </td> <td>15</td> </tr>
  <tr> <td></td> <td>съезды 20/22, 8/18</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    ЛЕБЯЖЬЕ - КАЛИЩЕ</td> </tr>
  <tr> <td>I</td> <td>62 км 6 пк - 81 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>I</td> <td>79 км 6 пк - 80 км 6 пк</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>кривые 80 км пк 7 – 81 км пк 3</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    КАЛИЩЕ</td> </tr>
  <tr> <td>I</td> <td>81 км 5 пк - 82 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>81 км 8 пк - 82 км 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>82 км 7 пк стр.пер.№ 4</td> <td>70</td> <td>70</td> <td>70</td> <td> </td> </tr>
  <tr> <td></td> <td>82 км 8 пк - 9 пк кривые</td> <td>90</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 4</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    КАЛИЩЕ - КОПОРЬЕ</td> </tr>
  <tr> <td>I</td> <td>82 км 10 пк - 100 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>82 км 10 пк - 83 км 6 пк кривые</td> <td>85</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>85 км 6 пк - 9 пк кривые</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>99 км 2 пк - 6 пк кривые</td> <td>85</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    КОПОРЬЕ</td> </tr>
  <tr> <td>I</td> <td>100 км 3 пк - 101 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>100 км 4 пк ж.д. переезд головой поезда</td> <td>20</td> <td>20</td> <td>20</td> <td> </td> </tr>
  <tr> <td></td> <td>100 км 5 пк - 101 км 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    КОПОРЬЕ - КОТЛЫ</td> </tr>
  <tr> <td>I</td> <td>101 км 6 пк - 119 км 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>I</td> <td>101 км 9 ж.д. переезд головой поезда</td> <td>20</td> <td>20</td> <td>20</td> <td></td> </tr>
  <tr> <td colSpan="6">    КОТЛЫ</td> </tr>
  <tr> <td>I</td> <td>119 км 10 пк - 121 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>120 км 5 пк - 120 км 7 пк кривая</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    КОТЛЫ - КОТЛЫ II</td> </tr>
  <tr> <td>I</td> <td>121 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    КОТЛЫ II</td> </tr>
  <tr> <td>III</td> <td>121 км 10 пк - 123 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>IА</td> <td>от стр.пер. №11 до стр.пер. №5</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>123 км 4 пк - 123 км 8 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>IIА</td> <td>от стр. № 15 до стр. №1</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 2/4, 12/14 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td></td> <td>съезд 5/7</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">    КОТЛЫ II - РАЗЪЕЗД 135 КМ</td> </tr>
  <tr> <td>I</td> <td>123 км 8 пк - 134 км 5 пк</td> <td>100</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>123 км 8 пк - 131 км 4 пк строительные работы</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>131 км 5 пк - 133 км 7 пк строительные работы</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>133 км 8 пк - 134 км 5 пк строительные работы</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>123 км 8 пк - 134 км 9 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>123 км 8 пк - 124 км 2 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    РАЗЪЕЗД 135 КМ</td> </tr>
  <tr> <td>I</td> <td>134 км 5 пк - 137 км 9 пк</td> <td>80</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td>II</td> <td>134 км 9 пк - 137 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>съезды 5/7, 6/8 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    РАЗЪЕЗД 135 КМ - ВЕЙМАРН</td> </tr>
  <tr> <td>I</td> <td>137 км 9 пк - 141 км 4 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>137 км 3 пк - 141 км 4 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ВЕЙМАРН</td> </tr>
  <tr> <td>I</td> <td>141 км 4 пк - 149 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>141 км 4 пк - 148 км 1 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>149 км 6 пк - 149 км 8 пк стрелочные переводы</td> <td>70</td> <td>70</td> <td>70</td> <td> </td> </tr>
  <tr> <td>II</td> <td>141 км 4 пк - 149 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>141 км 4 пк - 149 км 7 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>от стр.пер. № 21 до стр.пер. № 11 через стр.пер. № 13</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>IV</td> <td>IV гл. путь 121 км 10 пк - 123 км 5 пк (от стр.пер. № 21 до стр.пер. № 4 через стр.пер. № 46, 40, 30, 24, 20 и 6)</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>X</td> <td>X гл. путь 216 км 4 пк - 216 км 6 пк (от стр.пер. № 13 до стр.пер. № 25 через стр.пер. № 17,23)</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td>X</td> <td>X гл. путь (от стр.пер. № 25 до стр.пер. № 2 через стр.пер. № 42, 32, 18 и 54)</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 6/8, 10/12, 14/16, 26/28, 101/103, 105/107, 102/104, 114/116 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td></td> <td>121 км 9 пк - 121 км 10 пк стр.пер.№ 21 бок.напр.</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>"парк ""В"" путь 6, 8, 22, съезды 110/112, 109/111"</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
</React.Fragment>;