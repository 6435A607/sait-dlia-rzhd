import React from 'react';
import TableSpeed from './tablespeed.jsx';

export default function SpeedLugaBatetsk(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">Луга I - Батецкая</h4>
      <br />
      <TableSpeed tbody={data} />
    </React.Fragment>
  );
}

const data = <React.Fragment>
<tr> <td colSpan="6">    ЛУГА I </td> </tr>
<tr> <td>IV</td> <td>1 км 5 пк - 2 км 9 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
<tr> <td></td> <td>1 км 5 пк - 2 км 4 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
<tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
<tr> <td></td> <td>съезды 79/81, 2/4, 87/89, 34/36</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
<tr> <td></td> <td>съезд 5/5а для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
<tr> <td></td> <td>парк «Южный»</td> <td>40</td> <td>40</td> <td>40</td> <td>40</td> </tr>
<tr> <td></td> <td>парк «Южный» пути № 6, 8, 10, съезды 8/10, 38/40, стр.пер. № 14, 16, 42 бок. напр.</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
<tr> <td></td> <td>парк «Северный» пути № 4, 7, 9</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
<tr> <td></td> <td>парк «Северный» стр.пер. № 13, 15 бок. напр., пути 3, 5, 6</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
<tr> <td></td> <td>парк «Пассажирский» пути № 20, 22 и стр.пер. на них, пути 3, 5</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
<tr> <td colSpan="6">    ЛУГА I - ЛУГА II </td> </tr>
<tr> <td>I</td> <td>2 км 9 пк - 3 км 10 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
<tr> <td colSpan="6">    ЛУГА II </td> </tr>
<tr> <td>I</td> <td>3 км 10 пк - 5 км 2 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
<tr> <td></td> <td>4 км 2 пк - 5 км 1 пк</td> <td>50</td> <td>40</td> <td>40</td> <td></td> </tr>
<tr> <td></td> <td>5 км 1 пк - 2 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
<tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
<tr> <td colSpan="6">    ЛУГА II - БАТЕЦКАЯ </td> </tr>
<tr> <td>I</td> <td>5 км 2 пк - 35 км 6 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
<tr> <td></td> <td>5 км 2 пк - 18 км 10 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
<tr> <td></td> <td>23 км 1 пк - 34 км 10 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
<tr> <td></td> <td>35 км 1 пк - 6 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
<tr> <td colSpan="6">    БАТЕЦКАЯ </td> </tr>
<tr> <td>III</td> <td>35 км 6 пк - 40 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
<tr> <td></td> <td>35 км 6 пк - 37 км 5 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
<tr> <td></td> <td>1 гл. путь</td> <td></td> <td></td> <td></td> <td>70</td> </tr>
<tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
<tr> <td></td> <td>путь 6</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
</React.Fragment>;