import React from 'react';
import TableSpeed from './tablespeed.jsx';

export default function Speed116kmkukol(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">Блокпост 116 км - Куколь</h4>
      <br />
      <TableSpeed tbody={data} />
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    БЛОКПОСТ 116 КМ </td> </tr>
  <tr> <td>I</td> <td>116 км 5 пк - 8 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>116 км 6 пк - 117 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>съезды 2/4, 8/10, стр.пер. № 6, 14 бок. напр. для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td>III</td> <td>1 км 1 пк - 2 км 9 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>III гл. путь (от стр.пер. № 14 (1 км пк 1) до стр.пер. № 16 (1 км пк 5))</td> <td>80</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>III гл. путь (от стр.пер. № 16 (1 км пк 6) до стр.пер. № 3 (2 км пк 9))</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>1 км 1 пк - 3 пк</td> <td>80</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">    БЛОКПОСТ 116 КМ - ПОРОГИ </td> </tr>
  <tr> <td></td> <td>2 км 9 пк - 7 км 3 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>2 км 9 пк - 2 км 10 пк</td> <td>25</td> <td>25</td> <td>25</td> <td></td> </tr>
  <tr> <td>II</td> <td>1 км 3 пк - 7 км 3 пк</td> <td>120</td> <td>90</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>1 км 3 пк - 5 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>70</td> <td></td> </tr>
  <tr> <td></td> <td>6 км 5 пк – 7 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПОРОГИ </td> </tr>
  <tr> <td>I</td> <td>7 км 3 пк - 13 км 2 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>7 км 3 пк - 7 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>7 км 7 пк - 8 км 3 пк от стр.пер. № 2 до стр.пер. № 14</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>9 км 9 пк - 10 км 7 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>12 км 1 пк - 13 км 2 пк от сигн. Ч1В до сигн. НК</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td>II</td> <td>7 км 3 пк - 13 км 2 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>7 км 9 пк - 8 км 5 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>9 км 8 пк - 10 км 8 пк порожние грузовые вагоны</td> <td>80</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>12 км 8 пк - 13 км 2 пк от стр.пер. № 3 до сигн. НКД</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>съезд 5/7</td> <td>50</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>стр.пер. № 3 бок. напр.</td> <td>50</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПОРОГИ - КУКОЛЬ </td> </tr>
  <tr> <td>I</td> <td>13 км 2 пк - 19 км 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>19 км 6 пк – 10 пк </td> <td>80</td> <td>80</td> <td>70</td> <td></td> </tr>
  <tr> <td>II</td> <td>13 км 2 пк - 19 км 10 пк</td> <td>120</td> <td>90</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>19 км 6 пк - 10 пк</td> <td>80</td> <td>80</td> <td>70</td> <td></td> </tr>
  <tr> <td colSpan="6">    КУКОЛЬ </td> </tr>
  <tr> <td>II</td> <td>19 км 10 пк - 20 км 4 пк</td> <td>80</td> <td>80</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>20 км 1 пк - 4 пк кривые</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td>I</td> <td>19 км 10 пк - 20 км 4 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>20 км 1 пк - 3 пк кривые</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 1/3, 5/7, 6/8, 10/12</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
</React.Fragment>;