import React from 'react';
import TableSpeed from './tablespeed.jsx';

export default function SpeedGdovVeimarn(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">Гдов - Веймарн</h4>
      <br />
      <TableSpeed tbody={data} />
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    ГДОВ </td> </tr>
  <tr> <td>I</td> <td>115 км 1 пк - 116 км 5 пк</td> <td>25</td> <td>25</td> <td>25</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>15</td> </tr>
  <tr> <td colSpan="6">    ГДОВ - СЛАНЦЫ </td> </tr>
  <tr> <td>I</td> <td>116 км 5 пк - 156 км 10 пк</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td colSpan="6">    СЛАНЦЫ </td> </tr>
  <tr> <td>II</td> <td>156 км 10 пк - 159 км 1 пк</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 5</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
  <tr> <td colSpan="6">    СЛАНЦЫ - РУДНИЧНАЯ </td> </tr>
  <tr> <td>I</td> <td>159 км 1 пк - 164 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>159 км 1 пк - 7 пк кривые</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td colSpan="6">    РУДНИЧНАЯ </td> </tr>
  <tr> <td>I</td> <td>164 км 8 пк - 166 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>165 км 1 пк - 166 км 5 пк просроченный капитальный ремонт</td> <td>25</td> <td>25</td> <td>25</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    РУДНИЧНАЯ - ВЕРВЕНКА </td> </tr>
  <tr> <td>I</td> <td>166 км 7 пк - 179 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по переезду 173 км пк 3 головой поезда</td> <td>20</td> <td>20</td> <td>20</td> <td> </td> </tr>
  <tr> <td colSpan="6">    ВЕРВЕНКА </td> </tr>
  <tr> <td>I</td> <td>179 км 4 пк - 180 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ВЕРВЕНКА - ВЕЙМАРН </td> </tr>
  <tr> <td>I</td> <td>180 км 7 пк - 213 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>190 км 10 пк дефектность ИССО</td> <td>40</td> <td>25</td> <td>25</td> <td> </td> </tr>
  <tr> <td></td> <td>208 км 10 пк дефектность ИССО</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td colSpan="6">    ВЕЙМАРН </td> </tr>
  <tr> <td>X</td> <td>213 км 7 пк - 216 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>X</td> <td>X гл. путь 216 км 4 пк - 216 км 6 пк (от стр.пер. № 13 до стр.пер. № 25 через стр.пер. № 17,23)</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td>X</td> <td>X гл. путь (от стр.пер. № 25 до стр.пер. № 2 через стр.пер. № 42, 32, 18 и 54)</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 6/8, 10/12, 14/16, 26/28, 101/103, 105/107, 102/104, 114/116 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td></td> <td>стр.пер.№ 21 бок.напр.</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>Парк «В» путь 6, 8, 22, съезды 110/112, 109/111</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
</React.Fragment>;