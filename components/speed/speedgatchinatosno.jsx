import React from 'react';
import TableSpeed from './tablespeed.jsx';

export default function SpeedGatchinaTosno(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">Гатчина - Тосно</h4>
      <br />
      <TableSpeed tbody={data} />
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    ГАТЧИНА-ТОВАРНАЯ-БАЛТИЙСКАЯ </td> </tr>
  <tr> <td>I</td> <td>0 км 4 пк - 3 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>0 км 4 пк - 1 км 6 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>2 км 3 пк - 8 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>0 км 5 пк - 3 км 1 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>0 км 5 пк - 6 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>0 км 6 пк - 1 км 3 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>1 км 3 пк - 5 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>съезд 17/15</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>от стр.пер. № 15 до стр.пер. № 134</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>от стр.пер. № 134 до стр.пер. № 155</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>от стр.пер. № 155 до сигн. 1Н</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>съезд 7/9</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>от стр.пер. № 7 до стр.пер. № 118</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>от стр.пер. № 118 до стр.пер. № 125</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>от стр.пер. № 125 до стр.пер. № 111</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>съезды 18/20, 101/111 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td></td> <td>стр.пер. № 32, 34, 36  по пр. и бок. напр., № 26, 28 по бок. напр.</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">      Парк 1 </td> </tr>
  <tr> <td></td> <td>путь 3 (от стр.пер. № 17 через стр.пер.№ 27, 23 до сигнала ЧМ3 )</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 5 (от стр.пер.№ 27до сигнала ЧМ5)</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 7 (от стр.пер.№ 23 через стр.пер.№ 25, 35 до сигнала ЧМ7 )</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 7 (от сигнала ЧМ7 через стр.пер.                      № 34,32,28 до стр.пер. № 26)</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 9 (от стр. пер. № 35 до сигнала ЧМ9)</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 9 (от сигнала ЧМ9 до стр.пер. № 34)</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">      Парк 3П </td> </tr>
  <tr> <td></td> <td>путь 31, 32, 33, 34, 35</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td colSpan="6">    ГАТЧИНА-ТОВАРНАЯ-БАЛТИЙСКАЯ - ФРЕЗЕРНЫЙ </td> </tr>
  <tr> <td>I</td> <td>3 км 1 пк - 9 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>3 км 1 пк - 9 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ФРЕЗЕРНЫЙ </td> </tr>
  <tr> <td>I</td> <td>3 км 10 пк - 7 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>3 км 10 пк - 4 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>4 км 6 пк - 7 км 4 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>3 км 10 пк - 7 км 8 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>3 км 10 пк - 4 км 5 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>4 км 6 пк - 7 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 2/4, 6/8, 10/12 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td></td> <td>пути № 3, 5, 7, 9, 11, 13</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td colSpan="6">    ФРЕЗЕРНЫЙ - ВЛАДИМИРСКАЯ </td> </tr>
  <tr> <td>I</td> <td>7 км 8 пк - 16 км 9 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td>II</td> <td>7 км 8 пк - 16 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ВЛАДИМИРСКАЯ </td> </tr>
  <tr> <td>I</td> <td>16 км 9 пк - 19 км 4 пк</td> <td>100</td> <td>90</td> <td>90</td> <td></td> </tr>
  <tr> <td></td> <td>17 км 2 пк - 18 км 10 пк</td> <td>70</td> <td>70</td> <td>70</td> <td></td> </tr>
  <tr> <td></td> <td>19 км 1 пк - 4 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>16 км 9 пк - 19 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>17 км 3 пк - 19 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ВЛАДИМИРСКАЯ - НОВОЛИСИНО </td> </tr>
  <tr> <td>I</td> <td>19 км 4 пк - 26 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>19 км 4 пк - 6 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>19 км 4 пк - 26 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    НОВОЛИСИНО </td> </tr>
  <tr> <td>I</td> <td>27 км 1 пк - 30 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>27 км 3 пк - 27 км 6 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>27 км 7 пк - 30 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>27 км 1 пк - 30 км 3 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>27 км 2 пк - 29 км 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>29 км 10 пк - 30 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>пути № 5, 5А (от стр.пер. № 8 до стр.пер. № 31)</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>съезды 1/3, 5/7, 2/4, 12/14, стр.пер. № 21 бок. напр. для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td colSpan="6">    НОВОЛИСИНО - СТЕКОЛЬНЫЙ </td> </tr>
  <tr> <td>I</td> <td>30 км 3 пк - 32 км 7 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>30 км 3 пк - 32 км 7 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    СТЕКОЛЬНЫЙ </td> </tr>
  <tr> <td>II</td> <td>32 км 7 пк - 35 км 8 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>34 км 6 пк - 7 пк съезд</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>34 км 8 пк - 35 км 4 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>35 км 4 пк - 8 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>III</td> <td>32 км 7 пк - 33 км 3 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>съезд 1/3</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>съезд 2/4 для пасс. поездов</td> <td></td> <td></td> <td></td> <td>50</td> </tr>
  <tr> <td colSpan="6">    СТЕКОЛЬНЫЙ - ТОСНО </td> </tr>
  <tr> <td>II</td> <td>35 км 8 пк - 46 км 2 пк</td> <td>80</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>37 км 7 пк - 10 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ТОСНО </td> </tr>
  <tr> <td>IГ</td> <td>46 км 2 пк - 47 км 7 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>47 км 5 пк - 7 пк стр.пер.бок.напр.</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td></td> <td>ст.Тосно, 1Г гл. путь (от стр.пер. № 203 до стр.пер. № 208 (вкл) через стр.пер. № 205, 213 и 218)</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>от стр.пер. № 208 до стр.пер. № 105/105с (вкл) через стр.пер. № 202, 111 и 109</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td colSpan="6">      Парк Ш </td> </tr>
  <tr> <td></td> <td>путь 12, 14</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк А </td> </tr>
  <tr> <td></td> <td>путь 7А</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
  <tr> <td colSpan="6">      Парк Г </td> </tr>
  <tr> <td></td> <td>путь 4Г</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
</React.Fragment>;