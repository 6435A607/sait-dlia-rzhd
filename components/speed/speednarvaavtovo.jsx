import React from 'react';
import TableSpeed from './tablespeed.jsx';
import ButtonCollapseSpeed from './buttoncollapsespeed.jsx';

export default function SpeedNarvaAvtovo(props) {
  return (
    <React.Fragment>
      <ButtonCollapseSpeed id="narvaavtovo" text="Нарвская - Автово" />
      <div className="collapse" id="narvaavtovo">
        <TableSpeed tbody={data} />
      </div>
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    НАРВСКАЯ </td> </tr>
  <tr> <td>I</td> <td>1 км 1 пк - 2 км 1 пк</td> <td></td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 2</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">    НАРВСКАЯ - АВТОВО </td> </tr>
  <tr> <td>I</td> <td>2 км 1 пк - 6 пк</td> <td></td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td colSpan="6">    АВТОВО </td> </tr>
  <tr> <td>I</td> <td>2 км 6 пк - 4 км 5 пк</td> <td></td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
</React.Fragment>;