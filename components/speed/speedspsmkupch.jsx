import React from 'react';
import TableSpeed from './tablespeed.jsx';
import ButtonCollapseSpeed from './buttoncollapsespeed.jsx';

export default function SpeedSPSMKupch(props) {
  let id = "spsmkupch";
  return (
    <React.Fragment>
      <ButtonCollapseSpeed id={id} text="СПСМ - Купчинская" />
      <div className="collapse" id={id}>
        <TableSpeed tbody={data} />
      </div>
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-СОРТ-МОСКОВСКИЙ</td> </tr>
  <tr> <td>III</td> <td>11 км 7 пк - 13 км 9 пк</td> <td> </td> <td>50</td> <td>50</td> <td> </td> </tr>
  <tr> <td>II</td> <td>12 км 6 пк - 13 км 6 пк</td> <td> </td> <td>50</td> <td>50</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 149/151, 521/527, 515/517, 533/539,  821/835, 801/803, 541/547</td> <td> </td> <td> </td> <td> </td> <td>15</td> </tr>
  <tr> <td></td> <td>съезды 575/211/213, 145/185, 212/210, 214/216, 561/567, 532/526, 522/520, 555/557, 571/573, 529/531, 536/538, 632/857, 640/642, 199/135, 504/506, 146/136, 164/162, 120/122, 664/662/670, 225/223, 207/209, 131/133, 505/507, 624/602, 668/666, 160/688</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>по всем глухим пересечениям на путях 1, 5 и 6 парка</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
  <tr> <td colSpan="6">      Парк приёмо-отправочный № 1</td> </tr>
  <tr> <td></td> <td>путь 11, 12, 13, 14, 15, 16, 17, 18, 19, 110, 111, 112, 114</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк приемо-отправочный № 2</td> </tr>
  <tr> <td></td> <td>путь 25, 26</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк нечетный приемо-отправления № 5</td> </tr>
  <tr> <td></td> <td>путь 51, 52, 53, 54, 55, 56, 57, 58, 59, 510, 511, 512, 513, 514, 515, 516, 517, 518, 519</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк четный приемо-отправочный № 6</td> </tr>
  <tr> <td></td> <td>путь 61, 62, 63, 64, 65, 66, 67, 68, 69, 610, 611, 612, 613</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк Обухово</td> </tr>
  <tr> <td></td> <td>путь 7</td> <td> </td> <td> </td> <td> </td> <td>15</td> </tr>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-СОРТ-МОСКОВСКИЙ - КУПЧИНСКАЯ</td> </tr>
  <tr> <td>II</td> <td>13 км 6 пк</td> <td> </td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td colSpan="6">    КУПЧИНСКАЯ</td> </tr>
  <tr> <td>I</td> <td>14 км 8 пк - 10 пк</td> <td> </td> <td>50</td> <td>50</td> <td> </td> </tr>
  <tr> <td>IIА</td> <td>13 км 6 пк - 14 км 9 пк</td> <td> </td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
</React.Fragment>;