import React from 'react';
import TableSpeed from './tablespeed.jsx';

export default function SpeedVolhovIIVolhovI(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">Волховстрой II - Волховстрой I (парк "Новооктябрьский")</h4>
      <br />
      <TableSpeed tbody={data} />
    </React.Fragment>
  );
}

const data = <React.Fragment>
<tr>    <td colSpan="6">ВОЛХОВСТРОЙ II - ВОЛХОВСТРОЙ I</td></tr>
<tr><td>III </td> <td>Волховстрой II – Волховстрой I (парк «Новооктябрьский»), 3 гл. путь от стр.пер. № 4 до стр.пер. № 309</td><td>40</td><td>40</td><td>40</td><td></td></tr>
<tr> <td colSpan="6">    ВОЛХОВСТРОЙ I</td></tr>
<tr> <td>IIIA</td><td>Волховстрой I (парк приема) – Блокпост 116 км, IIIA гл. путь (от стр.пер. № 8 ст.Волховстрой I до стр.пер. № 3 (вкл.) ст.Блокпост 116 км)</td><td>25</td><td>25</td><td>25</td><td></td> </tr>
<tr> <td>I П</td><td>ст.Волховстрой I (парк приема стр.пер. № 156 бок. напр.)</td><td></td><td>25</td><td>25</td><td></td> </tr>
<tr> <td></td><td>ст.Волховстрой I (парк приема – парк «Новооктябрьский», от стр.пер. № 156 до стр.пер. № 331, обходной путь)</td><td></td><td>60</td><td>60</td><td></td> </tr>
<tr> <td></td><td>ст.Волховстрой I (парк «Новооктябрьский» стр.пер. № 331 бок. напр.)</td><td></td><td>40</td><td>40</td><td></td> </tr>
</React.Fragment>