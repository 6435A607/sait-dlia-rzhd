import React from 'react';
import TableSpeed from './tablespeed.jsx';
import ButtonCollapseSpeed from './buttoncollapsespeed.jsx';

export default function SpeedKupchShushary(props) {
  return (
    <React.Fragment>
      <ButtonCollapseSpeed id="kupchshushary" text="Купчинская - Шушары" />
      <div id="kupchshushary" className="collapse">
      <TableSpeed tbody={data} />
      </div>
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    КУПЧИНСКАЯ </td> </tr>
  <tr> <td>III</td> <td>1 км 1 пк - 3 км 1 пк</td> <td></td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td colSpan="6">    КУПЧИНСКАЯ - ШУШАРЫ </td> </tr>
  <tr> <td>I</td> <td>3 км 1 пк - 4 пк</td> <td></td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td colSpan="6">    ШУШАРЫ </td> </tr>
  <tr> <td>XV</td> <td>3 км 4 пк - 6 км 2 пк</td> <td></td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>съезд 125/127</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
</React.Fragment>;