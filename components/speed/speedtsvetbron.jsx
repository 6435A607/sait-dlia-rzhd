import React from 'react';
import TableSpeed from './tablespeed.jsx';
import ButtonCollapseSpeed from './buttoncollapsespeed.jsx';

export default function SpeedTsvetBron(props) {
  let id = "tsvetbron";
  return (
    <React.Fragment>
      <ButtonCollapseSpeed id={id} text="Цветочная - Броневая" />
      <div className="collapse" id={id} >
        <TableSpeed tbody={data} />
      </div>
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    ЦВЕТОЧНАЯ </td> </tr>
  <tr> <td>I</td> <td>1 км 1 пк</td> <td>25</td> <td>25</td> <td>25</td> <td></td> </tr>
  <tr> <td colSpan="6">    ЦВЕТОЧНАЯ - БРОНЕВАЯ </td> </tr>
  <tr> <td>I</td> <td>1 км 1 пк - 7 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td colSpan="6">    БРОНЕВАЯ </td> </tr>
  <tr> <td>V(4по)</td> <td>1 км 7 пк - 2 км 3 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
</React.Fragment>;