import React from 'react';
import TableSpeed from './tablespeed.jsx';

export default function SpeedSPbIvan(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">СПб - Ивангород</h4>
      <br />
      <TableSpeed tbody={data} />
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-БАЛТИЙСКИЙ</td> </tr>
  <tr> <td>I</td> <td>1 км 1 пк - 8 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>1 км 1 пк - 6 пк</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>от тупиков до БМРЦ</td> <td>25</td> <td>25</td> <td>25</td> <td></td> </tr>
  <tr> <td>III</td> <td>3 км 1 пк - 8 пк</td> <td> </td> <td>15</td> <td>15</td> <td> </td> </tr>
  <tr> <td>V</td> <td>1 км 1 пк - 9 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>1 км 1 пк - 7 пк</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>съезды 12/15, 26/30</td> <td> </td> <td> </td> <td> </td> <td>15</td> </tr>
  <tr> <td colSpan="6">      Парк Д</td> </tr>
  <tr> <td></td> <td>путь 2, 3, 4, 5, 6, 7, 8, 9, 10, 11</td> <td> </td> <td> </td> <td> </td> <td>15</td> </tr>
  <tr> <td colSpan="6">    САНКТ-ПЕТЕРБУРГ-БАЛТИЙСКИЙ - БРОНЕВАЯ</td> </tr>
  <tr> <td>I</td> <td>1 км 8 пк - 3 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>1 км 8 пк - 1 км 10 пк</td> <td>90</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>2 км 1 пк - 2 км 2 пк</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>1 км 9 пк - 2 км 2 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>III</td> <td>3 км 8 пк</td> <td> </td> <td>15</td> <td>15</td> <td></td>  </tr>
  <tr> <td colSpan="6">    БРОНЕВАЯ</td> </tr>
  <tr> <td>III</td> <td>3 км 8 пк - 10 пк</td> <td> </td> <td>15</td> <td>15</td> <td> </td> </tr>
  <tr> <td>IIБ</td> <td>2 км 2 пк - 4 км 2 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>4 км 2 пк - 5 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>IБ</td> <td>3 км 7 пк - 5 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    БРОНЕВАЯ - ЛИГОВО</td> </tr>
  <tr> <td>I</td> <td>5 км 2 пк - 12 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>5 км 1 пк - 12 км 6 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td colSpan="6">    ЛИГОВО</td> </tr>
  <tr> <td>I п</td> <td>12 км 6 пк - 15 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>12 км 8 пк - 14 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>14 км 1 пк - 7 пк по стр.пер. № 43, 45, 55, 24, 22</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>14 км 9 пк - 14 км 8 пк кривые</td> <td>50</td> <td>50</td> <td>50</td> <td> </td> </tr>
  <tr> <td></td> <td>14 км 10 пк - 15 км 9 пк</td> <td>70</td> <td>70</td> <td>70</td> <td> </td> </tr>
  <tr> <td>IIп</td> <td>12 км 6 пк - 15 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>от стр.пер. № 3 до стр.пер. № 37, 12 км пк 7 – 13 км пк 9</td> <td>60</td> <td>50</td> <td>50</td> <td> </td> </tr>
  <tr> <td></td> <td>от стр.пер. № 37 до стр.пер. № 26, 13 км пк 10 – 14 км пк 6</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>14 км 7 пк - 10 пк кривые</td> <td>60</td> <td>50</td> <td>50</td> <td> </td> </tr>
  <tr> <td></td> <td>15 км 1 пк - 9 пк</td> <td>70</td> <td>70</td> <td>70</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    ЛИГОВО - ГОРЕЛОВО</td> </tr>
  <tr> <td>I</td> <td>15 км 9 пк - 20 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>15 км 9 пк - 16 км 1 пк</td> <td>70</td> <td>70</td> <td>70</td> <td></td> </tr>
  <tr> <td>II</td> <td>15 км 9 пк - 20 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>15 км 9 пк - 16 км 1 пк</td> <td>70</td> <td>70</td> <td>70</td> <td></td> </tr>
  <tr> <td colSpan="6">    ГОРЕЛОВО</td> </tr>
  <tr> <td>I</td> <td>20 км 5 пк - 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>20 км 5 пк - 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>20 км 7 пк - 9 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>съезд 6/8</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ГОРЕЛОВО - КРАСНОЕ СЕЛО</td> </tr>
  <tr> <td>I</td> <td>20 км 9 пк - 24 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>20 км 9 пк - 24 км 10 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    КРАСНОЕ СЕЛО</td> </tr>
  <tr> <td>I</td> <td>24 км 10 пк - 27 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>25 км 7 пк - 27 км 1 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>27 км 2 пк стр.пер. № 2 в кривой</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>27 км 2 пк - 5 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td>II</td> <td>24 км 10 пк - 27 км 2 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 3, 8</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    КРАСНОЕ СЕЛО - ТАЙЦЫ</td> </tr>
  <tr> <td>I</td> <td>27 км 5 пк - 33 км 9 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td colSpan="6">    ТАЙЦЫ</td> </tr>
  <tr> <td>I</td> <td>33 км 9 пк - 35 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>33 км 9 пк - 34 км 10 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ТАЙЦЫ - ПУДОСТЬ</td> </tr>
  <tr> <td>I</td> <td>35 км 3 пк - 39 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ПУДОСТЬ</td> </tr>
  <tr> <td>I</td> <td>39 км 9 пк - 41 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>40 км 3 пк - 41 км 3 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ПУДОСТЬ - ГАТЧИНА-ПАССАЖИРСКАЯ-БАЛТИЙСКАЯ</td> </tr>
  <tr> <td>I</td> <td>41 км 6 пк - 45 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ГАТЧИНА-ПАССАЖИРСКАЯ-БАЛТИЙСКАЯ</td> </tr>
  <tr> <td>I</td> <td>45 км 8 пк - 47 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>46 км 2 пк - 47 км 4 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>47 км 4 пк - 8 пк</td> <td></td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ГАТЧИНА-ПАССАЖИРСКАЯ-БАЛТИЙСКАЯ - ГАТЧИНА-ТОВАРНАЯ-БАЛТИЙСКАЯ </td> </tr>
  <tr> <td>I</td> <td>47 км 8 пк - 48 км 3 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td colSpan="6">    ГАТЧИНА-ТОВАРНАЯ-БАЛТИЙСКАЯ</td> </tr>
  <tr> <td>IБ</td> <td>48 км 3 пк - 49 км 1 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>50 км 1 пк - 51 км 7 пк</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>51 км 7 пк - 52 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>от стр.пер. № 15 до стр.пер. № 134</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>от стр.пер. № 134 до стр.пер. № 155</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>от стр.пер. № 155 до сигн. 1Н</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>съезд 7/9</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>от стр.пер. № 7 до стр.пер. № 118</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>от стр.пер. № 118 до стр.пер. № 125</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>от стр.пер. № 125 до стр.пер. № 111</td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td>II</td> <td>49 км 9 пк - 51 км 10 пк</td> <td>60</td> <td>60</td> <td>60</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезд 17/15</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>съезды 18/20, 101/111 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td></td> <td>стр.пер. № 32, 34, 36  по пр. и бок. напр., № 26, 28 по бок. напр.</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">      Парк 1</td> </tr>
  <tr> <td></td> <td>путь 3 (от стр.пер. № 23 через стр.пер.№ 27 до сигнала ЧМ3 )</td> <td></td> <td></td> <td></td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 5 (от стр.пер.№ 27до сигнала ЧМ5)</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 7 (от стр.пер.№ 23 через стр.пер.№ 25, 35 до сигнала ЧМ7 )</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 9 (от стр. пер. № 35 до сигнала ЧМ9)</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">      Парк 3</td> </tr>
  <tr> <td></td> <td>путь 31, 32, 33, 34, 35</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    ГАТЧИНА-ТОВАРНАЯ-БАЛТИЙСКАЯ - ВОЙСКОВИЦЫ</td> </tr>
  <tr> <td>I</td> <td>52 км 1 пк - 54 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>51 км 10 пк - 54 км 9 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ВОЙСКОВИЦЫ</td> </tr>
  <tr> <td>I</td> <td>54 км 9 пк - 57 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>55 км 3 пк - 57 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по переезду 57 км пк 2 головой поезда</td> <td>80</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>54 км 9 пк - 57 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>55 км 2 пк - 56 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по переезду 57 км пк 2 головой поезда</td> <td>80</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезд 16/18 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ВОЙСКОВИЦЫ - ЕЛИЗАВЕТИНО</td> </tr>
  <tr> <td>I</td> <td>57 км 4 пк - 66 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>57 км 4 пк - 66 км 10 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>57 км 4 пк - 58 км 10 пк ж.д. переезд</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ЕЛИЗАВЕТИНО</td> </tr>
  <tr> <td>I</td> <td>66 км 10 пк - 69 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>67 км 5 пк - 68 км 9 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>66 км 10 пк - 69 км 4 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>67 км 4 пк - 68 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 1/3, 2/4, 10/12 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ЕЛИЗАВЕТИНО - КИКЕРИНО</td> </tr>
  <tr> <td>I</td> <td>69 км 4 пк - 75 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>69 км 4 пк - 75 км 5 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    КИКЕРИНО</td> </tr>
  <tr> <td>I</td> <td>75 км 5 пк - 78 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>75 км 10 пк - 78 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>75 км 5 пк - 78 км 1 пк</td> <td>120</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>75 км 9 пк - 78 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 1/3, 5/7 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    КИКЕРИНО - ВОЛОСОВО</td> </tr>
  <tr> <td>I</td> <td>78 км 1 пк - 84 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>78 км 1 пк - 84 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ВОЛОСОВО</td> </tr>
  <tr> <td>I</td> <td>84 км 8 пк - 87 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>84 км 6 пк - 87 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 2/4, 8/10, 11/13, 15/17 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ВОЛОСОВО-ВРУДА</td> </tr>
  <tr> <td>I</td> <td>87 км 1 пк - 95 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>87 км 1 пк - 95 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ВРУДА</td> </tr>
  <tr> <td>I</td> <td>95 км 10 пк - 98 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>95 км 10 пк - 98 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ВРУДА-МОЛОСКОВИЦЫ</td> </tr>
  <tr> <td>I</td> <td>98 км 3 пк - 109 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>98 км 3 пк - 109 км 4 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    МОЛОСКОВИЦЫ</td> </tr>
  <tr> <td>I</td> <td>109 км 4 пк - 111 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td>II</td> <td>109 км 4 пк - 111 км 8 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезды 1/3, 2/4 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    МОЛОСКОВИЦЫ - ВЕЙМАРН</td> </tr>
  <tr> <td>I</td> <td>111 км 5 пк - 119 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>II</td> <td>111 км 8 пк - 119 км 10 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ВЕЙМАРН</td> </tr>
  <tr> <td>II</td> <td>119 км 10 пк - 121 км 7 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td>IV</td> <td>119 км 10 пк - 123 км 5 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>ст.Веймарн 121 км 6 пк (от стр.пер. № 3 до стр.пер. № 9)</td> <td>70</td> <td>70</td> <td>70</td> <td></td> </tr>
  <tr> <td></td> <td>121км 6 пк - 121 км 7 пк от стр.пер. № 9 до стр.пер. № 11</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td></td> <td>121км 7 пк - 121 км 8 пк от стр.пер. № 11 до   стр.пер. № 21 через стр.пер. № 13</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>IV гл. путь 121 км 10 пк - 123 км 5 пк (от стр.пер. № 21 до стр.пер. № 4 через стр.пер. № 46, 40, 30, 24, 20 и 6)</td> <td>70</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>121 км 9 пк - 121 км 10 пк стр.пер.№ 21 бок.напр.</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>Парк «В»  путь 6, 8, 22, съезды 110/112, 109/111</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>съезды 6/8, 10/12, 14/16, 26/28, 101/103, 105/107, 102/104, 114/116 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td colSpan="6">    ВЕЙМАРН - КИНГИСЕПП</td> </tr>
  <tr> <td>I</td> <td>123 км 5 пк - 135 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>123 км 5 пк - 9 пк строительные работы</td> <td>70</td> <td>70</td> <td>70</td> <td></td> </tr>
  <tr> <td></td> <td>123 км 10 пк - 127 км 6 пк дефектность рельсов</td> <td>70</td> <td>70</td> <td>70</td> <td></td> </tr>
  <tr> <td colSpan="6">    КИНГИСЕПП</td> </tr>
  <tr> <td>I</td> <td>135 км 2 пк - 137 км 6 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>135 км 7 пк - 135 км 9 пк кривые</td> <td>70</td> <td>70</td> <td>70</td> <td></td> </tr>
  <tr> <td></td> <td>137 км 1 пк - 137 км 3 пк кривые</td> <td>70</td> <td>70</td> <td>70</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    КИНГИСЕПП - САЛА</td> </tr>
  <tr> <td>I</td> <td>137 км 6 пк - 146 км 1 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    САЛА</td> </tr>
  <tr> <td>I</td> <td>146 км 1 пк - 148 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 5, 6</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td colSpan="6">    САЛА - ИВАНГОРОД-НАРВСКИЙ</td> </tr>
  <tr> <td>I</td> <td>148 км 3 пк - 156 км 3 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td colSpan="6">    ИВАНГОРОД-НАРВСКИЙ</td> </tr>
  <tr> <td>I</td> <td>156 км 3 пк - 159 км 2 пк</td> <td>100</td> <td>80</td> <td>80</td> <td></td> </tr>
  <tr> <td></td> <td>156 км 7 пк - 159 км 2 пк </td> <td>60</td> <td>60</td> <td>60</td> <td></td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 4</td> <td></td> <td></td> <td></td> <td>15</td> </tr>
  <tr> <td colSpan="6">    ИВАНГОРОД-НАРВСКИЙ - НАРВА</td> </tr>
  <tr> <td>I</td> <td>159 км 2 пк</td> <td>60</td> <td>60</td> <td>60</td> <td></td></tr>
</React.Fragment>;