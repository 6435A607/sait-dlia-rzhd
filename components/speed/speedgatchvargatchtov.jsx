import React from 'react';
import TableSpeed from './tablespeed.jsx';

export default function SpeedGatchVarGatchTov(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">Гатчина-Варшавская (парк «Б») – Гатчина-Товарная-Балтийская</h4>
      <br />
      <TableSpeed tbody={data} />
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    ГАТЧИНА-ВАРШАВСКАЯ</td> </tr>
  <tr> <td>IА</td> <td>0 км 1 пк - 3 пк</td> <td>50</td> <td>50</td> <td>50</td> <td> </td> </tr>
  <tr> <td></td> <td>0 км 1 пк - 2 пк стр.пер. № 42 бок. напр.</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ГАТЧИНА-ВАРШАВСКАЯ - ГАТЧИНА-ТОВАРНАЯ-БАЛТИЙСКАЯ</td> </tr>
  <tr> <td>I</td> <td>0 км 3 пк - 6 пк</td> <td>50</td> <td>50</td> <td>50</td> <td></td> </tr>
  <tr> <td colSpan="6">    ГАТЧИНА-ТОВАРНАЯ-БАЛТИЙСКАЯ</td> </tr>
  <tr> <td>IC</td> <td>0 км 10 пк - 1 км 1 пк</td> <td>50</td> <td>50</td> <td>50</td> <td> </td> </tr>
  <tr> <td></td> <td>1 км 1 пк стр.пер. № 105 бок. напр.</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезд 17/15</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>съезды 18/20, 101/111</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
  <tr> <td></td> <td>стр.пер. № 32, 34, 36  по пр. и бок. напр., № 26, 28 по бок. напр.</td> <td></td> <td></td> <td></td> <td>40</td> </tr>
  <tr> <td colSpan="6">      Парк 1</td> </tr>
  <tr> <td></td> <td>путь 3, 5, 7, 9</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
  <tr> <td></td> <td>путь 3 (от сигнала ЧМ3 через стр.пер.№36, 32 по стр. пер. №28)</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>путь 5 (от сигнала ЧМ5 по стр. пер. № 36)</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">      Парк 3</td> </tr>
  <tr> <td></td> <td>путь 31, 32, 33, 34, 35</td> <td> </td> <td> </td> <td> </td> <td>25</td> </tr>
</React.Fragment>;