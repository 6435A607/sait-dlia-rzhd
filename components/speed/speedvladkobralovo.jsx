import React from 'react';
import TableSpeed from './tablespeed.jsx';

export default function SpeedVladKobralovo(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">Владимирская - Кобралово</h4>
      <br />
      <TableSpeed tbody={data} />
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr> <td colSpan="6">    ВЛАДИМИРСКАЯ</td> </tr>
  <tr> <td>I</td> <td>1 км 1 пк - 3 пк</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td colSpan="6">    ВЛАДИМИРСКАЯ - КОБРАЛОВО </td> </tr>
  <tr> <td>I</td> <td>1 км 3 пк - 2 км 6 пк</td> <td>40</td> <td>40</td> <td>40</td> <td></td> </tr>
  <tr> <td colSpan="6">    КОБРАЛОВО</td> </tr>
  <tr> <td>IV</td> <td>2 км 6 пк - 10 пк</td> <td>40</td> <td>40</td> <td>40</td> <td> </td> </tr>
  <tr> <td></td> <td>по приемо-отправочным путям и съездам</td> <td> </td> <td> </td> <td> </td> <td>40</td> </tr>
  <tr> <td></td> <td>съезд 6/8 для пасс. поездов</td> <td> </td> <td> </td> <td> </td> <td>50</td> </tr>
</React.Fragment>;