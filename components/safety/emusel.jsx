import React from 'react';

export default function EMUzel(props) {
  return (
    <>
      <h4 className="text-center">Электронная карта. Узел</h4>
      <br />
      <img className="img-fluid img-thumbnail mx-auto d-block" alt="image" src="../../img/emuzel.JPG" />
    </>
  );
}