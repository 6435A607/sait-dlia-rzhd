import React from 'react';

export default function BlokTable6_9(props) {
  return (
    <>
      <table className="table table-striped table-bordered table-secondary">
        <thead>
          <tr><th>Индикация монитора</th><th>Соответствующий модуль</th></tr>
        </thead>
        <tbody>
          <tr><td>1</td><td>БС-ДПС-CAN</td></tr>
          <tr><td>2</td><td>Монитор 5.2</td></tr>
          <tr><td>3</td><td>ИПД</td></tr>
          <tr><td>4</td><td>МП-АЛС</td></tr>
          <tr><td>5</td><td>ЭК (при наличии приема по каналу GPS)</td></tr>
          <tr><td>6</td><td>ММ</td></tr>
          <tr><td>7</td><td>УКТОЛ</td></tr>
          <tr><td>8</td><td>САУТ</td></tr>
          <tr><td>9</td><td>РК</td></tr>
          <tr><td>А</td><td>ЭПК151Д</td></tr>
          <tr><td>В</td><td>ТСКБМ</td></tr>
          <tr><td>С</td><td>Система МСУЛ</td></tr>
          <tr><td>Примечания:</td></tr>
          <tr><td>- Для 2ЭС6 - код "А" отсутствует;</td></tr>
          <tr><td colSpan="2">- Для ЭС1, ЭС2Г, ЭГ2Тв - код "7" соответствует "ВДС", буква С соответствует "Системе Управления".</td></tr>
        </tbody>
      </table>
    </>
  );
}