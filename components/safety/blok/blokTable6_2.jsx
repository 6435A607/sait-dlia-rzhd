import React from 'react';

export default function BlokTable6_2(props) {
  return (
    <>
      <table className="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th>Наименование блока</th> <th>Установка пломб. Место</th> <th>Кол-во пломб</th> <th>Тип пломбы</th>
          </tr>
        </thead>
        <tbody>
          <tr> <td>ДПС-У</td>  <td>Болт крышки</td> <td>1</td> <td>Навесная</td> </tr>
          <tr> <td rowSpan="2">СШ</td> <td>Корпус</td> <td>1</td> <td>Наклейка</td> </tr>
          <tr> <td>Соединители</td> <td>6</td> <td>навесная</td> </tr>
          <tr> <td>Разобщительный кран ТМ</td> <td>Фиксатор открытого положения</td> <td>1</td> <td>Навесная</td> </tr>
          <tr> <td>ЭПК*</td> <td>Болт кожуха</td> <td>1</td> <td>Навесная</td> </tr>
          <tr> <td rowSpan="2">КОН*</td> <td>Болт кожуха</td> <td>1</td> <td>Навесная</td> </tr>
          <tr> <td>Соединитель</td> <td>1</td> <td>Навесная</td> </tr>
          <tr> <td>МВ</td> <td>Корпус</td> <td>1</td> <td>Наклейка</td> </tr>
          <tr> <td>БИЛ</td> <td>Корпус</td> <td>1</td> <td>Наклейка</td> </tr>
          <tr> <td>МСС</td> <td>Корпус</td> <td>1</td> <td>Наклейка</td> </tr>
          <tr> <td>БС-СН/БЛОК (БР-У)</td>  <td>Корпус</td> <td>1</td> <td>Наклейка</td> </tr>
          <tr> <td rowSpan="2">АЛС-ТКС</td> <td>Корпус</td> <td>1</td> <td>Наклейка</td> </tr>
          <tr> <td>Соединители</td> <td>1</td> <td>Навесная</td> </tr>
          <tr> <td>БС-ДПС/М-CAN (БС-ДПС/М-БЗС-CAN)</td> <td>Корпус</td> <td>1</td>  <td>Наклейка</td> </tr>
          <tr>
            <td>Рукоятки РБ, РБС, РБП</td>
            <td>Болт крышки</td>
            <td>1</td>
            <td>Навесная</td>
          </tr>
          <tr>
            <td>ТСКБМ-ПCAN</td>
            <td>Корпус</td>
            <td>1</td>
            <td>Мастика в углублении корпуса</td>
          </tr>
          <tr>
            <td>ТСКБМ-Н</td>
            <td>Корпус</td>
            <td>1</td>
            <td>Налейка</td>
          </tr>
          <tr>
            <td>Узел стыковки КП-РС</td>
            <td>Корпус</td>
            <td>1</td>
            <td>Навесная</td>
          </tr>
          <tr>
            <td>ШЛЮЗ-CAN -MVB2*</td>
            <td>Корпус</td>
            <td>1</td>
            <td>Навесная</td>
          </tr>
          <tr>
            <td>Клапан 266-1*</td>
            <td>Корпус</td>
            <td>1</td>
            <td>навесная</td>
          </tr>
          <tr><td colSpan="4">* при наличии</td></tr>
        </tbody>
      </table>
    </>
  )
}