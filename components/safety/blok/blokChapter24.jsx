import React from 'react';

export default function blokChapter24(props) {
  return(
    <>
      <h4 className="text-center">Движение поездов по неправильному пути по сигналам автоматической локомотивной сигнализации.</h4>
      <br />
      <p>При организации двустороннего движения на двухпутных (многопутных) перегонах, оборудованных по каждому пути автоблокировкой в одном направлении, следование локомотивов (МВПС) осуществляется в правильном направлении по сигналам автоматической блокировки, а по неправильному пути - по сигналу "Б" на МСС, БИЛ-УМВ, БИЛ-ПМВ.</p>
      <p>Движение локомотивов (МВПС) по неправильному пути по показанию "Б" на МСС, БИЛ-УМВ, БИЛ-ПМВ осуществляется в соответствии с приказом начальника дороги.</p>
    </>
  )
}