import React from 'react';
import BlokTable6_1 from './blokTable6_1.jsx';
import BlokTable6_2 from './blokTable6_2.jsx';

export default function blokChapter1(props) {
  return (
    <>
      <h4 className="text-center">БЛОК. Общие положения, описание, функции.</h4>
      <br />
      <p>Комплекс БЛОК является модульным изделием. Применяется на электровозах постоянного и переменного тока, двухсистемных локомотивах, магистральных и маневровых тепловозах, газотурбовозах, специальном подвижном составе, в том числе и на комбинированном ходу и мотор-вагонном подвижном составе (МВПС). В состав комплекса входит базовая часть и компоненты, обеспечивающие адаптацию комплекса к конкретному типу подвижного состава, а так же взаимодействие с бортовыми системами на данном типе подвижного состава.</p>
      <p>Комплекс БЛОК-М предназначен для повышения безопасности во всех режимах движения локомотива путем:
        <li>приема и обработки информации о поездной ситуации по непрерывному (АЛСН, АЛС-ЕН) и точечному каналам связи (напольные устройства);</li>
        <li>индикации и сигнализации машинисту локомотива информации о поездной обстановке, характеристиках пути следования, параметрах движения поезда, его тормозной системы и режиме работы устройства;</li>
        <li>осуществления остановки перед путевым светофором с запрещающим показанием, имеющим кодирование, вне зависимости от действий машиниста;</li>
        <li>исключения движения со скоростью выше допустимой по показаниям путевых светофоров, по состоянию пути и других технических средств, участвующих в процессе движения поездов;</li>
        <li>исключение движения при неработоспособном состоянии машиниста;</li>
        <li>исключения несанкционированного движения (скатывание);</li>
        <li>запрета проследования путевого светофора с запрещающим показанием без предварительной остановки;</li>
        <li>использования всех видов диагностики, контроля наличия и исправности блоков и модулей устройства;</li>
        <li>регистрации информации, необходимой для идентификации нарушений безопасности движения в поездной и маневровой работе и выявления их причин.</li>
      </p>
      <p>Функции комплекса БЛОК в режиме следования по участкам оборудованных устройствами АЛСО обеспечивает следующие независимые режимы функционирования:
        <li>режим работы с каналом АЛС-ЕН - режим приема и обработки сигналов АЛСН и АЛС-ЕН или только сигналов АЛС-ЕН;</li>
        <li>режим работы с каналом ТКС - режим отсутствия приема сигналов АЛСН и АЛС-ЕН и при наличии ранее поступившей информации от ТКС;</li>
        <li>режим работы с каналом АЛСН - режим приема и обработки сигналов АЛСН при отсутствии сигналов АЛС-ЕН и ТКС;</li>
        <li>режим работы автономный - работа без взаимодействия со стационарными устройствами по каналам АЛСН, АЛС-ЕН и точечным каналам связи (ТКС);</li>
        <li>пропускной способности, комплекс БЛОК использует показания локомотивных светофоров АЛСН, АЛС-ЕН, данные переданные посредством устройств радиоканала и информацию, принятую от путевых устройств САУТ и устройств АЛСО.</li>
      </p>
      <p>При приеме комплексом БЛОК сигналов АЛСН на МСС, БИЛ-УМВ, БИЛ-ПМВ индицируются сигналы, соответствующие сигналам путевых светофоров, к которым приближается локомотив (МВПС) в соответствии с Инструкцией по сигнализации на железнодорожном транспорте Российской Федерации (ИСИ).</p>
      <p>При приеме комплексом БЛОК сигналов АЛС-ЕН на МСС, БИЛ-УМВ, БИЛ-ПМВ индицируются сигналы, соответствующие показаниям путевых светофоров согласно таблицы 6.1.</p>
        <BlokTable6_1 />
      <p>В состав системы БЛОК в ходят основные блоки:Блок индикации (Монитор 5, БИЛ-УМВ, БИЛ-ИП, ПМ3-САУТ-ЦМ/485). После включения питания блок Монитор 5 (БИЛ-УМВ) в активной кабине индицирует:
        <li>координата пути;</li>
        <li>текущее время;</li>
        <li>давление в тормозной магистрали (при наличии "7" в конфигурации);</li>
        <li>давление в уравнительном резервуаре (при наличии "7" в конфигурации);</li>
        <li>давление в тормозном цилиндре (при наличии "7" в конфигурации);</li>
        <li>фактическая скорость;</li>
        <li>ускорение;</li>
        <li>готовность съемного носителя информации (кассеты регистрации) - при наличии кассеты;</li>
        <li>несущая частота канала АЛСН (активность канала АЛС-ЕН);</li>
        <li>режим работы "ПОЕЗДНОЙ";</li>
        <li>номер пути;</li>
        <li>индикатор "Радиоканал" связи с носимой частью прибором ТСКБМ-Н (при условии включения прибора ТСКБМ-Н);</li>
        <li>сигнал "ВНИМАНИЕ" (кратковременно);</li>
        <li>номер карты (кратковременно на 4 с);</li>
        <li>тормозной коэффициент;</li>
        <li>индикация значка "служебное торможение" (только на блоке Монитор 5);</li>
        <li>сегмент экрана, отображающий информацию о поездной ситуации (на блоке Монитор 5 для ЭС1 "Ласточка").</li>
      </p>
      <p>После включения ключа ЭПК раздается кратковременный звуковой сигнал, при этом:
        <li>на МСС, БИЛ-УМВ, БИЛ-ПМВ появится сигнал локомотивного светофора "Б" на не кодируемом участке пути, а на кодируемом участке, через время не более 6 с сигнал светофора, соответствующий коду АЛСН или АЛС-ЕН данном участке пути.</li>
        <li>на блоке Монитор 5 (БИЛ-УМВ, БИЛ-ИП, ПМ3-САУТ-ЦМ/485 появится следующая информация:</li>
        <li>на блоке Монитор 5 (БИЛ-УМВ) отображение "круговой" шкалы красного цвета - значение допустимой скорости Vдоп;</li>
        <li>на блоке Монитор 5 (БИЛ-УМВ) отображение "круговой" шкалы желтого цвета - значение целевой скорости Vцел;</li>
        <li>на блоке БИЛ-ИП, ПМ3-САУТ-ЦМ/485 отображение цифровой индикации красного цвета - значение допустимой скорости Vдоп;</li>
        <li>на блоке БИЛ-ИП, ПМ3-САУТ-ЦМ/485 отображение цифровой индикации зеленого цвета - значение фактической скорости VФАК.</li>
      </p>
      <p>На отдельных участках железных дорог (например, на высокоскоростных участках) допускается использование специально разработанной для этого участка таблицы соответствия показаний локомотивного светофора, числу свободных блок - участков.</p>
      <p>На участках, как оборудованных, так и не оборудованных устройствами АЛСН, АЛС-ЕН и путевыми устройствами САУТ пользование существующими средствами сигнализации и связи при движении поездов должно производиться в полном соответствии с ПТЭ, ИДП и ИСИ.</p>
      <p>Все локомотивы (МВПС), оборудованные комплексом, отправляемые на участки как оборудованные, так и не оборудованные путевыми устройствами АЛСН, АЛС-ЕН, путевыми устройствами САУТ и устройствами АЛСО, должны иметь исправные каналы взаимодействия с данными путевыми устройствами.</p>
      <p>Устройства АЛСО применяются для организации интервального регулирования движения поездов с применением "подвижных блок-участков", где в качестве блок-участков рассматриваются отдельные рельсовые цепи.</p>
      <p>На участках оборудованных устройствами АЛСО с подвижными блок-участками на путях отсутствуют проходные светофоры, а так же при работе системы интервального регулирования в режиме автоматического пропуска поездов, станционные светофоры находятся в выключенном состоянии.</p>
      <p>В случае движения поезда по таким участкам машинисту передается информация о текущем расстояния от головы поезда до рассчитанного комплексом места остановки или места снижения скорости движения, а также о имеющемся у машиниста запасе времени перед применением им служебного торможения, которое должно быть своевременно выполнено в целях предотвращения действия автотормозов.</p>
      <p>Указанные данные формируются комплексом БЛОК на основании принятой информации, с использованием основных каналов приема АЛСН, АЛС-ЕН и ТКС.</p>
      <p>Комплекс БЛОК осуществляет автоматическое включение и выключение алгоритмов работы с данными каналов АЛСН и АЛС-ЕН при входе/выходе в/из зоны действия системы интервального регулирования, реализованной с применением подвижных блок-участков, так же автоматическое переключение алгоритмов работы с таблицами АЛС-ЕН. Кроме того, в комплексе БЛОК предусмотрен ручной ввод машинистом необходимого параметра, обеспечивающего работу БЛОК с устройствами АЛСО.</p>
      <p>Комплекс БЛОК в режиме следования по участкам оборудованных устройствами АЛСО автоматически получает номер пути следования поезда по данным от напольных устройств АЛС-ЕН и ТКС, а также предусмотрен ручной ввод машистом пути маршрута следования поезда.</p>
      <p>Комплекс БЛОК в режиме следования по участкам оборудованных устройствами АЛСО обеспечивает следующие независимые режимы функционирования:
        <li>режим работы с каналом АЛС-ЕН - режим приема и обработки сигналов АЛСН и АЛС-ЕН или только сигналов АЛС-ЕН;</li>
        <li>режим работы с каналом ТКС - режим отсутствия приема сигналов АЛСН и АЛС-ЕН и при наличии ранее поступившей информации от ТКС;</li>
        <li>режим работы с каналом АЛСН - режим приема и обработки сигналов АЛСН при отсутствии сигналов АЛС-ЕН и ТКС;</li>
        <li>режим работы автономный - работа без взаимодействия со стационарными устройствами по каналам АЛСН, АЛС-ЕН и точечным каналам связи (ТКС).</li>
      </p>
      <p>Запрещается выдавать из депо локомотивы (МВПС), оборудованные неисправным комплексом.</p>
      <p>Машинистам запрещается отправляться на ведущем локомотиве из основных депо или ПТО с выключенным или неисправным комплексом, а так же с не установленным фиксатором открытого положения разобщительного крана ЭПК.</p>
      <p>В пути следования локомотивной бригаде запрещается выключать исправно действующий комплекс.</p>
      <p>В случае отсутствия переходного запаса комплекса на ПТО локомотивов (МВПС), допускается следование локомотивов (МВПС) в основные депо с неисправным комплексом в нерабочей кабине.</p>
      <p>Отсутствие электронной карты и (или) базы путевых устройств САУТ, а также на участках с несформированной ЭК и ЛБПП не является причиной для запрета эксплуатации локомотивов (МВПС), оборудованных комплексом.</p>
      <p>Отсутствие или неисправность носимой части ТСКБМ-Н не является нарушением работы комплекса и причиной для запрета эксплуатации локомотивов (МВПС), оборудованных комплексом.</p>
      <p>Каждой локомотивной бригаде перед поездкой, дежурный по депо вместе с маршрутным листом обязан выдать необходимое количество съемных носителей информации (кассет регистрации) с обязательным внесением номеров выданных съемных носителей информации (кассет регистрации) в маршрутный лист. Количество съемных носителей информации (кассет регистрации) определяется числом маршрутов в предстоящей поездке. Каждый съемный носитель информации (кассета регистрации) предназначена для записи информации по одному маршруту следования. После возвращения из поездки, все съемные носители информации (кассеты регистрации) сдаются локомотивной бригадой дежурному по депо.</p>
      <p>Для локомотивной бригады локомотива (МВПС) перед заступлением на смену, дежурный по депо вместе с маршрутным листом обязан выдать один съемный носитель информации (кассету регистрации).</p>
      <p>Ответственными лицами за правильное пользование БЛОК во время поездки, а так же за сохранность этих устройств на локомотивах (МВПС), являются машинист и его помощник.</p>
      <p>Ответственные лица за сохранность БЛОК на локомотивах (МВПС), ожидающих ремонта или ТО, устанавливаются приказом начальника депо.</p>
      <p>Ответственность за содержание в исправном состоянии и бесперебойное действие БЛОК на локомотивах (МВПС) возлагается на причастных работников, а в гарантийный период, так же на завод-изготовитель БЛОК.</p>
      <p>Должностные лица, ответственные за своевременную замену элементов питания ТСКБМ-Н, определяются соответствующими руководящими документами (приказами) за подписью руководства предприятия приписки локомотива (МВПС).</p>
      <p>Пломбирование составных частей БЛОК и фиксатора открытого положения разобщительного крана тормозной магистрали ЭПК должно производиться с перечнем блоков БЛОК, подлежащих опломбированию, указанному в таблице 6.2.</p>
      <BlokTable6_2 />
      <p>Все виды работ по содержанию и обслуживанию БЛОК должны выполняться с соблюдением действующих правил и инструкций по технике безопасности.</p>
    </>
  );
}