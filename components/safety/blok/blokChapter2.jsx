import React from 'react';

export default function blokChapter2(props) {
  return(
    <>
      <h4 className="text-center">Порядок приемки комплекса.</h4>
      <br/>
      <p>Перед каждой поездкой (сменой), машинист должен произвести проверку исправности носимой части ТСКБМ-Н на системе ПНЧ. Система проверки носимой части ТСКБМ-Н, как правило, должна устанавливаться у дежурного по депо приписки локомотива (МВПС) или в другом месте, удобном для осуществления предрейсовой проверки ТСКБМ-Н, определенном соответствующим приказом.</p>
      <p>Каждому машинисту, работающему на локомотивах (МВПС), оборудованных комплексом, выдается носимая часть ТСКБМ-Н в личное пользование на период его нахождения в должности. Порядок выдачи и возврата носимых частей определяется приказом начальника эксплуатационного локомотивного депо.</p>
      <p>Порядок хранения резервных носимых частей ТСКБМ-Н и лица, ответственные за их сохранность, определяются соответствующим руководящим документом (приказом по депо).</p>
      <p>Замена элементов электропитания носимых частей ТСКБМ-Н должна производиться ответственным лицом, назначенным соответствующим приказом, с записью об этом в Журнале контроля смены элементов питания с периодичностью не реже одного раза в три месяца, вне зависимости от срока пользования ТСКБМ-Н.</p>
      <p>Перед приемкой локомотива (МВПС), оборудованного комплексом БЛОК, машинист должен убедиться в:
        <li>наличии в журнале ТУ-152 штампа - справки КП АЛСН с отметкой, заверенной подписью причастного работника, об исправности комплекса, с годным сроком действия;</li>
        <li>наличии в журнале технического состояния локомотива (МВПС) штампа - справки на право пользования комплекса с подписью работника предприятия приписки локомотива (МВПС), подтверждающей факт проверки и исправности перечисленных устройств;</li>
        <li>наличии и целостности пломб на комплексе, в соответствии с таблицей 6.2;</li>
        <li>работоспособности комплекса путем включения и проверки.</li>
      </p>
      <p>Машинист, принявший локомотив (МВПС), оборудованный комплексом, обязан:
        <li>следить за чистотой и сохранностью комплекса, целостностью пломб имеющихся на нем;</li>
        <li>проверять при осмотрах локомотива (МВПС) надежность крепления аппаратуры комплекса, особенно приемных катушек и датчиков пути и скорости;</li>
        <li>своевременно докладывать об обнаруженных в пути следования неисправностях и нарушениях в работе комплекса поездному диспетчеру (далее - ДНЦ) или дежурному по станции (далее - ДСП), а при нахождении в депо - дежурному по депо;</li>
        <li>во всех случаях обнаружения неисправностей и нарушений в работе комплекса делать подробную запись в журнале ТУ-152.</li>
      </p>
      <p>В случае отсутствия замечаний при приемке локомотива (МВПС) в депо машинист производит запись в журнале ТУ-152 об исправности и работоспособности комплекса и заверяет ее своей подписью, а в пунктах смены локомотивных бригад - машинисты принимающей и сдающей локомотивной бригады.</p>
      <p>Обнаруженные локомотивными бригадами недостатки, неисправности и нарушения в работе комплекса должны быть устранены причастными специалистами по прибытию локомотива (МВПС) в локомотивное депо, о чем в журнале ТУ-152 производится соответствующая запись. В случае отказа комплекса в пути следования, машинист локомотива (МВПС) обязан осуществлять дальнейшее движение в соответствии с п. 3.5 настоящего приложения.</p>
      <p>Требования настоящей Инструкции являются обязательными для выполнения руководящими и инженерно-техническими работниками железных дорог, машинистами и другими работниками, связанными с эксплуатацией и техническим обслуживанием комплекса БЛОК.</p>
    </>
  );
}