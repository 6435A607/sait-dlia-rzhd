import React from 'react';

export default function blokChapter15(props) {
  return(
    <>
      <h4 className="text-center">Порядок работы с комплексом по АЛС-ЕН.</h4>
      <br />
      <p>При приеме сигналов из канала АЛС-ЕН на индикаторе несущей частоты блока Монитор 5 (БИЛ-УМВ) высвечивается "ЕН". Комплекс БЛОК автоматически переходит на прием сигналов АЛСН при прекращении приема сигналов АЛС-ЕН, кроме случая приближения к светофору с запрещающим сигналом.</p>
      <p>При движении локомотива (МВПС) и наличии на МСС, БИЛ-УМВ, БИЛ-ПМВ индикации одного и более свободных блок - участков, Vцел на каждом блок - участке может принимать разные значения, зависящие от поездной ситуации. Это определяется при проектировании канала АЛС-ЕН для данного участка пути.</p>
      <p>Vдоп также не является постоянной величиной. Она рассчитывается для каждого блок - участка по следующему алгоритму:
        <li>если при проследовании границ блок - участков на локомотив (МВПС) поступает информация об увеличении Vцел или она не меняется, то VДОП на следующем блок - участке будет на 5 км/ч больше скорости Vцел следующего блок - участка;</li>
        <li>если при проследовании границ блок - участков на локомотив (МВПС) поступает информация об уменьшении величины Vцел, то Vдоп становится равной Vцел на предыдущем блок - участке.</li>
      </p>
      <p>При снижении Vцел происходит однократная проверка бдительности, кроме разрешающих сигналов на МСС, БИЛ-УМВ, БИЛ-ПМВ.</p>
    </>
  )
}