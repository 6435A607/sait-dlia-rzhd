import React from 'react';

export default class NotFound extends React.Component {
    render() {
      return (
        <React.Fragment>
          <h1>Запрашиваемая страница не найдена</h1>
        </React.Fragment>
      );
    }
  }