import React from 'react';

export default function TraImage(props) {
  return (
    <>
      <br />
      <img className="img-fluid img-thumbnail mx-auto d-block" alt="image" src = {props.src} />
      <br />
    </>
  );
}