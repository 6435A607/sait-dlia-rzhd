import React from 'react';
import TraImage from './traImage.jsx';
import TraTable from './traTable.jsx';

export default function TraStation(props) {
  const dataImg = props.src.map((src) => <TraImage src = {src} />);
  const dataTable = <TraTable tra= {props.tra} />;
return (
    <>
    <button className="btn-primary float-right" onClick={props.onclick}>Вернуться к списку станций</button>
      <br />
      {dataImg}
      {dataTable}
      <br />
      <button className="btn-primary float-right" onClick={props.onclick}>Вернуться к списку станций</button>
    </>
  );
}