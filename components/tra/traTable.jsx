import React from 'react';

export default function TraTable(props) {
  return (
    <>
      <table className="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th rowSpan="2">Номер пути</th>
            <th rowSpan="2">Назначение&nbsp;железнодорожных&nbsp;путей</th>
            <th rowSpan="2">Вместимость в условных вагонах</th>
            <th colSpan="2">Наличие на железнодорожном пути</th>
          </tr>
          <tr>
            <th>контактной сети</th>
            <th>устройств АЛСН</th>
          </tr>
        </thead>
        <tbody>
          {props.tra}
        </tbody>
      </table>
    </>
  );
}