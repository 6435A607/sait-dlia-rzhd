import React from 'react';
import traData from '../tra/traData.jsx';
import TraStation from './traStation.jsx';

export default class traPage extends React.Component {
  constructor(props) {
    super(props);
    this.handleMenuClick = this.handleMenuClick.bind(this);
    this.handleBtnBackClick = this.handleBtnBackClick.bind(this);
    const listMenu = traData.map((idx) => 
      <button id={idx.id} className="col" onClick={this.handleMenuClick}>{idx.name}</button>
    );
    this.state = { menu: true, listMenu: listMenu, traStation: null };
  }

  handleMenuClick(e) {
    let stationData = traData.find((idx) => idx.id === e.target.id ); //Id кнопки совпадает со станцией
    let station = <>
      <TraStation src = {stationData.img} tra = {stationData.tra} onclick = {this.handleBtnBackClick} />
    </>

    this.setState({ traStation: station });
    this.setState({ menu: false });
  }

  handleBtnBackClick() {
   this.setState({menu: true});
  }

  render() {
    return (
      <>
        {this.state.menu ? this.state.listMenu : this.state.traStation}
      </>
    );
  }

}

