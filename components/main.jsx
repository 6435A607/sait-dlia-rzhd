import React from 'react';
import Aside from './aside.jsx';
import Article from './article.jsx';

export default class Main extends React.Component {
    render() {
      return (
        <div id="main" class="row no-gutters">
          <div class="col-lg-3 d-none d-lg-block aside">
            <Aside />
          </div>
          <div className="col-lg-8 col article">
            <Article />
          </div>
        </div>
      );
    }
  }