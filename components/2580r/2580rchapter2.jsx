import React from 'react';

export default function r2580Chapter2(props) {
  return (
    <React.Fragment>
      <h4 class="text-center">2. Термины и определения</h4>
      <br />
      <p><strong>АЛСН</strong> - автоматическая локомотивная сигнализация непрерывного действия.</p>
      <p><strong>БЛОК</strong> - безопасный локомотивный объединенный комплекс.</p>
      <p><strong>БХВ</strong> - блок хвостового вагона.</p>
      <p><strong>ДЦУН</strong> - диспетчерский центр управления перевозками.</p>
      <p><strong>ДСП</strong> - дежурный по железнодорожной станции.</p>
      <p><strong>ДНЦ</strong> - диспетчер поездной.</p>
      <p><strong>ИДП </strong>- приложение No 8 к ПТЭ - инструкция по движению поездов и маневровой работе на железнодорожном транспорте Российской Федерации, утверждена Приказом Минтранса России от 4 июня 2012 г. No 162, зарегистрированном в Минюсте Российской Федерации 28 июня 2012 г.</p>
      <p><strong>ИСИ </strong>- приложение No 7 к ПТЭ - инструкция по сигнализации на железнодорожном транспорте Российской Федерации, утверждена Приказом Минтранса России от 4 июня 2012 г. No 162, зарегистрированном в Минюсте Российской Федерации 28 июня 2012 г.</p>
      <p><strong>КЛУБ</strong> - комплексное локомотивное устройство безопасности.</p>
      <p><strong>КЛУБ-У </strong>- комплексное локомотивное устройство безопасности унифицированное.</p>
      <p><strong>КТСМ </strong><strong>(КТСМ-01, </strong><strong>КТСМ-01Д, КТСМ-02, </strong><strong>КТСМ-К и др.)</strong> - комплексы технических средств многофункциональные.</p>
      <p><strong>КТ</strong> - сигнальный знак конец торможения.</p>
      <p><strong>Локомотивная бригада </strong>- работники, осуществляющие управление и обслуживание локомотивов, а также мотор-вагонных поездов.</p>
      <p><strong>Локомотив </strong>- железнодорожный подвижной состав, предназначенный для обеспечения передвижения по железнодорожным путям поездов или отдельных вагонов.</p>
      <p><strong>Машинист </strong>- работник, осуществляющий управление локомотивом, МВПС, ССПС и обеспечивающий безопасность движения при ведении поезда и выполнении маневровой работы.</p>
      <p><strong>МВПС</strong> <strong>(мотор-вагонный подвижной состав) </strong>- моторные и немоторные вагоны, из которых формируются электропоезда, дизель-поезда, автомотрисы, дизель-электропоезда, электромотрисы, предназначенные для перевозки пассажиров и (или) багажа.</p>
      <p><strong>Местная инструкция</strong> - нормативный документ, определяющий порядок выполнения пооперационной работы в структурном подразделении (депо), исходя из местных условий.</p>
      <p><strong>НТ </strong>- сигнальный знак начало торможения.</p>
      <p><strong>ПЕ</strong> - подвижная единица техники, как самоходная, так и не самоходная, предназначенная для передвижения по железнодорожным путям общего пользования (локомотив, вагон, мотор-вагонная секция и т.д.).</p>
      <p><strong>Поездная бригада </strong>- группа железнодорожных работников, назначаемая для сопровождения и обслуживания пассажирского поезда в которую входят начальник поезда, проводники и поездной электромеханик.</p>
      <p><strong>Правила № 151</strong> - Правила технического обслуживания тормозного оборудования и управления тормозами железнодорожного подвижного состава, утвержденные Советом по железнодорожному транспорту государств-участников Содружества (протокол от 6-7 мая 2014 г. No 60).</p>
      <p><strong>ПТЭ </strong>- Правила технической эксплуатации железных дорог Российской Федерации, утверждены Приказом Минтранса России от 21 декабря 2010 г. No 286, зарегистрированном в Минюсте Российской Федерации 28 января 2011 г.</p>
      <p><strong>ПТО </strong>- пункт технического обслуживания (вагонов).</p>
      <p><strong>ПЧ </strong>- дистанция пути.</p>
      <p><strong>ПЧД </strong>- диспетчер дистанции пути.</p>
      <p><strong>ССПС </strong>- специальный самоходный подвижной состав.</p>
      <p><strong>СКНБ </strong>- система контроля нагрева букс.</p>
      <p><strong>СКНР </strong>- система контроля нагрева редуктора.</p>
      <p><strong>Торможение экстренное </strong>- торможение, применяемое в случаях, требующих немедленной остановки поезда, путем применения максимальной тормозной силы.</p>
      <p><strong>ТРА </strong>- техническо-распорядительный акт железнодорожной станции.</p>
      <p><strong>ЦИК</strong> - центральный пункт контроля системы централизации СТК.</p>
      <p><strong>ЭПС</strong> - тяговый электроподвижной состав (электровозы электропоезда, электромотрисы).</p>
      <p><strong>ЭЧ </strong>- дистанция электроснабжения.</p>
      <p><strong>ЭЧЦ </strong>- энергодиспетчер.</p>
      <p><strong>ЭММ </strong>- электронный маршрут машиниста.</p>
    </React.Fragment>
  );
};
