import React from 'react';

export default function r2580Chapter1(props) {
  return (
    <React.Fragment>
      <p className="text-center"><strong>ОТКРЫТОЕ АКЦИОНЕРНОЕ ОБЩЕСТВО «РОССИЙСКИЕ ЖЕЛЕЗНЫЕ ДОРОГИ» РАСПОРЯЖЕНИЕ</strong></p>
      <p className="text-center"><strong>12 декабря 2017 г.&nbsp; Москва № 2580 р.</strong></p>
      <p className="text-center"><strong>О вводе в действие Регламента взаимодействия работников, связанных с движением поездов, с работниками локомотивных бригад при возникновении аварийных и нестандартных ситуаций на путях общего пользования инфраструктуры ОАО «РЖД»</strong></p>
      <p>В целях установления единого порядка по действиям работников,связанных с движением поездов, с работниками локомотивных бригад при возникновении аварийных и нестандартных ситуаций на путях общего пользования инфраструктуры ОАО «РЖД», с целью безусловного обеспечения требований безопасности движения:</p>
      <ol>
        <li>Утвердить прилагаемый Регламент взаимодействия работников, связанных с движением поездов, с работниками локомотивных бригад при возникновении аварийных и нестандартных ситуаций на путях общего пользования инфраструктуры ОАО «РЖД» (далее - Регламент).</li>
        <li>Заместителю генерального директора - начальнику Центральной дирекции инфраструктуры Верховых Г.В., заместителю генерального директора - начальнику Дирекции тяги Валинскому О.С., заместителю генерального директора - начальнику Центральной дирекции управления движением Иванову П.А., директору ОАО «РЖД» по пассажирским перевозкам Пегову Д.В., руководителям филиалов, структурных подразделений и дочерних обществ ОАО «РЖД» обеспечить ознакомление, изучение настоящего Регламента причастными работниками и внести во внутренние документы подразделений соответствующие изменения.</li>
        <li>Признать утратившим силу Регламент взаимодействия локомотивных бригад с причастными работниками ОАО «РЖД», деятельность которых непосредственно связана с движением поездов, при возникновении аварийных и нестандартных ситуаций на инфраструктуре ОАО «РЖД», утвержденный распоряжением ОАО «РЖД» от 30 декабря 2010 г. No 2817р.</li>
      </ol>
      <h4 className="text-center">1. Предисловие</h4>
      <br />
      <p><strong>1.1.</strong> РАЗРАБОТАН - Дирекцией тяги.</p>
      <p><strong>1.2.</strong> ВНЕСЕН - Дирекцией тяги.</p>
      <p><strong>1.3.</strong> УТВЕРЖДЕН И ВВЕДЕН В ДЕЙСТВИЕ - распоряжением первого вице-президента ОАО «РЖД».</p>
      <p><strong>1.4.</strong> ВВЕДЕН ВЗАМЕН - Регламента взаимодействия работников локомотивных бригад с причастными работниками ОАО «РЖД», деятельность которых непосредственно связана с движением поездов, при возникновении аварийных и нестандартных ситуаций на путях общего пользования инфраструктуры ОАО «РЖД», утвержденного распоряжением ОАО «РЖД» от 30 декабря 2010 г. No 2817р.</p>
      <p><strong>1.5.</strong> ПЕРЕСМОТР И ВНЕСЕНИЕ ИЗМЕНЕНИЙ в Регламент производится - по введению в действие новых нормативных документов ОАО «РЖД» и Минтранса России, определяющих организацию работы структурных подразделений на инфраструктуре ОАО «РЖД» в области обеспечения безопасности движения поездов.</p>
      <p><strong>1.6.</strong> КОНТРОЛЬНЫЙ ЭКЗЕМПЛЯР документа в электронной форме можно извлечь из Единой системы документооборота (ЕАСД)</p>
    </React.Fragment>
  );
};
