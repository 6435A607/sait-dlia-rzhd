import React from 'react';

export default function r2580Chapter4(prosp) {
  return (
    <React.Fragment>
      <h4 class="text-center">4. Общие положения</h4>
      <br />
      <p><strong>4.1.</strong> Настоящий Регламент разработан в целях установления единого порядка взаимодействия работников, связанных с движением поездов, с работниками локомотивных бригад на путях общего пользования инфраструктуры ОАО «РЖД».</p>
      <p><strong>4.2. </strong>Настоящий Регламент разработан в соответствие с требованиями, предъявляемыми нормативными документами ОАО «РЖД», МПС и Минтранса России:</p>
      <ul className="ul-article">
        <li>Правилами технической эксплуатации железных дорог Российской Федерации, утвержденными приказом Минтранса России 21 декабря 2010 г. No 286;</li>
        <li>Регламентом действий локомотивных бригад в аварийных и нестандартных ситуациях при работе на сопредельных участках других железнодорожных администраций, утвержденном на заседании Совета по железнодорожному транспорту государств-участников Содружества (протокол от 21-22 мая 2009 г. No 50);</li>
        <li>Приложением No 8 к ПТЭ (Инструкция по движению поездов и маневровой работе на железнодорожном транспорте Российской Федерации, утвержденной приказом Минтранса России от 4 июня 2012 г. No 162);</li>
        <li>Приложением No 7 к ПТЭ (Инструкция по сигнализации на железнодорожном транспорте Российской Федерации, утвержденной от 4 июня 2012 г. No 162);</li>
        <li>Правилами технического обслуживания тормозного оборудования и управление тормозами железнодорожного подвижного состава, утвержденными Советом по железнодорожному транспорту государств-участников Содружества (протокол от 5-6 мая 2014 г. No 60);</li>
        <li>Порядком действий работников ОАО «РЖД» при вынужденной остановке поезда на перегоне с последующим оказанием ему помощи вспомогательным локомотивом, утвержденным распоряжением ОАО «РЖД» от 27 февраля 2015 г. No 554р;</li>
        <li>Инструкцией по монтажу, вводу в эксплуатацию, техническому обслуживанию и ремонту устройств контроля схода железнодорожного подвижного состава, утвержденной МПС России 30 декабря 2002 г. No ЦВ-ЦШ-929;</li>
        <li>Инструкцией по размещению, установке и эксплуатации средств автоматического контроля технического состояния подвижного состава на ходу поезда, утвержденной распоряжением ОАО «РЖД» от 18 марта 2016 г. No 469р; Инструкцией проводника пассажирского вагона АО «ФГОС», утвержденной распоряжением АО «ФПК» от 27 апреля 2015 г. No 515р;</li>
        <li>Инструкцией начальника пассажирского поезда АО «ФПК», утвержденной распоряжением АО «ФГОС» от 20 июля 2015 г. No 916р;</li>
        <li>Правилами безопасности и порядка ликвидации аварийных ситуаций с опасными грузами при перевозке их по железным дорогам от 25 ноября 1996 г. No ЦМ-407; Регламентом действий работников структурных подразделений ОАО «РЖД» при получении информации о травмировании граждан, не связанных с производством, подвижным составом, утвержденным распоряжением ОАО «РЖД» от 29 мая 2015 г. No 290;</li>
        <li>Распоряжением ОАО «РЖД» от 27 декабря 2012 г. No 2707р «Об утверждении инструкции по охране труда для локомотивных бригад ОАО «РЖД»;</li>
        <li>Распоряжением ОАО «РЖД» от 11 января 2016 г. No 4р «О введении в действие типовой инструкции организации вождения поездов и выполнению маневровой работы машинистами без помощников машиниста (в одно лицо)».</li>
        <li>Распоряжением ОАО «РЖД» от 17 апреля 2017 г. No 734р «Об утверждении временной инструкции о порядке движения поездов на участках железнодорожной линии Прохоровка-Журавка-Чертково-Батайск, оборудованных системой интервального регулирования движения поездов с подвижными блок-участками на перегонах.</li>
      </ul>

    </React.Fragment>
  );
};
