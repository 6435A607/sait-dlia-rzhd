import React from 'react';

export default function r2580Chapter14(props) {
  return (
    <React.Fragment>
      <h4 class="text-center">14. Порядок действий при вынужденной остановке поезда на перегоне из-за неисправности локомотива</h4>
      <br />
      <p><strong>14.1. </strong>При отказе на локомотиве оборудования, обеспечивающего ведение поезда и невозможности устранения причины отказа, машинисту категорически запрещается проследовать станцию и отправляться на перегон.</p>
      <p><strong>14.2. </strong>При следовании по перегону, в зависимости от сложившейся ситуации и поездной обстановки и невозможности довести поезд до станции, машинист обязан:</p>
      <ul className="ul-article">
        <li>остановить поезд по возможности на площадке и прямом участке пути, если не требуется экстренной остановки;</li>
        <li>привести в действия автоматические тормоза поезда и вспомогательный тормоз локомотива с фиксацией его в крайнем тормозном положении, при необходимости произвести закрепление состава ручными тормозами и тормозными башмаками;</li>
        <li>немедленно объявить по радиосвязи о причинах и месте остановки в соответствии с требованиями настоящего Регламента (кроме того, машинист пассажирского поезда обязан сообщить начальнику или электромеханику пассажирского поезда, а машинист специального самоходного подвижного состава - руководителю работ в хозяйственном поезде), после чего начинается 10-ти минутный отсчет времени для определения возникшей неисправности и возможности ее устранения. В исключительных случаях, при отсутствии поездной радиосвязи с ДСП или ДНЦ машинист остановившегося поезда принимает меры для передачи сообщения об остановке (о затребовании вспомогательного локомотива) через машинистов поездов встречного (попутного) направления или используя сотовую связь.</li>
      </ul>
      <p><strong>14.3. </strong>После получения доклада от машиниста поезда о вынужденной остановке из-за неисправности тягового подвижного состава,<span className="text-danger"> ДНЦ и ДСП запрещается в течение 10 минут отвлекать локомотивную бригаду вызовами по радиосвязи.</span></p>
      <p><strong>14.4. </strong>При отказах оборудования локомотива (МВПС, ССПС) для восстановления их работоспособности локомотивная бригада должна, соблюдая меры безопасности использовать штатные аварийные схемы, предусмотренные заводом- изготовителем.</p>
      <p><strong>14.5. </strong><span className="text-danger">При невозможности устранения</span> возникшей неисправности по истечении 10 минут после остановки поезда машинист обязан:</p>
      <ul className="ul-article">
        <li>лично убедиться в фактическом месте нахождения поезда по ближайшему километровому и пикетному столбикам;</li>
        <li>через ДСП (ДНЦ) затребовать вспомогательный локомотив, при этом указать на каком километре, пикете находится голова поезда, в связи с чем требуется помощь и время ее затребования;</li>
        <li>если движение поезда не может быть возобновлено в течение 20 минут с момента остановки и невозможности удержать поезд на месте на автоматических тормозах, подать сигнал для приведения в действие имеющихся в составе ручных тормозов проводниками пассажирских вагонов, кондукторами, руководителями работ в хозяйственном поезде, дать указание помощнику машиниста на закрепление грузового поезда тормозными башмаками и ручными тормозами вагонов;</li>
        <li>доложить по радиосвязи ближайшему ДСП, ограничивающему перегон, и ДНЦ о закреплении поезда, указав количество тормозных башмаков, которыми закреплен подвижной состав;</li>
        <li>при обслуживании локомотивов пассажирских поездов одним машинистом выполнение операций по закреплению и ограждению поезда производится начальником поезда (поездным электромехаником) и проводником хвостового вагона по указанию машиниста, передаваемому по радиосвязи.</li>
      </ul>
      <p><strong>14.6. </strong>В случае устранения неисправности на локомотиве (МВПС, ССПС) машинисту локомотива (МВПС, ССПС), остановившемуся на перегоне и затребовавшему вспомогательный локомотив, категорически запрещается приводить локомотив (МВПС, ССПС) в движение, при этом машинист обязан доложить по радиосвязи ближайшему ДСП ограничивающему перегон и ДНЦ об устранении неисправности и согласовать с ним дальнейшие действия.</p>
      <p><span className="text-danger">Если движение поезда не может быть возобновлено в течение 20 минут</span> с момента остановки и невозможности удержать поезд на месте на автоматических тормозах, по указанию машиниста помощник машиниста обязан закрепить поезд тормозными башмаками и ручными тормозами вагонов.</p>
      <p>Укладка тормозных башмаков производится под груженые вагоны со стороны уклона (носок полоза тормозного башмака, уложенного на рельс должен касаться обода колеса вагона). <span className="text-danger">Закрепление производится из расчета один тормозной башмак под один вагон.</span></p>
      <p>При необходимости приводятся в действие ручные тормоза вагонов в количестве и соответствии с нормами, определенными приложением № 2, разделом III.7., таблица №III.4 Правил No 151.</p>
      <p>Аварийные таблицы с указанием норм закрепления в зависимости от веса поезда и профиля пути обслуживаемых участков разрабатываются в эксплуатационных локомотивных депо;</p>
      <ul className="ul-article">
        <li>после возвращения доложить машинисту, ДСП ограничивающим перегон (ДНЦ) о закреплении поезда тормозными башмаками с указанием их  количества, а также о количестве вагонов, на которых приведены в действие ручные тормоза, после чего об этом произвести запись в журнале формы ТУ-152.</li>
      </ul>
      <p>При получении от ДНЦ разрешения на отправление с перегона самостоятельно машинист обязан:</p>
      <ul className="ul-article">
        <li>произвести сокращенное опробование тормозов (при необходимости);</li>
        <li>дать команду помощнику машиниста на изъятие из-под колес вагонов тормозных башмаков и отпуск ручных тормозов вагонов, а в пассажирском поезде - передать информацию об изъятии тормозных башмаков и отпуске ручных тормозов начальнику поезда;</li>
        <li>после возвращения в кабину локомотива (МВПС, ССПС) помощника машиниста произвести отпуск ручных тормозов локомотива (МВПС, ССПС).</li>
      </ul>
      <p><strong>14.7. Помощник машиниста обязан:</strong></p>
      <ul className="ul-article">
        <li>зафиксировать время передачи машинистом по радиосвязи информации о причинах, месте и времени вынужденной остановки поезда, а также время и фамилии машинистов вслед идущего и (или) встречного поездов, ДСП, ДНЦ о подтверждении принятой ими информации на обратной стороне бланка предупреждений формы ДУ-61;</li>
        <li>привести в действие ручные тормоза локомотива;</li>
        <li>произвести набор воздуха в запасный резервуар токоприемника (если это предусмотрено конструкцией локомотива);</li>
        <li>убедиться в том, что поезд заторможен, а управляющий орган крана вспомогательного тормоза находится в крайнем тормозном положении с фиксацией ее на защелку специальным устройством;</li>
        <li>при необходимости устранения неисправности с заходом в высоковольтную камеру электровоза визуально убедиться в опускании токоприемников;</li>
        <li>при необходимости вести переговоры по радиосвязи с указанием своей фамилии и должности;</li>
        <li>контролировать отсчет времени от момента остановки и докладывать машинисту;</li>
        <li>для определения схемы укладки тормозных башмаков по натурному листу поезда установить нахождение в составе груженых вагонов и их порядковые номера с головы;</li>
        <li>если движение поезда не может быть возобновлено в течение 15 минут с момента остановки и невозможности удержать поезд на месте на автоматических тормозах, произвести действия согласно пункту 13.6 настоящего Регламента.</li>
      </ul>
      <p><strong>14.8. </strong>Приказ на закрытие перегона для движения поездов ДНЦ передает ДСП, ограничивающим перегон, машинисту поезда, затребовавшему помощь и машинисту вспомогательного локомотива.</p>
      <p><strong>14.9. </strong>При вынужденной остановке поезда на перегоне, машинист после получения приказа ДНЦ о закрытии перегона и информации о порядке оказания помощи обеспечивает ограждение поезда в соответствии с требованиями пунктов 45-49 ИСИ.</p>
      <p>Закрепление производится по указанию машиниста остановившегося поезда силами поездной бригады пассажирского поезда и помощника машиниста с остальными поездами.</p>
      <p><strong>14.10. </strong>После определения порядка оказания помощи (в соответствии с требованиями ИДП) ДНЦ, ДСП и машинист вспомогательного локомотива обязаны сверить по радиосвязи с машинистом остановившегося поезда данные о месте остановки локомотива (МВПС, ССПС), затребовавшего помощь и информацию об отправлении вспомогательного локомотива.</p>
      <p><strong>14.11. </strong>Разрешение формы ДУ-64 выдаются ДСП после получения приказа ДНЦ о закрытии перегона (пути перегона), а при организации движения при диспетчерской централизации регистрируемый приказ ДНЦ.</p>
      <p><strong>14.12. </strong>Машинист вспомогательного локомотива при оказании помощи остановившемуся поезду (на перегоне) обязан руководствоваться Порядком действий работников ОАО «РЖД» при вынужденной остановке поезда на перегоне с последующим оказанием ему помощи вспомогательным локомотивом, утвержденным распоряжением ОАО «РЖД» от 27 февраля 2015г. No 554р:</p>
      <ul className="ul-article">
        <li>при движении по неправильному пути, для оказания помощи остановившемуся на перегоне поезду с головы состава, со скоростью не более 60 км/час, а после остановки на расстоянии не менее 2-х км до места, указанного в разрешении формы ДУ-64 - со скоростью не более 20 км/час;</li>
        <li>при движении по правильному пути, для оказания помощи остановившемуся на перегоне поезду с хвоста состава по сигналам автоблокировки, а после остановки у красного проходного светофора - со скоростью не более 20 км/час;</li>
        <li>по правильному пути, для оказания помощи остановившемуся на перегоне поезду с хвоста состава при полуавтоблокировке, со скоростью не более 60 км/час, а после остановки на расстоянии не менее 2-х км до места, указанного в разрешении формы ДУ-64 - со скоростью не более 20 км/час;</li>
        <li>при электрожезловой системе и телефонных средствах связи аналогично следованию при полуавтоматической блокировке.</li>
      </ul>
      <p><strong>14.13. </strong>При оказании помощи остановившемуся поезду с головы машинист вспомогательного локомотива после остановки не менее чем за 2 км до места, указанного в разрешении формы ДУ-64, обязан:</p>
      <ul className="ul-article">
        <li>связаться по радиосвязи с машинистом остановившегося на перегоне поезда (МВПС, ССПС) для уточнения фактического места его нахождения и согласования действий;</li>
        <li>по сигналу, подаваемому помощником машиниста ограждающего поезд, остановиться и после снятия петард произвести его посадку на локомотив, продолжить следование со скоростью не более 20 км/час с особой бдительностью и готовностью остановиться;</li>
        <li>остановиться за 10-15 м до локомотива поезда, согласовать свои действия с машинистом остановившегося поезда, произвести сцепление, зарядить тормоза и произвести сокращенное опробование пассажирских, технологическое опробование в грузовых поездах, убедиться в том, что тормозные башмаки убраны, а ручные тормоза подвижного состава отпущены;</li>
        <li>доложить ДНЦ (через ДСП) о готовности к отправлению.</li>
      </ul>
      <p><strong>14.14. </strong>При вынужденной остановке на перегоне моторвагонного поезда и невозможности его дальнейшего самостоятельного движения разрешается прицеплять к нему следом идущий моторвагонный подвижной состав для вывода на железнодорожную станцию. Порядок оказания помощи определяется владельцем инфраструктуры.</p>

    </React.Fragment>
  );
}