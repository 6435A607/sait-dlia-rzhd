import React from 'react';

export default function r2580Chapter6(props) {
  return (
    <React.Fragment>
      <h4 class="text-center">6. Порядок действий при появлении признаков нарушения целостности тормозной магистрали в составе поезда</h4>
      <br />
      <p><strong>6.1.</strong> Причинами падения давления в тормозной магистрали подвижного состава являются:</p>
      <ul className="ul-article">
        <li>разъединение тормозных рукавов или другое нарушение целостности тормозной магистрали в составе поезда;</li>
        <li>обрыв (саморасцеп) автосцепных устройств в подвижном составе;</li>
        <li>сход подвижного состава с нарушением целостности тормозной магистрали;</li>
        <li>открытие крана экстренной остановки (стоп-крана);</li>
        <li>срабатывание автостопного торможения;</li>
        <li>срабатывание ускорителя экстренного торможения;</li>
        <li>нарушение производительности компрессоров;</li>
        <li>потеря связи с БХВ.</li>
      </ul>
      <p><strong>6.2.</strong> Признаками, по которым определяется падение давления в тормозной магистрали поезда, МВПС, являются:</p>
      <ul className="ul-article">
        <li>снижение скорости, не соответствующее профилю пути;</li>
        <li>частое включение компрессоров;</li>
        <li>быстрое снижение давления в главных резервуарах после выключения компрессоров при неработающих песочницах и тифонах;</li>
        <li>срабатывание датчика контроля состояния тормозной магистрали усл.No418.</li>
      </ul>
      <p>Контроль за целостностью тормозной магистрали поезда осуществляется машинистом по контрольным приборам (манометрам тормозной магистрали и главных резервуарах), расположенным в кабине управления локомотива, МВПС.</p>
      <p><strong>6.3. </strong>Порядок действий машиниста при падении давления в тормозной магистрали подвижного состава.</p>
      <p>При падении давления в тормозной магистрали пассажирского, почтово-багажного, грузопассажирского поезда, МВПС машинист должен применить экстренное торможение путем перевода управляющего органа крана машиниста в положение экстренного торможения, а ручку вспомогательного тормоза в крайнее тормозное положение до полной остановки. При применении экстренного торможения в обязательном порядке должна использоваться система подачи песка (при ее наличии) под колесные пары, которая должна быть прекращена при скорости движения подвижного состава равной 10 км/час.</p>
      <p><strong>6.4.</strong> Порядок действий машиниста при падении давления в тормозной магистрали грузового поезда.</p>
      <p>Если при следовании грузового поезда появились признаки возможного нарушения целостности тормозной магистрали (частые включения компрессоров или быстрое снижение давления в главных резервуарах после выключения компрессоров при неработающих приборах пескоподачи и тифонах, резкое замедление движения поезда, не соответствующее влиянию профиля пути), отключить тягу, перевести на 5-7 секунд управляющий орган крана машиниста в положение, не обеспечивающее поддержание заданного давления в тормозной магистрали после торможения, и наблюдать за давлением тормозной магистрали:</p>
      <ul className="ul-article">
        <li>в случае, когда не происходит быстрое и непрерывное снижение давления в тормозной магистрали и резкое замедление движения поезда, выполнить служебное торможение с разрядкой тормозной магистрали на величину первой ступени, затем отпустить автотормоза поезда установленным порядком, при этом включать тягу разрешается только после полного отпуска автотормозов поезда;</li>
        <li>в случае, когда происходит быстрое и непрерывное снижение давления в тормозной магистрали или резкое замедление движения поезда, не соответствующее влиянию профиля пути, выполнить служебное торможение на величину первой ступени. Затем управляющий орган крана машиниста перевести в положение, не обеспечивающее поддержание заданного давления в тормозной магистрали после торможения, и остановить поезд без применения вспомогательного тормоза локомотива.</li>
      </ul>
      <p>После остановки поезда управляющий орган крана вспомогательного тормоза перевести в крайнее тормозное положение.</p>
      <p>Если при следовании грузового поезда произошло срабатывание датчика контроля состояния тормозной магистрали или произошло самопроизвольное снижение давления в тормозной магистрали машинист обязан выполнить служебное торможение с разрядкой тормозной магистрали на величину первой ступени, после чего перевести управляющий орган крана машиниста в положение, не обеспечивающее поддержание заданного давления в тормозной магистрали после торможения и остановить поезд без применения вспомогательного тормоза локомотива.</p>
      <p>После остановки управляющий орган крана вспомогательного тормоза перевести в крайнее тормозное положение. Помощник машиниста должен осмотреть поезд, выяснить в полном ли он составе по номеру последнего вагона и проверить наличие поездного сигнала на этом вагоне, проверить целостность и плотность тормозной магистрали и выполнить сокращенное опробование тормозов.</p>
      <p>В случае повторения признаков возможного нарушения целостности тормозной магистрали поезда (в том числе срабатывание датчика контроля состояния тормозной магистрали в составе) необходимо перевести на 5-7 сек управляющий орган крана машиниста в положение, не обеспечивающее поддержание заданного давления в тормозной магистрали после торможения и наблюдать за давлением тормозной магистрали:</p>
      <ul className="ul-article">
        <li>в случае, когда не происходит быстрое и непрерывное снижение давления в тормозной магистрали и резкое замедление движения поезда, выполнить служебное торможение с разрядкой тормозной магистрали на величину первой ступени, затем отпустить автотормоза поезда установленным порядком, включать тягу разрешается только после полного отпуска автотормозов поезда;</li>
        <li>в случае, когда происходит быстрое и непрерывное снижение давления в тормозной магистрали или резкое замедление движения поезда, не соответствующее влиянию профиля пути, выполнить служебное торможение на величину первой ступени. Затем управляющий орган крана машиниста перевести в положение, не обеспечивающее поддержание заданного давления в тормозной магистрали после торможения, и остановить поезд без применения вспомогательного тормоза локомотива.</li>
      </ul>
      <p>После остановки поезда управляющий орган крана вспомогательного тормоза перевести в крайнее тормозное положение.</p>
      <p>При повторении признаков возможного нарушения целостности тормозной магистрали поезда, машинист обязан заявить ДНЦ через ДСП близлежащей станции контрольную проверку тормозов в соответствии с главой XIV Правил No 151.</p>
      <p><strong>6.5. </strong>Порядок передачи информации об остановке поезда по причине падения давления в тормозной магистрали.</p>
      <p>При вынужденной остановке поезда по причине падения давления в тормозной магистрали информация передается локомотивной бригадой порядком, указанным в пункте 5.1 настоящего Регламента.</p>
      <p><strong>6.6.</strong> Порядок осмотра состава поезда.</p>
      <p>Машинист должен направить помощника машиниста для осмотра состава поезда, предварительно проинструктировав его о порядке действий.</p>
      <p></p>
      <p>Помощник машиниста перед уходом для осмотра поезда обязан:</p>
      <ul className="ul-article">
        <li>выписать номер хвостового вагона из справки об обеспечении поезда тормозами и исправном их действии;</li>
        <li>взять с собой сигнальные принадлежности, переносную радиостанцию, для оперативной связи с машинистом, в темное время суток фонарь;</li>
        <li>при остановке грузового поезда на неблагоприятном профиле взять тормозные башмаки для закрепления вагонов, принять меры к закреплению состава необходимым количеством тормозных башмаков в соответствии с нормами и правилами;</li>
        <li>для определения причины падения давления в тормозной магистрали произвести осмотр всего состава, обращая особое внимание на наличие постоянного дутья сжатого воздуха из тормозной сети поезда, осмотр колесных пар на предмет схода, а также состояния автосцепного оборудования;</li>
        <li>дойдя до последнего вагона сверить его номер с номером, указанным в справке об обеспечении поезда тормозами формы ВУ-45, проверить наличие на вагоне у буферного бруса с правой стороны обозначения хвоста поезда в виде красного диска со светоотражателем, положение рукава тормозной магистрали вагона (должен быть в подвешенном состоянии).</li>
      </ul>
      <p>В пассажирском поезде дополнительно убедиться в наличии 3-х включенных красных сигнальных огней на хвостовом вагоне и уточнить у проводника последнего вагона, что он является хвостовым в составе поезда.</p>
      <p>Осмотр пассажирского поезда проводится совместно с начальником поезда или поездным электромехаником.</p>
      <p>При обслуживании локомотива машинистом в одно лицо осмотр пассажирского поезда производится поездной бригадой, для осмотра грузового поезда могут привлекаться работники локомотивных бригад встречных и попутных поездов, а также СПСС.</p>
      <p><strong>6.7.</strong> Порядок действий при разъединении тормозных рукавов или другом нарушении целостности тормозной магистрали в составе поезда.</p>
      <p>При выявлении разъединения тормозных рукавов локомотивная бригада обязана:</p>
      <ul className="ul-article">
        <li>произвести их осмотр, при выявлении неисправного рукава при необходимости произвести замену на исправный, который находится на локомотиве в технической аптечке, а в случае отсутствия снять его с хвостового вагона или переднего бруса локомотива;</li>
        <li>убедиться, что номер хвостового вагона соответствует номеру, указанному в справке об обеспечении поезда тормозами и исправном их действии;</li>
        <li>произвести проверку целостности тормозной магистрали;</li>
        <li>произвести сокращенное опробование тормозов.</li>
      </ul>
      <p>При выявлении нарушения целостности тормозной магистрали поезда из-за неисправности тормозного оборудования и невозможности ее устранения локомотивная бригада обязана:</p>
      <ul className="ul-article">
        <li>сообщить о характере неисправности ДНЦ или ДСП, затребовать у ДНЦ через ДСП близлежащей станции вспомогательный локомотив для вывода хвостовой части с перегона и далее руководствоваться указаниями ДНЦ;</li>
        <li>в случае необходимости перекрытия концевого крана до неисправного вагона, необходимо с учетом профиля пути и возникшей неисправности произвести закрепление хвостовой части состава от несанкционированного ухода в соответствии с нормами и правилами, установленными в соответствии требованием раздела III.7 Правил No 151.</li>
      </ul>
      <p><strong>6.8.</strong> Порядок действий при выявлении разъединения (разрыва) поезда.</p>
      <p>Порядок действий при разъединении (разрыве) поезда определен пунктами 9-13 приложения No 7 ИДП.</p>
      <p></p>
      <p>При разъединении (разрыве) поезда на перегоне машинист обязан:</p>
      <ul className="ul-article">
        <li>немедленно сообщить о случившемся по радиосвязи машинистам поездов, следующих по перегону, и ДСП станций, ограничивающих перегон, которые немедленно докладывают об этом ДНЦ;</li>
        <li>через помощника машиниста проверить состояние состава и сцепных приборов у разъединившихся вагонов и при их исправности сцепить составпоезда. Осаживать разъединившиеся части состава для сцепления следует с особой осторожностью, чтобы при соударении вагонов скорость не превышала 3 км/ч;</li>
        <li>поврежденные тормозные рукава заменить запасными или снятыми с хвостового вагона и у переднего бруса локомотива;</li>
      </ul>
      <p>Во всех случаях, когда операции по соединению разъединившихся частей состава поезда не могут быть выполнены в течение 20 минут, машинист обязан принять меры к тому, чтобы оставшаяся без локомотива часть поезда была закреплена тормозными башмаками и ручными тормозами.</p>
      <p>После сцепления разъединившихся частей помощник машиниста по номеру хвостового вагона и наличию на нем поездного сигнала должен убедиться в целостности состава. Перед возобновлением движения должны быть отпущены ручные тормоза, произведено сокращенное опробование автотормозов, изъяты тормозные башмаки из-под вагонов.</p>
      <p><strong>Не допускается соединять части поезда на перегоне:</strong></p>
      <ul className="ul-article">
        <li>во время тумана, метели и при других неблагоприятных условиях, когда сигналы трудно различимы;</li>
        <li>если отцепившаяся часть находится на уклоне круче 0,0025 и от толчка при соединении может уйти в сторону, обратную направлению движения поезда.</li>
      </ul>
      <p>В исключительных случаях для соединения с отцепившейся частью состава может быть использован локомотив сзади идущего поезда в порядке, предусмотренном в пункте 22 приложения No 7 ИДП.</p>
      <p>Если соединить поезд невозможно, машинист должен затребовать вспомогательный локомотив или восстановительный поезд в порядке, предусмотренном в пункте 2 приложения No 7 ИДП, указав дополнительно в заявке ориентировочное расстояние между разъединившимися частями поезда.</p>
      <p>В исключительных случаях, при отсутствии телефонной и радиосвязи с ДСП станции или ДНЦ для доставки на железнодорожную станцию письменного требования может быть использован поездной локомотив (с вагонами или без них). Отцеплять локомотив от состава разрешается лишь после закрепления вагонов от ухода укладкой под колеса вагонов тормозных башмаков и приведения в действие ручных тормозов. Перед отцепкой локомотива от состава должны быть приведены в действие также и автотормоза оставляемых вагонов (полным открытием концевого крана).</p>
      <p>Хвост такого локомотива должен быть обозначен днем - развернутым желтым флагом у буферного бруса с правой стороны, ночью - желтым огнем фонаря.</p>
      <p>Не разрешается использование локомотива пассажирского поезда для доставки требования на железнодорожную станцию.</p>
      <p>В случае обрыва автосцепных устройств вагонов машинист в соответствии с требованиями раздела XIV Правил № 151, обязан заявить ДНЦ через ДСП контрольную проверку тормозов.</p>
      <p><strong>6.9.</strong> Порядок действий при выявлении схода подвижного состава.</p>
      <p>При выявлении схода подвижного состава локомотивная бригада обязана:</p>
      <ul className="ul-article">
        <li>немедленно приступить к закреплению вагонов, стоящих на рельсах после сошедших вагонов, в соответствии требованием раздела III.7 Правил No 151;</li>
        <li>произвести ограждение места схода в соответствие с нормами и правилами ограждения и в соответствие с требованиями пунктов 48-49 ИСИ доложить машинисту поезда.</li>
      </ul>
      <p>Машинист поезда, получив информацию о сходе подвижного состава, обязан:</p>
      <ul className="ul-article">
        <li>доложить машинистам встречных и вслед идущих поездов, ДНЦ (ДСП, ограничивающих перегон);</li>
        <li>включить красные огни буферных фонарей;</li>
        <li>после личного осмотра места схода передать ДСП, ограничивающих перегон (ДНЦ) следующую информацию:
          <ul className="ul-article">
            <li>имеются ли человеческие жертвы,</li>
            <li>наличие габарита по соседнему пути,</li>
            <li>точно указать на каком километре и пикете произошел сход, характер местности, имеются ли подъезды к железнодорожному пути,</li>
            <li>сколько единиц подвижного состава сошло с рельсов (есть ли сход локомотива),</li>
            <li>номера сошедших вагонов, порядковый номер первого сошедшего вагона с головы поезда, разрыв между вагонами (в метрах);</li>
          </ul>
        </li>
        <li>данные о состоянии контактной сети и опор контактной сети;</li>
        <li>данные о состоянии и целостности устройств инфраструктуры (пути, устройств СЦБ);</li>
        <li>в дальнейшем руководствоваться указаниями ДНЦ.</li>
      </ul>
      <p>При возникновении аварийных ситуаций с вагонами занятыми опасными грузами (ОГ) машинист локомотива незамедлительно сообщает об этом по поездной радиосвязи или любым другим возможным в создавшейся ситуации видам связи ДНЦ, ДСП ближайших станций, ограничивающим перегон.</p>
      <p>Машинист локомотива и его помощник имеют право вскрыть пакет с перевозочными документами.</p>
      <p>Сообщение должно включать в себя описание характера аварийной ситуации, сведения о наличии пострадавших, содержащиеся в перевозочных документах наименование груза, номер аварийной карточки (номер ООН груза, при наличии), количество опасного груза в зоне аварийной ситуации, а на электрифицированных участках - сведения о необходимости снятия напряжения в контактной сети.</p>
      <p>В условиях аварийной ситуации локомотивная бригада принимает меры, руководствуясь указаниями, содержащимися в аварийной карточке на данный опасный груз.</p>
      <p><strong>6.10. </strong>Порядок действий при выявлении срыва стоп-крана в пассажирском поезде.</p>
      <p>Если при осмотре пассажирского поезда выяснится, что падение давления в тормозной магистрали произошло из-за срыва стоп-крана, по причине обнаружения посторонних шумов, рывков, ударов, срабатывания СКНБ, СКНР то дальнейший осмотр производится машинистом совместно с начальником поезда.</p>
      <p>По результатам осмотра решение о порядке дальнейшего следования принимается начальником поезда совместно с машинистом. Машинист локомотива должен получить акт установленной формы, который составляет начальник поезда по факту и о причинах срыва стоп-крана.</p>
      <p>Причину срыва стоп-крана машинист докладывает ДНЦ.</p>
      <p><strong>6.11.</strong> В грузовых поездах, в случае срыва стоп-крана (в вагонах рефрижераторных секций, пассажирских вагонов и др.) действовать в соответствии с требованием пункта 179 Правил No 151.</p>
      <p>Решение о порядке дальнейшего следование по результатам осмотра принимается лицом, сопровождающим вагон, совместно с машинистом, с составлением акта рукописной формы и передачи его машинисту.</p>

    </React.Fragment>
  );
}