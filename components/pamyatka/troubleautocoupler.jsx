import React from 'react';
import TableTrouble from './tabletrouble.jsx';
import ButtonCollapseTrouble from '../speed/buttoncollapsespeed.jsx';

export default function TroubleAutoCuopler(props) {
  let id = "troubleautocoupler";
  return (
    <React.Fragment>
      <ButtonCollapseTrouble id = {id} text = "Автосцепка" />
      <div className="collapse" id={id} >
        <TableTrouble tbody = {data} />
      </div>
    </React.Fragment>
    );
}

const data = <React.Fragment>
  <tr>
    <td>Провисание головки автосцепки более допускаемых размеров   в   сравнении   с   автосцепкой  соседнего вагона.</td>
    <td>Излом    или    потеря    маятниковых подвесок,    центрирующей    балочки, что может привести к саморасцепу.</td>
  </tr>
</React.Fragment>;