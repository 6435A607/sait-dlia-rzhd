import React from 'react';

export default function ExampleOfaTrip(props) {
    return(
      <React.Fragment>
        <h4 className="text-center">
          Пример оформления поездки на обратной стороне предупреждения.
        </h4>
        <br/>
      <p className="font-italic">22.11.2015г.  явка 08:00ч.  маршрут № 4315</p>
      <p className="font-italic">2ТЭ116У-0106</p>
      <p className="font-italic">КР №13077691, ЭК (версия): BF0010   </p>
      <p className="font-italic">ТЧМ Крылов А.В. таб.№1644.  ТЧПМ Борец М.В. т.№3322</p>
      <p className="font-italic">ТЧМИ Ефимов А.Н.</p>
      <p className="font-italic">Секция А</p>
      <p className="font-italic">П.№1993 – вес 8639 – 288 осей.</p>
      <p className="font-italic">Гатчина Т.Б. (1 п.)    /          10:12ч.</p>
      <p className="font-italic">Елизаветино (2п.)  10:44ч.  10:50ч.              Отцепка толкача М62-1579</p>
      <p className="font-italic">Молосковицы(3п)   11:25ч.   11:30ч.</p>
      <p className="font-italic">Веймарн(5п)           ---------       11:40ч.	            Веймарн-Котлы-2 следование по 2 неправильному пути.</p>
      <p className="font-italic">Котлы-2 (1п)            12:13ч.    12:22ч.  	При следовании по 1А пути ст. Котлы пропала ЭК, запись          
                                                                                                                                                                            в ТУ-152.
  </p>
      <p className="font-italic">Лужская Сев. (8 п)    13:05ч.	Транзитный парк – Лужская ввод путь «О» по причине не  
                                                                                   соответствия ординат светофоров с ЭК. Запись в ТУ-152
  </p>
      <p className="font-italic">Состав закреплен ДСПП Петровой.</p>
      <p className="font-italic">Состав в кривых осмотрен замечаний нет.</p>
      </React.Fragment>
    );
  }