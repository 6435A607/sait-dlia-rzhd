import React from 'react';

export default function TableTrouble(props) {
  return (
    <React.Fragment>
      <table className="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th>Характерные признаки неисправностей</th>
            <th>Характер неисправности</th>
          </tr>
        </thead>
        <tbody>
          {props.tbody}
        </tbody>
      </table>
    </React.Fragment>
  );
}