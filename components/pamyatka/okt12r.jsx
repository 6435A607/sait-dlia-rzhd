import React from 'react';

export default function Okt12r(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">Об установлении непрерывной продолжительности работы локомотивных бригад на срок действия графика движения поездов на 2018 г.</h4>
    </React.Fragment>
  );
}