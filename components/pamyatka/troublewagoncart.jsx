import React from 'react';
import TableTrouble from './tabletrouble.jsx';
import ButtonCollapseTrouble from '../speed/buttoncollapsespeed.jsx';

export default function TroubleWagonCart(props) {
  let id = "troublewagoncart";
  return (
    <React.Fragment>
      <ButtonCollapseTrouble id = {id} text = "Тележка вагона" />
      <div className="collapse" id={id} >
        <TableTrouble tbody = {data} />
      </div>
    </React.Fragment>
    );
}

const data = <React.Fragment>
  <tr>
    <td>Перекос кузова вдоль или поперек с выходом вагона за пределы габарита.</td>
    <td>Излом       боковины,       надрессорной балки,     рессорного     подвешивания, неравномерное распределение груза.</td>
  </tr>
  <tr>
    <td>Провисание, волочение, скрежет от соприкосновения триангеля с  колесом,  искрение, наличие    следов    касания    деталей        рычажной передачи  о   верхнее   строение   пути,   стрелочный перевод, переездный настил, излом планки нижней негабаритности.</td>
    <td>Обрыв или разъединение тормозных и распорочных тяг, излом вертикальных    рычагов,    триангеля, траверсы и их подвесок.</td>
  </tr>
</React.Fragment>;