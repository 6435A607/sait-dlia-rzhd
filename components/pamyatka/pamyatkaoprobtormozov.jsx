import React from 'react';
import BrakesFullTest from '../brakes/brakesfulltest.jsx';
import BrakesTestOnePerson from '../brakes/brakestestoneperson.jsx';
import BrakesTestUzot from '../brakes/brakestestuzot.jsx';
import BrakesReducedTest from '../brakes/brakesreducedtest.jsx';
import BrakesTechnologicalTest from '../brakes/brakestechnologicaltest.jsx';

export default function PamyatkaBrakeTesting(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">Полная проба тормозов грузовой поезд.</h4>
      <br />
      <BrakesFullTest />
      <br />
      <h4 className="text-center">Проба автотормозов ВЧД в одно лицо</h4>
      <br />
      <BrakesTestOnePerson />
      <br />
      <h4 className="text-center">Проба после УЗОТ</h4>
      <br />
      <BrakesTestUzot />
      <br />
      <h4 className="text-center">Выполнение сокращенного опробования автотормозов в грузовых поездах после смены локомотивных бригад, когда локомотив от поезда не отцеплялся</h4>
      <br />
      <BrakesReducedTest />
      <br />
      <h4 className="text-center">Выполнение технологического опробования тормозов в грузовых поездах</h4>
      <br />
      <BrakesTechnologicalTest />
    </React.Fragment>
  );
}