import React from 'react';
import TableTrouble from './tabletrouble.jsx';
import ButtonCollapseTrouble from '../speed/buttoncollapsespeed.jsx';

export default function TroubleCarriage(props) {
  let id = "troublecarriage";
  return (
    <React.Fragment>
      <ButtonCollapseTrouble id = {id} text = "Пассажирский вагон" />
      <div className="collapse" id={id} >
        <TableTrouble tbody = {data} />
      </div>
    </React.Fragment>
    );
}

const data = <React.Fragment>
  <tr>
    <td>Провисание       генератора,       карданного       вала, редуктора,   нарушение   крепления   шкива,   следы царапин   повреждений   негабаритной   планки   или бруса.</td>
    <td>Обрыв        генератора,        редуктора, карданного   вала,   шкива   ременной передачи.</td>
  </tr>
  <tr>
    <td>Появление     из     дверей,     окон,     пола,     люков, аккумуляторного ящика вагона дыма или пламени.</td>
    <td>Возникновение пожара в вагоне.</td>
  </tr>
</React.Fragment>;