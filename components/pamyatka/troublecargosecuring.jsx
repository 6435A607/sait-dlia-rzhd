import React from 'react';
import TableTrouble from './tabletrouble.jsx';
import ButtonCollapseTrouble from '../speed/buttoncollapsespeed.jsx';

export default function TroubleCargoSecuring(props) {
  let id = "troublecargosecuring";
  return (
    <React.Fragment>
      <ButtonCollapseTrouble id = {id} text = "Крепление грузов" />
      <div className="collapse" id={id} >
        <TableTrouble tbody = {data} />
      </div>
    </React.Fragment>
    );
}

const data = <React.Fragment>
  <tr>
    <td>Обрыв растяжек, излом щитов ограждений, укрытий верхней части погрузки, обрыв металлических стяжек. Выход груза за габарит.</td>
    <td>Расстройство погрузки.</td>
  </tr>
</React.Fragment>;