import React from 'react';
import Acceptance from '../mi/acceptance.jsx';
import TapePulls from '../mi/tapepulls.jsx';
import TrailerToTrain from '../mi/trailertotrain.jsx';

export default function PamyatkaAcceptance(props) {
    return (
      <React.Fragment>
        <Acceptance />
        <TapePulls />
        <br />
        <TrailerToTrain />
      </React.Fragment>
    );
}