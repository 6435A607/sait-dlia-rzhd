import React from 'react';
import TableTrouble from './tabletrouble.jsx';
import ButtonCollapseTrouble from '../speed/buttoncollapsespeed.jsx';

export default function TroubleAutoBrakes(props) {
  let id = "troubleautobrakes";
  return (
    <React.Fragment>
      <ButtonCollapseTrouble id = {id} text = "Автоматический тормоз" />
      <div className="collapse" id={id} >
        <TableTrouble tbody = {data} />
      </div>
    </React.Fragment>
    );
}

const data = <React.Fragment>
  <tr>
    <td>Колесные пары вращаются, наблюдается кругообразное искрение и выделение синего дыма.</td>
    <td>Не       полностью       отпущен       или неисправный тормоз.</td>
  </tr>
</React.Fragment>;