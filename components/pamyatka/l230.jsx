import React from 'react';

export default function L230(props) {
    return(
      <React.Fragment>
        <h4 className="text-center text-uppercase">
          ОАО "РОССИЙСКИЕ ЖЕЛЕЗНЫЕ ДОРОГИ"
        </h4>
        <br/>
        <p className="text-right text-uppercase">утверждена</p>
        <p className="text-right">распоряжением <span className="text-uppercase">оао "ржд"</span></p>
        <p className="text-right">от 04.02.2019 г. N 183р</p>
        <br/>
        <h4 className="text-center text-uppercase">
          ИНСТРУКЦИЯ ПО ЭКСПЛУАТАЦИИ ЛОКОМОТИВНЫХ УСТРОЙСТВ БЕЗОПАСНОСТИ
        </h4>
        <h4 className="text-center text-uppercase">
          Л230
        </h4>
   
      </React.Fragment>
    );
  }