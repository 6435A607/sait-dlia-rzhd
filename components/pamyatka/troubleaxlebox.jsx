import React from 'react';
import TableTrouble from './tabletrouble.jsx';
import ButtonCollapseTrouble from '../speed/buttoncollapsespeed.jsx';

export default function TroubleAxleBox(props) {
  let id = "troubleaxlebox";
  return (
    <React.Fragment>
      <ButtonCollapseTrouble id = {id} text = "Букса" />
      <div className="collapse" id={id} >
        <TableTrouble tbody = {data} />
      </div>
    </React.Fragment>
    );
}

const data = <React.Fragment>
  <tr>
    <td>Дым, пар, искры  или пламя из корпуса буксы. При сильном нагреве корпус буксы может иметь красный или белый цвет, отсутствие  смотровой крышки роликовой буксы или нарушение её крепления.</td>
    <td>Грение буксы</td>
  </tr>
  <tr>
    <td>Вибрация тележки, частые вертикальные колебания роликовой буксы, резкий стук рычажной передачи.</td>
    <td>Разрушен сепаратор роликового подшипника, ролики сгруппировались в нижней части буксы. Возможен нагрев буксы.</td>
  </tr>
</React.Fragment>;