import React from 'react';
import TableTrouble from './tabletrouble.jsx';
import ButtonCollapseTrouble from '../speed/buttoncollapsespeed.jsx';

export default function TroubleWheelPair(props) {
  let id = "troublewheelpair";
  return (
    <React.Fragment>
      <ButtonCollapseTrouble id={id} text="Колесная пара" />
      <div className="collapse" id={id} >
        <TableTrouble tbody={data} />
      </div>
    </React.Fragment>
  );
}

const data = <React.Fragment>
  <tr>
    <td>Слышен  сильный  стук,  частые  удары  колеса  о рельс.</td>
    <td>Ползун (выбоина), «навар» (смещение металла) на поверхности катания колеса свыше допускаемых размеров.</td>
  </tr>
  <tr>
    <td>Колесо не вращается (юз), между колесом и рельсом при   свистяще-шипящем звуке видно искренне из-под колеса. </td>
    <td>Заклинивание   колесной   пары   из-за неисправности воздухораспределителя, авторегулятора, рычажной передачи, тормозного башмака или заклинивания роликового подшипника.</td>
  </tr>
  <tr>
    <td>Перекос    боковины   тележки    и   кузова   вагона, характерный писк, свист от трения металла, искры между  колесной  парой  и  буксой,  между  рамой тележки и колесной парой, скрежет металла.</td>
    <td>Излом шейки оси колесной пары.</td>
  </tr>
  <tr>
    <td>Виляние колеса, трение боковой грани обода колеса о   внутреннюю   грань   головки   рельса,   слышен дополнительный шум.</td>
    <td>Сдвиг колеса на оси или сход колесной пары.</td>
  </tr>
</React.Fragment>;