import React from 'react';
import TroubleAxleBox from './troubleaxlebox.jsx';
import TroubleWheelPair from './troublewheelpair.jsx';
import TroubleAutoCuopler from './troubleautocoupler.jsx';
import TroubleWagonCart from './troublewagoncart.jsx';
import TroubleCarriegeBody from './troublecarriagebody.jsx';
import TroubleAutoBrakes from './troubleautobrakes.jsx';
import TroubleCarriage from './troublecarriage.jsx';
import TroubleCargoSecuring from './troublecargosecuring.jsx';
import TroubleCompositionFence from './troublecompositionfence.jsx';

export default function PamyatkaTroubles(props) {
  return (
    <React.Fragment>
      <h4 className="text-center">Наиболее распространенные неисправности подвижного состава, крепления грузов и их характерные признаки</h4>
      <TroubleAxleBox />
      <TroubleWheelPair />
      <TroubleAutoCuopler />
      <TroubleWagonCart />
      <TroubleCarriegeBody />
      <TroubleAutoBrakes />
      <TroubleCarriage />
      <TroubleCargoSecuring />
      <TroubleCompositionFence />
    </React.Fragment>
  );
}