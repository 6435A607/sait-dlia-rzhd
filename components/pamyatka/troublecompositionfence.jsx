import React from 'react';
import TableTrouble from './tabletrouble.jsx';
import ButtonCollapseTrouble from '../speed/buttoncollapsespeed.jsx';

export default function TroubleCompositionFence(props) {
  let id = "troublecompositionfence";
  return (
    <React.Fragment>
      <ButtonCollapseTrouble id = {id} text = "Ограждение состава" />
      <div className="collapse" id={id} >
        <TableTrouble tbody = {data} />
      </div>
    </React.Fragment>
    );
}

const data = <React.Fragment>
  <tr>
    <td>Отсутствия сигнального диска на концевом брусе последнего вагона поезда.</td>
    <td>Утеря      сигнального      диска      или проследование  поезда не  в  полном составе вследствие саморасцепа.</td>
  </tr>
</React.Fragment>;