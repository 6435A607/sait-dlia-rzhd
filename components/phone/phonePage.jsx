import React from 'react';
import dataPhone from './phoneData.jsx';
import PhoneCell from './phoneCell.jsx';

export default function PhonePage(props) {
  return (
    <>
      <h4 className="text-center">Телефонные номера</h4>
      <br />
      {dataPhone.map((person) => <PhoneCell tel={person.tel} name={person.name} />)}
    </>
  );
}