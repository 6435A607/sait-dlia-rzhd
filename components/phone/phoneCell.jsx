import React from 'react';

export default function PhoneCell(props) {
  return (
    <>
      <a href= {"tel: " + props.tel} className="btn btn-info rounded-0 col">{props.name}</a>
    </>
  );
}