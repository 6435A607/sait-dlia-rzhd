import React from 'react';

const dataPhone = [
  {
    name: "Билетное Бюро",
    tel: "88124572292"
  },
  {
    name: "Бухгалтерия",
    tel: "4575202"
  },
  {
    name: "Бухгалтерия",
    tel: "4575535"
  },
  {
    name: "ВУБ",
    tel: "7685681"
  },
  {
    name: "Группа Учета",
    tel: "4364412"
  },
  {
    name: "Диспетчер Локомотивный",
    tel: "88124368778"
  },
  {
    name: "Диспетчер Локомотивный",
    tel: "88124582007"
  },
  {
    name: "Диспетчер Локомотивный",
    tel: "88124579006"
  },
  {
    name: "ДНЦ Кириши",
    tel: "88124579617"
  },
  {
    name: "ДНЦ Лужская",
    tel: "88124586967"
  },
  {
    name: "ДНЦ-Луга",
    tel: "4579017"
  },
  {
    name: "ДНЦ Нарвский",
    tel: "88124579043"
  },
  {
    name: "ДНЦ Оредеж",
    tel: "88124579055"
  },
  {
    name: "ДНЦ Региональный",
    tel: "4368995"
  },
  {
    name: "ДНЦ Региональный мобильный",
    tel: "89313669924"
  },
  {
    name: "ДНЦ Узловой",
    tel: "88124579012"
  },
  {
    name: "ДСП Автово",
    tel: "4575165"
  },
  {
    name: "ДСП Веймарн",
    tel: "4363022"
  },
  {
    name: "ДСП ГТБ",
    tel: "88124363423"
  },
  {
    name: "ДСП Купчинская",
    tel: "88124575006"
  },
  {
    name: "ДСП Лужская",
    tel: "88124363033"
  },
  {
    name: "ДСП Луга",
    tel: "4363761"
  },
  {
    name: "ДСП Нарвская",
    tel: "88124575601"
  },
  {
    name: "ДСП Новый Порт",
    tel: "4586124"
  },
  {
    name: "ДСП Предпортовая",
    tel: "4572719"
  },
  {
    name: "ДСП Рыбацкое",
    tel: "4581477"
  },
  {
    name: "ДСП СПб Витеб Пасс",
    tel: "4364290"
  },
  {
    name: "ДСП СПСМ 1 парк",
    tel: "4581778"
  },
  {
    name: "ДСП СПСМ 2 парк",
    tel: "4362161"
  },
  {
    name: "ДСП СПСМ 5 парк",
    tel: "4581264"
  },
  {
    name: "ДСП СПСМ 5 парк",
    tel: "4362817"
  },
  {
    name: "ДСП СПСМ 6 парк",
    tel: "4362772"
  },
  {
    name: "ДСП Строганово",
    tel: "4572954"
  },
  {
    name: "ДСП Сиверская",
    tel: "4572311"
  },
  {
    name: "ДСП Фрезерская",
    tel: "4572549"
  },
  {
    name: "ДСП Царское Село",
    tel: "88124666741"
  },
  {
    name: "ДСП Царское Село",
    tel: "88124586225"
  },
  {
    name: "Инженер по обучению",
    tel: "4573710"
  },
  {
    name: "Кадры ТЧ-12",
    tel: "4581909"
  },
  {
    name: "Кадры ТЧ-14",
    tel: "4364210"
  },
  {
    name: "Книга замечаний",
    tel: "89319691761"
  },
  {
    name: "Нарядчик Старший",
    tel: "4573623"
  },
  {
    name: "Нарядчики",
    tel: "4573639"
  },
  {
    name: "НРЛБ",
    tel: "4573568"
  },
  {
    name: "НЭВЗ Горячая линия",
    tel: "88006000904"
  },
  {
    name: "Операторы",
    tel: "4575147"
  },
  {
    name: "Отдел пути",
    tel: "89214211642"
  },
  {
    name: "Отдел пути",
    tel: "4364218"
  },
  {
    name: "Отдел эксплуатации ТЧ-14",
    tel: "88124364791"
  },
  {
    name: "Охрана труда",
    tel: "88124364190"
  },
  {
    name: "ПРМО Шушары",
    tel: "4586034"
  },
  {
    name: "Психологи",
    tel: "4573511"
  },
  {
    name: "ПЧ-11",
    tel: "4572016"
  },
  {
    name: "ПЧ-19",
    tel: "89213469258"
  },
  {
    name: "ПЧ-19",
    tel: "4575136"
  },
  {
    name: "ПЧ-19",
    tel: "89213727739"
  },
  {
    name: "ПЧ-24",
    tel: "4363428"
  },
  {
    name: "РЖД Горячая линия",
    tel: "88002006767"
  },
  {
    name: "РЖД Поликлиника",
    tel: "7663765"
  },
  {
    name: "РЖД Поликлиника",
    tel: "4900422"
  },
  {
    name: "РЖД Справочное бюро",
    tel: "4576111"
  },
  {
    name: "Склад ТЧ-14",
    tel: "4575153"
  },
  {
    name: "Секретарь ТЧ-14",
    tel: "4573637"
  },
  {
    name: "Тех отдел",
    tel: "4364377"
  },
  {
    name: "ТЧ-12 21й Контрольный Пост",
    tel: "4362224"
  },
  {
    name: "ТЧД ГТб",
    tel: "4572536"
  },
  {
    name: "ТЧД ГТб мобильный",
    tel: "89218872451"
  },
  {
    name: "ТЧД Лужская",
    tel: "4586965"
  },
  {
    name: "ТЧД Лужская",
    tel: "89213030665"
  },
  {
    name: "ТЧД Веймарн",
    tel: "89636474762"
  },
  {
    name: "ТЧД 12 Электровозы",
    tel: "4581082"
  },
  {
    name: "ТЧД 12 Тепловозы",
    tel: "4581454"
  },
  {
    name: "ТЧД 14",
    tel: "4575783"
  },
  {
    name: "ТЧД 21",
    tel: "89213902537"
  },
  {
    name: "ТЧД Шушары",
    tel: "4575568"
  },


];

export default dataPhone;