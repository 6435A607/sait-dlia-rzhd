import React from 'react';
import LinksList from './linkslist.jsx'

export default class Header extends React.Component {
    constructor(props) {
      super(props);
      this.handleMenuClick = this.handleMenuClick.bind(this);
    }
  
    handleMenuClick(e) {
      if(e.target.attributes[1].name === 'href') $("#collapsibleNavbar").collapse('toggle');
    }
  
    render() {
      return (
        <nav className="navbar navbar-expand-lg  bg-danger justify-content-between justify-content-lg-center">
          <a href="/"><h1>Автотормоза</h1></a>
          <button className="navbar-toggler " type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span className="fa fa-bars"></span>
          </button>
          <div className="collapse navbar-collapse" id="collapsibleNavbar">
          <div className="list-group d-block d-lg-none" onClick={this.handleMenuClick}>
            <LinksList />
          </div>
          </div>
        </nav>
      );
    }
  }