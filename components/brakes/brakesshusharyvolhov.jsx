import React from 'react';

export default function BrakesShusharyVolhov(props) {
    return(
      <React.Fragment>
        <table className="table table-responsive table-striped table-bordered table-secondary">
          <thead>
            <tr>
              <th colspan="2" className="tcw46" >Место проверки тормозов  (перегон, километр, пикет)</th>
              <th scope="col" className="tcw12">Скорость начала торможения</th>
              <th scope="col" className="tcw12">Тормозной путь в метрах при снижении скорости на 10 км/час</th>
              <th scope="col" className="tcw30">Примечание</th>
            </tr>
          </thead>
          <tbody>
            <tr className="table-light"><td colspan="5">Участок Шушары-Рыбацкое-Волховстрой-1</td></tr>
            <tr>
              <td>Шушары-Купчинская</td>
              <td>2км 2пк</td>
              <td>25</td>
              <td>230</td>
              <td>Основное место проверки  (четное)</td>
            </tr>
            <tr>
              <td>Купчинская-Рыбацкое</td>
              <td>2км 1пк</td>
              <td>25</td>
              <td>250</td>
              <td>Основное место проверки  (четное)</td>
            </tr>
            <tr>
              <td>Рыбацкое-Ижоры</td>
              <td>19км 1пк</td>
              <td>50</td>
              <td>320</td>
              <td>Основное место проверки  (четное)</td>
            </tr>
            <tr>
              <td>Ижоры-Саперная</td>
              <td>23км 6пк</td>
              <td>60</td>
              <td>415</td>
              <td>Доп.место проверки (четное)</td>
            </tr>
            <tr>
              <td>Саперная-Пелла</td>
              <td>31км 5пк</td>
              <td>60</td>
              <td>500</td>
              <td>Доп.место проверки (четное)</td>
            </tr>
            <tr>
              <td>Пелла-Горы</td>
              <td>39км 9пк</td>
              <td>60</td>
              <td>530</td>
              <td>Доп.место проверки (четное)</td>
            </tr>
            <tr>
              <td>Горы-Мга</td>
              <td>44км 6пк</td>
              <td>60</td>
              <td>420</td>
              <td>Доп.место проверки (четное)</td>
            </tr>
            <tr>
              <td>Мга-Назия</td>
              <td>56км 1пк</td>
              <td>60</td>
              <td>370</td>
              <td>Доп.место проверки (четное)</td>
            </tr>
            <tr>
              <td>Назия-Жихарево</td>
              <td>73км 5пк</td>
              <td>60</td>
              <td>400</td>
              <td>Доп.место проверки (четное)</td>
            </tr>
            <tr>
              <td>Жихарево-Войбокало</td>
              <td>87км 1пк</td>
              <td>60</td>
              <td>460</td>
              <td>Доп.место проверки (четное)</td>
            </tr>
            <tr>
              <td>Новый Быт-Пупышево</td>
              <td>108км 1пк</td>
              <td>60</td>
              <td>435</td>
              <td>Доп.место проверки (четное)</td>
            </tr>
            <tr>
              <td>Пупышево-Б.Пост 116км</td>
              <td>115км 1пк</td>
              <td>60</td>
              <td>450</td>
              <td>Доп.место проверки (четное)</td>
            </tr>
            <tr>
              <td>Пороги - Куколь</td>
              <td>13км 10пк</td>
              <td>60</td>
              <td>415</td>
              <td>Доп.место проверки (четное)</td>
            </tr>
            <tr className="table-warning">
              <td>Пороги - Куколь</td>
              <td>19км 5пк</td>
              <td>50</td>
              <td>430</td>
              <td>Через Пороги при открытом Входном ст. Куколь!!!</td>
            </tr>
          </tbody>
        </table>
      </React.Fragment>
    );
  }