import React from 'react';

export default function BrakesPskovDno(props) {
  return (
    <React.Fragment>
      <table class="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th colspan="2" className="tcw46">Место проверки тормозов  (перегон, километр, пикет)</th>
            <th scope="col" className="tcw12">Скорость начала торможения</th>
            <th scope="col" className="tcw12">Тормозной путь в метрах при снижении скорости на 10 км/час</th>
            <th scope="col" className="tcw30">Примечание</th>
          </tr>
        </thead>
        <tbody>
          <tr className="table-light"> <td colspan="5">Участок Псков-Дно  грузовое движение</td> </tr>
          <tr> <td>Псков пасс.-Березки</td> <td>652км 5пк</td> <td>50</td> <td>315</td> <td>Основное место проверки (четное)</td> </tr>
          <tr> <td>Березеки -Кеб</td> <td>646км 5пк</td> <td>50</td> <td>355</td> <td>Основное место проверки (четное)</td> </tr>
          <tr> <td>Карамышево-Вешки </td> <td>622км 5пк</td> <td>50</td> <td>315</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Вешки -Подсевы</td> <td>611км 5пк</td> <td>50</td> <td>315</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Подсевы-Лунево</td> <td>602км 5пк</td> <td>50</td> <td>360</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Лунево-Порхов</td> <td>593км 5пк</td> <td>50</td> <td>360</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Порхов -Роща</td> <td>574км 5пк</td> <td>50</td> <td>360</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Роща-Дно</td> <td>559км 10пк</td> <td>50</td> <td>360</td> <td>Доп.место проверки (четное)</td> </tr>
        </tbody>
      </table>
    </React.Fragment>
  );
}