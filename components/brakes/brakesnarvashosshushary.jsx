import React from 'react';

export default function BrakesNarvaShosShushary(props) {
  return (
    <React.Fragment>
      <table class="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th colspan="2" className="tcw46">Место проверки тормозов  (перегон, километр, пикет)</th>
            <th scope="col" className="tcw12">Скорость начала торможения</th>
            <th scope="col" className="tcw12">Тормозной путь в метрах при снижении скорости на 10 км/час</th>
            <th scope="col" className="tcw30">Примечание</th>
          </tr>
        </thead>
        <tbody>
          <tr className="table-light"> <td colspan="5">Участок Нарва-Шоссейная-Шушары  грузовое движение</td> </tr>
          <tr> <td>Ивангород-Сала</td> <td>155км 5пк</td> <td>50</td> <td>330</td> <td>Основное место проверки  (четное)</td> </tr>
          <tr> <td>Сала-Кингисепп</td> <td>143км 5пк</td> <td>50</td> <td>330</td> <td>Основное место проверки  (четное)</td> </tr>
          <tr className="table-warning"> <td>Веймарн-Волосово</td> <td>105км 4пк</td> <td>60</td> <td>350</td> <td>Основное место проверки  (четное)</td> </tr>
          <tr> <td>Молосковицы-Волосово</td> <td>91км 2пк</td> <td>50</td> <td>310</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Волосово-Кикерино</td> <td>81км 10пк</td> <td>50</td> <td>340</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Кикерино-Елизаветино</td> <td>74км 1пк</td> <td>50</td> <td>310</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Елизаветино-Войсковицы</td> <td>62км 2пк</td> <td>50</td> <td>310</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Войсковицы-Гатчина тов</td> <td>53км 10пк</td> <td>50</td> <td>510</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Гатчина Варш.-Верево</td> <td>42км 4пк</td> <td>50</td> <td>400</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Верево-Александровская</td> <td>27км 6пк</td> <td>50</td> <td>380</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Александровская-Шоссейная</td> <td>18км 1пк</td> <td>50</td> <td>460</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Ср.Рогатская-Шушары</td> <td>2км 2пк</td> <td>25</td> <td>260</td> <td>Основное место проверки  (четное)</td> </tr>
        </tbody>
      </table>
    </React.Fragment>
  );
}