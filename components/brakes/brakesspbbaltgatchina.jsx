import React from 'react';

export default function BrakesSpbBaltGatchina(props) {
  return (
    <React.Fragment>
      <table class="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th colspan="2" className="tcw46">Место проверки тормозов  (перегон, километр, пикет)</th>
            <th scope="col" className="tcw12">Скорость начала торможения</th>
            <th scope="col" className="tcw12">Тормозной путь в метрах при снижении скорости на 10 км/час</th>
            <th scope="col" className="tcw30">Примечание</th>
          </tr>
        </thead>
        <tbody>
          <tr className="table-light"><td colspan="5">Участок СПб.Балтийский-Гатчина Балт.пасс.  грузовое движение </td></tr>
          <tr><td>СПб.Балтийский-Лигово	8км 9пк</td>	<td>25</td>	<td>240</td>	<td>Основное место проверки  (нечетное)</td></tr>
          <tr><td>Лигово-Красное Село	32км 9пк</td>	<td>25</td>	<td>260</td>	<td>Основное место проверки  (нечетное)</td></tr>
        </tbody>
      </table>
    </React.Fragment>
  );
}