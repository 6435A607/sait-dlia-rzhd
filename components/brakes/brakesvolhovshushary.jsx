import React from 'react';

export default function BrakesVolhovShushary(props) {
    return (
      <React.Fragment>
        <table class="table table-responsive table-striped table-bordered table-secondary">
          <thead>
            <tr>
              <th colspan="2" className="tcw46">Место проверки тормозов  (перегон, километр, пикет)</th>
              <th scope="col" className="tcw12">Скорость начала торможения</th>
              <th scope="col" className="tcw12">Тормозной путь в метрах при снижении скорости на 10 км/час</th>
              <th scope="col" className="tcw30">Примечание</th>
            </tr>
          </thead>
          <tbody>
          <tr className="table-light"><td colspan="5">Участок Волховстрой-1-Рыбацкое-Шушары</td></tr>
            <tr>
              <td>Куколь-Пороги</td>
              <td>12км 5пк</td>
              <td>60</td>
              <td>460</td>
              <td>Доп.место проверки (нечетное)</td>
            </tr>
            <tr>
              <td>Б.Пост 116км-Пупышево</td>
              <td>113км 8пк</td>
              <td>50</td>
              <td>305</td>
              <td>Основное место проверки  (нечетное)</td>
            </tr>
            <tr>
              <td>Пупышево-Новый Быт</td>
              <td>104км 10пк</td>
              <td>60</td>
              <td>470</td>
              <td>Доп.место проверки (нечетное)</td>
            </tr>
            <tr>
              <td>Новый Быт-Войбокало</td>
              <td>94км 10пк</td>
              <td>60</td>
              <td>440</td>
              <td>Доп.место проверки (нечетное)</td>
            </tr>
            <tr>
              <td>Войбокало-Жихарево</td>
              <td>84км 5пк</td>
              <td>60</td>
              <td>415	</td>
              <td>Доп.место проверки (нечетное)</td>
            </tr>
            <tr>
              <td>Жихарево-Назия</td>
              <td>70км 5пк</td>
              <td>60</td>
              <td>560</td>
              <td>Доп.место проверки (нечетное)</td>
            </tr>
            <tr>
              <td>Назия-Мга</td>
              <td>64км 5пк</td>
              <td>60</td>
              <td>590</td>
              <td>Доп.место проверки (нечетное)</td>
            </tr>
            <tr>
              <td>Мга-Горы</td>
              <td>46км 3пк</td>
              <td>60</td>
              <td>410</td>
              <td>Доп.место проверки (нечетное)</td>
            </tr>
            <tr>
              <td>Горы-Пелла</td>
              <td>41км 4пк</td>
              <td>60</td>
              <td>425</td>
              <td>Доп.место проверки (нечетное)</td>
            </tr>
            <tr>
              <td>Пелла-Саперная</td>
              <td>34км 7пк</td>
              <td>60</td>
              <td>480</td>
              <td>Доп.место проверки (нечетное)</td>
            </tr>
            <tr>
              <td>Саперная-Ижоры</td>
              <td>25км 3пк</td>
              <td>60</td>
              <td>415</td>
              <td>Доп.место проверки (нечетное)</td>
            </tr>
            <tr>
              <td>Ижоры-Рыбацкое</td>
              <td>17км 4пк</td>
              <td>60</td>
              <td>415</td>
              <td>Доп.место проверки (нечетное)</td>
            </tr>
            <tr>
              <td>Купчинская-Шушары</td>
              <td>4км 3пк</td>
              <td>25</td>
              <td>180</td>
              <td>Основное место проверки  (нечетное)</td>
            </tr>
          </tbody>
        </table>
      </React.Fragment>
    );
  }