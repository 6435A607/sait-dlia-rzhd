import React from 'react';

export default function BrakesBudoMgaGatchina(props) {
  return (
    <React.Fragment>
      <table class="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th colspan="2" className="tcw46">Место проверки тормозов  (перегон, километр, пикет)</th>
            <th scope="col" className="tcw12">Скорость начала торможения</th>
            <th scope="col" className="tcw12">Тормозной путь в метрах при снижении скорости на 10 км/час</th>
            <th scope="col" className="tcw30">Примечание</th>
          </tr>
        </thead>
        <tbody>
          <tr className="table-light"> <td colspan="5">Участок Будогощь-Мга-Гатчина тов.  грузовое движение</td> </tr>
          <tr> <td>Будогощь-Пчевжа</td> <td>93км 5пк</td> <td>50</td> <td>450</td> <td>Основное место проверки  (нечетное)</td> </tr>
          <tr> <td>Пчевжа - Кириши</td> <td>81км 4пк</td> <td>50</td> <td>380</td> <td>Доп.место проверки  (нечетное)</td> </tr>
          <tr> <td>Кириши-Б/П 63км</td> <td>64км 5пк</td> <td>50</td> <td>350</td> <td>Основное место проверки  (нечетное)</td> </tr>
          <tr> <td>Посадниково-Жарок</td> <td>52км 7пк</td> <td>50</td> <td>300</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Жарок-Погостье</td> <td>37км 5пк</td> <td>50</td> <td>300</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Погостье-Сологубовка </td> <td>19км 1пк</td> <td>50</td> <td>350</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Сологубовка-Мга</td> <td>5км 6пк</td> <td>50</td> <td>380</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Мга-Пустынька</td> <td>14км 1пк</td> <td>50</td> <td>380</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Пустынька-Стекольный</td> <td>24км 7пк</td> <td>50</td> <td>450</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Стекольный-Новолисино</td> <td>30км 2пк</td> <td>50</td> <td>340</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr className="table-warning"> <td>Владимерская Фрезерный</td> <td>11км 5 пк</td> <td>60</td> <td>450</td> <td>Доп.место проверки (нечетное)</td> </tr>
        </tbody>
      </table>
    </React.Fragment>
  );
}