import React from 'react';

export default function BrakesShusharyVladNarva(props) {
  return (
    <React.Fragment>
      <table class="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th colspan="2" className="tcw46">Место проверки тормозов  (перегон, километр, пикет)</th>
            <th scope="col" className="tcw12">Скорость начала торможения</th>
            <th scope="col" className="tcw12">Тормозной путь в метрах при снижении скорости на 10 км/час</th>
            <th scope="col" className="tcw30">Примечание</th>
          </tr>
        </thead>
        <tbody>
          <tr className="table-light"> <td colspan="5">Участок Шушары-Владимирская-Нарва   грузовое движение</td> </tr>
          <tr> <td>Шушары-Царское Село</td> <td>20км 7пк</td> <td>40</td> <td>170</td> <td>Основное место проверки  (нечетное)</td> </tr>
          <tr> <td rowSpan="2">Войсковицы-Елизаветино</td> <td>63км 6пк</td> <td>50</td> <td>380</td> <td>Основное место проверки  (нечетное)</td> </tr>
          <tr className="table-warning"> <td>64км 1пк</td> <td>50</td> <td>460</td> <td>ДЛЯ ПМД Основное место проверки (нечетное)</td> </tr>
          <tr> <td>Елизаветино-Кикерино</td> <td>73км 2пк</td> <td>50</td> <td>370</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Кикерино-Волосово</td> <td>82км 10пк</td> <td>50</td> <td>340</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td rowSpan="2">Волосово-Молосковицы</td> <td>92км 1пк</td> <td>50</td> <td>310</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>103км 1пк</td> <td>50</td> <td>350</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Молосковицы-Веймарн</td> <td>113км 7пк</td> <td>50</td> <td>430</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Веймарн-Кингисепп</td> <td>125км 8пк</td> <td>50</td> <td>370</td> <td>Основное место проверки  (нечетное)</td> </tr>
          <tr> <td>Сала-Ивангород</td> <td>150км 1пк</td> <td>50</td> <td>350</td> <td>Основное место проверки  (нечетное)</td> </tr>
        </tbody>
      </table>
    </React.Fragment>
  );
}