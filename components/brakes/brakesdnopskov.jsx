import React from 'react';

export default function BrakesDnoPskov(props) {
  return (
    <React.Fragment>
      <table class="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th colspan="2" className="tcw46">Место проверки тормозов  (перегон, километр, пикет)</th>
            <th scope="col" className="tcw12">Скорость начала торможения</th>
            <th scope="col" className="tcw12">Тормозной путь в метрах при снижении скорости на 10 км/час</th>
            <th scope="col" className="tcw30">Примечание</th>
          </tr>
        </thead>
        <tbody>
          <tr className="table-light"> <td colspan="5">Участок Дно- Псков  грузовое движение</td> </tr>
          <tr> <td>Дно-Роща</td> <td>561км 5пк</td> <td>40</td> <td>245</td> <td>Основное место проверки  (нечетное)</td> </tr>
          <tr> <td>Роща-Порхов</td> <td>575км 5пк</td> <td>50</td> <td>360</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Порхов-Лунево</td> <td>587км 5пк</td> <td>50</td> <td>340</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Лунево-Подсевы</td> <td>605км 5пк</td> <td>50</td> <td>405</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Подсевы-Вешки</td> <td>611км 5пк</td> <td>50</td> <td>315</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Карамышево-Кеб</td> <td>638км 5пк</td> <td>50</td> <td>360</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Кеб-Березки</td> <td>649км 5пк</td> <td>50</td> <td>385</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Березки-Псков пасс.</td> <td>653км 5пк</td> <td>50</td> <td>335</td> <td>Основное место проверки  (нечетное)</td> </tr>
        </tbody>
      </table>
    </React.Fragment>
  );
}