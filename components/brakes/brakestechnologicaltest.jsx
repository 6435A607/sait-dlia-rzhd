import React from 'react';

export default function BrakesTechnologicalTest(props) {
    return (
        <React.Fragment>
            <img src="../img/9-7.PNG" className="img-fluid img-thumbnail mx-auto d-block" alt="image" />
            <br />
            <ol className="ol-article">
                <li>Проверка поддержания зарядного давления, замер плотности во 2м положении РКМ,которая не должна отличаться на 20% в любую сторону</li>
                <li>Проба по 5ти вагонов, ступень 0.6-0.7 кгс/см<sup>2</sup> , -отпуск тормозов, завышение на 0.3-0.7кгс/см<sup>2</sup> с отметкой о времени производства тех.пробы</li>
                <li>Проверка целостности ТМ перед отправлением (более 100 осей)</li>
            </ol>
            <p className="font-weight-bold">При данном виде опробовании протяжки ск/ленты запрещены</p>
            <p className="font-weight-bold">При сокращённом опробовании автотормозов на промежуточных станциях, ленту категорически запрещено протягивать</p>
        </React.Fragment>
    );
}