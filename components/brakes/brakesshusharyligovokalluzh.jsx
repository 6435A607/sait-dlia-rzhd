import React from 'react';

export default function BrakesShusharyLigovoKalLuzh(props) {
  return (
    <React.Fragment>
      <table class="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th colspan="2" className="tcw46">Место проверки тормозов  (перегон, километр, пикет)</th>
            <th scope="col" className="tcw12">Скорость начала торможения</th>
            <th scope="col" className="tcw12">Тормозной путь в метрах при снижении скорости на 10 км/час</th>
            <th scope="col" className="tcw30">Примечание</th>
          </tr>
        </thead>
        <tbody>
          <tr className="table-light"> <td colspan="5">Участок Шушары-Лигово-Калище-Лужская   грузовое движение</td> </tr>
          <tr> <td>Шушары-Ср.Рогатская</td> <td>9км 2пк</td> <td>25</td> <td>240</td> <td>Основное место проверки  (нечетное)</td> </tr>
          <tr> <td>Ср.Рогатская-Предпортовая</td> <td>12км 4пк</td> <td>40</td> <td>420</td> <td>Основное место проверки  (нечетное)</td> </tr>
          <tr> <td>Предпортовая-Лигово</td> <td>5км 6пк</td> <td>25</td> <td>200</td> <td>Основное место проверки  (нечетное)</td> </tr>
          <tr> <td>Лигово-Стрельна</td> <td>19км 5пк</td> <td>50</td> <td>320</td> <td>Основное место проверки  (нечетное)</td> </tr>
          <tr> <td>Стрельна-Новый Петергоф</td> <td>30км 10пк</td> <td>50</td> <td>320</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Ораниенбаум-Бронка</td> <td>44км 10пк</td> <td>50</td> <td>320</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Бронка-Б.Ижора</td> <td>49км 8пк</td> <td>50</td> <td>380</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Лебяжье-Калище</td> <td>72км5пк</td> <td>50</td> <td>290</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Калище-Копорье</td> <td>89км 2пк</td> <td>50</td> <td>310</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Капорье-Котлы</td> <td>110км 6пк</td> <td>50</td> <td>320</td> <td>Доп.место проверки (нечетное)</td> </tr>
        </tbody>
      </table>
    </React.Fragment>
  );
}