import React from 'react';

export default function BrakesBabaevoVolhov(props) {
  return (
    <React.Fragment>
      <table class="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th colspan="2" className="tcw46">Место проверки тормозов  (перегон, километр, пикет)</th>
            <th scope="col" className="tcw12">Скорость начала торможения</th>
            <th scope="col" className="tcw12">Тормозной путь в метрах при снижении скорости на 10 км/час</th>
            <th scope="col" className="tcw30">Примечание</th>
          </tr>
        </thead>
        <tbody>
          <tr className="table-light"> <td colspan="5">Участок Бабаево - Волховстрой-1 грузовое движение</td> </tr>
          <tr> <td>Бабаево-Тешемля</td> <td>337км 5пк</td> <td>60</td> <td>420</td> <td>Основное место проверки  (нечетное)</td> </tr>
          <tr> <td>Тешемля-Верхневольский</td> <td>324км 5пк</td> <td>60</td> <td>440</td> <td>Основное место проверки  (нечетное)</td> </tr>
          <tr> <td>Верхневольский-Заборье</td> <td>310км 10пк</td> <td>60</td> <td>415</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Заборье-Подборовье</td> <td>302км 10пк</td> <td>60</td> <td>425</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Подборовье-Ефимовская</td> <td>290км 5пк</td> <td>60</td> <td>480</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Коли-Пикалево-2</td> <td>251км 5пк</td> <td>60</td> <td>460</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Пикалево-2-Пикалево-1</td> <td>242км 5пк</td> <td>60</td> <td>560</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Пикалево-1-Большой Двор</td> <td>234км 5пк</td> <td>60</td> <td>600</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Большой Двор-Тихвин</td> <td>219км 5пк</td> <td>60</td> <td>570</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Тихвин-Цвыливо</td> <td>191км 5пк</td> <td>60</td> <td>510</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Цвыливо-Валя</td> <td>175км 5пк</td> <td>60</td> <td>430</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Валя-Мыслино</td> <td>150км 1пк</td> <td>60</td> <td>440</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Мыслино-Куколь</td> <td>138км 5пк</td> <td>60</td> <td>570</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Куколь-Волховстрой -2</td> <td>128км 5пк</td> <td>60</td> <td>415</td> <td>Доп.место проверки (нечетное)</td> </tr>
        </tbody>
      </table>
    </React.Fragment>
  );
}