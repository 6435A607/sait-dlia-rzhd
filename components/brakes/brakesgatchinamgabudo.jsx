import React from 'react';

export default function BrakesGatchinaMgaBudo(props) {
  return (
    <React.Fragment>
      <table class="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th colspan="2" className="tcw46">Место проверки тормозов  (перегон, километр, пикет)</th>
            <th scope="col" className="tcw12">Скорость начала торможения</th>
            <th scope="col" className="tcw12">Тормозной путь в метрах при снижении скорости на 10 км/час</th>
            <th scope="col" className="tcw30">Примечание</th>
          </tr>
        </thead>
        <tbody>
          <tr className="table-light"> <td colspan="5">Участок Гатчина тов.-Мга-Будогощь  грузовое движение</td> </tr>
          <tr> <td>Гатчина тов-Фрезерный</td> <td>5км 1пк</td> <td>50</td> <td>520</td> <td>Основное место проверки  (четное)</td> </tr>
          <tr> <td>Фрезерный-Владимирская</td> <td>10км 5пк</td> <td>50</td> <td>310</td> <td>Основное место проверки  (четное)</td> </tr>
          <tr> <td>Владимирская-Новолисино</td> <td>21км 5пк</td> <td>50</td> <td>370</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Новолисино-Стекольный</td> <td>31км 8пк</td> <td>50</td> <td>380</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Стекольный-Пустынька</td> <td>28км 5пк</td> <td>50</td> <td>310</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Пустынька-Войтоловка</td> <td>10км 8пк</td> <td>50</td> <td>370</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Войтоловка-Мга</td> <td>5км 5пк</td> <td>50</td> <td>430</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Сологубовка-Малукса</td> <td>13км 8пк</td> <td>50</td> <td>400</td> <td>Основное место проверки  (четное)</td> </tr>
          <tr> <td>Малукса-Погостье</td> <td>29км 6пк</td> <td>50</td> <td>340</td> <td>Основное место проверки  (четное)</td> </tr>
          <tr> <td>Погостье-Жарок</td> <td>38км 2пк</td> <td>50</td> <td>310</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Жарок-Посадниково</td> <td>45км 6пк</td> <td>50</td> <td>340</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Посадниково-Кириши</td> <td>61км 8пк</td> <td>50</td> <td>380</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Кириши-Пчевжа</td> <td>72км 5пк</td> <td>50</td> <td>325</td> <td>Основное место проверки  (четное)</td> </tr>
          <tr> <td>Пчевжа - Будогощь</td> <td>95км 7пк</td> <td>50</td> <td>380</td> <td>Доп.место проверки (четное)</td> </tr>
        </tbody>
      </table>
    </React.Fragment>
  );
}