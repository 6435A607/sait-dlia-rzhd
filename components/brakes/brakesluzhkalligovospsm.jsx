import React from 'react';

export default function BrakesLuzhKalLigovoSpsm(props) {
  return (
    <React.Fragment>
      <table class="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th colspan="2" className="tcw46">Место проверки тормозов  (перегон, километр, пикет)</th>
            <th scope="col" className="tcw12">Скорость начала торможения</th>
            <th scope="col" className="tcw12">Тормозной путь в метрах при снижении скорости на 10 км/час</th>
            <th scope="col" className="tcw30">Примечание</th>
          </tr>
        </thead>
        <tbody>
          <tr className="table-light"> <td colspan="5">Участок Лужская-Калище-Лигово-СПСМ 1парк   грузовое движение</td> </tr>
          <tr> <td>Лужская-Котлы</td> <td>15км 2пк</td> <td>50</td> <td>380</td> <td>Основное место проверки  (четное)</td> </tr>
          <tr> <td>Котлы-Копорье</td> <td>106км 7пк</td> <td>50</td> <td>400</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Копорье-Калище</td> <td>98км 4пк</td> <td>50</td> <td>400</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Калище-Лебяжье</td> <td>68км 10пк</td> <td>50</td> <td>330</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Лебяжье-Б.Ижора</td> <td>55км 2пк</td> <td>50</td> <td>330</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Б.Ижора-Бронка</td> <td>50км 2пк</td> <td>50</td> <td>350</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Бронка-Ораниенбаум</td> <td>45км 2пк</td> <td>50</td> <td>310</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Ораниенбаум-Стрельна</td> <td>28км 4пк</td> <td>50</td> <td>340</td> <td>Основное место проверки  (четное)</td> </tr>
          <tr> <td>Стрельна-Лигово</td> <td>18км 2пк</td> <td>50</td> <td>340</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Лигово-Броневая</td> <td>6км 9пк</td> <td>25</td> <td>270</td> <td>Основное место проверки  (четное)</td> </tr>
          <tr> <td>Броневая-Цветочная</td> <td>6км 7пк</td> <td>25</td> <td>280</td> <td>Основное место проверки  (четное)</td> </tr>
          <tr> <td>Волковская-СПСМ 1парк</td> <td>у телетайпа</td> <td>25</td> <td>250</td> <td>Основное место проверки  (четное)</td> </tr>
        </tbody>
      </table>
    </React.Fragment>
  );
}