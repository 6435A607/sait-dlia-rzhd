import React from 'react';

export default function BrakesSlanVeimarn(props) {
  return (
    <React.Fragment>
      <table class="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th colspan="2" className="tcw46">Место проверки тормозов  (перегон, километр, пикет)</th>
            <th scope="col" className="tcw12">Скорость начала торможения</th>
            <th scope="col" className="tcw12">Тормозной путь в метрах при снижении скорости на 10 км/час</th>
            <th scope="col" className="tcw30">Примечание</th>
          </tr>
        </thead>
        <tbody>
          <tr className="table-light"> <td colspan="5">Участок Сланцы - Веймарн  грузовое движение</td> </tr>
          <tr> <td>Рудничная-Вервенка</td> <td>172км 10пк</td> <td>50</td> <td>290</td> <td>Основное место проверки  (четное)</td> </tr>
          <tr> <td>Вервенка-Веймарн</td> <td>192км 5пк</td> <td>50</td> <td>310</td> <td>Доп.место проверки (четное)</td> </tr>
        </tbody>
      </table>
    </React.Fragment>
  );
}