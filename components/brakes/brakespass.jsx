import React from 'react';

export default function BrakesPass(props) {
  return (
    <React.Fragment>
      <table id="passenger" 
        className={props.collapse + " table table-responsive table-striped table-bordered table-secondary"}>
        <thead>
          <tr>
            <th colspan="2" className="tcw46" >Место проверки тормозов  (перегон, километр, пикет)</th>
            <th scope="col" className="tcw12">Скорость начала торможения</th>
            <th scope="col" className="tcw12">Тормозной путь в метрах при снижении скорости на 10 км/час</th>
            <th scope="col" className="tcw30">Примечание</th>
          </tr>
        </thead>
        <tbody>
          <tr className="table-light"> <td colSpan="5">Участок Санкт-Петербург Варш. Балт. – Луга – Псков - Пыталово</td> </tr>
          <tr> <td>СПб Балтийский - Броневая</td> <td>2км 10пк</td> <td>60</td> <td>275</td> <td></td> </tr>
          <tr> <td>Броневая–Предпортовая</td> <td>7км 10пк</td> <td>60</td> <td>230</td> <td></td> </tr>
          <tr> <td>Броневая–Лигово</td> <td>12км 1пк</td> <td>60</td> <td>240</td> <td></td> </tr>
          <tr> <td>Луга–Серебрянка</td> <td>150км 8пк</td> <td>60</td> <td>240</td> <td></td> </tr>
          <tr> <td>Луга–Серебрянка</td> <td>158км 1пк</td> <td>60</td> <td>270</td> <td></td> </tr>
          <tr> <td>Псков-тур.– Черёха</td> <td>277км 5пк</td> <td>60</td> <td>255</td> <td></td> </tr>
          <tr> <td>Псков-тур.– Черёха</td> <td>280км 1пк</td> <td>60</td> <td>270</td> <td></td> </tr>
          <tr className="table-light"> <td colSpan="5">Участок Пыталово – Псков – Луга - Санкт-Петербург Варш. Балт</td> </tr>
          <tr> <td>Пыталово–Ритупе</td> <td>365км10пк</td> <td>60</td> <td>230</td> <td></td> </tr>
          <tr> <td>Пыталово-Ритупе</td> <td>363км 10пк</td> <td>60</td> <td>200</td> <td></td> </tr>
          <tr> <td>Псков - Любятово</td> <td>276км 6пк</td> <td>50</td> <td>200</td> <td></td> </tr>
          <tr> <td>Любятово - Торошино</td> <td>266км 10пк</td> <td>60</td> <td>240</td> <td></td> </tr>
          <tr className="table-light"> <td colSpan="5">На тупиковый путь</td> </tr>
          <tr> <td>Верево - Александровская</td> <td>24км 10пк</td> <td>80</td> <td>570</td> <td></td> </tr>
          <tr> <td>Лигово - Броневая</td> <td>5км 10пк</td> <td>60</td> <td>260</td> <td></td> </tr>
          <tr className="table-light"> <td colSpan="5">Участок Санкт - Петербург – Витебский – Новосокольники  </td> </tr>
          <tr> <td>СПБ.Вит.пасс.-Тов.</td> <td>4км 6пк</td> <td>50</td> <td>300</td> <td></td> </tr>
          <tr> <td>СПБ.Вит.тов. - Шушары</td> <td>10км 5пк</td> <td>60</td> <td>300</td> <td></td> </tr>
          <tr> <td>Дно - Вязье</td> <td>251км 1пк</td> <td>60</td> <td>350</td> <td></td> </tr>
          <tr> <td>Дно - Вязье</td> <td>254км 5пк</td> <td>60</td> <td>300</td> <td></td> </tr>
          <tr className="table-light"> <td colSpan="5">Новосокольники -  Санкт - Петербург – Витебский</td> </tr>
          <tr> <td>Новосокольники - Шубино</td> <td>418км 10пк</td> <td>60</td> <td>350</td> <td></td> </tr>
          <tr> <td>Новосокольники - Шубино</td> <td>415км 5 пк</td> <td>60</td> <td>280</td> <td></td> </tr>
          <tr className="table-light"> <td colSpan="5">На тупиковый путь</td> </tr>
          <tr> <td>Шушары – СПБ.Вит.тов.</td> <td>7км 5пк</td> <td>60</td> <td>400</td> <td></td> </tr>
          <tr className="table-light"> <td colSpan="5">Участок  Дно – Псков</td> </tr>
          <tr> <td>Дно – Роща</td> <td>561км 5пк60</td> <td>245</td> <td></td> <td>ПТ</td> </tr>
          <tr> <td>Дно – Роща</td> <td>563км 5пк60</td> <td>130</td> <td></td> <td>ЭПТ</td> </tr>
          <tr className="table-light"> <td colSpan="5">При отправлении со ст. Гатчина Балт.-пас. (Гатчина тов.Б.)</td> </tr>
          <tr> <td>Гатчина-тов.Б. – Войсковицы</td> <td>52км 6пк</td> <td>60</td> <td>280</td> <td>ПТ</td> </tr>
          <tr> <td>Войсковицы – Елизаветино</td> <td>63км 6пк</td> <td>60</td> <td>280</td> <td>ЭПТ</td> </tr>
          <tr> <td>Кингисепп – Сала</td> <td>139км 1пк</td> <td>60</td> <td>230</td> <td>ПТ</td> </tr>
          <tr> <td>Кингисепп – Сала</td> <td>144км 1пк</td> <td>60</td> <td>220</td> <td>ЭПТ</td> </tr>
          <tr className="table-light"> <td colSpan="5">Участок  Нарва –Тосно</td> </tr>
          <tr> <td>Ивангород – Сала</td> <td>154км10пк</td> <td>60</td> <td>230</td> <td>ПТ</td> </tr>
          <tr> <td>Ивангород – Сала</td> <td>150км 10пк</td> <td>60</td> <td>240</td> <td>ЭПТ</td> </tr>
          <tr> <td>Кингисепп – Веймарн</td> <td>133км 5пк</td> <td>60</td> <td>240</td> <td>ПТ</td> </tr>
          <tr> <td>Веймарн – Молосковицы</td> <td>118км 1пк</td> <td>60</td> <td>280</td> <td>ЭПТ</td> </tr>
          <tr className="table-light"> <td colSpan="5">При отправлении со ст. Гатчина Балт.-пас. (Гатчина тов.Б.)</td> </tr>
          <tr> <td>Гатчина-тов.Балт. – Фрезерный</td> <td>5км 1пк</td> <td>60</td> <td>370</td> <td>ПТ</td> </tr>
          <tr> <td>Фрезерный – Владимиркая</td> <td>9км 10пк</td> <td>60</td> <td>290</td> <td>ЭПТ</td> </tr>
          <tr className="table-light"> <td colSpan="5">Участок Санкт-Петербург – Витебский – Новолисино</td> </tr>
          <tr> <td>СП.Б.-Вит.-тов.</td> <td>4км 6пк</td> <td>50</td> <td>320</td> <td>ПТ</td> </tr>
          <tr> <td>СП.Б.-Вит.-тов. – Шушары</td> <td>10км 5пк</td> <td>60</td> <td>300</td> <td>ЭПТ</td> </tr>
          <tr className="table-light"> <td colSpan="5">Дача Долгорукова- ручьи(чётное направление)</td> </tr>
          <tr> <td>Дача Долгорукова-Полюстрово</td> <td>10км3пк</td> <td>60</td> <td>250</td> <td>ПТ</td> </tr>
          <tr> <td>Полюстрово-Ручьи</td> <td>2км2пк</td> <td>60</td> <td>290</td> <td>ЭПТ</td> </tr>
          <tr className="table-light"> <td colSpan="5">Участок Суоярви. – Кузнечное чётное направление</td> </tr>
          <tr> <td>Суоярви - Пийтсийки</td> <td>397км8пк</td> <td>70</td> <td>377</td> <td>ПТ</td> </tr>
          <tr> <td>Суоярви - Пийтсийки</td> <td>394км 1пк</td> <td>70</td> <td>321</td> <td>ЭПТ</td> </tr>
          <tr> <td>Сортавала-Куокканизми</td> <td>257км 5пк</td> <td>50</td> <td>233</td> <td>ПТ</td> </tr>
          <tr> <td>Сортавала-Куокканизми</td> <td>255км 5пк</td> <td>60</td> <td>192</td> <td>ЭПТ</td> </tr>
        </tbody>
      </table>
    </React.Fragment>
  );
}