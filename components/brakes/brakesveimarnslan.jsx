import React from 'react';

export default function BrakesVeimarnSlan(props) {
  return (
    <React.Fragment>
      <table class="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th colspan="2" className="tcw46">Место проверки тормозов  (перегон, километр, пикет)</th>
            <th scope="col" className="tcw12">Скорость начала торможения</th>
            <th scope="col" className="tcw12">Тормозной путь в метрах при снижении скорости на 10 км/час</th>
            <th scope="col" className="tcw30">Примечание</th>
          </tr>
        </thead>
        <tbody>
          <tr className="table-light"> <td colspan="5">Участок Веймарн-Сланцы  грузовое движение</td> </tr>
          <tr> <td>Веймарн-Вервенка</td> <td>211км 5пк</td> <td>50</td> <td>310</td> <td>Основное место проверки  (нечетное)</td> </tr>
          <tr> <td>Вервенка-Рудничная</td> <td>175км 9пк</td> <td>50</td> <td>310</td> <td>Основное место проверки  (нечетное)</td> </tr>
        </tbody>
      </table>
    </React.Fragment>
  );
}