import React from 'react';

export default function BrakesSpbVitebVladLugaPskov(props) {
  return (
    <React.Fragment>
      <table class="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th colspan="2" className="tcw46">Место проверки тормозов  (перегон, километр, пикет)</th>
            <th scope="col" className="tcw12">Скорость начала торможения</th>
            <th scope="col" className="tcw12">Тормозной путь в метрах при снижении скорости на 10 км/час</th>
            <th scope="col" className="tcw30">Примечание</th>
          </tr>
        </thead>
        <tbody>
          <tr className="table-light"> <td colspan="5">Участок СПб.Витебский-Владимирская-Луга - Псков   грузовое движение</td> </tr>
          <tr> <td>СПб. Витебский - Шушары</td> <td>9 км 8 пк</td> <td>50</td> <td>290</td> <td>Основное место проверки  (нечетное)</td> </tr>
          <tr> <td>Шушары-Царское Село</td> <td>20км 7пк</td> <td>40</td> <td>170</td> <td>Основное место проверки  (нечетное)</td> </tr>
          <tr> <td>Гатчина Варш.-Суйда</td> <td>51км 1пк</td> <td>50</td> <td>340</td> <td>Основное место проверки  (нечетное)</td> </tr>
          <tr> <td>Суйда-Сиверская</td> <td>65км 6пк</td> <td>50</td> <td>430</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Сиверская-Строганово</td> <td>74км 3пк</td> <td>50</td> <td>320</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td rowSpan="2">Строганово-Ммшинская</td> <td>81км 1пк</td> <td>50</td> <td>420</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>88км 8пк</td> <td>50</td> <td>310</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td rowSpan="2">Мшинская-Толмачево</td> <td>109км 3пк</td> <td>50</td> <td>370</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>124км 1пк</td> <td>50</td> <td>420</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Рзд.Ген.Омельченко-Луга</td> <td>134км 1пк</td> <td>50</td> <td>430</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Луга-Серебрянка</td> <td>153км 2пк</td> <td>50</td> <td>300</td> <td>Основное место проверки  (нечетное)</td> </tr>
          <tr> <td>Серебрянка-Лямцево</td> <td>164км 7пк</td> <td>50</td> <td>300</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Лямцево-Плюсса</td> <td>173км 7пк</td> <td>50</td> <td>400</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Плюсса-Струги Красные</td> <td>203км 6пк</td> <td>50</td> <td>300</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Вл.Лагерь-Лапино</td> <td>213км 4пк</td> <td>50</td> <td>400</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Новоселье-Молоди</td> <td>230км 6пк</td> <td>50</td> <td>400</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Молоди-Торошино</td> <td>242км 6пк</td> <td>50</td> <td>400</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Торошино-Любятово</td> <td>257км 6пк</td> <td>50</td> <td>300</td> <td>Доп.место проверки (нечетное)</td> </tr>
        </tbody>
      </table>
    </React.Fragment>
  );
}