import React from 'react';

export default function BrakesVolhovBabaevo(props) {
  return (
    <React.Fragment>
      <table class="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th colspan="2" className="tcw46">Место проверки тормозов  (перегон, километр, пикет)</th>
            <th scope="col" className="tcw12">Скорость начала торможения</th>
            <th scope="col" className="tcw12">Тормозной путь в метрах при снижении скорости на 10 км/час</th>
            <th scope="col" className="tcw30">Примечание</th>
          </tr>
        </thead>
        <tbody>
          <tr className="table-light"> <td colspan="5">Участок Волховстрой-1 - Бабаево грузовое движение</td> </tr>
          <tr> <td>Волховстрой-2-Куколь</td> <td>130км 5пк</td> <td>50</td> <td>340</td> <td>Основное место проверки  (четное)</td> </tr>
          <tr> <td>Мыслино-Валя</td> <td>142км 10пк</td> <td>60</td> <td>460</td> <td>Основное место проверки  (четное)</td> </tr>
          <tr> <td>Мыслино-Валя</td> <td>158км 10пк</td> <td>60</td> <td>440</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Валя-Цвылево</td> <td>172км 10пк</td> <td>60</td> <td>500</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Цвылево-Тихвин</td> <td>188км 10пк</td> <td>60</td> <td>500</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Тихвин-Большой Двор</td> <td>211км 10пк</td> <td>60</td> <td>450</td> <td>Основное место проверки  (четное)</td> </tr>
          <tr> <td>Пикалево-2-Коли</td> <td>256км 10пк</td> <td>60</td> <td>420</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Коли- Ефимовская</td> <td>265км 10пк</td> <td>60</td> <td>570</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Ефимовская-Подборовье</td> <td>288км 10пк</td> <td>60</td> <td>540</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Подборовье-Заборье</td> <td>304км 10пк</td> <td>60</td> <td>510</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Заборье-Верхневольский</td> <td>311км 5пк</td> <td>60</td> <td>425</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Верхневольский-Тешемля</td> <td>326км 10пк</td> <td>60</td> <td>440</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Тешемля-Бабаево</td> <td>336км 10пк</td> <td>60</td> <td>425</td> <td>Доп.место проверки (четное)</td> </tr>
        </tbody>
      </table>
    </React.Fragment>
  );
}