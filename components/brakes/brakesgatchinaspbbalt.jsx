import React from 'react';

export default function BrakesGatchinaSpbBalt(props) {
  return (
    <React.Fragment>
      <table class="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th colspan="2" className="tcw46">Место проверки тормозов  (перегон, километр, пикет)</th>
            <th scope="col" className="tcw12">Скорость начала торможения</th>
            <th scope="col" className="tcw12">Тормозной путь в метрах при снижении скорости на 10 км/час</th>
            <th scope="col" className="tcw30">Примечание</th>
          </tr>
        </thead>
        <tbody>
          <tr className="table-light"><td colspan="5">Участок Гатчина Балт.Пасс.-СПб.Балтийский  грузовое движение </td></tr>
          <tr><td>Лигово-Броневая</td>	<td>6км пк9	</td><td>25</td>	<td>270	</td><td>Основное место проверки  (четное)</td></tr>
        </tbody>
      </table>
    </React.Fragment>
  );
}