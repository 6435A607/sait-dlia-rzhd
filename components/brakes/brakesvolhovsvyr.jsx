import React from 'react';

export default function BrakesVolhovSvyr(props) {
  return (
    <React.Fragment>
      <table class="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th colspan="2" className="tcw46">Место проверки тормозов  (перегон, километр, пикет)</th>
            <th scope="col" className="tcw12">Скорость начала торможения</th>
            <th scope="col" className="tcw12">Тормозной путь в метрах при снижении скорости на 10 км/час</th>
            <th scope="col" className="tcw30">Примечание</th>
          </tr>
        </thead>
        <tbody>
          <tr className="table-light"> <td colspan="5">Участок Волховстрой - Свирь  грузовое движение</td> </tr>
          <tr> <td>Мур.Ворота-Колчаново</td> <td>134км1пк</td> <td>60</td> <td>420</td> <td>Основное место проверки  (четное)</td> </tr>
          <tr> <td>Лунгаги- Юги</td> <td>158км1пк</td> <td>60</td> <td>460</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Юги-Паша</td> <td>176км1пк</td> <td>60</td> <td>530</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Паша-Оять</td> <td>200км1пк</td> <td>60</td> <td>410</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Оять-Заостровье</td> <td>211км1пк</td> <td>60</td> <td>410</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Заостровье-Лодейное Поле</td> <td>237км1пк</td> <td>60</td> <td>410</td> <td>Доп.место проверки (четное)</td> </tr>
          <tr> <td>Лодейное Поле-Янега</td> <td>248км1пк</td> <td>60</td> <td>410</td> <td>Основное место проверки  (четное)</td> </tr>
          <tr> <td>Яньдеба-Подпорожье</td> <td>277км5пк</td> <td>60</td> <td>510</td> <td>Доп.место проверки (четное)</td> </tr>
        </tbody>
      </table>
    </React.Fragment>
  );
}