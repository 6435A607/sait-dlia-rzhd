import React from 'react';

export default function BrakesSvyrVolhov(props) {
  return (
    <React.Fragment>
      <table class="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th colspan="2" className="tcw46">Место проверки тормозов  (перегон, километр, пикет)</th>
            <th scope="col" className="tcw12">Скорость начала торможения</th>
            <th scope="col" className="tcw12">Тормозной путь в метрах при снижении скорости на 10 км/час</th>
            <th scope="col" className="tcw30">Примечание</th>
          </tr>
        </thead>
        <tbody>
          <tr className="table-light"> <td colspan="5">Участок Свирь - Волховстрой грузовое движение</td> </tr>
          <tr> <td>Яньдеба-Янега</td> <td>265км 5пк</td> <td>60</td> <td>460</td> <td>Основное место проверки  (нечетное)</td> </tr>
          <tr> <td>Янега-Лодейное Поле</td> <td>247км 5пк</td> <td>60</td> <td>420</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Лодейное Поле-Заостровье</td> <td>234км 5пк</td> <td>60</td> <td>430</td> <td>Основное место проверки  (нечетное)</td> </tr>
          <tr> <td>Заостровье-Оять</td> <td>219км 10пк</td> <td>60</td> <td>420</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Оять-Паша</td> <td>198км 10пк</td> <td>60</td> <td>420</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Паша-Юги</td> <td>182км 10пк</td> <td>60</td> <td>440</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Юги-Лунгачи</td> <td>161км 10пк</td> <td>60</td> <td>440</td> <td>Доп.место проверки (нечетное)</td> </tr>
          <tr> <td>Колчаново-Мур.Ворота</td> <td>138км 10пк</td> <td>60</td> <td>420</td> <td>Доп.место проверки (нечетное)</td> </tr>
        </tbody>
      </table>
    </React.Fragment>
  );
}