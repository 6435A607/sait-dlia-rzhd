import React from 'react';
import LinksList from './linkslist.jsx';

export default class Aside extends React.Component {
    render() {
      return (
        <div className="list-group" id="collapsibleSidebar">
            <LinksList />
        </div>
      );
    }
  }