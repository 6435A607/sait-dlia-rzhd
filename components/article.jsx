import React, { Suspense } from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import r962Chapter1 from './962r/962rChapter1.jsx';
import r962Chapter2 from './962r/962rChapter2.jsx';
import r962Chapter3 from './962r/962rChapter3.jsx';
import r962Chapter4 from './962r/962rChapter4.jsx';
import r962Chapter5 from './962r/962rChapter5.jsx';
import r962Chapter6 from './962r/962rChapter6.jsx';
import r962Chapter7 from './962r/962rChapter7.jsx';
import r962Chapter8 from './962r/962rChapter8.jsx';
import r962Chapter9 from './962r/962rChapter9.jsx';
import r962Chapter10 from './962r/962rChapter10.jsx';
import r962Chapter11 from './962r/962rChapter11.jsx';
import r962Chapter12 from './962r/962rChapter12.jsx';

const r1433 = React.lazy(() => import('./pamyatka/1433r.jsx'));
const DownloadButton = React.lazy(() => import('./downloadbutton.jsx'));
const Welcome = React.lazy(() => import('./welcome.jsx'));
const MIChapter1 = React.lazy(() => import('./mi/michapter1.jsx'));
const MIChapter2 = React.lazy(() => import('./mi/michapter2.jsx'));
const MIChapter3 = React.lazy(() => import('./mi/michapter3.jsx'));
const MIChapter4 = React.lazy(() => import('./mi/michapter4.jsx'));
const MIChapter5 = React.lazy(() => import('./mi/michapter5.jsx'));
const MIChapter6 = React.lazy(() => import('./mi/michapter6.jsx'));
const MIChapter7_1 = React.lazy(() => import('./mi/michapter7_1.jsx'));
const MIChapter7_1_note = React.lazy(() => import('./mi/michapter7_1_note.jsx'));
const MIChapter7_2 = React.lazy(() => import('./mi/michapter7_2.jsx'));
const MIChapter7_3 = React.lazy(() => import('./mi/michapter7_3.jsx'));
const MIChapter7_4 = React.lazy(() => import('./mi/michapter7_4.jsx'));
const MIChapter7_4_note = React.lazy(() => import('./mi/michapter7_4_note.jsx'));
const ExampleOfaTrip = React.lazy(() => import('./pamyatka/exampleofatrip.jsx'));
const L230 = React.lazy(() => import('./pamyatka/l230.jsx'));
const BrakesShusharyVolhov = React.lazy(() => import('./brakes/brakesshusharyvolhov.jsx'));
const BrakesVolhovShushary = React.lazy(() => import('./brakes/brakesvolhovshushary.jsx'));
const NotFound = React.lazy(() => import('./notfound.jsx'));
const MIChapter7_5 = React.lazy(() => import('./mi/michapter7_5.jsx'));
const MIChapter8 = React.lazy(() => import('./mi/michapter8.jsx'));
const MIChapter9_1 = React.lazy(() => import('./mi/michapter9_1.jsx'));
const MIChapter9_2 = React.lazy(() => import('./mi/michapter9_2.jsx'));
const MIChapter9_3 = React.lazy(() => import('./mi/michapter9_3.jsx'));
const MIChapter9_4 = React.lazy(() => import('./mi/michapter9_4.jsx'));
const MIChapter9_5 = React.lazy(() => import('./mi/michapter9_5.jsx'));
const MIChapter9_6 = React.lazy(() => import('./mi/michapter9_6.jsx'));
const MIChapter9_7 = React.lazy(() => import('./mi/michapter9_7.jsx'));
const MIChapter9_8 = React.lazy(() => import('./mi/michapter9_8.jsx'));
const MIChapter9_9 = React.lazy(() => import('./mi/michapter9_9.jsx'));
const MIChapter9_10 = React.lazy(() => import('./mi/michapter9_10.jsx'));
const MIChapter10 = React.lazy(() => import('./mi/michapter10.jsx'));
const MIChapter11 = React.lazy(() => import('./mi/michapter11.jsx'));
const MIChapter12 = React.lazy(() => import('./mi/michapter12.jsx'));
const MIChapter13 = React.lazy(() => import('./mi/michapter13.jsx'));
const MIChapter14 = React.lazy(() => import('./mi/michapter14.jsx'));
const MIChapter15 = React.lazy(() => import('./mi/michapter15.jsx'));
const MIChapter16 = React.lazy(() => import('./mi/michapter16.jsx'));
const MIChapter17 = React.lazy(() => import('./mi/michapter17.jsx'));
const MIChapter18 = React.lazy(() => import('./mi/michapter18.jsx'));
const MIChapter19 = React.lazy(() => import('./mi/michapter19.jsx'));
const Appendix5 = React.lazy(() => import('./mi/appendix5.jsx'));
const Appendix4 = React.lazy(() => import('./mi/appendix4.jsx'));
const Appendix2 = React.lazy(() => import('./mi/appendix2.jsx'));
const Appendix1 = React.lazy(() => import('./mi/appendix1.jsx'));
const Appendix3 = React.lazy(() => import('./mi/appendix3.jsx'));
const BrakesUzel = React.lazy(() => import('./brakes/brakesuzel.jsx'));
const BrakesVolhovSvyr = React.lazy(() => import('./brakes/brakesvolhovsvyr.jsx'));
const BrakesSvyrVolhov = React.lazy(() => import('./brakes/brakessvyrvolhov.jsx'));
const BrakesVolhovBabaevo = React.lazy(() => import('./brakes/brakesvolhovbabaevo.jsx'));
const BrakesBabaevoVolhov = React.lazy(() => import('./brakes/brakesbabaevovolhov.jsx'));
const BrakesSpbVitebDno = React.lazy(() => import('./brakes/brakesspbvitebdno.jsx'));
const BrakesDnoSpbViteb = React.lazy(() => import('./brakes/brakesdnospbviteb.jsx'));
const BrakesSpbVitebVladLugaPskov = React.lazy(() => import('./brakes/brakesspbvitebvladlugapskov.jsx'));
const BrakesPskovLugaVladSpbViteb = React.lazy(() => import('./brakes/brakespskovlugavladspbviteb.jsx'));
const BrakesShusharyVladNarva = React.lazy(() => import('./brakes/brakesshusharyvladnarva.jsx'));
const BrakesNarvaVladShushary = React.lazy(() => import('./brakes/brakesnarvavladshushary.jsx'));
const BrakesShusharyShosNarva = React.lazy(() => import('./brakes/brakesshusharyshosnarva.jsx'));
const BrakesNarvaShosShushary = React.lazy(() => import('./brakes/brakesnarvashosshushary.jsx'));
const BrakesShusharyShosVeimarnLuzh = React.lazy(() => import('./brakes/brakesshusharyshosveimarnluzh.jsx'));
const BrakesLuzhVladShusharySpsm = React.lazy(() => import('./brakes/brakesluzhvladshusharyspsm.jsx'));
const BrakesShusharyLigovoKalLuzh = React.lazy(() => import('./brakes/brakesshusharyligovokalluzh.jsx'));
const BrakesLuzhKalLigovoSpsm = React.lazy(() => import('./brakes/brakesluzhkalligovospsm.jsx'));
const BrakesGatchinaMgaBudo = React.lazy(() => import('./brakes/brakesgatchinamgabudo.jsx'));
const BrakesBudoMgaGatchina = React.lazy(() => import('./brakes/brakesbudomgagatchina.jsx'));
const BrakesVeimarnSlan = React.lazy(() => import('./brakes/brakesveimarnslan.jsx'));
const BrakesSlanVeimarn = React.lazy(() => import('./brakes/brakesslanveimarn.jsx'));
const BrakesSpbBaltGatchina = React.lazy(() => import('./brakes/brakesspbbaltgatchina.jsx'));
const BrakesDnoPskov = React.lazy(() => import('./brakes/brakesdnopskov.jsx'));
const BrakesPskovDno = React.lazy(() => import('./brakes/brakespskovdno.jsx'));
const BrakesGatchinaSpbBalt = React.lazy(() => import('./brakes/brakesgatchinaspbbalt.jsx'));
const Zima = React.lazy(() => import('./pamyatka/zima.jsx'));
const PamyatkaPB = React.lazy(() => import('./safety/pamyatkapb.jsx'));
const PamyatkaBrakeTesting = React.lazy(() => import('./pamyatka/pamyatkaoprobtormozov.jsx'));
const BrakesPass = React.lazy(() => import('./brakes/brakespass.jsx'));
const ManagementOfTrain = React.lazy(() => import('./pamyatka/vedenie.jsx'));
const PamyatkaAcceptance = React.lazy(() => import('./pamyatka/pamyatkaacceptance.jsx'));
const PamyatkaRezervSplotka = React.lazy(() => import('./pamyatka/pamyatkarezervsplotka.jsx'));
const Coaxiality = React.lazy(() => import('./mi/coaxiality.jsx'));
const MaxWeightNorm = React.lazy(() => import('./weightnorm/maxweightnorm.jsx'));
const WeightNorm = React.lazy(() => import('./weightnorm/weightnorm.jsx'));
const AreasWeightNorm = React.lazy(() => import('./weightnorm/areasweightnorm.jsx'));
const r554 = React.lazy(() => import('./pamyatka/554r.jsx'));
const r1414 = React.lazy(() => import('./pamyatka/1414r.jsx'));
const r2580Chapter1 = React.lazy(() => import('./2580r/2580rchapter1.jsx'));
const r2580Chapter2 = React.lazy(() => import('./2580r/2580rchapter2.jsx'));
const r2580Chapter3 = React.lazy(() => import('./2580r/2580rchapter3.jsx'));
const r2580Chapter4 = React.lazy(() => import('./2580r/2580rchapter4.jsx'));
const r2580Chapter5 = React.lazy(() => import('./2580r/2580rchapter5.jsx'));
const r2580Chapter6 = React.lazy(() => import('./2580r/2580rchapter6.jsx'));
const r2580Chapter7 = React.lazy(() => import('./2580r/2580rchapter7.jsx'));
const r2580Chapter8 = React.lazy(() => import('./2580r/2580rchapter8.jsx'));
const r2580Chapter9 = React.lazy(() => import('./2580r/2580rchapter9.jsx'));
const r2580Chapter10 = React.lazy(() => import('./2580r/2580rchapter10.jsx'));
const r2580Chapter11 = React.lazy(() => import('./2580r/2580rchapter11.jsx'));
const r2580Chapter12 = React.lazy(() => import('./2580r/2580rchapter12.jsx'));
const r2580Chapter13 = React.lazy(() => import('./2580r/2580rchapter13.jsx'));
const r2580Chapter14 = React.lazy(() => import('./2580r/2580rchapter14.jsx'));
const r2580Chapter15 = React.lazy(() => import('./2580r/2580rchapter15.jsx'));
const r2580Chapter16 = React.lazy(() => import('./2580r/2580rchapter16.jsx'));
const r2580Chapter17 = React.lazy(() => import('./2580r/2580rchapter17.jsx'));
const r2580Chapter18 = React.lazy(() => import('./2580r/2580rchapter18.jsx'));
const r2580Chapter19 = React.lazy(() => import('./2580r/2580rchapter19.jsx'));
const r2580Chapter20 = React.lazy(() => import('./2580r/2580rchapter20.jsx'));
const r2580Chapter21 = React.lazy(() => import('./2580r/2580rchapter21.jsx'));
const r2580Chapter22 = React.lazy(() => import('./2580r/2580rchapter22.jsx'));
const r2580Chapter23 = React.lazy(() => import('./2580r/2580rchapter23.jsx'));
const r2580Chapter24 = React.lazy(() => import('./2580r/2580rchapter24.jsx'));
const r2580Chapter25 = React.lazy(() => import('./2580r/2580rchapter25.jsx'));
const r2580Chapter26 = React.lazy(() => import('./2580r/2580rchapter26.jsx'));
const r2580Chapter27 = React.lazy(() => import('./2580r/2580rchapter27.jsx'));
const r2580Chapter28 = React.lazy(() => import('./2580r/2580rchapter28.jsx'));
const r2580Chapter29 = React.lazy(() => import('./2580r/2580rchapter29.jsx'));
const r2580Chapter30 = React.lazy(() => import('./2580r/2580rchapter30.jsx'));
const Okt12r = React.lazy(() => import('./pamyatka/okt12r.jsx'));
const KPD3 = React.lazy(() => import('./safety/kpd3.jsx'));
const SpeedSPbDnoNovosokol = React.lazy(() => import('./speed/speedspbdnonovosokol.jsx'));
const SpeedMgaBudoOvinishe = React.lazy(() => import( './speed/speedmgabudoovinishe.jsx'));
const SpeedSPbIvan = React.lazy(() => import( './speed/speedspbivan.jsx'));
const SpeedLigovoVeimarn = React.lazy(() => import( './speed/speedligovoveimarn.jsx'));
const SpeedGdovVeimarn = React.lazy(() => import( './speed/speedgdovveimarn.jsx'));
const SpeedSpbLugaPskovGos = React.lazy(() => import( './speed/speedspblugapskovgos.jsx'));
const SpeedGatchVarGatchPass = React.lazy(() => import( './speed/speedgatchvargatchpass.jsx'));
const SpeedGatchinaFrezer = React.lazy(() => import( './speed/speedgatchinafrezer.jsx'));
const SpeedGatchVarGatchTov = React.lazy(() => import( './speed/speedgatchvargatchtov.jsx'));
const SpeedVladKobralovo = React.lazy(() => import( './speed/speedvladkobralovo.jsx'));
const SpeedVyritsaPoselok = React.lazy(() => import( './speed/speedvyritsaposelok.jsx'));
const SpeedSPSMSvyr = React.lazy(() => import( './speed/speedspsmsvyr.jsx'));
const SpeedVolhovIIVolhovI = React.lazy(() => import( './speed/speedvohovIIvolhovI.jsx'));
const SpeedKushelSuoyarvi = React.lazy(() => import( './speed/speedkushelsuoyarvi.jsx'));
const SpeedMgaStekol = React.lazy(() => import( './speed/speedmgastekol.jsx'));
const SpeedLugaBatetsk = React.lazy(() => import( './speed/speedlugabatets.jsx'));
const SpeedGatchinaTosno = React.lazy(() => import( './speed/speedgatchinatosno.jsx'));
const SpeedPavlovskNovolisino = React.lazy(() => import( './speed/speedpavlovsknovolisino.jsx'));
const Speed116kmkukol = React.lazy(() => import( './speed/speed116kmkukol.jsx'));
const SpeedVolhovIIBabaevo = React.lazy(() => import( './speed/speedvolhovIIbabevo.jsx'));
const SpeedUzel = React.lazy(() => import( './speed/speeduzel.jsx'));
const SpeedKotlyLuzh = React.lazy(() => import('./speed/speedkotlyluzh.jsx'));
const PamyatkaProblemKM394 = React.lazy(() => import('./pamyatka/pamyatkaproblemkm394.jsx'));
const PamyatkaRazryv = React.lazy(() => import('./pamyatka/pamyatkarazryv.jsx'));
const PamyatkaSignsOfFreezing = React.lazy(() => import('./pamyatka/pamyatkasignsoffreezing.jsx'));
const PamyatkaSpinningOfWheels = React.lazy(() => import('./pamyatka/pamyatkaspinningofwheels.jsx'));
const PamyatkaTroubles = React.lazy(() => import('./pamyatka/pamyatkatroubles.jsx'));
const Pamyatka859r = React.lazy(() => import('./pamyatka/pamyatka859r.jsx'));
const PamyatkaCouplerCheck = React.lazy(() => import ('./pamyatka/pamyatkacouplercheck.jsx'));
const PamyatkaLocoTrouble = React.lazy(() => import ('./pamyatka/pamyatkalocotrouble.jsx'));
const traPage = React.lazy(() => import('./tra/traPage.jsx'));
const PhonePage = React.lazy(() => import('./phone/phonePage.jsx'));
const PamyatkaPantograph = React.lazy(() => import('./pamyatka/pamyatkapantograph.jsx'));
const PamyatkaVL10Cold = React.lazy(()=> import ('./pamyatka/pamyatkavl10cold.jsx'));
const Pamyatka2ES4Cold = React.lazy(() => import ('./pamyatka/pamyatka2es4cold.jsx'));
const TrainExport = React.lazy(() => import('./weightnorm/trainexport.jsx'));
const PamyatkaRadio = React.lazy(() => import('./safety/pamytkaradio.jsx'));
const EMUzel = React.lazy(() => import('./safety/emusel.jsx'));
const blokChapter1 = React.lazy(() => import('./safety/blok/blokChapter1.jsx'));
const blokChapter2 = React.lazy(() => import('./safety/blok/blokChapter2.jsx'));
const blokChapter3 = React.lazy(() => import('./safety/blok/blokChapter3.jsx'));
const blokChapter4 = React.lazy(() => import('./safety/blok/blokChapter4.jsx'));
const blokChapter5 = React.lazy(() => import('./safety/blok/blokChapter5.jsx'));
const blokChapter6 = React.lazy(() => import('./safety/blok/blokChapter6.jsx'));
const blokChapter7 = React.lazy(() => import('./safety/blok/blokChapter7.jsx'));
const blokChapter8 = React.lazy(() => import('./safety/blok/blokChapter8.jsx'));
const blokChapter9 = React.lazy(() => import('./safety/blok/blokChapter9.jsx'));
const blokChapter10 = React.lazy(() => import('./safety/blok/blokChapter10.jsx'));
const blokChapter11 = React.lazy(() => import('./safety/blok/blokChapter11.jsx'));
const blokChapter12 = React.lazy(() => import('./safety/blok/blokChapter12.jsx'));
const blokChapter13 = React.lazy(() => import('./safety/blok/blokChapter13.jsx'));
const blokChapter14 = React.lazy(() => import('./safety/blok/blokChapter14.jsx'));
const blokChapter15 = React.lazy(() => import('./safety/blok/blokChapter15.jsx'));
const blokChapter16 = React.lazy(() => import('./safety/blok/blokChapter16.jsx'));
const blokChapter17 = React.lazy(() => import('./safety/blok/blokChapter17.jsx'));
const blokChapter18 = React.lazy(() => import('./safety/blok/blokChapter18.jsx'));
const blokChapter19 = React.lazy(() => import('./safety/blok/blokChapter19.jsx'));
const blokChapter20 = React.lazy(() => import('./safety/blok/blokChapter20.jsx'));
const blokChapter21 = React.lazy(() => import('./safety/blok/blokChapter21.jsx'));
const blokChapter22 = React.lazy(() => import('./safety/blok/blokChapter22.jsx'));
const blokChapter23 = React.lazy(() => import('./safety/blok/blokChapter23.jsx'));
const blokChapter24 = React.lazy(() => import('./safety/blok/blokChapter24.jsx'));

export default class Article extends React.Component {
  constructor(props) {
    super(props);
    this.handleOnClick = this.handleOnClick.bind(this);
  }

  handleOnClick() {
    let url = location.hash;
    let fileName;

    switch (true) {
      case /michapter/.test(url):
      case /appendix/.test(url):
        fileName = '/docs/Местная инструкция.pdf';
        break;

      case /brakes/.test(url):
        fileName = '/docs/Места пробы тормозов.pdf';
        break;

      case /l230/.test(url):
        fileName = '/docs/Инструкция Л230.pdf'
        break;

      case /exampleOf/.test(url):
        fileName = '/docs/Пример оформления предупреждения.pdf';
        break;

      case /zima/.test(url):
        fileName = '/docs/Зимний тех. бюллетень.pdf';
        break;

      case /pamyatkaPB/.test(url):
        fileName = '/docs/Памятка по приборам безопасности.pdf';
        break;

      case /pamyatkaBrakes/.test(url):
        fileName = '/docs/Опробование автотормозов с протяжками.pdf';
        break;

      case /Vedenie/.test(url):
        fileName = '/docs/Отправление со станции ведение по участку.pdf';
        break;

      case /Acceptance/.test(url):
        fileName = '/docs/Памятка по приёмке тормозного оборудования и протяжки на ленте.pdf';
        break;

      case /ReservSplotka/.test(url):
        fileName = '/docs/Резерв Сплотка.pdf';
        break;

      case /coaxiality/.test(url):
        fileName = '/docs/Приёмка писцы.pdf';
        break;

      case /max-weight/.test(url):
        fileName = '/docs/Максимальные весовые нормы и межпоездные интервалы по условиям тягового электроснабжения.pdf';
        break;

      case /areas-weight/.test(url):
        fileName = '/docs/Перечень участков, на которых запрещено взятие с места поезда.pdf';
        break;

      case /weight-norm/.test(url):
        fileName = '/docs/Нормы массы и длины гузовых поездов по сериям локомотивов.pdf';
        break;

      case /speed/.test(url):
        fileName = '/docs/Выписка скоростей.xlsx';
        break;

      case /554r/.test(url):
        fileName = '/docs/554r.pdf';
        break;

      case /1414r/.test(url):
        fileName = '/docs/1414_р сотм от 03.07.2018.pdf';
        break;

      case /2580r/.test(url):
        fileName = '/docs/2580р.pdf';
        break;

      case /okt12r/.test(url):
        fileName = '/docs/окт-т12р.pdf';
        break;

      case /kpd3/.test(url):
        fileName = '/docs/Памятка по КПД.JPG';
        break;

      case /KM394/.test(url):
        fileName = '/docs/Неисправ_КМ394.pdf';
        break;

      case /Razryv/.test(url):
        fileName = '/docs/МЕРЫ предупреждения разрывов.pdf';
        break;

      case /SignsOfFreezing/.test(url):
        fileName = '/docs/Признаки перемерзаний.pdf';
        break;

      case /SpinningOfWheels/.test(url):
        fileName = '/docs/ПРЕДУПРЕЖДЕНИЕ БОКСОВАНИЯ КОЛЕКСНЫХ ПАР.pdf';
        break;

      case /Troubles/.test(url):
        fileName = '/docs/Наиболее распространенные неисправности подвижного состава.pdf';
        break;

      case /859r/.test(url):
      case /couplercheck/.test(url):
      case /locotrouble/.test(url):
        fileName = '/docs/859р.pdf';
        break;

      case /train-export/.test(url):
        fileName = '/docs/Нормы массы передаточных и вывозных поездов.pdf';
        break; 

      case /tra/.test(url):
        fileName = '/docs/ТРА станций.zip';
        break;

      case /pantograph/.test(url):
        fileName = '/docs/Памятка по токоприёмникам.pdf';
        break;
      
      case /vl10cold/.test(url):
        fileName= '/docs/Вл10.JPG';
        break;

      case /2es4cold/.test(url):
        fileName= '/docs/2эс4к.JPG';
        break;

      case /Radio/.test(url):
        fileName= '/docs/Памятка по рации.pdf';
        break;

      case /emuzel/.test(url):
        fileName = '/img/emuzel.JPG';
        break;

      case /962r/.test(url):
        fileName = '/docs/962р.pdf';
        break;

      case /blok/.test(url):
        fileName = '/docs/Блок.pdf';
        break;

      default:
        fileName = '/';
        break;
    }

    document.location.href = fileName;
  }

  render() {
    return (
      <React.Fragment>
        <Suspense fallback={<div>Loading...</div>}>
        <div>
          {location.hash !== '#/' && <DownloadButton onClick={this.handleOnClick} />}
          <br />
          <br />
          <br />
          
            <Switch>
              <Route exact path="/" component={Welcome} />
              <Route path="/michapter1" component={MIChapter1} />
              <Route path="/michapter2" component={MIChapter2} />
              <Route path="/michapter3" component={MIChapter3} />
              <Route path="/michapter4" component={MIChapter4} />
              <Route path="/michapter5" component={MIChapter5} />
              <Route path="/michapter6" component={MIChapter6} />
              <Route path="/michapter7-1" component={MIChapter7_1} />
              <Route path="/michapter7-1-note" component={MIChapter7_1_note} />
              <Route path="/michapter7-2" component={MIChapter7_2} />
              <Route path="/michapter7-3" component={MIChapter7_3} />
              <Route path="/michapter7-4" component={MIChapter7_4} />
              <Route path="/michapter7-4-note" component={MIChapter7_4_note} />
              <Route path="/michapter7-5" component={MIChapter7_5} />
              <Route path="/michapter8" component={MIChapter8} />
              <Route path="/michapter9_1" component={MIChapter9_1} />
              <Route path="/michapter9_2" component={MIChapter9_2} />
              <Route path="/michapter9_3" component={MIChapter9_3} />
              <Route path="/michapter9_4" component={MIChapter9_4} />
              <Route path="/michapter9_5" component={MIChapter9_5} />
              <Route path="/michapter9_6" component={MIChapter9_6} />
              <Route path="/michapter9_7" component={MIChapter9_7} />
              <Route path="/michapter9_8" component={MIChapter9_8} />
              <Route path="/michapter9_9" component={MIChapter9_9} />
              <Route path="/michapter9_10" component={MIChapter9_10} />
              <Route path="/michapter10" component={MIChapter10} />
              <Route path="/michapter11" component={MIChapter11} />
              <Route path="/michapter12" component={MIChapter12} />
              <Route path="/michapter13" component={MIChapter13} />
              <Route path="/michapter14" component={MIChapter14} />
              <Route path="/michapter15" component={MIChapter15} />
              <Route path="/michapter16" component={MIChapter16} />
              <Route path="/michapter17" component={MIChapter17} />
              <Route path="/michapter18" component={MIChapter18} />
              <Route path="/michapter19" component={MIChapter19} />
              <Route path="/appendix1" component={Appendix1} />
              <Route path="/appendix2" component={Appendix2} />
              <Route path="/appendix3" component={Appendix3} />
              <Route path="/appendix4" component={Appendix4} />
              <Route path="/appendix5" component={Appendix5} />


              <Route path="/exampleofatrip" component={ExampleOfaTrip} />
              <Route path="/l230" component={L230} />
              <Route path="/brakes-pass" component={BrakesPass} />
              <Route path="/brakes-shushary-volhov" component={BrakesShusharyVolhov} />
              <Route path="/brakes-volhov-shushary" component={BrakesVolhovShushary} />
              <Route path="/brakes-uzel" component={BrakesUzel} />
              <Route path="/brakes-volhov-svyr" component={BrakesVolhovSvyr} />
              <Route path="/brakes-svyr-volhov" component={BrakesSvyrVolhov} />
              <Route path="/brakes-volhov-babaevo" component={BrakesVolhovBabaevo} />
              <Route path="/brakes-babaevo-volhov" component={BrakesBabaevoVolhov} />
              <Route path="/brakes-spbviteb-dno" component={BrakesSpbVitebDno} />
              <Route path="/brakes-dno-spbviteb" component={BrakesDnoSpbViteb} />
              <Route path="/brakes-spbviteb-vlad-luga-pskov" component={BrakesSpbVitebVladLugaPskov} />
              <Route path="/brakes-pskov-luga-vlad-spbviteb" component={BrakesPskovLugaVladSpbViteb} />
              <Route path="/brakes-shushary-vlad-narva" component={BrakesShusharyVladNarva} />
              <Route path="/brakes-narva-vlad-shushary" component={BrakesNarvaVladShushary} />
              <Route path="/brakes-shushary-shos-narva" component={BrakesShusharyShosNarva} />
              <Route path="/brakes-narva-shos-shushary" component={BrakesNarvaShosShushary} />
              <Route path="/brakes-shushary-shos-veimarn-luzh" component={BrakesShusharyShosVeimarnLuzh} />
              <Route path="/brakes-luzh-vlad-shushary-spsm" component={BrakesLuzhVladShusharySpsm} />
              <Route path="/brakes-shushary-ligovo-kal-luzh" component={BrakesShusharyLigovoKalLuzh} />
              <Route path="/brakes-luzh-kal-ligovo-spsm" component={BrakesLuzhKalLigovoSpsm} />
              <Route path="/brakes-gatchina-mga-budo" component={BrakesGatchinaMgaBudo} />
              <Route path="/brakes-budo-mga-gatchina" component={BrakesBudoMgaGatchina} />
              <Route path="/brakes-veimarn-slan" component={BrakesVeimarnSlan} />
              <Route path="/brakes-slan-veimarn" component={BrakesSlanVeimarn} />
              <Route path="/brakes-spbbalt-gatchina" component={BrakesSpbBaltGatchina} />
              <Route path="/brakes-gatchina-spbbalt" component={BrakesGatchinaSpbBalt} />
              <Route path="/brakes-dno-pskov" component={BrakesDnoPskov} />
              <Route path="/brakes-pskov-dno" component={BrakesPskovDno} />

              <Route path="/zima" component={Zima} />
              <Route path="/pamyatkaPB" component={PamyatkaPB} />
              <Route path="/pamyatkaBrakesTest" component={PamyatkaBrakeTesting} />
              <Route path="/pamyatkaVedenie" component={ManagementOfTrain} />
              <Route path="/pamyatkaAcceptance" component={PamyatkaAcceptance} />
              <Route path="/pamyatkaReservSplotka" component={PamyatkaRezervSplotka} />
              <Route path="/coaxiality" component={Coaxiality} />

              <Route path="/max-weight-norm" component={MaxWeightNorm} />
              <Route path="/weight-norm" component={WeightNorm} />
              <Route path="/areas-weight-norm" component={AreasWeightNorm} />
              <Route path="/train-export" component={TrainExport} />

              <Route path="/554r" component={r554} />
              <Route path="/1414r" component={r1414} />
              <Route path="/1433r" component={r1433} />
              <Route path="/okt12r" component={Okt12r} />
              <Route path="/kpd3" component={KPD3} />
              <Route path="/pamyatkaProblemKM394" component={PamyatkaProblemKM394} />
              <Route path="/pamyatkaRazryv" component={PamyatkaRazryv} />
              <Route path="/pamyatkaSignsOfFreezing" component={PamyatkaSignsOfFreezing} />
              <Route path="/pamyatkaSpinningOfWheels" component={PamyatkaSpinningOfWheels} />
              <Route path="/pamyatkaTroubles" component={PamyatkaTroubles} /> 
              <Route path="/pamyatka859r" component={Pamyatka859r} />
              <Route path="/pamyatkacouplercheck" component={PamyatkaCouplerCheck} />
              <Route path="/pamyatkalocotrouble" component={PamyatkaLocoTrouble} />
              <Route path="/pamyatkapantograph" component={PamyatkaPantograph} />
              <Route path="/vl10cold" component={PamyatkaVL10Cold} />
              <Route path="/2es4cold" component={Pamyatka2ES4Cold} />
              <Route path="/pamyatkaRadio" component={PamyatkaRadio} />
              <Route path="/emuzel" component={EMUzel} />

              <Route path="/speedspbdnonovosokol" component={SpeedSPbDnoNovosokol} />
              <Route path="/speedkotlyluzh" component={SpeedKotlyLuzh} />
              <Route path="/speedmgabudoovinishe" component={SpeedMgaBudoOvinishe} />
              <Route path="/speedspbivan" component={SpeedSPbIvan} />
              <Route path="/speedligovoveimarn" component={SpeedLigovoVeimarn} />
              <Route path="/speedgdovveimarn" component={SpeedGdovVeimarn} />
              <Route path="/speedspblugapskovgos" component={SpeedSpbLugaPskovGos} />
              <Route path="/speedgatchvargatchpass" component={SpeedGatchVarGatchPass} />
              <Route path="/speedgatchinafrezer" component={SpeedGatchinaFrezer} />
              <Route path="/speedgatchvargatchtov" component={SpeedGatchVarGatchTov} />
              <Route path="/speedvladkobralovo" component={SpeedVladKobralovo} />
              <Route path="/speedvyritsaposelok" component={SpeedVyritsaPoselok} />
              <Route path="/speedspsmsvyr" component={SpeedSPSMSvyr} />
              <Route path="/speedvolhovIIvolhovI" component={SpeedVolhovIIVolhovI} />
              <Route path="/speedkushelsuoyarvi" component={SpeedKushelSuoyarvi} />
              <Route path="/speedmgastekol" component={SpeedMgaStekol} />
              <Route path="/speedlugabatetsk" component={SpeedLugaBatetsk} />
              <Route path="/speedgatchinatosno" component={SpeedGatchinaTosno} />
              <Route path="/speedpavlovsknovolisino" component={SpeedPavlovskNovolisino} />
              <Route path="/speed116kmkukol" component={Speed116kmkukol} />
              <Route path="/speedvolhovIIbabaevo" component={SpeedVolhovIIBabaevo} />
              <Route path="/speeduzel" component={SpeedUzel} />

              <Route path="/2580rchapter1" component={r2580Chapter1} />
              <Route path="/2580rchapter2" component={r2580Chapter2} />
              <Route path="/2580rchapter3" component={r2580Chapter3} />
              <Route path="/2580rchapter4" component={r2580Chapter4} />
              <Route path="/2580rchapter5" component={r2580Chapter5} />
              <Route path="/2580rchapter6" component={r2580Chapter6} />
              <Route path="/2580rchapter7" component={r2580Chapter7} />
              <Route path="/2580rchapter8" component={r2580Chapter8} />
              <Route path="/2580rchapter9" component={r2580Chapter9} />
              <Route path="/2580rchapter10" component={r2580Chapter10} />
              <Route path="/2580rchapter11" component={r2580Chapter11} />
              <Route path="/2580rchapter12" component={r2580Chapter12} />
              <Route path="/2580rchapter13" component={r2580Chapter13} />
              <Route path="/2580rchapter14" component={r2580Chapter14} />
              <Route path="/2580rchapter15" component={r2580Chapter15} />
              <Route path="/2580rchapter16" component={r2580Chapter16} />
              <Route path="/2580rchapter17" component={r2580Chapter17} />
              <Route path="/2580rchapter18" component={r2580Chapter18} />
              <Route path="/2580rchapter19" component={r2580Chapter19} />
              <Route path="/2580rchapter20" component={r2580Chapter20} />
              <Route path="/2580rchapter21" component={r2580Chapter21} />
              <Route path="/2580rchapter22" component={r2580Chapter22} />
              <Route path="/2580rchapter23" component={r2580Chapter23} />
              <Route path="/2580rchapter24" component={r2580Chapter24} />
              <Route path="/2580rchapter25" component={r2580Chapter25} />
              <Route path="/2580rchapter26" component={r2580Chapter26} />
              <Route path="/2580rchapter27" component={r2580Chapter27} />
              <Route path="/2580rchapter28" component={r2580Chapter28} />
              <Route path="/2580rchapter29" component={r2580Chapter29} />
              <Route path="/2580rchapter30" component={r2580Chapter30} />

              <Route path="/tra" component={traPage} />
              <Route path="/phones" component={PhonePage} />

              <Route path="/blokChapter1" component={blokChapter1} />
              <Route path="/blokChapter2" component={blokChapter2} />
              <Route path="/blokChapter3" component={blokChapter3} />
              <Route path="/blokChapter4" component={blokChapter4} />
              <Route path="/blokChapter5" component={blokChapter5} />
              <Route path="/blokChapter6" component={blokChapter6} />
              <Route path="/blokChapter7" component={blokChapter7} />
              <Route path="/blokChapter8" component={blokChapter8} />
              <Route path="/blokChapter9" component={blokChapter9} />
              <Route path="/blokChapter10" component={blokChapter10} />
              <Route path="/blokChapter11" component={blokChapter11} />
              <Route path="/blokChapter12" component={blokChapter12} />
              <Route path="/blokChapter13" component={blokChapter13} />
              <Route path="/blokChapter14" component={blokChapter14} />
              <Route path="/blokChapter15" component={blokChapter15} />
              <Route path="/blokChapter16" component={blokChapter16} />
              <Route path="/blokChapter17" component={blokChapter17} />
              <Route path="/blokChapter18" component={blokChapter18} />
              <Route path="/blokChapter19" component={blokChapter19} />
              <Route path="/blokChapter20" component={blokChapter20} />
              <Route path="/blokChapter21" component={blokChapter21} />
              <Route path="/blokChapter22" component={blokChapter22} />
              <Route path="/blokChapter23" component={blokChapter23} />
              <Route path="/blokChapter24" component={blokChapter24} />

              <Route path="/962rChapter1" component={r962Chapter1} />
              <Route path="/962rChapter2" component={r962Chapter2} />
              <Route path="/962rChapter3" component={r962Chapter3} />
              <Route path="/962rChapter4" component={r962Chapter4} />
              <Route path="/962rChapter5" component={r962Chapter5} />
              <Route path="/962rChapter6" component={r962Chapter6} />
              <Route path="/962rChapter7" component={r962Chapter7} />
              <Route path="/962rChapter8" component={r962Chapter8} />
              <Route path="/962rChapter9" component={r962Chapter9} />
              <Route path="/962rChapter10" component={r962Chapter10} />
              <Route path="/962rChapter11" component={r962Chapter11} />
              <Route path="/962rChapter12" component={r962Chapter12} />

              <Route component={NotFound} />
            </Switch>
          
        </div>
        </Suspense>
      </React.Fragment>
    );
  }
}