import React, { Suspense } from 'react';
import Header from './header.jsx';
import Main from './main.jsx';
import Footer from './footer.jsx';

export default class Page extends React.Component {
    render() {
      return(
        <React.Fragment>
          <Header />
       
          <Main />
      
          <Footer />
        </React.Fragment>
      );
    }
    
  }