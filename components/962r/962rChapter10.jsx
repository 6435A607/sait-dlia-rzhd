import React from 'react';

export default function r962Chapter10(props) {
  return (
    <>
      <h4 class="text-center">Порядок взаимодействия работников ТЧЭ, ТЧприг, СЛД, структурного подразделения ДОСС и ЭЧК (ЭЧ) при отключениях деповского фидера и обнаружении подбоев токоприемников</h4>
      <br />
      <p>10.1. При питании контактной сети ТЧЭ, ТЧприг, СЛД, структурного подразделения ДОСС отдельным фидером тяговой подстанции его автоматическое повторное включение исключается. После аварийного отключения деповского фидера его включение производится только после сообщения дежурного по эксплуатационному, моторвагонному локомотивному депо или депо сервисного обслуживания, ЭЧЦ о причине отключения и принятых мерах к недопущению повторного отключения.</p>
      <p>10.2. На электрифицированных железнодорожных путях ТЧЭ, ТЧприг, СЛД, структурного подразделения ДОСС запрещается производить отыскание места повреждения электрооборудования ЭПС высоким напряжением с поднятием токоприемника на контактную сеть, находящуюся под напряжением. Поиск места повреждения, а также проверка изоляции силовых цепей ЭПС, находящегося на электрифицированных железнодорожных путях ТЧЭ, ТЧприг, СЛД, структурном подразделении ДОСС должны производиться измерительными приборами или на специализированных путях депо имеющих устройства для снятия напряжения и заземления контактной сети.</p>
      <p>10.3. При обнаружении на ПТОЛ и в ТЧприг, структурном подразделении ДОСС повреждений и в том числе ударов, подбоев на токоприемниках, следов взаимодействия контактного провода за пределами рабочей части полоза токоприемников дежурный по депо сообщает об этом локомотивному диспетчеру, который в свою очередь сообщает об этом ЭЧЦ. Особое внимание при этом обращается на однотипные повреждения угольных вставок, накладок полозов, места их расположения на полозе.</p>
      <p>ЭЧЦ немедленно уведомляет руководство ЭЧ и организует внеочередной осмотр контактной сети с целью выявления и устранения ее неисправностей.</p>
      <p>10.4 После аварийного отключения деповского фидера дежурный по депо дает запрет ДСП о направлении ЭПС на тракционные пути соответствующего депо. Направление ЭПС на тракционные пути возобновляется после получения ДСП разрешения от дежурного по депо.</p>
    </>
  )
}