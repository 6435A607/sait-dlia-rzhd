import React from 'react';

/* id area loco length weight */

const dataTrainExport =

  <>
    <tr> <td rowSpan="2">Волковская - Оредеж</td> <td>ТЭМ7</td> <td>30</td> <td>3600</td> </tr>
    <tr> <td>М62, ТЭМ2</td> <td>30</td> <td>2200</td> </tr>
    <tr> <td>С-Петербург-Тов.-Вит. - Вырица</td> <td>ЧМЭЗ</td> <td>30</td> <td>2200</td> </tr>
    <tr> <td rowSpan="2">Оредеж - Волковская</td> <td>ТЭМ7</td> <td>30</td> <td>4000</td> </tr>
    <tr> <td>М62, ТЭМ2</td> <td>30</td> <td>2400</td> </tr>
    <tr> <td>Вырица - С-Петербург-Тов.-Вит.</td> <td>ЧМЭЗ</td> <td>30</td> <td>2400</td> </tr>
    <tr> <td rowSpan="2">Шушары - Луга</td> <td>ТЭМ7</td> <td>30</td> <td>3000</td> </tr>
    <tr> <td>М62, ЧМЭЗ</td> <td>30</td> <td>2400</td> </tr>
    <tr> <td rowSpan="2">Луга - Шушары</td> <td>ТЭМ7</td> <td>30</td> <td>3000</td> </tr>
    <tr> <td>М62, ЧМЭЗ</td> <td>30</td> <td>2000</td> </tr>
    <tr> <td rowSpan="2">Шушары - Предпортовая</td> <td>М62</td> <td>30</td> <td>2200</td> </tr>
    <tr> <td>ЧМЭЗ</td> <td>30</td> <td>1600</td> </tr>
    <tr> <td>Предпортовая - Шушары</td> <td>М62, ЧМЭЗ</td> <td>30</td> <td>1300</td> </tr>
    <tr> <td>Предпортовая - Автово</td> <td>ЧМЭЗ, ТЭМ2</td> <td>30</td> <td>2400</td> </tr>
    <tr> <td>Автово - Предпортовая</td> <td>ЧМЭЗ, ТЭМ2</td> <td>30</td> <td>1200</td> </tr>
    <tr> <td rowSpan="4">Фрезерный - Веймарн</td> <td>2М62</td> <td>57</td> <td>4000</td> </tr>
    <tr> <td>М62</td> <td>30</td> <td>2000</td> </tr>
    <tr> <td>ТЭМ2, ЧМЭЗ</td> <td>30</td> <td>1000</td> </tr>
    <tr> <td>ТЭМ7</td> <td>30</td> <td>1300</td> </tr>
    <tr> <td rowSpan="3">Веймарн - Фрезерный</td> <td>2М62</td> <td>57</td> <td>3700</td> </tr>
    <tr> <td>ТЭМ2, ЧМЭЗ</td> <td>30</td> <td>1000</td> </tr>
    <tr> <td>ТЭМ7</td> <td>30</td> <td>1300</td> </tr>
    <tr> <td rowSpan="3">Веймарн - Сала </td> <td>М62</td> <td>30</td> <td>2500</td> </tr>
    <tr> <td>ТЭМ2, ЧМЭЗ</td> <td>30</td> <td>1200</td> </tr>
    <tr> <td>ТЭМ7</td> <td>30</td> <td>1500</td> </tr>
    <tr> <td rowSpan="3">Сала - Веймарн</td> <td>М62</td> <td>30</td> <td>1900</td> </tr>
    <tr> <td>ТЭМ2, ЧМЭЗ</td> <td>30</td> <td>1000</td> </tr>
    <tr> <td>ТЭМ7</td> <td>30</td> <td>1300</td> </tr>
    <tr> <td rowSpan="3">Сала - Ивангород</td> <td> М62</td> <td> 30</td> <td> 5000</td>  </tr>
    <tr> <td>ТЭМ2, ЧМЭЗ</td> <td> 30</td> <td> 2500</td> </tr>
    <tr> <td>ТЭМ7</td> <td> 30</td> <td> 3000</td>  </tr>
    <tr> <td rowSpan="3">Ивангород - Сала</td> <td> М62</td> <td> 30</td> <td> 5000</td>  </tr>
    <tr> <td>ТЭМ2, ЧМЭЗ</td> <td> 30</td> <td> 2500</td>  </tr>
    <tr> <td>ТЭМ7</td> <td> 30</td> <td> 3000</td>  </tr>
    <tr> <td>Предпортовая - Лигово</td> <td> М62, ЧМЭЗ</td> <td> 30</td> <td> 3500</td>  </tr>
    <tr> <td>Лигово - Предпортовая</td> <td>  М62, ЧМЭЗ</td> <td> 30</td> <td> 3500</td>  </tr>
    <tr> <td rowSpan="2">Лигово - Красное Село</td> <td> М62, ЧМЭЗ</td> <td> 30</td> <td> 2000</td>  </tr>
    <tr> <td>ТЭМ18</td> <td> 30</td> <td> 1500</td>  </tr>
    <tr> <td rowSpan="2">Красное Село - Лигово</td> <td> М62, ЧМЭЗ</td> <td> 30</td> <td> 3500</td>  </tr>
    <tr> <td>ТЭМ18</td> <td> 30</td> <td> 2000</td>  </tr>
    <tr> <td rowSpan="2">Красное Село - Гатчина Балт.</td> <td>М62, ЧМЭЗ</td> <td> 30</td> <td> 2000</td>  </tr>
    <tr> <td>ТЭМ18</td> <td> 30</td> <td> 1500</td>  </tr>
    <tr> <td rowSpan="2">Гатчина Балт. - Красное Село</td> <td> М62, ЧМЭЗ</td> <td> 30</td> <td> 3500</td>  </tr>
    <tr> <td>ТЭМ18</td> <td> 30</td> <td> 2000</td>  </tr>
    <tr> <td>С-Петербург Балт. - Котлы</td> <td> М62</td> <td> 30</td> <td> 2200</td>  </tr>
    <tr> <td>Котлы - С-Петербург Балт.</td> <td> М62</td> <td> 30</td> <td> 2500</td>  </tr>
    <tr> <td>С-Петербург Балт. - Калище</td> <td> ЧМЭЗ</td> <td> 30</td> <td> 2400</td>  </tr>
    <tr> <td>Лигово - Калище</td> <td>ТЭМ18</td> <td>30</td> <td>1500, 2000*</td></tr>
    <tr> <td colSpan="4">*Безостановочный пропуск по ст. Лебяжье, при отсутствии ограничений скорости менее 25 км/ч</td> </tr>
    <tr> <td>Калище - Лигово</td> <td>ТЭМ18</td> <td> 30</td> <td> 2000</td>  </tr>
    <tr> <td>Калище - С-Петербург Балт.</td> <td> ЧМЭЗ</td> <td> 30</td> <td> 2000</td>  </tr>
    <tr> <td rowSpan="3">Котлы - Веймарн</td> <td> М62</td> <td> 30</td> <td> 2200</td>  </tr>
    <tr> <td>ТЭМ2, ЧМЭЗ</td> <td> 30</td> <td> 1100</td>  </tr>
    <tr> <td>ТЭМ7</td> <td> 30</td> <td> 1400</td>  </tr>
    <tr> <td rowSpan="3">Веймарн - Котлы</td> <td> М62</td> <td> 30</td> <td> 2500</td>  </tr>
    <tr> <td>ТЭМ2, ЧМЭЗ</td> <td> 30</td> <td> 1300</td>  </tr>
    <tr> <td>ТЭМ7</td> <td> 30</td> <td> 1600</td>  </tr>
    <tr> <td rowSpan="3">Котлы - Лужская</td> <td> М62</td> <td> 30</td> <td> 2200</td>  </tr>
    <tr> <td>ТЭМ2, ЧМЭЗ</td> <td> 30</td> <td> 1300</td>  </tr>
    <tr> <td>ТЭМ7</td> <td> 30</td> <td> 1600</td>  </tr>
    <tr> <td rowSpan="3">Лужская - Котлы</td> <td> М62</td> <td> 30</td> <td> 2000</td>  </tr>
    <tr> <td>ТЭМ2, ЧМЭЗ</td> <td> 30</td> <td> 1100</td>  </tr>
    <tr> <td>ТЭМ7</td> <td> 30</td> <td> 1400</td>  </tr>
    <tr> <td rowSpan="3">Лужская Сорт. - Лужская Север, Лужская Южн.</td> <td> М62, ТЭМ2, ЧМЭЗ,ТЭМ18</td> <td> 30</td> <td> 4600</td>  </tr>
    <tr> <td>ТЭМ7</td> <td> 30</td> <td> 6500</td>  </tr>
    <tr> <td> 2ТЭМ18</td> <td> 57</td> <td> 8000</td>  </tr>
    <tr> <td rowSpan="4">Лужская Север, Лужская Южн. - Лужская Сорт.</td> <td> М62, ТЭМ2,ЧМЭЗ,ТЭМ18</td> <td> 30</td> <td> 1300</td>  </tr>
    <tr> <td>ТЭМ7</td> <td> 30</td> <td> 1700</td>  </tr>
    <tr> <td>2ТЭ116У</td> <td> 57</td> <td> 6500</td>  </tr>
    <tr> <td>2ТЭМ18</td> <td> 57</td> <td> 4000</td>  </tr>
    <tr> <td>Веймарн - Гдов</td> <td>  М62, ТЭМ2, ЧМЭЗ, ТЭМ7</td> <td> 30</td> <td> 2000</td>  </tr>
    <tr> <td>Гдов - Веймарн</td> <td> М62, ТЭМ2, ЧМЭЗ, ТЭМ7</td> <td> 30</td> <td> 2000</td>  </tr>
    <tr> <td>Лигово - СПСМ</td> <td>ТЭМ18</td> <td>30</td> <td>1500, 2000*</td></tr>
    <tr> <td colSpan="4">*Безостановочный пропуск по ст. Волковская, при отсутствии ограничений скорости менее 25 км/ч</td> </tr>
    <tr> <td rowSpan="2">СПСМ - Шушары</td> <td>ЧМЭЗ</td> <td>30</td> <td>2000</td>  </tr>
    <tr> <td>ТЭМ7</td> <td>30</td> <td>3000</td>  </tr>
    <tr> <td rowSpan="2">Шушары - С-Петербург-Сорт.Моск.</td> <td>ЧМЭЗ</td> <td>30</td> <td>2000</td>  </tr>
    <tr> <td>ТЭМ7</td> <td>30</td> <td>2500</td>  </tr>
    <tr> <td rowSpan="2">СПСМ - Волковская</td> <td> ТЭМ7</td> <td>30</td> <td>3600</td>  </tr>
    <tr> <td>ТЭМ2</td> <td>30</td> <td>3500</td>  </tr>
    <tr> <td rowSpan="2">Волковская - СПСМ</td> <td>ТЭМ7</td> <td>30</td> <td>4000</td>  </tr>
    <tr> <td>ТЭМ2</td> <td>30</td> <td>2600</td>  </tr>
  </>
  ;

export default dataTrainExport;