import React from 'react';

export default function TableWeightNorm(props) {
  return (
    <React.Fragment>
      <button className="col" data-toggle="collapse" data-target={"#" + props.id}>
        {props.tname}
      </button>
      <table id={props.id} className="table table-responsive table-striped table-bordered table-secondary collapse">
        <thead>
          <tr>
            <th rowSpan="2">Серия локомотива</th>
            <th colSpan="2">Длина поезда, усл.</th>
            <th colSpan="3">Весовая норма, т</th>
            <th rowSpan="2">Условия&nbsp;пропуска&nbsp;поездов</th> {/* %nbsp используется для ширины столбца */}
            <th colSpan="3">Подталкивание</th>
          </tr>
          <tr>
            <th>Унифиц.</th>
            <th>Макс.</th>
            <th>Унифиц.</th>
            <th>Паралл.</th>
            <th>Критич.</th>
            <th>Серия локомотива</th>
            <th>Участок подталкивания</th>
            <th>Максимальная масса поезда</th>
          </tr>
        </thead>
        <tbody>
          {props.tbody}
        </tbody>
      </table>
    </React.Fragment>
  );
}