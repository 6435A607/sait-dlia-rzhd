import React from 'react';

const dataAreasWeight = [
  {
    id: "SpsmBabaevo",
    tname: "С-Петербург сорт.Моск. - Бабаево",
    tbody: <React.Fragment>
      <tr> <td>Мга - Назия</td> <td>52-53км</td> <td>6,4</td> <td>5900</td> <td>8900</td> <td></td> <td></td> </tr>
      <tr> <td>Мга - Назия</td> <td>64-66км</td> <td>6,3</td> <td>6000</td> <td>9100</td> <td></td> <td></td> </tr>
      <tr> <td>Куколь - Мыслино</td> <td>136-138км</td> <td>6</td> <td>6300</td> <td>9500</td> <td></td> <td></td> </tr>
      <tr> <td>Тихвин - Большой Двор</td> <td>201-202км</td> <td>5,9</td> <td>6400</td> <td></td> <td></td> <td></td> </tr>
      <tr> <td>Тихвин - Большой Двор</td> <td>204-206</td> <td>5,2</td> <td>7100</td> <td></td> <td></td> <td></td> </tr>
      <tr> <td>Тихвин - Большой Двор</td> <td>219-220км</td> <td>8,9</td> <td>4400</td> <td>6600</td> <td></td> <td></td> </tr>
      <tr> <td>Тихвин - Большой Двор</td> <td>223-225км</td> <td>6,1</td> <td>6200</td> <td></td> <td></td> <td></td> </tr>
      <tr> <td>Большой Двор - Пикалёво 1</td> <td>228-235</td> <td>7</td> <td>5500</td> <td>8300</td> <td></td> <td></td> </tr>
      <tr> <td>Пикалёво 1 - Пикалёво 2</td> <td>239-244</td> <td>6,6</td> <td>5800</td> <td>8700</td> <td></td> <td></td> </tr>
      <tr> <td>Пикалёво 2 - Коли</td> <td>246-247</td> <td>6,7</td> <td>5700</td> <td>8600</td> <td></td> <td></td> </tr>
      <tr> <td>Пикалёво 2 - Коли</td> <td>253-254км</td> <td>6</td> <td>6300</td> <td></td> <td></td> <td></td> </tr>
      <tr> <td>Коли - Ефимовская</td> <td>273-274км</td> <td>7</td> <td>5500</td> <td>8300</td> <td></td> <td></td> </tr>
      <tr> <td>Ефимовская - Подборовье</td> <td>275-276км</td> <td>6,2</td> <td>6100</td> <td></td> <td></td> <td></td> </tr>
    </React.Fragment>
  },
  {
    id: "BabaevoSpsm",
    tbody: <React.Fragment>
      <tr> <td>Ефимовкая - Коли</td> <td>272-271км</td> <td>6,5</td> <td>5900</td> <td>8800</td> <td></td> <td></td> </tr>
      <tr> <td>Ефимовкая - Коли</td> <td>266-264</td> <td>6,1</td> <td>6200</td> <td></td> <td></td> <td></td> </tr>
      <tr> <td>Цвылёво - Валя</td> <td>179-178км</td> <td>6,8</td> <td>5600</td> <td>8500</td> <td></td> <td></td> </tr>
    </React.Fragment>,
    tname: "Бабаево-С-Петербург сорт.Моск"
  },
  /* Обратить внимание, нет в новом документе */
  {
    tbody: <React.Fragment>
      <tr> <td>Куколь Пороги</td> <td>10</td> <td>5,3</td> <td>5000</td> <td></td> <td></td> <td></td> </tr>
    </React.Fragment>,
    tname: "Куколь - Б/п 116 км",
    id: "Kukol116km"
  },
  {
    tbody: <React.Fragment>
      <tr> <td>Свирь - Подпорожье</td> <td>284-281км</td> <td>8,3</td> <td>4700</td> <td>7100</td> <td></td> <td></td> </tr>
      <tr> <td>Подпорожье - Яндеба</td> <td>279</td> <td>6,7</td> <td>5700</td> <td>8600</td> <td></td> <td></td> </tr>
      <tr> <td>Яндеба - Янега</td> <td>269-268</td> <td>8,9</td> <td>4400</td> <td>6600</td> <td></td> <td></td> </tr>
    </React.Fragment>,
    tname: "Свирь - Лодейное Поле",
    id: "SvyrLodPole"
  },
  {
    tbody: <React.Fragment>
      <tr> <td>Янега - Яндеба</td> <td>251-252км</td> <td>6,8</td> <td>5600</td> <td>8500</td> <td></td> <td></td> </tr>
      <tr> <td>Янега - Яндеба</td> <td>257-260</td> <td>8,5</td> <td>4600</td> <td>6900</td> <td></td> <td></td> </tr>
      <tr> <td>Яндеба - Подпорожье</td> <td>272-274</td> <td>8,2</td> <td>4700</td> <td>7200</td> <td></td> <td></td> </tr>
      <tr> <td>Подпорожье - Свирь</td> <td>285-287</td> <td>8,4</td> <td>4600</td> <td>7000</td> <td></td> <td></td> </tr>
    </React.Fragment>,
    tname: "Лодейное Поле - Свирь",
    id: "LodPoleSvyr"
  },
  {
    tbody: <React.Fragment>
      <tr> <td>Паша - Юги</td> <td>188-186</td> <td>10</td> <td>3900</td> <td>6000</td> <td></td> <td></td> </tr>
      <tr> <td>Паша - Юги</td> <td>176-175</td> <td>7,5</td> <td>5100</td> <td>7800</td> <td></td> <td></td> </tr>
    </React.Fragment>,
    tname: "Лод.Поле - Волховстрой",
    id: "LodPoleVolhov"
  },
  {
    tbody: <React.Fragment>
      <tr> <td>Колчаново - Лунгачи</td> <td>148-149</td> <td>7,8</td> <td>5000</td> <td>7500</td> <td></td> <td></td> </tr>
      <tr> <td>Юги - Паша</td> <td>173-174</td> <td>6,7</td> <td>5700</td> <td>8600</td> <td></td> <td></td> </tr>
      <tr> <td>Юги - Паша</td> <td>185-186</td> <td>5,8</td> <td>6500</td> <td>9700</td> <td></td> <td></td> </tr>
    </React.Fragment>,
    tname: "Волховстрой - Лод.Поле",
    id: "VolhovLodPole"
  },
  {
    tbody: <React.Fragment>
      <tr> <td>Котлы - Лужская</td> <td>10-11</td> <td>8</td> <td>4800</td> <td>7300</td> <td>5600</td> <td>7200</td> </tr>
    </React.Fragment>,
    tname: "Котлы - Лужская",
    id: "KotlyLuzh"
  },
  {
    tbody: <React.Fragment>
      <tr> <td>Вервенка - Веймарн</td> <td>202-203</td> <td>7,1</td> <td></td> <td></td> <td>6200</td> <td>8000</td> </tr>
      <tr> <td>Вервенка - Веймарн</td> <td>210</td> <td>7,1</td> <td></td> <td></td> <td>6200</td> <td>8000</td> </tr>
      <tr> <td>Вервенка - Веймарн</td> <td>214-215</td> <td>8,3</td> <td></td> <td></td> <td>5400</td> <td>6900</td> </tr>
    </React.Fragment>,
    tname: "Рудничная - Веймарн",
    id: "RudVeimarn"
  },
  {
    tbody: <React.Fragment>
      <tr> <td>Мга - Войтоловка</td> <td>7-8</td> <td>6,3</td> <td>6000</td> <td>9100</td> <td>7000</td> <td>9000</td> </tr>
      <tr> <td>Пустынька - Стекольный</td> <td>22</td> <td>8,7</td> <td>4500</td> <td>6800</td> <td>5100</td> <td>6600</td> </tr>
      <tr> <td>Пустынька - Стекольный</td> <td>23</td> <td>9,9</td> <td>4000</td> <td>6000</td> <td>4500</td> <td>5800</td> </tr>
      <tr> <td>Владимирская - Фрезерный </td> <td>16-14</td> <td>8,4</td> <td>4600</td> <td>7000</td> <td>5300</td> <td>6800</td> </tr>
      <tr> <td>Фрезерный - Гатчина</td> <td>5-3</td> <td>8,5</td> <td>4600</td> <td>6900</td> <td>5300</td> <td>6800</td> </tr>
    </React.Fragment>,
    tname: "Мга - Гатчина Т. Балт (Зап.П)",
    id: "MgaGatchina"
  },
  {
    tbody: <React.Fragment>
      <tr> <td>Войсковицы - Елизаветино</td> <td>56-58</td> <td>8</td> <td>4800</td> <td>7300</td> <td>5600</td> <td>7200</td> </tr>
      <tr> <td>Войсковицы - Елизаветино</td> <td>69-70</td> <td>7,8</td> <td>5000</td> <td>7500</td> <td>5700</td> <td>7300</td> </tr>
    </React.Fragment>,
    tname: "Гатчина Т. Балт (Зап.П) - Нарва",
    id: "GatchinaNarva"
  },
  {
    tbody: <React.Fragment>
      <tr> <td>Будогощь - Кириши</td> <td>85</td> <td>6,7</td> <td>5700</td> <td>8600</td> <td>6600</td> <td>8500</td> </tr>
    </React.Fragment>,
    tname: "Будогощь - Мга",
    id: "BudoMga"
  }
];

export default dataAreasWeight;