import React from 'react';

export default function TableTrainExport(props) {
  return (
    <>
      <table className="table table-responsive table-striped table-bordered table-secondary">
        <thead>
          <tr>
            <th>Участки&nbsp;обслуживания</th>
            <th>Локомотив</th>
            <th>Макс. длина поезда (усл. вагонов)</th>
            <th>Критическая норма массы поезда (тонны)</th>
          </tr>
        </thead>
        <tbody>
          {props.data}
        </tbody>
      </table>
    </>
  );
}