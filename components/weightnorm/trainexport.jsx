import React from 'react';
import TableTrainExport from './tabletrainexport.jsx';
import dataTrainExport from './datatrainexport.jsx';

export default function TrainExport(props) {
  return (
    <>
      <TableTrainExport data={dataTrainExport} />
    </>
  );
}