import React from 'react';

export default function TableAreasWeightNorm(props) {
  return (
    <React.Fragment>
      <button className="col" data-toggle="collapse" data-target={"#" + props.id}>
        {props.tname}
      </button>
      <table id={props.id} className="table table-responsive table-striped table-bordered table-secondary collapse">
        <thead>
          <tr>
            <th rowSpan="2">Перегон участка</th>
            <th rowSpan="2">Километры участка</th>
            <th rowSpan="2">Уклон</th>
            <th colSpan="4">Максимальная масса поезда (т) по сериям локомотива</th>
          </tr>
          <tr>
            <th>ВЛ10, 2ЭС4К, 2ЭС6</th>
            <th>ВЛ15, 3ЭС4К</th>
            <th>2ТЭ116</th>
            <th>2ТЭ116У, УД</th>
          </tr>
        </thead>
        <tbody>
          {props.tbody}
        </tbody>
      </table>
    </React.Fragment>
  );
}
