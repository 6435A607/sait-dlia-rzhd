import React from 'react';
import dataWeightNorm from './dataweightnorm.jsx';
import TableWeightNorm from './tableweightnorm.jsx';

export default function WeightNorm(props) {
  const data = dataWeightNorm.map((idx) => 
  <TableWeightNorm key={idx.id} id={idx.id} tname={idx.tname} tbody={idx.tbody} /> );
  return (
    <React.Fragment>
      <h4 className="text-center">Нормы массы и длины гузовых поездов по сериям локомотивов</h4>
      {data}
    </React.Fragment>
  );
};
