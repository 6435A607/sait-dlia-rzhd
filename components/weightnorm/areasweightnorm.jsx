import React from 'react';
import TableAreasWeightNorm from './tableareasweightnorm.jsx';
import dataAreasWeight from './datatableareasweightnorm.jsx';

export default function AreasWeightNorm(props) {
  const data = dataAreasWeight.map((idx) => 
    <TableAreasWeightNorm key={idx.id} id = {idx.id} tname = {idx.tname} tbody = {idx.tbody} />
  );
  return (
    <React.Fragment>
      <h4 className="text-center">Перечень участков, на которых запрещено взятие с места поезда без вспомогательного локомотива (при вынужденной остановке поезда)</h4>
      {data}
    </React.Fragment>
  );
};

