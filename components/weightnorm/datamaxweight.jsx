import React from 'react';

const tbodySpsmMga = <React.Fragment>
  <tr> <td>ВЛ10, ВЛ10У</td> <td rowSpan="2">4500</td> <td>6500</td> <td>9 мин.</td> <td>10 мин.  Uxx=3600В на ЭЧЭ Мга.</td> </tr>
  <tr> <td>2ЭС4К</td> <td>12600</td> <td></td> <td>14 мин. Uхх=3600В, 2ПВА в работе на ЭЧЭ Мга (при движении 4,5тт-4,5тт-12600тт-4,5тт-4,5тт)</td> </tr>
  <tr> <td rowSpan="2">3ЭС4К, ВЛ15</td> <td></td> <td>7800</td> <td></td> <td>13 мин. Uxx=3600В на ЭЧЭ Мга. (при движении 4,5тт-4,5тт-7,8тт-4,5тт-4,5тт)</td> </tr>
  <tr> <td></td> <td>8000</td> <td></td> <td>13 мин. Uxx=3600В на ЭЧЭ Мга (при движении 4,5тт-8тт-4,5тт)</td> </tr>
</React.Fragment>;
const tnameSpsmMga = "С.Петербург Сорт. Моск - Мга";
const tidSpsmMga = "SpsmMga";

const tbodyMgaSpsm = <React.Fragment>
  <tr> <td>ВЛ10, ВЛ10У </td> <td rowSpan="2">6000</td> <td>6500</td> <td>9 мин</td> <td>12 мин.</td> </tr>
  <tr> <td>2ЭС4К</td> <td>12600</td> <td></td> <td>10/15/15/10 Uxx=3600В, 2 агрегата в работе на ЭЧЭ Мга</td> </tr>
  <tr> <td>3ЭС4К, ВЛ15</td> <td></td> <td>8000</td> <td></td> <td>14 мин. (при движении 6тт/8тт/6тт)</td> </tr>
</React.Fragment>;
const tnameMgaSpsm = "Мга - С. Петербург сорт. Моск.";
const tidMgaSpsm = "MgaSpsm";

const tbodyMgaVolhov = <React.Fragment>
  <tr><td>ВЛ10, ВЛ10У              </td> <td>4500</td> <td>6500</td> <td>10 мин</td> <td>12 мин.</td> </tr>
  <tr> <td>2ЭС4К</td> <td></td> <td>12600</td> <td></td> <td>Один поезд на МПЗ Мга-75км, Uхх=3600В на ЭЧЭ Мга, Волховстрой. Uхх=3650В на ЭЧЭ 75км</td> </tr>
  <tr> <td rowSpan="2">3ЭС4К, ВЛ15</td> <td rowSpan="2"></td> <td>7800</td> <td rowSpan="2"></td> <td rowSpan="2">15мин. Uxx=3600В на ЭЧЭ Мга, 75км, Нов. Быт, 2 агрегата в работе на ЭЧЭ Новый Быт (при движении 4,5тт/8000тт/4,5тт) и (4,5тт-7800тт-4,5тт)</td> </tr>
  <tr> <td>8000</td> </tr>
</React.Fragment>;
const tnameMgaVolhov = "Мга - Волховстрой";
const tidMgaVolhov = "MgaVolhov";

const tbodyVolhovMga = <React.Fragment>
  <tr> <td>ВЛ10, ВЛ10У </td> <td rowSpan="2">6000</td> <td>6500</td> <td>10 мин</td> <td>12 мин.</td> </tr>
  <tr> <td>2ЭС4К</td> <td>12600</td> <td></td> <td>Один поезд на межподстанционной зоне 75км-Новый Быт.  Uхх=3600В на ЭЧЭ Мга, Новый Быт. Uхх=3700В на ЭЧЭ 75км.                </td> </tr>
  <tr> <td rowSpan="2">3ЭС4К, ВЛ15</td> <td rowSpan="2"></td> <td>8000</td> <td></td> <td>15 мин. Uxx=3600В на ЭЧЭ Мга, 75км, Нов. Быт, 2 агрегата в работе на ЭЧЭ Новый Быт (при движении 6000т/8000т/6000т)</td> </tr>
  <tr> <td>9000</td> <td></td> <td>15мин. Uxx=3600В на ЭЧЭ Мга, 75км, Нов. Быт, 2 агрегата в работе на ЭЧЭ Новый Быт (6000т-9000тт-6000т)</td> </tr>
</React.Fragment>;
const tnameVolhovMga = "Волховстрой - Мга";
const tidVolhovMga = "VolhovMga";

const tbodyVolhovBabaevo = <React.Fragment>
  <tr> <td rowSpan="2">ВЛ10, ВЛ10У, 2ЭС4К </td> <td rowSpan="2">4500</td> <td>6500</td> <td>9 мин</td> <td>13 мин.</td> </tr>
  <tr> <td>12600</td> <td></td> <td>Один поезд  на зоне Тихвин- Б. Двор, Б. Двор-Пикалево. Uхх=3600В на всех ЭЧЭ,2агрегата в работе. Б.Двор - 248км Толкач  ограничение тока ТЭД не более: 400А.</td> </tr>
  <tr> <td rowSpan="2">3ЭС4К, ВЛ15</td> <td rowSpan="2"></td> <td>7800</td> <td></td> <td rowSpan="2">14 мин. (при движении 4,5тт/8тт/4,5тт) и (4,5тт-7,8тт-4,5тт) Uxx=3550В на всех ЭЧЭ, 2 агрегата в работе.</td> </tr>
  <tr> <td>8000</td> <td></td> </tr>
</React.Fragment>;
const tnameVolhovBabaevo = "Волховстрой- Бабаево";
const tidVolhovBabaevo = "VolhovBabaevo";

const tbodyBabaevoVolhov = <React.Fragment>
  <tr> <td rowSpan="2">ВЛ10, ВЛ10У, 2ЭС4К</td> <td rowSpan="2">6000</td> <td>6500</td> <td>9 мин</td> <td>10 мин. Uхх=3600В на всех ЭЧЭ.</td> </tr>
  <tr> <td>12600</td> <td></td> <td>16мин. (при движении 6тт-6тт-12600т-6тт-6тт)</td> </tr>
  <tr> <td rowSpan="2">3ЭС4К, ВЛ15</td> <td></td> <td>8000</td> <td></td> <td>12 мин. (при движении 6тт/8тт/6тт).Uxx=3550В на всех ЭЧЭ, 2 агрегата в работе.</td> </tr>
  <tr> <td></td> <td>9000</td> <td></td> <td>13 мин. (при движении 6тт/9тт/6тт). Uxx=3550В на всех ЭЧЭ, 2 агрегата в работе.</td> </tr>
</React.Fragment>;
const tnameBabaevoVolhov = "Бабаево - Волховстрой";
const tidBabaevoVolhov = "BabaevoVolhov";

const tbodySvyrVolhov = <React.Fragment>
  <tr> <td>ВЛ10, ВЛ10У 2ЭС4К</td> <td>5200</td> <td>6000</td> <td>12 мин</td> <td>13 мин. Uxx=3500В на всех ЭЧЭ.</td> </tr>
  <tr> <td>3ЭС4К, ВЛ15</td> <td></td> <td>8000</td> <td></td> <td>26 мин Uxx=3600В на ЭЧЭ Паша, Заостровье, 2 агрегата в работе (при движении 5,2тт/8тт/5,2тт)</td> </tr>
</React.Fragment>;
const tnameSvyrVolhov = "Свирь - Волховстрой";
const tidSvyrVolhov = "SvyrVolhov";

const tbodyVolhovSvyr = <React.Fragment>
  <tr> <td rowSpan="2">ВЛ10, ВЛ10У 2ЭС4К</td> <td>3000</td> <td>5200              6000</td> <td>12 мин</td> <td>16 мин. 16 мин. на ЭЧЭ Л. Поле, Яндеба Uхх=3600В.</td> </tr>
  <tr> <td></td> <td>6500</td> <td></td> <td>17 мин. на ЭЧЭ Л. Поле, Яндеба Uхх=3600В, 2 агрегата в работе</td> </tr>
  <tr> <td>3ЭС4К, ВЛ15</td> <td></td> <td>8000</td> <td></td> <td>30 мин (при движении 3тт/8тт/3тт). На ЭЧЭ Лод. Поле, Яндеба, Паша, Заостровье Uхх = 3650 В. На ЭЧЭ Лод. Поле, Яндеба в работе 2 агрегата</td> </tr>
</React.Fragment>;
const tnameVolhovSvyr = "Волховстрой - Свирь";
const tidVolhovSvyr = "VolhovSvyr";

const tbodyKirishiMga = <React.Fragment>
  <tr> <td>ВЛ10</td> <td rowSpan="2">4500</td> <td>5600</td> <td rowSpan="2">10 мин.</td> <td>12 мин.</td> </tr>
  <tr> <td>2ЭС4К</td> <td>6500</td> <td>14 мин.</td> </tr>
  <tr> <td>3ЭС4К, ВЛ15</td> <td></td> <td>8000</td> <td></td> <td>1 поезд на каждой МПЗ. Uхх=3600В на ЭЧЭ Мга, Малукса, 3650В на ЭЧЭ Жарок, ЭЧЭ Кириши</td> </tr>
</React.Fragment>;
const tnameKirishiMga = "Кириши- Мга";
const tidKirishiMga = "KirishiMga";

const tbodyMgaKirishi = <React.Fragment>
  <tr> <td>ВЛ10, 2ЭС4К</td> <td>3000</td> <td>4500</td> <td>10 мин.</td> <td>12 мин.</td> </tr>
  <tr> <td>3ЭС4К, ВЛ15</td> <td></td> <td>6700</td> <td></td> <td>16 мин. (4,5тт-6,7тт-4,5тт)</td> </tr>
</React.Fragment>;
const tnameMgaKirishi = "Мга-Кириши";
const tidMgaKirishi = "MgaKirishi";

const tbodyMgaGatchina = <React.Fragment>
  <tr> <td rowSpan="2">ВЛ10, 2ЭС4К</td> <td rowSpan="2">4500</td> <td>6500</td> <td>10 мин</td> <td>12  мин. На ЭЧЭ Мга Uxx=3600В</td> </tr>
  <tr> <td>12600</td> <td></td> <td>Один поезд на зоне Мга-Новолисино, Uхх=3600В на ЭЧЭ Мга, 2 агрегата в работе.</td> </tr>
  <tr> <td rowSpan="2">3ЭС4К, ВЛ15</td> <td rowSpan="2">-</td> <td>7800</td> <td>-</td> <td>14 мин. (4,5тт-7,8тт-4,5тт), Uxx=3600В на ЭЧЭ Мга, Новолисино и 2 агрегата в работе</td> </tr>
  <tr> <td>9000</td> <td></td> <td>15 мин. Uxx=3600В на ЭЧЭ Мга, Новолисино и 2 агрегата в работе</td> </tr>
</React.Fragment>;
const tnameMgaGatchina = "Мга - Гатчина тов. Балтийская";
const tidMgaGatchina = "MgaGatchina";

const tbodyGatchinaMga = <React.Fragment>
  <tr> <td>ВЛ10, 2ЭС4К</td> <td>1400</td> <td>5000</td> <td>6 мин</td> <td>15мин. Uxx=3600В на ЭЧЭ Мга.</td> </tr>
  <tr> <td>3ЭС4К, ВЛ15</td> <td></td> <td>7000</td> <td></td> <td>16 мин. На Uxx=3600В на ЭЧЭ Мга, 2 агрегата в работе.</td> </tr>
</React.Fragment>;
const tnameGatchinaMga = "Гатчина тов. Балтийская - Мга";
const tidGatchinaMga = "GatchinaMga";

const tbodyGatchinaLuzh = <React.Fragment>
  <tr> <td rowSpan="3">ВЛ10, ВЛ10у, 2ЭС4К</td> <td rowSpan="3">-</td> <td>3600</td> <td rowSpan="3"></td> <td>10 мин</td> </tr>
  <tr> <td>5000</td> <td>12мин Uxx=3600В на всех ЭЧЭ. </td> </tr>
  <tr> <td>6500</td><td>12 мин Uxx=3600В на всех ЭЧЭ.</td></tr>
  <tr> <td rowSpan="3">3ЭС4К, ВЛ15</td> <td rowSpan="3">-</td> <td>5400</td> <td rowSpan="3"></td> <td>11мин </td> </tr>
  <tr> <td>7000</td><td>14мин  Uxx=3600В на всех ЭЧЭ. </td> </tr>
  <tr> <td>9000</td> <td>15мин Uxx=3600В на всех ЭЧЭ.</td> </tr>
</React.Fragment>;
const tnameGatchinaLuzh = "Гатчина тов. Балт. - Лужская";
const tidGatchinaLuzh = "GatchinaLuzh";

const tbodyLuzhGatchina = <React.Fragment>
  <tr> <td>ВЛ10, ВЛ10у, 2ЭС4К</td> <td>-</td> <td>4900</td> <td></td> <td>12 мин</td> </tr>
  <tr> <td>3ЭС4К, ВЛ15</td> <td>-</td> <td>6800</td> <td></td> <td>13 мин Uxx=3600В на всех ЭЧЭ.</td> </tr>
</React.Fragment>;
const tnameLuzhGatchina = "Лужская - Гатчина тов. Балт.";
const tidLuzhGatchina = "LuzhGatchina";

let id = [
    tidSpsmMga,
    tidMgaSpsm,
    tidMgaVolhov,
    tidVolhovMga,
    tidVolhovBabaevo,
    tidBabaevoVolhov,
    tidSvyrVolhov,
    tidVolhovSvyr,
    tidKirishiMga,
    tidMgaKirishi,
    tidMgaGatchina,
    tidGatchinaMga,
    tidGatchinaLuzh,
    tidLuzhGatchina
];

let name = [
    tnameSpsmMga,
    tnameMgaSpsm,
    tnameMgaVolhov,
    tnameVolhovMga,
    tnameVolhovBabaevo,
    tnameBabaevoVolhov,
    tnameSvyrVolhov,
    tnameVolhovSvyr,
    tnameKirishiMga,
    tnameMgaKirishi,
    tnameMgaGatchina,
    tnameGatchinaMga,
    tnameGatchinaLuzh,
    tnameLuzhGatchina
];

let body = [
    tbodySpsmMga,
    tbodyMgaSpsm,
    tbodyMgaVolhov,
    tbodyVolhovMga,
    tbodyVolhovBabaevo,
    tbodyBabaevoVolhov,
    tbodySvyrVolhov,
    tbodyVolhovSvyr,
    tbodyKirishiMga,
    tbodyMgaKirishi,
    tbodyMgaGatchina,
    tbodyGatchinaMga,
    tbodyGatchinaLuzh,
    tbodyLuzhGatchina
];

let dataMaxWeight = [];
for(let i=0; i < id.length; i++) {
    dataMaxWeight[i] = { id: null, tname: null, tbody: null };
    dataMaxWeight[i].id = id[i];
    dataMaxWeight[i].tname = name[i];
    dataMaxWeight[i].tbody = body[i];
}

export default dataMaxWeight;