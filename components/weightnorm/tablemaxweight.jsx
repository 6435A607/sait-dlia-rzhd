import React from 'react';

export default function TableMaxWeight(props) {
  return (
    <React.Fragment>
      <button className="col" data-toggle="collapse" data-target={"#" + props.id}>
        {props.tname}
      </button>
      <table id={props.id} className="table table-responsive table-striped table-bordered table-secondary collapse">
        <thead>
          <tr>
            <th rowSpan="2">Серия локомотива</th>
            <th colSpan="2">Норма массы, т</th>
            <th rowSpan="2">Межпоездные интервалы при графиковой норме массы, не менее мин.</th>
            <th rowSpan="2">Межпоездные интервалы при критической норме массы, не менее мин. Особые условия пропуска.</th>
          </tr>
          <tr>
            <th>Унифиц.</th>
            <th>Крит.</th>
          </tr>
        </thead>
        <tbody>
          {props.tbody}
        </tbody>
      </table>
    </React.Fragment>
  );
}