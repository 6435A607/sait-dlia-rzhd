import React from 'react';
import TableMaxWeight from './tablemaxweight.jsx';
import dataMaxWeight from './datamaxweight.jsx';

export default function MaxWeightNorm(props) {
  const data = dataMaxWeight.map((idx) => 
    <TableMaxWeight key={idx.id} id={idx.id} tname={idx.tname} tbody={idx.tbody} />
  );
  return (
    <React.Fragment>
      <h4 className="text-center">Максимальные весовые нормы и межпоездные интервалы по условиям тягового электроснабжения</h4>
      {data}
    </React.Fragment>
  );
}
