import React from 'react';
import { Link, BrowserRouter } from 'react-router-dom';

export default function LinksList(props) {
  const classNameSection = "list-group-item list-group-item-secondary " +
    "list-group-item-action";
  const classNameChapter = classNameSection + " chapter";
  return (
    <React.Fragment>
      <div className={classNameSection} data-toggle="collapse"
        data-target="#MI">Местная инструкция
        </div>
      <ol className="list-group collapse" id="MI">
        <li><Link to="/michapter1" className={classNameChapter}>Назначение и область применения</Link></li>
        <li><Link to="/michapter2" className={classNameChapter}>Нормативные ссылки</Link></li>
        <li><Link to="/michapter3" className={classNameChapter}>Нормативные документы</Link></li>
        <li><Link to="/michapter4" className={classNameChapter}>Определения терминов, сокращения, обозначения</Link></li>
        <li><Link to="/michapter5" className={classNameChapter}>Общее положение</Link></li>
        <li><Link to="/michapter6" className={classNameChapter}>Порядок регистрации на скоростемерной ленте и контроля за работой 3СЛ 2М в процессе эксплуатации. </Link></li>
        <li><a data-toggle="collapse" data-target="#chapter-7">Перечень работ и правила проверки тормозного оборудования выполняемых локомотивными бригадами</a>
          <ol id="chapter-7" className="list-group collapse">
            <li><Link to="/michapter7-1" className={classNameChapter}>При приемке</Link></li>
            <li><Link to="/michapter7-2" className={classNameChapter}>При смене л/бр без отцепки</Link></li>
            <li><Link to="/michapter7-3" className={classNameChapter}>При сдаче или приемке из депо</Link></li>
            <li><Link to="/michapter7-4" className={classNameChapter}>При смене локомотивных бригад (из рук в руки)</Link></li>
            <li><Link to="/michapter7-5" className={classNameChapter}>Проверки после ТО и отстоя без локомотивных бригад</Link></li>
            <li><Link to="/michapter7-1-note" className={classNameChapter}>Пояснения к п.7.1</Link></li>
            <li><Link to="/michapter7-4-note" className={classNameChapter}>Пояснения к пп. 7.3, 7.4</Link></li>
          </ol>
        </li>
        <li><Link to="/michapter8" className={classNameChapter}>Обязательные протяжки ленты</Link></li>
        <li><a data-toggle="collapse" data-target="#chapter-9">Порядок прицепки к составу, опробования автотормозов на станции</a>
          <ol id="chapter-9" className="list-group collapse">
            <li><Link to="/michapter9_1" className={classNameChapter}>Прицепка к составу</Link></li>
            <li><Link to="/michapter9_2" className={classNameChapter}>Порядок действий перед опробованием тормозов</Link></li>
            <li><Link to="/michapter9_3" className={classNameChapter}>Полное опробование тормозов грузовых и грузопассажирских поездов</Link></li>
            <li><Link to="/michapter9_4" className={classNameChapter}>Порядок полного опробования тормозов в одно лицо</Link></li>
            <li><Link to="/michapter9_5" className={classNameChapter}>Порядок сокращённого опробования  тормозов  грузовых поездов.</Link></li>
            <li><Link to="/michapter9_6" className={classNameChapter}>Выполнение сокращенной пробы тормозов в грузовых поездах после смены локомотивных бригад, когда локомотив от поезда не отцеплялся</Link></li>
            <li><Link to="/michapter9_7" className={classNameChapter}>Выполнение технологического опробования тормозов в грузовых поездах</Link></li>
            <li><Link to="/michapter9_8" className={classNameChapter}>Выполнение полного опробования тормозов в пассажирских  поездах</Link></li>
            <li><Link to="/michapter9_9" className={classNameChapter}>Выполнение сокращенного опробования в пассажирских поездах</Link></li>
            <li><Link to="/michapter9_10" className={classNameChapter}>Опробование тормозов одиночно следующего локомотива</Link></li>
          </ol>
        </li>
        <li><Link to="/michapter10" className={classNameChapter}>Порядок проверки целостности ТМ перед отправлением</Link></li>
        <li><Link to="/michapter11" className={classNameChapter}>При маневровых передвижениях</Link></li>
        <li><Link to="/michapter12" className={classNameChapter}>Действия локомотивной бригады с классными вагонами после остановки экстренным торможением</Link></li>
        <li><Link to="/michapter13" className={classNameChapter}>Перечень мест где машинист может привести поезд в движение ранее отпуска автотормозов</Link></li>
        <li><Link to="/michapter14" className={classNameChapter}>Осмотр локомотива и экипажной части на промежуточной станции</Link></li>
        <li><Link to="/michapter15" className={classNameChapter}>Проверка действия автотормозов в пути следования</Link></li>
        <li><Link to="/michapter16" className={classNameChapter}>Порядок подъезда к запрещающим сигналам и проследования светофора с одним желтым (немигающим) огнем, находящегося на расстоянии менее требуемого тормозного пути от следующего светофора</Link></li>
        <li><Link to="/michapter17" className={classNameChapter}>Порядок предупреждения замораживания и образования ледяных пробок в ТМ в зимний период</Link></li>
        <li><Link to="/michapter18" className={classNameChapter}>Порядок применения автотормозов в зимний период</Link></li>
        <li><Link to="/michapter19" className={classNameChapter}>Действия локомотивной бригады в нестандартной ситуации</Link></li>
        <li><Link to="/appendix1" className={classNameChapter}>Приложение №1</Link></li>
        <li><Link to="/appendix2" className={classNameChapter}>Приложение №2</Link></li>
        <li><Link to="/appendix3" className={classNameChapter}>Приложение №3</Link></li>
        <li><Link to="/appendix4" className={classNameChapter}>Приложение №4</Link></li>
        <li><Link to="/appendix5" className={classNameChapter}>Приложение №5</Link></li>
      </ol>
      <div className={classNameSection} data-toggle="collapse" data-target="#BrakesLocation">
        Пробы тормозов</div>
      <ul className="list-group collapse" id="BrakesLocation">
        <li><Link to="/brakes-pass" className={classNameChapter}>Пассажирское движение</Link></li>
        <li><Link to="/brakes-uzel" className={classNameChapter}>Узел</Link></li>
        <li><Link to="/brakes-shushary-volhov" className={classNameChapter}>Шушары - Волховстрой</Link></li>
        <li><Link to="/brakes-volhov-shushary" className={classNameChapter}>Волховстрой - Шушары</Link></li>
        <li><Link to="/brakes-volhov-svyr" className={classNameChapter}>Волховстрой - Свирь</Link></li>
        <li><Link to="/brakes-svyr-volhov" className={classNameChapter}>Свирь - Волховстрой</Link></li>
        <li><Link to="/brakes-volhov-babaevo" className={classNameChapter}>Волховстрой - Бабаево</Link></li>
        <li><Link to="/brakes-babaevo-volhov" className={classNameChapter}>Бабаево - Волховстрой</Link></li>
        <li><Link to="/brakes-spbviteb-dno" className={classNameChapter}>СПб.Витебский - Дно</Link></li>
        <li><Link to="/brakes-dno-spbviteb" className={classNameChapter}>Дно - СПб.Витебский</Link></li>
        <li><Link to="/brakes-spbviteb-vlad-luga-pskov" className={classNameChapter}>СПб.Витебский - Владимирская - Луга - Псков</Link></li>
        <li><Link to="/brakes-pskov-luga-vlad-spbviteb" className={classNameChapter}>Псков - Луга - Владимирская - СПб.Витебский</Link></li>
        <li><Link to="/brakes-shushary-vlad-narva" className={classNameChapter}>Шушары - Владимирская - Нарва</Link></li>
        <li><Link to="/brakes-narva-vlad-shushary" className={classNameChapter}>Нарва - Владимирская - Шушары</Link></li>
        <li><Link to="/brakes-shushary-shos-narva" className={classNameChapter}>Шушары - Шоссейная - Нарва</Link></li>
        <li><Link to="/brakes-narva-shos-shushary" className={classNameChapter}>Нарва - Шоссейная - Шушары</Link></li>
        <li><Link to="/brakes-shushary-shos-veimarn-luzh" className={classNameChapter}>Шушары - Шоссейная - Веймарн - Лужская</Link></li>
        <li><Link to="/brakes-luzh-vlad-shushary-spsm" className={classNameChapter}>Лужская - Владимирская - Шушары - СПСМ</Link></li>
        <li><Link to="/brakes-shushary-ligovo-kal-luzh" className={classNameChapter}>Шушары - Лигово - Калище - Лужская</Link></li>
        <li><Link to="/brakes-luzh-kal-ligovo-spsm" className={classNameChapter}>Лужская - Калище - Лигово - СПСМ</Link></li>
        <li><Link to="/brakes-gatchina-mga-budo" className={classNameChapter}>Гатчина Тов. - Мга - Будогощь</Link></li>
        <li><Link to="/brakes-budo-mga-gatchina" className={classNameChapter}>Будогощь - Мга - Гатчина Тов.</Link></li>
        <li><Link to="/brakes-veimarn-slan" className={classNameChapter}>Веймарн - Сланцы</Link></li>
        <li><Link to="/brakes-slan-veimarn" className={classNameChapter}>Сланцы - Веймарн</Link></li>
        <li><Link to="/brakes-spbbalt-gatchina" className={classNameChapter}>СПб.Балтийский - Гатчина Балт.</Link></li>
        <li><Link to="/brakes-gatchina-spbbalt" className={classNameChapter}>Гатчина Балт. - СПб.Балтийский</Link></li>
        <li><Link to="/brakes-dno-pskov" className={classNameChapter}>Дно - Псков</Link></li>
        <li><Link to="/brakes-pskov-dno" className={classNameChapter}>Псков - Дно</Link></li>
      </ul>
      <div className={classNameSection} data-toggle="collapse" data-target="#Pamyatka">Памятки</div>
      <ul className="list-group collapse" id="Pamyatka">
        <li><Link to="/coaxiality" className={classNameChapter}>Приемка писцы</Link></li>
        <li><Link to="/pamyatkaAcceptance" className={classNameChapter}>Памятка по приемке тормозного оборудования и протяжки на ленте</Link></li>
        <li><Link to="/pamyatkaBrakesTest" className={classNameChapter}>Опробование автотормозов с протяжками</Link></li>
        <li><Link to="/pamyatkaVedenie" className={classNameChapter}>Отправление со станции и ведение поезда</Link></li>
        <li><Link to="/pamyatkaReservSplotka" className={classNameChapter}>Памятка по резерву и сплотке</Link></li>
        <li><Link to="/exampleofatrip" className={classNameChapter}>Правила оформления поездки на обратной стороне предупреждения</Link></li>
        <li><Link to="/pamyatkaProblemKM394" className={classNameChapter}>Неисправности КМ394</Link></li>
        <li><Link to="/pamyatkaRazryv" className={classNameChapter}>Меры по предупреждению разрывов поездов</Link></li>
        <li><Link to="/pamyatkaSignsOfFreezing" className={classNameChapter}>Основные признаки и места перемерзания пневмосхемы ВЛ-10</Link></li>
        <li><Link to="/pamyatkaSpinningOfWheels" className={classNameChapter}>Предупрежение боксования колесных пар локомотива</Link></li>
        <li><Link to="/pamyatkaTroubles" className={classNameChapter}>Наиболее распространенные неисправности подвижного состава, крепления грузов и их характерные признаки</Link></li>
        <li><Link to="/pamyatkacouplercheck" className={classNameChapter}>Памятка о порядке проведения проверки автосцепок</Link></li>
        <li><Link to="/pamyatkalocotrouble" className={classNameChapter}>Памятка по неисправностям локомотива</Link></li>
        <li><Link to="/pamyatkapantograph" className={classNameChapter}>Памятка по токоприемникам</Link></li>
        <li><Link to="/vl10cold" className={classNameChapter}>Перевод ВЛ10 в холодное состояние</Link></li>
        <li><Link to="/2es4cold" className={classNameChapter}>Перевод 2ЭС4К в холодное состояние</Link></li>
      </ul>
      <div className={classNameSection} data-toggle="collapse" data-target="#Decree">Распоряжения</div>
      <ul className="list-group collapse" id="Decree">
        <li><div data-toggle="collapse" data-target="#r962">962p</div></li>
        <ul className="list-group collapse" id="r962">
          <li><Link to="/962rChapter1" className={classNameChapter}>Общие положения</Link></li>
          <li><Link to="/962rChapter2" className={classNameChapter}>Термины, определения и сокращения</Link></li>
          <li><Link to="/962rChapter3" className={classNameChapter}>Порядок использования токоприемников ЭПС при различных условиях эксплуатации</Link></li>
          <li><Link to="/962rChapter4" className={classNameChapter}>Порядок передачи сообщения в случаях повреждения токоприемников или устройств контактной сети</Link></li>
          <li><Link to="/962rChapter5" className={classNameChapter}>Порядок взаимодействия работников локомотивной бригады и НТЭ, ДСП, ДНЦ при повреждениях или неисправностях токоприемников и устройств контактной сети</Link></li>
          <li><Link to="/962rChapter6" className={classNameChapter}>Порядок действий при отключении напряжения в контактной сети</Link></li>
          <li><Link to="/962rChapter7" className={classNameChapter}>Порядок действия локомотивной бригады скоростных электропоездов «Ласточка» и «ЭШ2» всех модификаций при срабатывании КБСУ на опускание токоприемников или в случае пропадания напряжения в контактной сети</Link></li>
          <li><Link to="/962rChapter8" className={classNameChapter}>Порядок действия локомотивных бригад электропоездов «АЛЛЕГРО» и «САПСАН» при срабатывании системы КБСУ на опускание токоприемников или снятия напряжения в контактной сети</Link></li>
          <li><Link to="/962rChapter9" className={classNameChapter}>Порядок действий работников локомотивных бригад и работников ЭЧК (ЭЧ) при следовании ЭПС в местах, не допускающих проследование с поднятыми токоприемниками и при появлении нарушения условий токосъема</Link></li>
          <li><Link to="/962rChapter10" className={classNameChapter}>Порядок взаимодействия работников ТЧЭ, ТЧприг, СЛД, структурного подразделения ДОСС и ЭЧК (ЭЧ) при отключениях деповского фидера и обнаружении подбоев токоприемников</Link></li>
          <li><Link to="/962rChapter11" className={classNameChapter}>Порядок оформления акта о повреждении токоприемника</Link></li>
          <li><Link to="/962rChapter12" className={classNameChapter}>Перечень нормативных документов</Link></li>
        </ul>
        <li><div data-toggle="collapse" data-target="#r2580">2580р</div></li>
        <ul className="list-group collapse" id="r2580">
          <li><Link to="/2580rchapter1" className={classNameChapter}>Предисловие</Link></li>
          <li><Link to="/2580rchapter2" className={classNameChapter}>Термины и определения</Link></li>
          <li><Link to="/2580rchapter3" className={classNameChapter}>Назначение и область применения</Link></li>
          <li><Link to="/2580rchapter4" className={classNameChapter}>Общие положения</Link></li>
          <li><Link to="/2580rchapter5" className={classNameChapter}>Порядок действий при вынужденной остановке поезда</Link></li>
          <li><Link to="/2580rchapter6" className={classNameChapter}>Порядок действий при появлении признаков нарушения целостности тормозной магистрали в составе поезда</Link></li>
          <li><Link to="/2580rchapter7" className={classNameChapter}>Порядок действий при обнаружении неисправности верхнего строения пути</Link></li>
          <li><Link to="/2580rchapter8" className={classNameChapter}>Порядок действий при несанкционированных остановках поездов у светофоров с запрещающим показанием</Link></li>
          <li><Link to="/2580rchapter9" className={classNameChapter}>Порядок действий в случаях неудовлетворительной работы автотормозов в поезде</Link></li>
          <li><Link to="/2580rchapter10" className={classNameChapter}>Порядок действий при получении информации о следовании встречного поезда, потерявшего управление тормозами или при несанкционированном движении вагонов</Link></li>
          <li><Link to="/2580rchapter11" className={classNameChapter}>Порядок действий при тревожных показаниях средств автоматического контроля технического состояния подвижного состава на ходу поезда (КТСМ)</Link></li>
          <li><Link to="/2580rchapter12" className={classNameChapter}>Порядок действий при срабатывании устройств контроля схода подвижного состава (УКСПС)</Link></li>
          <li><Link to="/2580rchapter13" className={classNameChapter}>Порядок действий при повреждении планки нижнего габарита подвижного состава</Link></li>
          <li><Link to="/2580rchapter14" className={classNameChapter}>Порядок действий при вынужденной остановке поезда на перегоне из-за неисправности локомотива</Link></li>
          <li><Link to="/2580rchapter15" className={classNameChapter}>Порядок действий при неисправности контактной сети или повреждении токоприемников</Link></li>
          <li><Link to="/2580rchapter16" className={classNameChapter}>Порядок действий при отключении напряжения в контактной сети</Link></li>
          <li><Link to="/2580rchapter17" className={classNameChapter}>Порядок действий при перезарядке тормозной магистрали в составе пассажирского поезда</Link></li>
          <li><Link to="/2580rchapter18" className={classNameChapter}>Порядок действий при перезарядке тормозной магистрали в составе грузового поезда</Link></li>
          <li><Link to="/2580rchapter19" className={classNameChapter}>Порядок действий при возникновении пожара в поезде</Link></li>
          <li><Link to="/2580rchapter20" className={classNameChapter}>Порядок действий при обнаружении в пути следования неисправностей колесных пар подвижного состава</Link></li>
          <li><Link to="/2580rchapter21" className={classNameChapter}>Порядок действий при нарушении работы устройств поездной радиосвязи</Link></li>
          <li><Link to="/2580rchapter22" className={classNameChapter}>Порядок действий при неисправности локомотивных устройств безопасности</Link></li>
          <li><Link to="/2580rchapter23" className={classNameChapter}>Порядок действий в случае получения сообщения о минировании поезда или совершения террористического акта в поезде</Link></li>
          <li><Link to="/2580rchapter24" className={classNameChapter}>Порядок действий в случае потери машинистом способности управлять локомотивом</Link></li>
          <li><Link to="/2580rchapter25" className={classNameChapter}>Порядок действий при наезде на человека, механизмы, посторонний предмет или столкновении с автотранспортным средством</Link></li>
          <li><Link to="/2580rchapter26" className={classNameChapter}>Порядок действий при опробовании, обслуживании и управлении тормозами пассажирского поезда</Link></li>
          <li><Link to="/2580rchapter27" className={classNameChapter}>Порядок действий по предупреждению образования ползунов колесных пар в пассажирских поездах после применения экстренного торможения</Link></li>
          <li><Link to="/2580rchapter28" className={classNameChapter}>Порядок действий в случае обнаружения проезда людей на внешних частях МВПС</Link></li>
          <li><Link to="/2580rchapter29" className={classNameChapter}>Порядок действий в случае обнаружения проезда людей па крыше МВПС</Link></li>
          <li><Link to="/2580rchapter30" className={classNameChapter}>Порядок действий при возникновении у пассажира в вагоне пригородного поезда состояния или заболевания, угрожающего его жизни и здоровью</Link></li>
        </ul>
        <li><Link to="/554r" className={classNameChapter}>554р (оказание помощи)</Link></li>
        <li><Link to="/1414r" className={classNameChapter}>1414р</Link></li>
        <li><Link to="/1433r" className={classNameChapter}>1433р</Link></li>
        <li><Link to="/okt12r" className={classNameChapter}>Окт-Т-12р</Link></li>
        <li><Link to="/pamyatka859r" className={classNameChapter}>Порядок присвоения номеров поездам (859р) </Link></li>

      </ul>
      <div className={classNameSection} data-toggle="collapse" data-target="#DeviceSafety">Приборы безопасности</div>
      <ul className="list-group collapse" id="DeviceSafety">
        <li><Link to="/pamyatkaPB" className={classNameChapter}>Памятка по приборам безопасности</Link></li>
        <li><Link to="/kpd3" className={classNameChapter}>Памятка по пользованию КПД-3</Link></li>
        <li><Link to="/pamyatkaRadio" className={classNameChapter}>Памятка по переключению диапазонов и частот</Link></li>
        <li><Link to="/emuzel" className={classNameChapter}>Электронная карта. Узел.</Link></li>
        <li><Link to="/l230" className={classNameChapter}>Л230</Link></li>
        
        <li><div data-toggle="collapse" data-target="#BLOK">БЛОК</div></li>
        <ul className="list-group collapse" id="BLOK">
          <li><Link to="/blokChapter1" className={classNameChapter}>Общие положения</Link></li>
          <li><Link to="/blokChapter2" className={classNameChapter}>Порядок приемки комплекса</Link></li>
          <li><Link to="/blokChapter3" className={classNameChapter}>Включение и выключение комплекса</Link></li>
          <li><Link to="/blokChapter4" className={classNameChapter}>Подготовка комплекса к работе</Link></li>
          <li><Link to="/blokChapter5" className={classNameChapter}>Настройка, выбор режима работы</Link></li>
          <li><Link to="/blokChapter6" className={classNameChapter}>Проверка работоспособности</Link></li>
          <li><Link to="/blokChapter7" className={classNameChapter}>Проверка выполнения функции контроля бодрствования машиниста</Link></li>
          <li><Link to="/blokChapter8" className={classNameChapter}>Порядок смены кабины управления локомотивом (МВПС)</Link></li>
          <li><Link to="/blokChapter9" className={classNameChapter}>Осмотр, производимый локомотивной бригадой</Link></li>
          <li><Link to="/blokChapter10" className={classNameChapter}>Эксплуатация комплекса в пути следования</Link></li>
          <li><Link to="/blokChapter11" className={classNameChapter}>Порядок проведения проверок бдительности машиниста</Link></li>
          <li><Link to="/blokChapter12" className={classNameChapter}>Порядок работы комплекса при начале движения</Link></li>
          <li><Link to="/blokChapter13" className={classNameChapter}>Порядок работы комплекса</Link></li>
          <li><Link to="/blokChapter14" className={classNameChapter}>Порядок работы комплекса без ЭК, САУТ, ТСКБМ по АЛСН</Link></li>
          <li><Link to="/blokChapter15" className={classNameChapter}>Порядок работы с комплексом по АЛС-ЕН</Link></li>
          <li><Link to="/blokChapter16" className={classNameChapter}>Порядок работы комплекса с ЭК, ТСКБМ без САУТ</Link></li>
          <li><Link to="/blokChapter17" className={classNameChapter}>Порядок работы комплекса с САУТ без ЭК, ТСКБМ</Link></li>
          <li><Link to="/blokChapter18" className={classNameChapter}>Порядок работы комплекса на участках оборудованными устройствами АЛСО</Link></li>
          <li><Link to="/blokChapter19" className={classNameChapter}>Порядок работы комплекса с РК и МСЛР</Link></li>
          <li><Link to="/blokChapter20" className={classNameChapter}>Порядок работы с комплексом при проведении маневров</Link></li>
          <li><Link to="/blokChapter21" className={classNameChapter}>Порядок работы комплекса при двойной тяге и по системе многих единиц</Link></li>
          <li><Link to="/blokChapter22" className={classNameChapter}>Порядок действий машиниста при нарушениях нормальной работы комплекса</Link></li>
          <li><Link to="/blokChapter23" className={classNameChapter}>Отключение электропневматического клапана</Link></li>
          <li><Link to="/blokChapter24" className={classNameChapter}>Движение поездов по неправильному пути по сигналам АЛСН</Link></li>
        </ul>
        
      </ul>

      <div className={classNameSection} data-toggle="collapse" data-target="#WeightNorm">Весовые нормы</div>
      <ul className="list-group collapse" id="WeightNorm">
        <li><Link to="/weight-norm" className={classNameChapter}>Нормы массы и длины гузовых поездов по сериям локомотивов</Link></li>
        <li><Link to="/areas-weight-norm" className={classNameChapter}>Перечень участков, на которых запрещено взятие с места поезда</Link></li>
        <li><Link to="/train-export" className={classNameChapter}>Нормы вывозных поездов</Link></li>
      </ul>
      <Link to="/zima" className={classNameSection}>Зимний тех. бюллетень</Link>
      <div className={classNameSection} data-toggle="collapse" data-target="#Speed">Выписки скоростей</div>
      <ul className="list-group collapse" id="Speed">
        <li><Link to="/speeduzel" className={classNameChapter}>Узел</Link></li>
        <li><Link to="/speedspbdnonovosokol" className={classNameChapter}>СПб - Дно - Новосокольники</Link></li>
        <li><Link to="/speedpavlovsknovolisino" className={classNameChapter}>Павловск - Новолисино</Link></li>
        <li><Link to="/speedvladkobralovo" className={classNameChapter}>Владимирская - Кобралово</Link></li>
        <li><Link to="/speedvyritsaposelok" className={classNameChapter}>Вырица - Поселок</Link></li>
        <li><Link to="/speedkushelsuoyarvi" className={classNameChapter}>Кушелевка - Суоярви</Link></li>
        <li><Link to="/speedspsmsvyr" className={classNameChapter}>СПСМ - Свирь</Link></li>
        <li><Link to="/speedvolhovIIvolhovI" className={classNameChapter}>Волховстрой I (парк "Новооктябрьский")</Link></li>
        <li><Link to="/speed116kmkukol" className={classNameChapter}>Блокпост 116 км - Куколь</Link></li>
        <li><Link to="/speedvolhovIIbabaevo" className={classNameChapter}>Волховстрой II - Бабаево</Link></li>
        <li><Link to="/speedmgabudoovinishe" className={classNameChapter}>Мга - Будогощь - Овинище I</Link></li>
        <li><Link to="/speedmgastekol" className={classNameChapter}>Мга - Стекольный</Link></li>
        <li><Link to="/speedspbivan" className={classNameChapter}>СПб - Ивангород</Link></li>
        <li><Link to="/speedspblugapskovgos" className={classNameChapter}>СПб - Луга I - Псков - Госграница</Link></li>
        <li><Link to="/speedlugabatetsk" className={classNameChapter}>Луга I - Батецкая</Link></li>
        <li><Link to="/speedligovoveimarn" className={classNameChapter}>Лигово - Веймарн</Link></li>
        <li><Link to="/speedgdovveimarn" className={classNameChapter}>Гдов - Веймарн</Link></li>
        <li><Link to="/speedkotlyluzh" className={classNameChapter}>Котлы - Лужская</Link></li>
        <li><Link to="/speedgatchinatosno" className={classNameChapter}>Гатчина - Тосно</Link></li>
        <li><Link to="/speedgatchinafrezer" className={classNameChapter}>Гатчина-Варшавская - Фрезерный</Link></li>
        <li><Link to="/speedgatchvargatchpass" className={classNameChapter}>Гатчина-Варшавская - Гатчина-Пассажирская-Балтийская</Link></li>
        <li><Link to="/speedgatchvargatchtov" className={classNameChapter}>Гатчина-Варшавская (парк «Б») – Гатчина-Товарная-Балтийская</Link></li>
      </ul>
      <Link to="/tra" className={classNameSection}>ТРА</Link>
      <Link to="/phones" className={classNameSection}>Телефоны</Link>
    </React.Fragment>
  );
}